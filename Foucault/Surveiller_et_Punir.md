# Surveiller et punir (Michel Foucault, 1975)

## Partie 1 : Supplice

### Chapitre 1 : Le corps des condamnés

Foucault commence par la description incroyablement violente d'un condamné à mort en 1757. Il enchaine par l'emploi du temps millimétré de prisonniers 75 ans plus tard.

C'est pendant cette période (fin XVIII début XIX) que les systèmes de punition changèrent radicalement, un peu partout en Europe. L'exposition de la punition violente fit place à une punition plus discrète.

> "son efficacité, on la demande à sa fatalité, non à son intensité visible ; la certitude d'être puni, c'est cela, et non plus l'abominable théatre qui doit détourner du crime"

Ainsi, la justice cherche à se séparer de l'exécution de la peine. Avant, elle prenait part à l'exécution ce qui pouvait entacher sa réputation publique. Désormais, elle ne punit plus, elle corrige et redresse en envoyant le condamné faire sa punition en prison ou au bagne, en tout cas dans une administration bien séparée d'elle-même.
Fini les milliers de façons de donner la mort de manière douloureuse, on ne touche désormais plus au corps, on ne lui inflige pas de douleur. Même dans les mises à mort, on noie le condamné de sédatifs, pour une mort unique, qui ne dépend pas de la peine. Et avant cela, la guillotine :

> "Presque sans toucher au corps, la guillotine supprime la vie, comme la prison ote la liberté, ou une amende prélève des biens. Elle est censée appliquer la loi moins à un corps réel susceptible de douleur, qu'à un sujet juridique [...] Elle devait avoir l'abstraction de la loi elle-même."

La notion de crime a aussi été profondément modifiée :

> "on punit des agressions, mais à travers elles des agressivités ; des viols, mais en même temps des perversions ; des meurtres qui sont aussi des pulsions et des désirs."

Ainsi le rôle de la peine va être de contrôler ces comportements déviants et de les redresser. La pratique pénale vise ainsi à juger l'ame des criminels, et se veut scientifique. On pratique des expertises psychiatriques, on parle d'anthropologie criminelle.

Le Moyen-Age avait la vision suivante de la justice : établir l'infraction, trouver le criminel, juger en connaissance de la loi. La nouvelle justice va plutôt se demander pourquoi tel acte délictuel a été commis et comment redresser le comportement de l'individu, presque comme une prescription médicale.
La vision de la folie aussi a changé. La loi disait qu'une personne reconnue folle ne pouvait pas être coupable. Cette loi sera de moins en moins respectée par la suite car un fou est une personne à soigner, une personne déviante, ou donc coupable au moins de cela.

> "Et la sentence qui condamne ou acquitte n'est pas simplement un jugement de culpabilité, une décision légale qui sanctionne ; elle porte avec elle une appréciation de normalité et une prescription technique pour une normalisation possible."

Le pouvoir judiciaire et sa responsabilité sont dilués, partagés par le juge mais aussi par le psychiatre, l'éducateur, le surveillant, et tous ceux qui émettent un avis.

Le corps est plongé dans des rapports politiques de pouvoir. On peut prendre l'exemple du féodalisme ou le travail d'un corps est exploité par un autre. On peut dire qu'il existe un savoir sur les corps, la "technologie politique du corps". Ces connaissances permettent d'élaborer des stratégies de contrôle et de domination des corps à travers des relations de pouvoir qui sont diffuses et très profondes.

> "il n'y a pas de relation de pouvoir sans constitution corrélative d'un champ de savoir, ni de savoir qui ne suppose et ne constitue pas en même temps des relations de pouvoir."

### Chapitre 2 : L'éclat des supplices

Les supplices prémodernes n'étaient généralement pas les écartelements mortels qui ont marqué les mémoires. Mais que ce soit par exposition, piloris, fouet ou marque au fer rouge, la punition devait passer par la souffrance corporelle. Le supplice est un rituel précis, codifié, la souffrance doit correspondre au crime, au criminel et aux victimes.

Les accusés ne connaissaient rien de l'enquête et ne voyaient qu'une fois les juges avant qu'ils ne rendent leur verdict.

> "Dans l'ordre de la justice criminelle, le savoir était le privilège absolu de la poursuite."

Cela renvoie au fait que la justice est chasse gardée des juges et du souverain, c'était leur droit exclusif et absolu. Le système est régulé par une certaine arithmétique de la justice. Les preuves sont classées par catégorie et chaque catégorie de preuve donne lieu à des peines possibles :

> "les [preuves] semi-pleines peuvent entrainer des peines afflictives, mais jamais la mort."

Les preuves peuvent également se combiner, deux preuves semi-pleines peuvent faire une preuve pleine. La justice met aussi beaucoup l'accent sur les aveux, qui sont les plus fortes des preuves. Cela implique de faire prêter serment à l'accusé mais aussi de le torturer.

La torture (la question) n'est pas une simple mais barbarie mais une sorte de joute entre les juges et le supplicié ("patient"). Le juge met dans la balance les éléments de l'enquête, les charges, et si le patient tient bon, toutes les charges doivent être abandonnées. Dans les cas les plus graves, les charges peuvent être conservées mais on perd le droit de condamner le patient à mort, le juge réfléchit donc à deux fois avant de procéder à la question.

Il se trouve que la torture était à la fois une technique de l'enquête et une peine possible. Pourquoi ? Car une personne suspecte était déjà un peu coupable :

> "on ne peut pas être innocemment sujet de suspicion."

Et même si les preuves ne sont que des demi-preuves, l'accusé est vu comme un demi-coupable, il n'y a pas de vérité binaire comme coupable et innocent. L'accusé est toujours un peu coupable et l'on peut se servir de cet élément pour infliger la torture, même avant de rendre justice.

La justice doit éclater par la voix du condamné. On fait de lui le héraut de la justice, on lui demande d'avouer son crime en publique. On le promène dans la ville avec une pancarte et on lui demande de nombreuses fois d'avouer son crime. La peine était souvent symbolique, dans le sens ou elle s'opposait au crime.

> "on coupe la langue des blasphémateurs, on brule les impurs, on coupe le poing qui a tué"

Le lieu de la mise à mort pouvait être celui de l'assassinat par exemple. La justice est un rituel, un théatre et en quelques sortes il porte déjà les signes de l'au-delà. Les peines terrestres se soustraient aux peines divines, et Dieu peut avoir pitié d'un homme ou bien le laisser des heures sur la roue. Le coupable peut devenir fou, ou mourir dans les sentiments les plus chrétiens :

> "Le cycle est bouclé : de la question à l'exécution, le corps a produit et reproduit la vérité du crime."

Enfreindre la loi, c'est aussi une offense au souverain car la force de la loi vient de sa force personnelle, physique. Quand le supplice est rendu, il y a donc cette idée que le souverain, à travers ses exécuteurs, se vengent de son offenseur en le brisant physiquement :

> "Le supplice ne rétablissait pas la justice ; il réactivait le pouvoir."

Le peuple joue un rôle primordial dans ces punitions. Il est spectateur d'une part et les juges cherchent aussi à le terroriser d'une certaine façon, à faire un exemple. Le public peut être favorable à la justice et vouloir lui-même lyncher le coupable. Il peut aussi avoir de la compassion pour le condamné et chercher à le libérer. Les dernières paroles des mis à mort sont généralement très attendues car elles sont souvent violentes envers le pouvoir, la justice, Dieu, et récoltent les acclamations de la foule. La foule exige d'être présente, que ce soit pour le spectacle ou pour être témoin que la justice est bien rendue. Il est donc aussi acteur, il a son mot à dire, et bien souvent, l'agitation du peuple pouvait empêcher des exécutions. Cela arrivait notamment contre des condamnations pour des faits jugés mineurs mais sévèrement réprimés comme le vol d'un maitre par son domestique, et le peuple avait souvent de la sympathie pour les vagabonds, mendiants, voleurs à la tire ...

> "L'épouvante des supplies allumait en fait des foyers d'illégalisme : les jours d'exécution, le travail s'interrompait, les cabarets étaient remplis, on insultait les autorités, on lançait des injures ou des pierres au bourreau, aux exempts et aux soldats ; on cherchait à s'emparer du condamné, que ce soit pour le sauver ou pour le tuer mieux ; on se battait, et les voleurs n'avaient pas de meilleures occasions que la bousculade et la curiosité autour de l'échafaud."

Le jour d'exécution ne faisait donc pas peur qu'au peuple. Il glorifie d'une certaine façon les petits illégalismes, les méfaits dans lesquels le peuple peut se retrouver. Cela se renversera complètement dans les siècles suivants, notamment avec la litérature policière : le sujet est alors la grandeur du crime, des criminels d'exceptions, rusés et très loin de la réalité du peuple.

## Partie 2 : Punition

### Chapitre 1 : La punition généralisée

Dans la deuxième moitié du XVIIIè, de nombreuses critiques vont émerger contre le système pénal de supplices.

> "Il faut que la justice criminelle, au lieu de se venger, enfin punisse."

On déclare que tout le monde porte une part d'humanité et que c'est sur elle qu'il faut agir.

D'un point de vue historique, on assiste à une modification des crimes et délits entre la fin du XVIIè et le XVIIIè siècle. Les grandes bandes de malfaiteurs se séparent, les attaques contre les personnes deviennent moins fréquentes et les attaques contre les biens plus fréquentes. On passe de la "'criminalité de masse' vers une 'criminalité des franges et de marges'".
Dans le même temps, la justice s'alourdit :

> "en Angleterre sur les 223 crimes capitaux qui étaient définis, au début du XIXè siècle, 156 l'avaient été au cours des cent dernières années ; en France la législation sur le vagabondage avait été renouvelée et aggravée à plusieurs reprises depuis le XVIIè siècle"

La justice s'intéresse de plus près à la petite délinquance et notamment au vol et "elle prend désormais des allures bourgeoises de justice de classe." Simultanément, la croyance que les crimes se multiplient devient partagée.

> "le déplacement des pratiques illégalistes est corrélatif d'une extension et d'un affinement des pratiques punitives."

Les critiques des réformateurs sont principalement axés contre une justice trop irrégulière, trop peu unifiée. La justice dépend du roi, des seigneurs locaux, de leurs intérêts et de rapports de propriétés. Le but de la réforme est de mieux répartir le pouvoir de punir, de l'État à la collectivité locale, en se coupant de l'arbitraire monarchique ou seigneurial.

> "punir avec une sévérité atténuée peut-être, mais pour punir avec plus d'universalité et de nécessité ; insérer le pouvoir de punir plus profondément dans le corps social."

Un exemple est l'illégalisme pratiqué par les plus démunis et souvent toléré, qui leur permet souvent de survivre. Avec la transformation de l'idée de propriété terrestre qui devient absolue, ces actes sont de moins en moins tolérés, la propriété est protégée, l'illégalisme devient donc infraction contre la propriété.

> "Et cet illégalisme, s'il est mal supporté par la bourgeoisie dans la propriété foncière, est intolérable dans la propriété commerciale et industrielle : le développement des ports, l'apparition des grands entrepôts ou s'accumulent les marchandises, l'organisation des ateliers des vastes dimensions [...] nécessitent aussi une répression rigoureuse de l'illégalisme."

Le développement du capitalisme impose donc un contrôle plus strict et la fin de la tolérance de certaines pratiques populaires. La justice devient une justice de classe : les vols sont jugés par des tribunaux ordinaires qui prononcent des chatiments et les illégalismes de droit (fraude, évasion fiscale, ...) sont punis d'amendes dans des tribunaux spécialisés.

Dans la nouvelle économie du pouvoir de punir,

> "Le droit de punir a été déplacé de la vengeance du souverain à la défense de la société."

Le citoyen, ayant rompu le contrat social, devient un ennemi intérieur à la société.

Le choix des santions changent également de philosophie. Les chatiments ne sont plus proportionnels à la gravité des actes, ils servent à dissuader que ces actes se reproduisent, la récidive comme les imitations.

> "Ne pas viser l'offense passée mais le désordre futur."

Et les crimes les susceptibles d'être reproduits, ce ne sont pas les grands crimes qui sont extraordinaires, mais la petite délinquance. Cette nouvelle justice va avoir un côté très rationel et calculateur dans ses actions :

> "Il faut punir exactement assez pour empêcher."

C'est-à-dire que la peine doit procurer un désavantage légèrement supérieur à l'avantage du délit. Cette peine doit agir dans l'espace des idées : c'est la représentation ed la peine qui doit dissuader "non sa réalité corporelle". Il faut faire forte impression dans les esprits, mais pas forcément dans les corps.

La nouvelle justice doit également être inexorable, inflexible, car l'arbitraire des décisions entretient un flou qui laisse espérer que les peines se s'appliqueront pas. Cela passe par un livre de lois précis et disponible, à la vue de tous.

> "Il faut un code exhaustif et explicite, définissant les crimes, fixant les peines."

Et pour vérifier le respect des règles au quotidien, quoi de mieux d'un organe spécialement dédié : la police.
Ce côté mécaniste, déterministe de la loi a d'autres conséquences : on cherche à démontrer la culpabilité du suspect, qui reste innocent sauf preuve du contraire. On abandonne la torture, on cherche plutôt une vérité presque mathématique : on ne veut plus de demi-coupables, chaque criminel doit être puni précisément en accord avec son crime.
Ceci va aussi impliquer une indivualisation des peines. On s'intéresse au parcours de vie, au status, au risque de récidive pour fixer une peine. On s'intéresse à la méchanceté supposée des individus, les récidivistes ont donc prouvé qu'ils étaient méchants et peuvent être punis plus sévèrement, les auteurs d'un crime passionnel par contre peuvent être excusables, il n'y a pas de volonté de faire du mal, seulement des passions qui ne peuvent pas être contrôlées.

### Chapitre 2 : La douceur des peines

> "Trouver pour un crime le chatiment qui convient, c'est trouver le désavantage dont l'idée soit telle qu'elle rende définitivement sans attrait l'idée d'un méfait."

On cherche à mettre en relation directe le crime et sa punition. Il faut que pour tous les hommes, l'accomplissement de tel méfait conduise irrémédiablement à telle punition. Il faut en tout cas que ces associations soient inconscientes, qu'elles paraissent naturelles. Il faut imaginer que la justice ne fait que suivre une loi naturelle, et l'assassinat entrainera la mort, l'incendie le bucher, l'usure l'amende, ...

On va par exemple aller à la racine supposée des maux :

> "Derrière les délits de vagabondage, il y a la paresse : c'est elle qu'il faut combattre."

Dans cet exemple, il est donc préférable de forcer les vagabonds au travail plutôt que de les enfermer en prison. Si quelqu'un vole sa liberté à autrui, on lui apprendra ce que c'est que de perdre sa liberté. La temporalité de la peine doit donc être en lien avec un travail de transformation. On peut s'attendre à ce que la dureté de la peine diminue avec le temps. Les peines sont donc limitées dans le temps, sauf pour les irrécupérables :

> "le code de 1791 prévoit la mort pour les traitres et les assassins ; toutes les autres peines doivent avoir un terme (le maximum est de vingt ans)."

Il y a l'idée que la peine doit être utile : au lieu de tuer quelqu'un, autant le faire travailler pour la communauté, par exemple en faisant des routes.

Le jugement et la potentielle exécution ne doivent plus être une fête, mais une école. On doit apprendre au peuple ce qui arrive en cas de méfait, ces moments doivent donc être solennel, triste, mais aussi très clair pour que tout le monde les comprenne. Cela permet également de ne plus héroiser les criminels.

> "Concevons les lieux de chatiments comme un Jardin des Lois que les familles visiteraient le dimanche."

Enfin la peine ultime doit concerner les parricides : attachés dans une cage de fer, nus, on leur crevera les yeux et on les laissera la jusqu'à ce qu'ils meurent, tout en les nourrissant de pain et d'eau.

A cette époque, la prison n'est la peine que de peu de crimes. C'est en effet un système couteux, qui plonge les détenus dans l'oisiveté, les met à la merci de leur gardien, et invisibilise leur punition au plus grand nombre.
Or dès 1810, la peine la plus courante dans le code pénal est l'incarcération. Un certain nombre de prisons sont construites, à toutes les échelles (maisons d'arrêt, maisons de correction, maisons centrales, bagnes, ...).

> "À partir de la Restauration et sous la monarchie de Juillet, c'est [...] entre 40 et 43 000 détenus qu'on trouvera dans les prisons françaises (un prisonnier à peu près pour 600 habitants)."

La diversité des peines à donc été réduite presque instantanément (moins de vingt ans) à une peine uniforme, la prison. Ce processus aura lieu un peu partout en Europe.

Avant la réforme du système pénal, la prison existait mais n'était pas considérée comme une peine. Elle servait principalement à garder un suspect, ou bien à remplacer une peine qui ne pouvait se faire (femmes et enfants ne pouvaient pas être envoyés en galère). De plus, la prison était vu comme étant lié à l'arbitraire du roi et des nobles (lettres de cachet), au monde de l'extra-judiciaire.
Les cahiers de doléances appelaient déjà à raser les prisons.

Un des premiers modèles de prison est le Rasphuis d'Amsterdam, ouvert en 1596. Il est fait pour les mendiants et les vagabonds : les personnes qui ne travaillent pas. L'idée est, dans cet enfermement, de les contraindre au travail forcé, de leur apprendre le gout du travail et ainsi de les corriger et d'éviter les crimes (qui ne sont causés que par l'oisiveté).
En Angleterre (1775, Hanway) et aux USA (1779, Blackstone et Howard) on reprendra ce schéma auquel on ajoutera l'isolement. Celui doit permettre d'éviter des mouvements collectifs comme des révoltes, et doit permettre une introspection spirituelle du détenu. Le détenu doit aussi être corrigé religieusement.
Les prisonniers travaillent et financent ainsi la prison, ils touchent un salaire qui doit leur permettre de se réinsérer. L'emploi du temps est très strict.
Les modèles précédents furent du courte durée ou peu employés contrairement au modèle de Philadelphie (prison de Walnut Street ouverte en 1790).

La prison a donc pour but la correction du détenu, et elle va donc fonctionner comme un grand observatoire. Le personnel de prison connait le dossier de l'incarcéré, et chercher à lui appliquer les bonnes conditions pour le corriger moralement (violence, oisiveté, morale dépravée, désorganisation). Le prisonnier est sous surveillance constante et on produit des rapports très fréquents sur son comportement pour ajuster la peine.

> "La prison fonctionne là comme un appareil de savoir."

Si cette prison est conforme aux rêves des réformateurs c'est dans le sens ou l'on s'adapte à l'individu pour le corriger. Mais les techniques de correction sont différentes. Les réformateurs se basaient principalement sur un représentation du crime, sur le symbolique. Les prisons s'appliquent aux corps et à l'ame directement. Le but est de redresser les corps et les ames par l'habitude.

> 'Plutôt que sur un art de représentations, celle-ci doit reposer sur une manipulation réfléchie de l'individu'

La dimension du spectacle est exclut. Le pouvoir pénitentiaire est tout-puissant et autonome, et oeuvre dans le secret de la prison.

À la fin du XVIIIè, on a donc trois technologies de pouvoir. Le vieux système monarchique qui supplicie les corps. Le système des réformateurs qui vise à manipuler les signes, montrer la correction de l'individu déviant. Le système coercitif qui vise à redresser les corps en secret.

## Partie 3 : Discipline

### Chapitre 1 : les corps dociles

Au début du XVIIIè siècle, les soldats étaient des gens fort reconnaissables de part leur allure, leur insigne, leur musculature ... A la fin de ce siècle, le soldat est devenu une "pate informe", une machine dont on fait ce dont on a besoin.

> "Il y a eu, au cours de l'age classique, toute une découverte du corps comme objet et cible du pouvoir."

Quelques définitions :

> "Est docile un corps qui peut être soumis, qui peut être utilisé, qui peut être tranformé et perfectionné."

> "Ces méthodes qui permettent le contrôle minutieux des opérations du corps, qui assurent l'assujetissement constant de ses forces et leur imposent un rapport de docilité-utilité, c'est cela qu'on peut appeler les 'disciplines'."

On trouve donc des moyens, des disciplines pour assurer un système de domination en en cachant la violence (on peut faire une comparaison avec l'esclavage qui selon Foucault obtient des résultats identitiques d'assujetissement mais de manière beaucoup plus violente).

> "La discipline majore les forces du corps (en termes économiques d'utilité) et diminue ces mêmes forces (en termes politiques d'obéissance)."

Ces disciplines croisent de nombreuses institutions : l'école, l'hopital, la caserne, l'atelier.
Elles font extrêmement attention aux détails, grace à des outils techniques, et les consignes dans des règlements détaillés.

#### L'art des répartitions

> "La discipline parfois exige la cloture"

Les casernes, closes, sont construites, le collège indéal est l'internat, on enferme les vagabonds et les misérables, on ferme les portes de l'usine pendant la journée de travail.

La discipline c'est aussi une maitrise de l'espace : à chaque poste, un individu. On évite le collectif, et l'on surveille chaque individu (absentéisme, ...). La discipline classe, ordonne, individualise. Elle doit permettre la circulation, la surveillance à la fois générale et personnelle.
Il s'agit de créer des tableaux vivants, organiser les activités pour les rendre compréhensibles, lisibles et plus efficaces. On organise la circulation des marchandises, on surveille les maladies, ...

> "Le tableau, au XVIIIè siècle, c'est à la fois une technique et pouvoir et une procédure de savoir."

#### Le contrôle de l'activité

Le contrôle du temps passe par des emplois du temps stricts, qui sont à l'origine l'apanage des monastères. Tout est fait pour rendre le temps utile, productif : surveillance des retards, rejet de toute distraction sur le lieu de travail (amusement, alcool le midi, ...). Les gestes et postures sont également réglementées. On crée un complexe corps-machine : certaines parties du corps sont associées à certaines machines via certaines actions. Le maitre de classe est ainsi censé faire respecté une position corporelle bien précise aux élèves qui apprennent à écrire, les soldats apprennent à marcher comme il faut, à tenir son arme.

#### L'organisation des genèses

L'apprentissage (militaire ou d'un métier ...) va suivre un schéma très précis dans le temps disciplinaire. Le temps de formation est subdivisé en de très nombreuses catégories, liées à des compétences spécifiques. On peut progresser dans l'apprentissage en passant des examens qui sanctionnent l'acquisition des compétences.

Il y a cette idée de progression, de genèse de l'individu. Les personnes progressent grace à des exercices, répétitions contraintes du corps, de difficulté graduée. Les individus sont charactérisés par cette matrice : quelles sont leurs compétences c'est-à-dire quelles sont leur réussite dans les exercices.

La domination est donc également temporelle d'une part, dans le suivi d'un programme de formation. D'autre part, la domination impose sa hiérarchie définit par ses critères : quelles sont les compétences de tel ou tel individu, sachant que les compétences sont définis par tel ou tel cahier des charges.

#### La composition des forces

Invention du fusil vers 1699, bataille de Steinkerque.

Après avoir appris de manière individuelle sa tache, on fait des exercices collectifs. Les individus sont comme les pièces d'une machinerie complexe, on les forge d'abord pour leur fonction, puis, on les insère dans le système pour qu'ils puissent fonctionner avec les autres pièces.

Vouloir rendre le temps utile revient également à adapter les taches en fonction de l'age. Les enfants sont une ressource comme une autre que l'on peut former et faire travailler. Les vieux peuvent être professeurs. De manière générale, on peut se reposer sur les personnes qualifiées pour assurer la surveillance de celles qui apprennent.

> "En résumé, on peut dire que la discipline fabrique à partir des corps qu'elle contrôle quatre types d'individualité, ou plutôt une individualité qui est dotée de quatre caractères : elle est cellulaire (par le jeu de la répartition spatiale), elle est organique (par le codage des activités), elle est génétique (par le cumul du temps), elle est combinatoire (par la composition des forces). Et pour ce faire, elle met en oeuvre quatre grandes techniques : elle construit des tableaux ; elle prescrit des manoeuvres ; elle impose des exercices ; enfin, pour assurer la combinaison des forces, elle aménage des tactiques."

Les tactiques et la composition des forces visent à obtenir d'un groupe, une action d'une valeur supérieure à la somme de la valeur des actions des individus.

### Chapitre 2 : le moyens du bon dressement

#### La surveillance hiérarchique

Pour contrôler, il faut pouvoir observer et surveiller. Ces techniques ont été affinées grace notamment au camp militaire :

> "Longtemps on retrouvera dans l'urbanisme, dans la construction des cités ouvrières, des hôpitaux, des asiles, des prisons, des maisons d'éducation, ce modèle du camp ou du moins le principe qui le sous-tend : l'emboitement spatial des surveillances hiérarchisées."

Il faut que l'organisation interne permette la surveillance et qu'elle véhicule les relations de pourvoir. On installe des postes centraux de surveillance, on pose des cloisons transparentes, on surveille partout tout le temps.

> "Les pierres doivent rendre docile et connaissable."

On crée ainsi des structures de pouvoir qui sont douces, sans violence évidente. Le pouvoir est continu et diffus. Si la structure des institutions crée des relations de pouvoir, les relations hiérarchiques tendent à les renforcer : partout des petits chefs qui surveillent, tout doit être vu et su, contrôlé.

> "Et s'il est vrai que son organisation pyramidale lui donne un 'chef', c'est l'appareil tout entier qui produit du 'pouvoir' et distribue les individus dans ce champ permanent et continu."

#### La sanction normalisatrice

> "Au coeur de tous les sytèmes displinaires fonctionne un petit mécanisme pénal."

Tout doit être punissable, les retards, bavardages, insolence, chapardage, attitude incorrecte, manque de zèle : bref, tous les écarts à l'attitude souhaité.

> "Est pénalisable le domaine indéfini du non-conforme."

Les chatiments sont principalement correctif. Contrairement à la justice classique qui doit distinguer le légal et l'illégal, le système pénal dans une école va distinguer les bons et les mauvais élèves. Les bons auront des récompenses, les mauvais des punitions. Cela permet également de classer les individus, de noter leurs compétences. On sait à tous moments ou sont les personnes sur l'échelle de l'attitude attendue :

> "La pénalité perpétuelle [...] compare, différencie, hiérarchise, homogénéise, exclut. En un mot elle normalise."

Ce petit tribunal est donc distinct du grand tribunal criminel, on peut même dire que c'est lui qui l'a inspiré dans ses fonctions disciplinaires.

#### L'examen

À l'hopital, l'examen médical a bien changé dans sa version moderne. Au lieu des examens irréguliers et brefs, on passe à l'inverse, des examens étendus et très réguliers. Le médecin acquiert alors un pouvoir conséquent au sein d'une institution d'assistance gérée alors par les religieux.
L'examen permet d'accumuler du savoir sur les patients.
Il en va de même à l'école ou les examens sont constitutifs de la pédagogie.

> "C'est le fait d'être vu sans cesse, de pouvoir toujours être vu, qui maintient dans son assujetissement l'individu disciplinaire. Et l'examen, c'est la technique par laquelle le pouvoir, au lieu d'émettre les signes de sa puissance, au lieu d'imposer sa marque à ses sujets, capte ceux-ci dans un mécanisme d'objectivation. Dans l'espace qu'il domine, le pouvoir disciplinaire manifeste, pour l'essentiel, sa puissance en aménageant des objets. L'examen vaut comme la cérémonie de cette objectivation."

Le pouvoir royal, assimilé au triomphe, au spectacle, c'est du passé.
Désormais, chaque individu est un cas d'étude digne d'intérêt, qu'on va observer, noter, comparer. Ces informations forment la base de ce nouveau pouvoir, plus fin, à l'échelle individuelle. C'est une vrai renversement par rapport à l'époque féodale dans laquelle seuls les hommes de pouvoirs avaient une individualité propre et connue.
À l'époque moderne, les personnes les plus individualisées sont les enfants, les fous/folles. Se crée une véritable science de l'homme, qui devient calculable.

### Chapitre 3 : Le panoptisme

En cas de peste au XVIIè siècle, voici comment l'on réagissait : on découpe la ville en quartiers, gérés par un intendant, puis en rues, gérées par les syndics. Tous les habitants sont enfermés chez eux pour la quarantaine. Ils ne peuvent sortir sous peine de mort. Les syndics font l'appel tous les jours, les intendants visites régulièrement les syndics. Les gardes patrouilles pour faire respecter l'ordre. L'appel s'appuie sur des registres, et la surveillance donne lieu à des rapports : tout est consigné. Les magistrats désignent les médecins.

> "Le rapport de chacun à sa maladie et à sa mort passe par les instances du pouvoir, l'enregistrement qu'elles en font, les décisions qu'elle prennent."

> "pour voir fonctionner des disciplines parfaites, les gouvernants rêvaient de l'état de peste. [...] c'est l'utopie de la société parfaitement gouvernée."

On agissait très différemment pour les lépreux qui étaient simplement exclus. Au cours du XIXè siècle, on va appliquer les techniques disciplinaires dans ces centres d'exclusions, créant par la même les asiles psychiatriques, pénitencier, maisons de correction, ...

Le *Panopticon* de Bentham : un batiment en anneau, découpé en cellule largement vitrée qui laisse passer la lumière de part en part.

> "Il suffit alors de placer un surveillant dans la tour centrale, et dans chaque cellule d'enfermer un fou, un malade, un condamné, un ouvrier ou un écolier."

On veut [enfin Bentham, pas moi] que le détenu interiorise sa surveillance. Le surveillant ne doit jamais être vu, on en doit pas savoir s'il est la. On n'est jamais totalement surveillé mais on peut tout le temps l'être.

Le panoptique permet aussi de faire des expériences sur les détenus de manière contrôlée.

Au coursde la période classique, ces nouvelles institutions de discipline vont prospérer. La discipline va d'ailleurs parfois sortir de ces endroits clos pour pénétrer dans la société. Par exemple, l'école chrétienne donne aussi prétexte à surveiller les parents, leur piété, leurs moeurs. Des groupes de religieux vont aussi parcourir la France pour empêcher les impiétés.
Et bien sur, la surveillance va être très tôt assumée par la police. Cette force, vécue comme arbitraire pour la population, et qui est bien hiérarchisée et produit des rapports sur la conduite des individus. La police paie parfois des délateurs. La police surveille les espaces non-disciplinaires et assure leur continuité.

L'extension du système disciplinaire se caractérise par la volonté d'avoir un faible coût économique et politique (discrétion), pour un résultat maximal de docilité et d'utilité.

Au XVIIIè siècle, il y avait un fort enjeu de rationalisation. D'une part de l'activité économique en pleine expansion, d'autre part la gestion des institutions (hopitaux, écoles, ...) soumis à une forte croissance démographique.

**Pour Foucault, l'accumulation du capital ne peut être dissociée de l'accumulation des hommes, rendue possible par les techniques disciplinaires.**

Et si la bourgeoisie a bati une démocratie repreésentative au XIXè siècle, c'est parce que la discipline assurait la soumission "des forces et des corps" :

> "Les disciplines réelles et corporelles ont constitué le sous-sol des libertés formelles et juridiques. [...] Les 'Lumières' qui ont découvert les libertés ont aussi inventé les disciplines."

Les disciplines vont contre les droits (liberté, ...). Elles introduisent une asymétrie "insurmontable" et excluent les réciprocités.

## Partie 4 : La prison

### Chapitre 1 : Des institutions complètes et austères

Au XIXè siècle, la prison s'impose comme une évidence. Elle semble avoir un rôle égalitaire car elle prive les citoyen·ne·s d'un droit qu'ils ont tous de manière égale, leur liberté. Une forme de comptabilité s'installe car l'on prélève du temps de liberté pour payer des crimes : le temps est une sorte de monnaie.
La prison a aussi pour but le redressement des individus, en reprenant les codes disciplinaires :

> "La prison : une caserne un peu stricte, une école sans indulgence, un sombre atelier, mais, à la limite, rien de qualitativement différent."

La prison se base sur :

- l'isolement. Il permet d'éviter les contacts entre détenus et permet un 'tête-à-tête' entre lea détenu·e et le pouvoir. Le modèle de la prison d'Auburn préconise même que les personnes incarcérées ne peuvent d'adresser qu'aux gardiens, et à voix basse. Les détenu·e·s peuvent être en contact les un·e·s les autres, c'est le silence qui fait l'isolement. Le modèle de Philadelphie préconise un isolement physique strict pour faire marcher la conscience du / de la prisonnier·e. Le gardien doit être bon pour être une sorte de sauveur. Dans tous les cas, les relations personnelles doivent être contrôlées par le système.
- le travail. Il sert à remplir les journées des détenu·e·s et à les habituer au travail (les criminel·les sont bien sur paresseux, instables, ...). C'est un redressement moral qui apprend qu'on peut vivre de son travail (et non du vol). À ce titre, en France, les prisonnier·es étaient payé·es. Le prison doit donc créer des prolétaires dociles.

> "Le travail devait être la religion des prisons."

- la durée des peines. La peine doit servir à redresser les comportements déviants. À l'instar d'une opération chirurgicale, la durée doit être suffisante pour 'guérir' l'individu, ni plus ni moins. Cette individualisation des peines impliquent une autonomie et un pouvoir du personnel de prison.

En résumé :

> "La cellule, l'atelier, l'hôpital."

Pour changer les individus et rendre des comptes aux juges qui veulent surveiller les prisons qui s'autonomisent, l'accumulation d'informations, la surveillance devient nécessaire.

Les prisons étudient la délinquance vue comme "un écart pathologique de l'espèce humaine". Les délinquant·es sont donc soit des malades soit faisant partie d'une population à part. On étudie donc la délinquance pour dégager des grands thèmes. Les détenu·es dont la grande intelligence a été pervertie doivent être traité·e·s de telle et telle façon. Il y a celleux qu'il faut éduquer, ou bien isoler, ou bien faire travailler en commun.

### Chapitre 2 : Illégalismes et délinquance

Description de la chaine, qui, encore au XIXè siècle reprend des thèmes de l'Ancien Régime. Les condamné·es sont enchainé·es et marchent sous les yeux d'un public passioné. En 1837, la chaine est remplacée par un véhicule pénitencier de style benthamien. La voiture comprend six celulles, le voyage est un supplice, entre les chaines, les privations de sommeil et les éventuelles punitions.

À la même époque débutent les premières critiques des prisons. Elles sont inefficaces à réduire le crime et la récidive est importante. Les détenu·es sont séparé·es de la société, peinent à trouver du travail et leurs familles sont précarisées. Les réponses de la prison ? Le pénitencier n'a pas encore fourni ses effets, il faut juste aller plus loin.

> "il faut s'étonner que depuis 150 ans la proclamation de l'échec de la prison se soit toujours accompagnée de son maintien."

On peut alors voir une justice de classe qui est la pour différencier les illégalismes plus que pour les réprimer. Le pénal, en gérant ainsi les illégalismes participent au mécanismes de domination.

Au XIXè siècle, l'illégalisme prend souvent une forme politique. On se bat contre les nouvelles lois de la bourgeoisie. Il y a donc cette idée que les criminelles sont des personnes de la classe inférieure. Ils sont criminalisés par la loi, asymétrique, bourgeoise.

> "Au constat que la prison échoue à réduire les crimes il faut peut-être substituer l'hypothèse que la prison a fort bien réussi à produire la délinquance, type spécifié, forme politiquement ou économiquement moins dangereuse - à la limite utilisable - d'illégalisme ; à produire les délinquants, milieu apparemment marginalisé mais centralement contrôlé ; à produire le délinquant comme sujet pathologisé."

Cela permet d'avoir, au lieu d'un illégalisme pratiqué de temps en temps par des gens du quotidien et qui peut être mouvant (mendiants, vagabonds), la délinquance est pratiquée par oun groupe de personne assez restreint et clos qu'il est possible de contrôler. Celui signifie que l'on tolère des pratiques illégales car elles sont contrôlables et peuvent être utiles.

> "On peut dire que la délinquance, solidifiée par un système pénal centré sur la prison, représente un détournement d'illégalisme pour les circuits de profit et de pouvoir illicites de la classe dominante."

Un exemple donné est l'utilisation de délinquant comme indicateurs.

Cette délinquance implique aussi une surveillance fine de la population. Les services de police se développe, ainsi que le fichage.

> "La surveillance policière fournit à la prison des infracteurs que celle-ci transforme en délinquants, cibles et auxiliaires des contrôles policiers qui renvoient régulièrement certains d'entre eux à la prison."

L'État se sert de cette délinquance pour l'associer aux mouvements ouvriers, et pour justifier le déploiement policier. Les journaux contribuent à l'hystérie populaire avec les faits divers. Les riches ne sont pas inquiétés par la loi. Les tribunaux se transforment parfois en lieux politiques ou l'on dénonce cette justice de classe.

Les journaux populaires lancent des contre faits-divers qui montrent les dérives de la bourgeoisie et informent sur le rôle de la société dans les crimes qui viennent du peuple.

> "Il n'y a donc pas une nature criminelle mais des jeux de force qui, selon la classe à laquelle appartiennent les individus, les conduiront au pouvoir ou à la prison"

### Chapitre 3 : Le carcéral

Dès le début du XIXè siècle, les cercles carcéraux sont assez larges. Ils comprennent des colonies pour enfants pauvres, des refuges, des charités, des instituts pour "les pupilles vicieux et insoumis de l'Assistance Publique", des orphelinats, des usines-couvents ...
La continuité carcérale peut se trouver partout. Du plus jeune age avec l'école ou les instituts, puis à l'age adulte avec l'atelier ou l'on travaille, l'hopital ou l'on se soigne, la case prison ou l'on passe parfois, et en fin de vie avec l'hospice.

> "Dans cette société panoptique dont l'incarcération est l'armature omniprésente, le délinquant n'est pas hors la loi; il est, et même dès le départ, dans la loi, au coeur même de la loi"

Cet archipel carcéral a aussi pour avantage de limiter l'aspect violent de la prison. Le régime pénitencier étant présent un peu partout, la gradation avec le régime de la prison diminue. Cela signifie aussi que l'on accepte de plus en plus d'être puni, car la moindre déviance implique redressement.
