# Histoire de la sexualité 1 : La volonté de savoir (M. Foucault)

## Partie 1. Nous autres, victoriens

Le discours classique nous dit qu'au début du XVIIè encore, on était tolérant et que c'est au XIXè siècle que la répression du sexe commença véritablement. Pour Foucault, il y a deux raisons à ce discours. La première est que certain·es voudraient faire commencer la répression du sexe avec la naissance du capitalisme et l'idéologie bourgeoise. La deuxième est que cela parait subversif de parler du sexe après sa répression et que cela semble hater une libération, sous une forme de prédication.

Foucault veut étudier plusieurs choses : la réalité et la nature de la "répression du sexe", quel lien avec les relations de pouvoir, et est-ce que nous faisons face aujourd'hui à une rupture avec cette répression.
La question est donc de savoir qui parle de sexe, comment, ou, et pourquoi ; quels sont les liens avec les institutions, le pouvoir, quels sont les impacts sur le plaisir : quelles sont les articulations entre pouvoir, savoir, et plaisir.

## Partie 2. L'hypothèse répressive

### Chapitre 1. L'incitation aux discours.

On note à partir du XVIIè siècle une croissance des discours liés au sexe. Par exemple, lors des confessions, on va demander de manière précise un discours sur les désirs et le sexe. Cela fait donc des siècles que l'on a "branché sur le sexe le discours".

> "Nait vers le XVIIIè siècle une incitation politique, économique, technique à parler du sexe."

On s'intéresse au sexe de manière rationelle et non plus tant morale, on cherche à classifier, à quantifier. Cela permet de connaitre, de réglementer et de renforcer l'État.
On sait qu'un État tire sa richesse et sa puissance de sa population, la gestion de la natalité (et du mariage pour le côté moral) est donc primordiale.

> "la conduite sexuelle de la population est prise à la fois pour un objet d'analyse et cible d'intervention"

Par exemple, la sexualité des jeunes (enfants / ados) a commencé à être très encadrée par l'institution pédagogique à partir du XVIIIè siècle. Les discours et les enseignements sont codifiés, on définit qui parle (enseignants, parents, médecins), et même l'architecture est pensée par rapport au sexe (cloisons, rideaux, ...).
Le sexe est donc omniprésent dans les discours, et ces discours sont très variés dans leurs locuteurices comme dans les instruments utilisés.

> "Ce qui est propre aux sociétés modernes, ce n'est pas qu'elles aient voué le sexe à rester dans l'ombre, c'est qu'elles se soient vouées à en parler toujours en le faisant valoir comme *le* secret."

### Chapitre 2. L'implantation perverse.

Jusqu'au XVIIIè siècle, les lois sur la sexualité sont principalement centrées sur le couple. On parle de devoir conjugal, on fait des recommendations, on impose des périodes d'abstinence (carême). Toutes les déviances sont condamnées, mais elles le sont à peu près de la même façon.
À partir du siècle suivant, les couples gagnent en liberté, ce sont d'autres sexualités qui intéressent. Celle des fols, des criminels, des enfants, des homosexuel·les. Si les punitions se sont adoucies, tout un champ des pervertions a vu le jour, avec un recours intensif à la médecine. La lutte contre l'onanisme chez les ados est ainsi vue comme une lutte contre une épidémie. Tous les enfants sont par défaut coupables, et l'on culpabilise aussi les parents imprudents, on leur donne des recommendations.
Cette catégorisation des déviances crée de nouveaux individus comme l'homosexuel·le. Si, auparavant, la sodomie était interdite au même titre que d'autres pratiques comme l'adultère, elle devient une catégorie à part de déviance, de perversion, étudiée par la médecine qui cherche des signes physiques, par les psys qui cherchent des raisons personnelles ou métaphysiques, une déviance qui devient constitutive de l'individu.

> "Comme sont espèces tous ces petits pervers que les psychiatres du XIXè siècle entomologisent en leur donnant d'étranges noms de baptême : il y a les exhibitionnistes de Lasègue, les fétichistes de Binet, les zoophiles et zooérastes de Krafft-Ebing, les auto-monosexualistes de Rohleder; il y aura les mixoscopophiles, les gynécomastes, les presbyophiles, les invertis sexoesthétiques et les femmes dyspareunistes."

Les déviances sont médicales : les contrôles des corps sont renforcés, et la norme est établie en négatif. Le contrôle n'est donc plus tant du côté de la loi.

Pour Foucault, il y a une dualité plaisir - pouvoir qui se met en place. Plaisir d'interroger, de comprendre, de palper les corps et d'exercer le pouvoir, mais aussi plaisir d'échapper au pouvoir.

> "Il faut sans doute abandonner l'hypothèse que les sociétés industrielles modernes ont inauguré sur le sexe un age de répression accrue."

## Partie 3. *Scientia sexualis*.

Tous les discours autour du sexe ont permis d'éviter de réellement parler de sexe. On en parle de manière détachée, scientifique, on parle surtout des déviances, ... Cette science du XIXè est obsédée par l'hygiène, elle chasse les maladies du corps social.

> "Au nom d'une urgence biologique et historique, elle justifiait les racismes d'État, alors imminents. Elle les fondait en "vérité"."

Deux médecines distinctes se fondent, une étude de la reproduction, assez scientifique et une étude du sexe, très morale. Et cela va plus loin, cett étude du sexe semble vouloir ignorer la vérité volontairement. On peut prendre l'exemple des observations médicales comme à la Salpétrière, ou les rapports filtrent la réalité. Le sexe devient un enjeu de vérité.

Il existe deux façons de produire la vérité sur le sexe. La première provient directement de l'observation du plaisir, l'*ars erotica*. On cherche le plaisir, on note son intensité, sa durée, ... Généralement, une experte initie un novice à ce savoir sous une forme secrète, le savoir ne se crie pas sur les toits car il se perdrait.

Notre civilisation n'a pas d'*ars erotica* mais a développé un *scientia sexualis*.
Cette civilisation a énormément basée sa production de vérité sur l'aveu. Dans la justice comme la médecine et d'autres pans de la société, le rapport est celui de l'expert qui récolte ou extorque un aveu qui permet de batir la vérité. Celui qui recueille l'aveu est celui qui déchiffre, il est "le maitre de la vérité. Sa fonction est herméneutique".

Avant réservés à la pénitence, les modalités d'aveux se sont multipliées et prennent différentes formes : "interrogatoires, consultations, récits autobiographiques, lettres". A tel point que la médecine se base en partie sur le sexe, à chaque maux on cherche une explication dans une déviance sexuelle. Le sexe n'est plus seulement affaire de péché ou de transgression mais aussi de pathologie. On crée ainsi le concept de sexualité.

## Partie 4. Le dispositif de sexualité.

### Chapitre 1. Enjeu.

L'idée communément admise et un peu limitatrice est que le pouvoir est uniquement répressif au sujet du sexe. Et que ces limitations sont inscrites dans la loi.

Les grandes institutions de pouvoir dont l'État se sont développées dans le droit, dans le cadre juridique. La monarchie et le droit étaient emmêlés. Une des critiques de cette monarchie au XVIIIè a été justement de dire que le système juridique ne pouvait pas fonctionner correctement, de manière rationelle, avec la monarchie quie ne cessait de se placer en dehors du droit.
Depuis la Révolution, le droit ne suffit plus à encoder les formes de pouvoir.

> "Il faut batir une analytique du pouvoir qui ne prendra plus le droit pour modèle et pour code."

Encore une fois : voir une répression légale du sexe, c'est un peu court. On a assisté au développement de nouvelles technologies du sexe, de nouveaux dispositifs, et d'un pouvoir positif du sexe.

### Chapitre 2. Méthode.

> "Par pouvoir, il me semble qu'il faut d'abord comprendre la multiplicité des rapports de force qui sont immanents au domaine ou ils s'exercent"

Par pouvoir on n'entend donc pas tant le pouvoir institutionnel, mais des rapports de force à une échelle plus locale. Des pouvoirs qui sont présents partout, tout le temps. Ces pouvoirs locaux se basent sur des tactiques qui, regroupées à plus grande échelle forment des dispositifs. Et si les rapports de force locaux sont explicites, les dispositifs créés peuvent sembler n'avoir aucun auteur ou aucun but précis.

Ces multiples pouvoirs rencontrent également de multiples points de résistance.

> "Tout comme le réseau des relations de pouvoir finit par former un épais tissu qui traverse les appareils et les institutions, sans se localiser exactement sur eux, de même l'essaimage des points de résistance traverse les stratifications sociales et les unités individuelles. Et, c'est sans doute le codage stratégique de ces points de résistance qui rend possible une révolution, un peu comme l'État repose sur l'intégration instutitionnelle des rapports de pouvoir."

On s'intéresse donc aux foyers locaux de pouvoir-savoir, phénomènes mouvants. Il faut donc mettre à jour, non pas les discours dominants et les discours dominés mais la répartition, l'articulations entre ces discours, entre ce qui est dit, et ce qui est caché. On s'intéresse donc aux tactiques et aux stratégies, aux objectifs mis en avant par les rapports de force.
Par exemple, le XIXè a vu une augmentation de la surveillance de l'homosexualité / sodomie / pédérastie d'un point de vue médical. Ces catégories, auparavant floues sont devenues visibles, et la répression est devenue possible de manière plus systémique. Mais d'un autre côté, l'homosexualité a pris conscience d'elle-même, elle "s'est mise à parler d'elle-même".

### Chapitre 3. Domaine.

Foucault distingue quatre grands ensembles stratégiques qui ont amené à la création de dispositifs autour du sexe:

- L'hystérisation du corps de la femme (corps saturé de sexualité) -> la femme hystérique
- La pédagogisation du sexe de l'enfant et notamment la lutte contre l'onanisme -> l'enfant masturbateur
- La socialisation des conduites procréatrices : encourager (ou non) par des politiques sociales les couples à avoir plus d'enfants -> le couple malthusien
- La psychiatrisation du plaisir pervers -> l'adulte pervers

Le dispositif d'alliance correspond au mariage et aux relations entre conjoint·es. Ces relations sont régies par la loi qui définit ce qui est licite et ce qui ne l'est pas. Ce dispositif a perdu de son importance. Il vise principalement à reproduire le statut d'épouxse, est centré sur la reproduction et est donc très lié à l'économie.
A partir du XVIIIè on vient superposer sur le dispositif d'alliance le dispositif de sexualité. Il s'intéresse au corps et à ses plaisirs et ne vise pas la reproduction d'un ordre mais au contraire à s'étendre et à contrôler les corps des populations d'une manière de plus en plus globale.

> "La famille, c'est le cristal dans le dispositif de sexualité : elle semble diffuser une sexualité qu'en fait elle réfléchit et diffracte. Par sa pénétrabilité et par ce jeu de renvois vers l'extérieur, elle est pour ce dispositif un des éléments tactiques les plus précieux."

### Chapitre 4. Périodisation.

Au XIXè siècle, la médecine du sexe devient de plus en plus indépendante et prend ses distances de la médecine en général. On invente l'étude des perversions:

> "La médecine des perversions et les programmes de l'eugénisme ont été, dans la technologie du sexe, les deux grandes innovations de la seconde moitié du XIXè siècle."

> "L'ensemble perversion-hérédité-dégénérescence a constitué le noyau solide des nouvelles technologies du sexe."

C'est la psychanalyse qui va reprendre ce thème en essayant de rompre avec l'hérédité et la dégénérescence (donc avec l'eugénisme).

Foucault réfute donc l'hypothèse qui pose deux grands moments dans la répression du sexe: répression avec l'age classique et assouplissement à l'époque contemporaine. Pour lui, il y a eu une "inventivité perpétuelle", une continuité avec deux moments féconds:

> "vers le milieu du XVIè siècle, le développement des procédures de direction et d'examen de conscience ; au début du XIXè siècle, l'apparition des technologies médicales du sexe."

Les dispositifs de sexualité sont nés chez et pour les classes privilégiées : bourgeoisie et aristocratie. Ils se sont lentement diffusés dans la sociétés en plusieurs étapes :
politiques de contrôle de la natalité, "organisation de la famille 'canonique" de nouveau comme outil politique de contrôle, et enfin la médicalisation des perversions.

> "Il faut y soupçonner l'autoaffirmation d'une classe, plutôt que l'asservissement d'une autre : une défense, une protection, un renforcement, une exaltation, qui furent par la suite - au prix de différentes transformations - étendus aux autres comme moyen de contrôle économique et de sujétion politique. Dans cet investissement de son propre sexe par une technologie de pouvoir et de savoir qu'elle-même inventait, la bourgeoisie faisait savoir le haut prix politique de son corps, de ses sensations, de ses plaisirs, de sa santé, de sa survie."

La bourgeoisie a modelé son corps en un corps de classe. Elle s'est d'abord dotée d'un corps et d'un sexe en reniant celui des prolétaires. Le problème du sexe et donc de l'hygiène, du corps, de la reproduction des prolos ne s'est fait que parce que les épidémies posaient problème, qu'il fallait bien réguler la population etc Mais cela ne s'est fait qu'après l'apparition d'un certain nombre de garde-fous administratifs (école, hygiène publique, médicalisation généralisée de la population).

## Partie 5. Droit de mort et pouvoir sur la vie.

A la période classique, le souverain dispose du droit "de vie et de mort", qui consiste en réalité plutôt en le choix de tuer ou laisser vivre. Cette disposition est limité à un cadre, celui de la protection du souverain. Il ne peut tuer (ses sujets directement ou indirectement à la guerre) que pour se défendre.
Cette réalité va évoluer. La peine de mort se fera plus discrète et on va commencer à prétendre à un droit positif sur la vie. Il faut faire perdure la vie, la nation, la multiplier. Les guerres de défense deviennent des guerres pour la vie :

> "Les massacres sont devenus vitaux."

Ainsi la peine de mort doit être réduite car on ne lutte pas pour la vie en tuant des gens ... sauf si ceux-ci menacent la vie.

Le pouvoir sur la vie commence à se developper à partir du XVIIè siècle, suivant deux axes. Le premier est concentré sur la docilité, le contrôle du corps, le dressage, et l'intégration dans un système d'exploitation économique, et charactérisé par les disciplines. C'est "*l'anatomo-politique du corps humain*". Le deuxième axe est centré sur la vie, sur sa prolifération par le contrôle des naissances, l'extension de la longévité etc : c'est "*une biopolitique de la population*", le "bio-pouvoir".

À l'articulation entre ces axes se trouve le dispositif de sexualité. Ces changements dans la société ont entre autres permis le développement du capitalisme qui a pu se basé sur une force de travail nombreuse et docile. Pour Foucault, c'est plus là qu'il faut chercher un terreau fertile pour le capitalisme plutôt que dans le développement d'une morale ascétique : dans la rencontre entre la vie et les techniques de pouvoir politique.

> "Mais ce qu'on pourrait appeler le 'seuil de modernité biologique' d'une société se situe au moment ou l'espèce entre comme enjeu dans ses propres stratégies politiques."

La loi tend de plus en plus à agir comme norme, continue, régulatrice et correctrice. Elle s'intègre de plus en plus à un continuum d'appareils administratifs, médicaux, ...

Le sexe est donc à un carrefour. La sexualisation de l'enfant et donc le contrôle qui en résulte sont le fruit "d'une campagne pour la santé de la race" car la sexualité précoce était vue comme une épidémie. L'hystérisation des femmes, qui a appelé à une médicalisation minutieuse de leur corps, se fait au titre de leur devoir vis-à-vis des enfants.

Le changement de nature de la société peut donc être vu comme le passage d'une *symbolique du sang* (droit de mort etc) à une *analytique de la sexualité* (le contrôle de la vie etc).

Le dispositif de sexualité a créé un sexe désirable, objet de convoitise.

> "C'est de l'instance du sexe qu'il faut s'affranchir si, par un retournement tactique des divers mécanismes de la sexualité, on veut faire valoir contre les prises de pouvoir, les corps, les plaisirs, les savoir, dans leur multiplicité et leur possibilité de résistance. Contre le dispositif de sexualité, le point d'appui de la contre-attaque ne doit pas être le sexe-désir, mais le corps et les plaisirs."
