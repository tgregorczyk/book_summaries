# Le pouvoir psychiatrique (Michel Foucault)

Cours au Collège de France (1973-1974)

## Leçon du 7 novembre 1973

Idéalement, le pouvoir psychiatrique opère dans un lieu clos, l'asile, et fait règner l'ordre. Cet ordre est nécessaire en premier lieu pour le savoir médical lui-même : il permet de faire des observations dans un milieu contrôlé. Cet ordre est aussi posé comme condition nécessaire à la guérison.

L'ordre se décompose en le médecin, le surveillant qui est le bras et l'oeil du médecin et le servant, qui est censé servir le patient mais qui en réalité est au ordre du médecin.

Jusqu'à la fin du XVIIIè siècle, la folie était caractérisée par le fait que lea fol se trompait, était dans l'erreur. À partir du XIXè, les fols sont caractérisé·es par une force insurrectionnelle. Une force qui peut être dirigée selon différentes cibles qui vont être catégorisées par la psychiatrie. Par exemple la force pure se rapporte au "furieux", la manie s'attaque aux passions / pulsions etc
Le but de l'opération thérapeutique (guérison) est donc de dompter cette force. Cela peut être fait de deux façons. La première est médicamenteuse. La deuxième ne se base pas sur un savoir médical mais sur le pouvoir. À la violence physique, on répond par la violence physique. À l'entêtement moral, on répond par la volonté de fer du médecin. L'important n'est pas d'adapter le traitement pour soigner mais de faire plier lea fol, de dompter sa force.

C'est quand lea fol baisse les armes que la vérité éclate, car iel accepte de reconnaitre (de mauvais gré) et de se soumettre au corps médical. La vérité est acquise par l'aveu.

La psychiatrie, dans les années 1800-1830 se développe donc de manière très différente de la médecine clinique de la même période. Pas de recherche de connaissance scientifique, mais création d'un champ de bataille, d'un rapport de pouvoir, au sein de l'asile.

Dans son *histoire de la folie*, Foucault place au centre du problème les questions de représentation et de perception : comment est vue la folie. Ce qui intéresse maintenant Foucault, ce sont les dispositifs de pouvoir. Ce sont ces stratégies et tactiques de pouvoir qui donne lieu à un "jeu de la vérité", et à certaines pratiques discursives.

Foucault critique également la notion de violence qui part d'un *a priori* de violence physique "irrégulière", passionnel ... En négatif, le bon pouvoir est sans violence, sans contact avec le physique. Or pour lui, tout pouvoir est directement un pouvoir sur le corps. La violence n'est qu'un des visages du pouvoir.
Il est clair qu'en psychiatrie, le pouvoir est calculé et dispose d'un certains nombres de tactiques qui ont des impacts sur les corps, une de ces tactiques étant la violence physique, directe et asymétrique.

Ce qui intéresse Foucault dans ce cours, c'est d'analyser les dispositifs de pouvoir, d'en détailler les stratégies et tactiques.

## Leçon du 14 novembre 1973

Foucault commence par analyser la scène rapportée par Pinel de la guérison du roi Georges III. Ce dernier, ayant sombré dans la folie, est soigné. On le met dans une salle avec des matelas sur tous les murs et on lui impose la discipline.

> "Alors que le pouvoir souverain se manifeste essentiellement par les symboles de la force fulgurante de l'individu qui le détient, le pouvoir disciplinaire, lui, est un pouvoir discret, réparti ; c'est un pouvoir qui fonctionne en réseau et dont la visibilité ne se trouve que dans la docilité de la soumission de ceux sur qui, en silence, il s'exerce."

Pour Foucault il y a une véritable inversion du statut du souverain qui devient comme un des sujets les plus pauvres du royaume essayant de se rebeller contre un pouvoir muet. Le processus de guérison est décider par le médecin sans se soucier des problèmes réels du souverain, et deux pages de très grande force sont au contact du roi. Contrairement à ce que Foucault exprime dans l'*Histoire de la folie*, il n'y a pas de statut de parenté qui intervienne, ce dernier apparaitra plus tard.

Pour Foucault, on voit dans cette scène le coeur nucléaire de la psychiatrie, sans modèle familial, sans discours de vérité, sans institution, toutes ces composantes n'arrivant qu'ultérieurement. Il veut étudier la psychiatrie comme rapport de pouvoir, avant même que ce pouvoir puisse être récupérer par telle ou telle institution.

Cette scène est également une métaphore de la transition entre le pouvoir macro-physique du souverain à celui micro-physique de la discipline. En effet, pendant la période de la proto-psychiatrie (toute fin du XVIIIè-1830), lea fol était la personne qui se prenait pour le roi. Soit littéralement, soit en ayant une idée fixe qu'iel veut imposer à son entourage.

## Leçon du 21 novembre 1973

On va donc chercher à analyser la psychiatrie à partir d'un pouvoir bien particulier, le pouvoir disciplinaire. Ce dernier nous vient de sociétés religieuses du Moyen-Age avant de se répandre surtout aux XVIIIè et XIXè siècles. Foucault parle de forme "terminale, capillaire du pouvoir" qui agit directement sur les corps, il parle d'un "contact synaptique corps-pouvoir" dont l'archétype est le Panopticon de Bentham de 1791. Ainsi va s'intéresser à ce pouvoir dans le cadre de la psychiatrie, contrairement à d'autres critiques (comme dans les années 1930-40) qui avaient pour cible l'institution psychiatrique.

Foucault oppose le pouvoir disciplinaire à un autre type de pouvoir, celui de la souveraineté. Ce pouvoir est caractéristique des relations asymétriques dominants/dominé·es, c'est un système de prélèvement-dépense, au sens ou le dominant prélève un certain nombre de choses chez lea dominé·e (temps, fruits du travail, ...) puis va se servir de prélèvements pour faire des dépenses, qui peuvent parfois aider lea dominé·e (don, protection, ...).
La souveraineté se justifie par un acte fondateur, mais cette justification doit être régulièrement rappelée, reproduite, réactualisée par des marques de respect, des rites, des gestes.Et cette reproduction implique souvent la violence :

> "Le verso de la souveraineté, c'est la violence, c'est la guerre."

Il aussi noter que la souveraineté ne concerne pas des individus, mais plutôt des groupes :

> "C'est dans la mesure ou l'on est fils de X, bourgeois de telle ville, etc., que l'on va être pris dans un rapport de souveraineté, que l'on soit le souverain ou, au contraire, que l'on soit sujet"

Le pouvoir de souveraineté est également un pouvoir discontinu, il intervient par exemple lorsqu'on se rend à une cérémonie qu'on rend hommage, que le corps est marqué par "le sceau de la souveraineté qui l'accepte". Ou bien dans des moments de violence ou le dominant vient réclamer sans dû par la force.

On pourrait apercevoir une individualité si l'on regarde vers le haut de cet édifice, vers le souverain, vers le roi. Mais de manière paradoxale, la souveraineté n'est pas liée uniquement au corps du roi car elle doit persister après sa mort. Le corps du roi doit donc prendre une apparence multiple et se retrouver dans la couronne, le royaume etc

> "D'un côté, des corps mais pas d'individualité ; de l'autre côté, une individualité mais une multiplicité de corps. \[...\] le rapport de souveraineté \[...\] ne fait jamais apparaitre l'individualité."

Le pouvoir disciplinaire peut s'opposer terme à terme au pouvoir de la souveraineté. D'abord, il ne cherche pas à extraire quelque chose de l'individu :

> "C'est une prise du corps et non pas du produit ; c'est une prise du temps dans sa totalité et non pas du service. \[...\] Tout système disciplinaire, je crois, tend à être une occupation du temps, de la vie et du corps de l'individu."

Par exemple, au niveau de l'armée, on est passé d'une conscription temporaire de vagabonds à la discipline militaire : des soldats de métier, confinés dans la caserne et dédiant leur vie au métier.

Le pouvoir disciplinaire est continu, il s'exerce en permanence en rendant les sujets visibles et en les regardant tout le temps. Il ne fait pas appel à un acte passé pour se justifier, au contraire, il regarde vers l'avenir et cherche à ce que sa reproduction soit toujours plus facile, que tout roule. Cette reproduction passe par l'exercice et la répétition.

L'écriture joue également un rôle très important dans la discipline. Elle permet de consigner des comportements, de faire des rapports, des réglements, de noter, de répertorier. Les sujets sont ainsi en permanence scruté·es et le pouvoir peut intervenir très vite.

> "le verso du rapport disciplinaire, c'est maintenant la punition, la pression punitive, à la fois minuscule et continue."

La discipline se veut presque prédictive, elle voudrait pouvoir réagir avant même que le comportement n'intervienne. Cela est possible par l'analyse de chaque individu, la discipline individualise vers le bas. En haut, les chefs ne sont que des rouages anonymes, des fonctions dans le système.

Et cette discipline qui classe, surveille, hiérarchise, implique la production de marges, de celleux qui en peuvent pas rentrer dans le moule. Le débile mental n'existait pas avant la discipline scolaire, le déserteur avant la discipline militaire. Et le malade mental est celui qui ne rentre dans aucune case.

En produisant la norme, les systèmes disciplinaires créent des résidus, des marges qui entraineront la création de nouvelles disciplines pour récupérer ces marges, et ceci à l'infini.

La discipline est donc particulière car elle s'adapte parfaitement aux individus. Elle produit des corps assujettis et ses techniques sont au plus proches de chaque corps. Et l'individu est donc directement le produit de ces techniques, il n'existe pas en dehors de la discipline qui le produit.

À partir de l'age classique nait ainsi l'individu, à la fois comme sujet d'un système juridique qui lui reconnait des droits mais aussi comme création d'un certain type de pouvoir, la discipline.

## Leçon du 28 novembre 1973

Pour Foucault, l'origine des dispositifs disciplinaires est à rechercher dans les communautés religieuses. Ils interviennent comme manière de rompre de système féodal qui s'était imposé jusqu'au coeur des communautés. La réforme de Citeaux du XI-XIIè siècle remet au goût du jour la règle de la pauvreté, la disparition des possessions personnelles et le resserrement de la hiérarchie. Les dispositifs disciplinaires sont également des nouveaux outils politiques qui seront utiles aux deux grands pôles de pouvoir centralisé qui se développe càd la monarchie et la papauté.

L'expansion des systèmes disciplinaires qui va avoir lieu entre le XVIè et le XVIIIè siècle se base sur plusieurs piliers. L'un d'eux est le système scolaire. On peut noter par exemple que c'est à partir de l'

> "ascétisme que l'on trouve chez les Frères de la Vie commune, que l'on voit se dessiner les grandes schémas de la pédagogie, càd l'idée qu'on ne peut apprendre les choses qu'en passant par un certain nombre de stades obligatoires et nécessaires, que ces stades se suivent dans le temps et, dans le même mouvement qui les conduit à travers le temps, qu'ils marquent autnt de progrès qu'il y a d'étapes. Le jumelage temps-progrès est caractéristique de l'exercice ascétique et va se trouver également caractéristique de la pratique pédagogique."

Dans les écoles créées par les Frères de la Vie commune, on commence à voir le closement apparaitre, entre les classes de niveaux mais aussi avec le monde extérieur.

On retrouve également ces systèmes disciplinaires chez les peuples colonisés, notamment dans les villages ("républiques communistes") Guarani du Paraguay. Les jésuites, opposés à l'esclavage ont instauré un autre mode de contrôle des colonisé·es. Les villages étaient surveillés, le rythme de vie de la population était imposé et un espèce de système pénal fut instauré.

On peut également de colonisation interne au sujet du sort "des vagabonds, des mendiants, des nomades, des délinquants, des prostituées etc". Des systèmes d'enfermement ont été créés pour ces catégorie de personnes et étaient gérés, encore une fois, par les religieux.

Au XVIIIè siècle apparaissent l'encasernement des militaires et les grands ateliers de production pour les militaires. Les ouvriers possèdent un livret dans lequel est consigné le nom de leur (ancien) employeur, les causes de la rupture du précédent contrat etc

Les grandes lignes de ces pouvoirs disciplinaires sont donc :

> "la fixation spatiale, l'extraction optimale du temps, l'applicationet l'exploitation des forces du corps par une réglementation des gestes, des attitudes d'un pouvoir punitif immédiat, et enfin l'organisation d'un pouvoir réglementaire qui, en lui-même, dans son fonctionnement, est anonyme, non individuel, mais qui aboutit toujours à un repérage des individualités assujetties. En gros : prise en charge du corps singulier par un pouvoir qui l'encadre et qui le constitue comme individu, c'est-à-dire comme corps assujetti."

Alors pourquoi ce développement des procédés disciplinaires ? Pour Foucault, cela est nécessaire pour répondre à la demande d'accumulation des hommes, en parralèle de l'accumulation du capital dans ce nouveau système économique naissant. Les systèmes disciplinaires permettent de rendre tous les corps utilisables et de rendre leur apprentissage efficace.

La discipline, comme tactique, rompt avec la vieille taxonomie qui cherche à classer et ordonner. Les sciences de l'homme se modifient donc pour accueillir cette tactique qui vise à distribuer les corps de manière optimal.

En 1787, Bentham développe son Panopticon. C'est bien plus qu'un modèle de prison, c'est une sorte de méchanisme qui permet au pouvoir qui dirige n'importe quelle institution (prison, école, hopital, ...) d'acquérir le maximum de potentiel, "une force herculéenne". Une force sur les corps, donc, mais une force qui ne doit pas s'exercer physiquement mais plus par l'esprit. La domination ne doit pas être physique mais de l'esprit à l'esprit. Le pouvoir est immatériel.

D'un côté, le pouvoir est désindividualisé. Peu importe qui exerce le pouvoir, il ne s'incarne pas dans une personne. Par contre, ce pouvoir s'exerce sur des individus. C'est même un de ses buts, individualiser, nier les liens collectifs, les relations de groupe, pour s'appliquer à un corps isolé. Et cette individualisation permet l'observation et la connaissance du sujet :

> "c'est un appareil de savoir et de pouvoir à la fois, qui individualise d'un côté et qui, en individualisant, connait."

Mais ces procédés disciplinaires n'ont pas colonisés toute la société. Pour Foucault, la famille est encore à ranger du côté du pouvoir souverain de part l'autorité du père et les liens forts qui persistent entre les membres de la famille. Et c'est même indispensable que cela reste ainsi car c'est ce fait qui permet à la société disciplinaire de fonctionner correctement. C'est parce qu'il y a autorité qu'on force les enfants à aller à l'école, c'est parce qu'il faut prendre soin les uns des autres qu'il faut accepter d'aller travailler à l'atelier. C'est la famille et son pouvoir souverain qui permet de brancher les individus aux pouvoirs disciplinaires.

> "Elle est l'échangeur, le point de jonction qui assure le passage d'un système disciplinaire à l'autre, d'un dispositif à l'autre. La meilleure preuve, c'est que, lorsqu'un individu se trouve rejeté hors d'un système disciplinaire comme anormal ou est-il renvoyé ? À sa famille."

La famille est nécessaire au même titre que l'était la multiplicité du corps du roi.

Avec la transition à l'age classique, la famille s'est concentrée, réduite à un noyau restreint, et renforcée. Les lois et dispositifs disciplinaires vont dans ce sens : on fait en sorte que la famille vive unie, on crée des logements ouvriers, on interdit le ménage à vivre sans être marié, et quand la famille fait défaut, on crée des orphelinats et l'assistance à l'enfance.
Et des psys. Foucault explique que c'est dans les substituts à la famille qu'il faut chercher la psychiatrie. Et petit à petit,

> "cette fonction-Psy a joué le rôle de discipline pour tous les indisciplinables."

Elle intervient pour redresser les individus, en imputant leurs tares à des déficiances familiales.

> "Elle a pour référentiel constant la famille, la souveraineté familiale, et ceci dans la mesure même ou elle est l'instance théorique de tout dispositif disciplinaire."

## Leçon du 5 décembre 1973

À l'époque classique, la désignation et la captation du fou relève principalement de l'interdiction. L'interdiction c'est la perte des droits civils, demandée par la famille (curatelle) et validée par la justice. La demande d'internement est également possible. La loi de 1838 fait passer l'internement au premier planx, à la place de l'interdiction. La famille également passe au second plan car le fou est désormais désigné comme tel par l'autorité préfectorale alliée à l'institution médicale et seule décisionaire de l'internement.

> "Le fou émerge maintenant comme adversaire social, comme danger pour la société, et non plus comme l'individu qui peut mettre en péril les droits, les richesses, les possessions, les privilèges d'une famille."

Les disciplines qui se développent au XIXè siècle ont donc ce motif : s'emparer des pouvoirs de la famille élargie pour les confier à une institution technico-étatique qui va se baser sur la petite cellule familiale.

Pour la psychiatrie, la famille est un ennemi, pour soigner le malade il faut absolument le couper de sa famille et de ses ami·es. D'abord car on soupçonne le cercle proche d'être sinon à l'origine, au moins d'être l'occasion de la folie. Ensuite parce que le malade va attribuer ses maux à ses proches, il se sent persécuté. Enfin, les relations de pouvoir (de souveraineté) au sein de la famille pose problème pour la guérison. Elles peuvent renforcer la folie ou bien concurrencer l'autorité médicale.

> "ce qui guérit à l'hopital, c'est l'hôpital."

C'est l'organisation spatiale, l'architecture, bref l'organisation benthamienne de l'asile qui guérit.

> "c'est comme appareil panoptique que l'hôpital guérit."

L'asile est généralement conçu de manière pavillonaire, les habitations étant bien sûre construite pour rendre la surveillance facile. La tour centrale du Panopticon n'est pas présente, elle est remplacée par une hiérarchie pyramidale :

> "C'est-à-dire que l'on a une hiérarchie constituée de gardiens, infirmiers, surveillants, médecins, qui tous se font des rapports selon la voie hiérarchique, et qui culmine au médecin chef, seul responsable de l'asile, car le pouvoir administratif et le pouvoir médical ne doivent pas être dissociés"

Les "malades" sont volontairement mis en contact avec d'autres fols pour que, indirectement ils prennent conscience de leur folie. Par exmple si l'un se prend pour Louis XVI et voit trois autres personnes qui réclament cette identité, il peut se dire que quelque chose ne va pas.

Enfin, des punitions existent, notamment via ce que Foucault appelle des appareils orthopédiques. Ce sont des instruments qui visent à redresser les corps, qui tendent donc à ne plus être utile et qui font le plus mal lorsqu'on leur résiste. On peut citer par exemple le collier à pointes, la camisole ou la chaise qui donne le vertige. Idéalement ils ont un effet continu dans le temps.

À partir des années 1850, le point de vue va changer et l'on va essayer de faire rentrer le modèle familial dans l'institution. Il semble que ce changement se soit produit dans des différents contextes. Par exemple, après une colonisation sanglante, les missionnaires viennent avec l'autorité d'un père pour rééduquer les sauvages. Le modèle familial a aussi été testé vis-à-vis de certains détenus dans les prisons.

Il semble qu'après une première phase ou les disciplines ont permis d'ajuster les hommes au nouvel appareil de production capitaliste, il a fallu s'occuper des marges. En effet, les disciplines, normalisantes créent forcément ces marges. Un exemple donné par Foucault est celui de la prostitution, qui va s'institutionalisé autour des maisons closes. Celles-ci permettent de rendre profitable les désirs sexuels des hommes, le profit existant par l'interdiction légale et la tolérance de fait (il faut donc payer). Ces réseaux permettent de faire travailler des délinquants en tant que souteneurs et canalisent donc toute une économie vers les voies classiques capitalistes.

La familialisation a eu lieu dans les maisons de santé bourgeoises. Le deal est simple : la famille paie et on lui rend une personne qui est de nouveau en état d'être intégrée à la cellule familiale. Il s'agit donc de créer dans l'établissement une famille orthopédique qui va raviver les sentiments familiaux chez le malade. Cela implique aussi que la famille soit vigilente et prévienne lorsqu'un membre de la famille ne parait plus adapté, il y a donc disciplinarisation de la famille.

La famille devient ainsi une maison de santé miniature, on lui demande de contrôler la normalité de ses membres. Apparaissent ainsi par exemple les devoirs à la maison ou les parents d'élèves : familialisation des disciplines et disciplinarisation des familles. Les disciplines demandent aux familles de leur trouver des fols, des débiles, des vicieux pour pouvoir les traiter et, au passage faire du profit. C'est "le bénéfice économique de l'irrégularité".

## Leçon du 12 décembre 1973

> "Le contrôle de la stature, des gestes, de la manière de se conduire, le contrôle de la sexualité, les instruments qui empêchent la masturbation, etc., tout cela pénètre la famille \[bourgeoise\] par une disciplinarisation qui se déroule au cours du XIXè siècle et qui aura pour effet que la sexualité de l'enfant deviendra finalement objet de savoir, à l'intérieur même de la famille, par cette disciplinarisation."

Les enfants sont donc considérés comme le matériau brut qui permet le profit par la psychiatrie, et l'enfance est aussi le soucis privilégié des psychiatres.

Foucault décrit un exemple assez parlant d'asile. Dans celui-ci, la plupart des patient·es sont envoyés et pris en charge financièrement par l'autorité préfectorale. Quand c'est possible on les envoie travailler à la ferme pour les hommes et faire les taches domestiques pour les femmes. Quand c'est impossible on les enferme. On suit classiquement le modèle disciplinaire. Ensuite, il existe un certain nombre de patient·es riches dont la famille paie cher le séjour. Celleux-ci se reposent dans des quartiers confortables et l'asile suit avec elleux le modèle familial.

Le modèle prôné pour guérir les malades part du constat suivant : leur problème est que ce qu'il croit est en désaccord avec la vérité. Par exemple, l'un va croire qu'on le poursuit pour l'amener devant le tribunal. Un autre croit qu'il est atteint d'une maladie alors que non. Il faut alors adapter la réalité à la vérité distordue de malade pour que la réalité coincide avec celle-ci, pour qu'il n'y ait plus de dissonance en quelque sorte. Par exemple, on va organiser un faux tribunal pour le premier patient et l'acquitter. Pour le deuxième patient, on va faire venir des médecins qui vont confirmer qu'il est malade et qui vont (faire semble de) le soigner.

> "le pouvoir psychiatrique, c'est ce supplément de pouvoir par lequel le réel est imposé à la folie au nom d'une vérité détenue une fois pour toutes par ce pouvoir sous le nom de science médicale, de psychiatrie."

La psychiatrie se développe au XIXè siècle comme une science. Elle tache de ressembler à la médecine, catégorise ce qu'elle appelle des maladies mentales et leurs effets sur les corps. Mais en pratique, la psychiatrie ne se sert jamais de ce savoir. Ce savoir, permet à la psychiatrie de se positionner dans le camp du scientifique, de la vérité.
La psychiatrie dit donc en substance : la vérité est de mon côté, voilà l'origine de mon pouvoir.

> "la folie répondait : si tu prétends détenir une fois pour toutes la vérité en fonction d'un savoir qui est déjà tout constitué, eh bien, moi, je vais poser en moi-même le mensonge."

Autrement dit, la simulation permet aux patient·es de remettre la question de la vérité en jeu et de lutter contre le pouvoir psychiatrique. Avec la grande crise de la psychiatrie de la fin du XIXè siècle, on voit émerger la psychanalyse. Et l'on pourrait être tenter de se dire que ce n'est qu'une histoire de progrès scientifique : une science butte, une crise apparait et on la résoud avec de nouvelles théories. Mais pour Foucault, l'origine de la crise, c'est la résistance des fols et il faut replacer l'histoire de la psychiatrie à leur niveau.

> "Il y a eut ce qu'on pourrait appeler une grande insurrection simulatrice qui a parcouru tout le monde asilaire au XIXè siècle, et dont le foyer constant, perpétuellement allumé, a été la Salpêtrière, asile pour femmes."

## Leçon du 19 décembre 1973

Lors de l'arrivée en psychiatrie, le psychiatre cherche immédiatement à affirmer la relation asymétrique qu'il y a entre lui et lea fol. Il faut remplacer la volonté toute-puissante des patient·es par "la volonté étrangère" de la psychiatrie. Toute-puissante car c'est bien ce qui caractérise la folie, c'est affirmer de toute sa force que l'on refuse le "vrai", la démonstration. On apprend donc en premier lieu au malade la hiérarchie.

L'apprentissage ne se fait pas avec des discours et des arguments, on impose au malade la vérité. On lui fait apprendre les noms de ses supérieurs, on lui fait respecter les ordres, on lui réapprend des choses très scolaires comme des poèmes. C'est par le langage que le médecin entre en relation avec lea patient·e, qu'on lea rend attentif·ve cad qu'on se fait obéir par ellui.

> "Langage, donc, qui est transparent à cette réalité du pouvoir."

On voyait donc en premier lieu l'asile comme un moyen thérapeutique car il permettait de faire obéir les psychiatrisé·es, de leur faire suivre un emploi du temps, de leur faire respecter un ordre.

Un des moyens principaux utilisés autour des années 1830 fut la création de carences. Nourriture insuffisante, privation de liberté etc qu'on pouvait compléter grace à de l'argent que l'on pouvait obtenir en travaillant. On impose ainsi un système aux fols, on instaure une place importante pour l'argent, on force une réalité qui est comprise bon gré mal gré. On crée aussi la vision d'un monde extérieur d'abondance, souhaitable.

La guérison doit également passer par l'aveu. L'aveu de la vérité bien sur, même sous la contrainte, sous les douches froides ou autre, il faut faire céder lea malade et qu'il accepte de se soumettre à la vérité ne serait-ce que d'un point de vue performatif. Aveu également de lui-même, lea patient·e doit avouer qui iel est, son identité, sa biographie (et arrêter de crier "je suis Napoléon"). C'est une façon d'accepter, de se soumettre à la réalité administrative.

Enfin il faut faire attention car lea malade peut prendre plaisir à sa maladie, à l'attention que l'asile lui porte, voire à sa cure.

## Leçon du 9 janvier 1974

> "Le pouvoir psychiatrique est donc maitrise, tentative pour subjuguer \[...\] le psychiatre est quelqu'un qui dirige le fonctionnement de l'hôpital et les individus."

Le pouvoir psychiatrique a un rapport bien particulier avec la réalité. Il cherche, d'une part, a donné un supplément de pouvoir à la réalité, à la rendre imposante de manière à s'imposer devant la "volonté en insurrection, volonté illimitée" des fols. D'autre part, la psychiatrie se justifie elle-même par le fait qu'elle est la réalité.

> "Donc, donner pouvoir à la réalité, et fonder le pouvoir sur la réalité, c'est la tautologie asilaire."

On avait déjà pu dégager quelques particularités de la cure : le pouvoir asymétrique du psy, la recherche de l'aveu, faire reconnaitre à lea patient·e qu'iel est fol, débusquer les pulsions immorales qui créent la folie, insérer des techniques liées à l'argent ou aux besoins pour mieux contrôler. On dessine donc en négatif la guérison :

> "qu'est-ce que l'individu guéri, sinon précisément celui qui aura accepté ces quatre jougs de la dépendance, de l'aveu, de l'irrecevabilité du désir, et de l'argent ? La guérison, c'est le processus d'assujetissement physique quotidien, immédiat, opéré à l'asile, et qui va constituer comme individu guéri le porteur d'une quadruple réalité."

Les pseudo-connaissances médicales, censées trouver des causes physiques aux problèmes psychiques ont engendré des techniques comme la douche ou certains médicaments qui ont pour but de raviver la circulation sanguine ou autre. Ces techniques se sont rapidement transformées en techniques punitives. Les médicaments ont généralement des effets tranquilisants, continuant l'aspect disciplinaire jusque dans le corps.

> "L'asile c'est le corps du psychiatre, allongé, distendu, porté aux dimensions d'un établissement, étendu au point que son pouvoir va s'exercer comme si chaque partie de l'asile était une partie de son propre corps, commandée par ses propres nerfs."

Pour l'asile a-t-il besoin d'un médecin plus que d'un directeur ? Parce que le médecin sait. Que sait-il ? Peu importe. Ce qui compte c'est

> "l'existence d'un savoir \[...\] quel que soit le contenu effectif du savoir."

Ceci est important notamment pour la clinique. La clinique c'est réunir des gens pour le faire un cours en présence du / de la patient·e. C'est ramener l'amphithéatre dans l'asile. Cela permet de mettre en scène le savoir médical, et lea patient·e doit bien admettre que même si iel ne croit pas le médecin, d'autres le croient.

> "Ce sont ces marques de savoir, et non le contenu d'une science, qui vont permettre à l'aliéniste de fonctionnner comme médecin à l'intérieur de l'asile."

Foucault parle de microphysique du pouvoir asilaire pour désigner le

> "jeu entre le corps du fou et le corps du psychiatre qui est au-dessus de lui, qui le domine, qui le surplombe et, en même temps, l'absorbe."

Enfin toute cette technique psychiatre va être développée et utilisée également hors de l'asile, à l'école, en prison, ...

## Leçon du 16 janvier 1974

> "il me semble que dans tout le XIXè siècle, c'est surtout l'enfant qui a été le support de la diffusion du pouvoir psychiatrique."

Mais de manière peu intuitive, la figure de l'enfant fou apparait tardivement dans la psychiatrie, dans des consultations privées. De plus, l'enfance n'est pas interrogée comme cause de la folie. Pour Foucault, la psychiatrisation de l'enfance vient surtout de la figure de l'enfant idiot·e, imbécile et donc non-fol.

À la fin du XVIIIè siècle, l'imbécillité est vue comme une forme de folie. C'ett en effet une sorte de délire, mais poussé à l'extrême, une situation permanente ou il n'y a même plus de vérité, une forme totale du délire.
Vers les années 1815-1825 apparaissent de nouvelles définitions. L'idiotie n'est plus considérée comme une maladie mais comme une absence du développement des capacités cognitives. Ceci a plusieurs implications : l'idiotie apparait tôt car c'est une absence de développement (donc puberté) et c'est un état stable.

> "l'idiotie est toujours liée à des vices organiques de constitution."

> "L'homme en démence est privé de ses biens dont il jouissait autrefois : c'est un riche devenu pauvre ; l'idiot a toujours été dans l'infortune et la misère."

Plus tard, Séguin parlera de personne arriérée pour parler d'un développement plus lent que la normale. Et l'on voit donc deux normes surgirent, d'une part les autres enfants qui déterminent la vitesse normale de développement, et d'autre part les adultes qui définissent la fin du développement. On voit donc que les deux profils de l'arriéré et de l'idiot ne sont plus définis comme des fols mais comme des déviations d'une norme. Iels appartiennent donc à l'enfance. Iels vivent avec leur instinct car leur apprentissage de la morale ne s'est pas développé et il faut donc guérir ces anomalies par la pédagogie.

On voit apparaitre à la même époque (disons vers 1840) la constitution de quartiers pour enfant·es idiot·es dans les HP. Notons que si vers la fin du siècle la question est d'avoir un endroit pour accueillir les enfant·es turbulent·es ou "non-adapté·es" à l'école, à cette époque la principale occupation est de libérer les parents pour qu'iels puissent aller travailler.

> "Vous savez que si l'on a établi les 'salles d'asile', cad les crèches et les jardins d'enfants, vers les années 1830, et si l'on a scolarisé les enfants à cette époque, ce n'était pas tellement pour les rendre aptes à un travail futur, que pour rendre les parents libres de travailler, n'ayant plus à s'occuper des enfants."

Sur ces "idiot·es", le pouvoir psychiatrique va s'appliquer comme pour tous les autres, pour briser leur instinct.

> "la volonté adulte se caractéris\[e\] pour Séguin comme une volonté capable d'obéir. L'idiot est quelqu'un qui dit obstinément 'non', ou plutôt, l'instinct, c'est une série indéfinie de petits refus qui s'opposent à toute volonté d'autrui."

C'est l'inverse du fol qui dit toujours "oui" à tout. Il faut contrôler la fol et lui faire dire "non" et inversement pour l'idiot·e.

C'est la collectivité qui finançaient généralement les internements, il fallait donc une bonne raison de le faire, la psychiatrie a donc largement prétendue que les interné·es étaient dangereux. Et à la fin du siècle, dans la littérature médicale,

> "les enfants idiots seront effectivement devenus dangereux."

> "en 1895, Bourneville dit : 'L'anthropologie criminelle a démontré qu'une grande proportion des criminels, des ivrognes invétérés et des prostituées est, en réalité, des imbéciles de naissance qu'on a jamais cherché à améliorer ou à discipliner.'"

En résumé, au XIXè siècle, c'est l'adulte qui est fol et l'enfant·e qui est anormal·e. La psychiatrie va ainsi pouvoir se brancher sur tout un tas d'institutions et revendiquer la définition et le traitement de l'anormalité.
Il va également y avoir jonction, au sein de l'espace familial, entre l'adulte fol et l'enfant·e anormal·e. Et c'est dans cet espace que va se développer la psychanalyse, "c'est-à-dire la destinée familiale de l'instinct". On voit apparaitre la notion de dégénéresence qui, avant sa reprise par l'évolutionnisme, désigne "l'effet d'anomalie produit sur l'enfant par les parents", donc l'influence de la folie sur l'anormalité. Et l'enfant·e dégénéré·e risque, plus tard de devenir adulte fol.

## Leçon du 23 janvier 1974

La question de la vérité s'est posée à travers plusieurs points. À travers d'abord l'interrogatoire et l'arrachement d'aveux qui posent la question de la vérité de l'identité. ensuite par l'utilisation du magnétisme, de l'hypnose mais aussi de drogues, "essentiellement l'éther, le chloroforme, l'opium, le laudanum et le haschisch". On peut voir ces techniques comme des moyens d'accéder à la vérité, de la provoquer.

Pour Foucault, cela est représentatif d'un changement fondamental dans la vision de la vérité. La vérité est désormais considérée comme une chose présente en permanence et qu'il faut rtaquer, chasser, déloger. La vision précédente qui fut recouverte est celle d'une vérité qui n'est présente que de temps en temps, à certains endroits (Oracle de Delphes) et qui s'exprime par certains media. Il est donc intéressant de constater que cette nouvelle modalité de connaissance de la réalité n'a que peu à voir avec la connaissance en réalité, mais plus avec le pouvoir.

> "Montrer que la démonstration scientifique n'est au fond qu'un rituel, montrer que le sujet supposé universel de la connaissance n'est en réalité qu'un individu historiquement qualifié selon un certain nombre de modalités, montrer que la découverte de la vérité est en réalité une certaine modalité de production de la vérité ; comme vérité de démonstration, sur le socle des rituels, le socle des qualifications de l'individu connaissant, sur le système de la vérité-évènement, c'est cela que j'appellerai l'archéologie du savoir."

Étudier l'histoire (des idées) permet de comprendre ce phénomène de recouvrement entre un type de vérité et l'autre, à travers une "généalogie de la connaissance".

Un exemple typique de la vérité-foudre est la justice du Moyen-Age. Le but de l'interrogatoire / torture n'était alors pas de découvrir la vérité au sens de ce qu'il s'était réellement passé mais plutôt on cherche à acter la condamnation, à écraser lea condamné·e dans ce rapport de force inégal.

Prenons l'exemple de l'alchimie. Ce n'est pas une science de la même nature que la chimie. On ne cherche pas à accumuler des connaissances et à batir quelque chose dessus. On va chercher à produire un rituel, dans lequel la vérité éclatera potentiellement. Cela peut paraitre non-scientifique et ridicule mais c'est sur ce même principe de vérité-épreuve que s'est pratiquée la médecine pendant plus de vingt siècles, autour de la notion de 'crises'.

La crise, c'est le moment de la bataille, le moment ou se joue la vie de lea patient·e, ou le passage en une maladie chronique. La temporalité est importante, ainsi Hippocrate distinguait les fièvres qui avaient lieu les jours pairs de celles qui avaient lieu les jours impairs.

> "au moment ou se passe la crise, la maladie éclate dans sa vérité ; c'est-à-dire que non seulement c'est un moment discontinu, mais c'est, de plus, le moment ou la maladie - je ne dis pas : 'révèle' une vérité qu'elle aurait caché en elle, mais se produit dans ce qui est sa vérité propre, sa vérité intrinsèque. Avant la crise la réalité de la maladie est ceci ou cela, elle n'est, à vrai dire, rien. La crise, c'est précisément là que le médecin doit intervenir."

Dans la médecine, jusqu'au XVIIIè siècle, le rôle du médecin est d'assister les forces de la nature pour éliminer la maladie. Pour ce faire, il doit préparer l'arrivée de la crise qui est le moment de résolution du conflit. Le médecin n'affronte donc pas directement la maladie, c'est plus une sorte d'arbitre qui va essayer de faire pencher la balance en faveur de la nature mais qui peut être battu.

Le changement qui a eu lieu au XVIIIè siècle (et pas seulement en médecine) peut être imputé à plusieurs facteurs. D'abord le resserrement de l'enquête. Les rapports, la constitution d'un savoir à l'échelle institutionnelle, tout cela remplace la simple enquête fiscale du Moyen-Age. On ne s'intéresse plus seulement aux biens des gens mais à leur vie, à ce qu'ils pensent, disent, l'enquête devient policière.
Cette enquête s'est étendue sur tout le globe avec colonisation qui va finalement des gestes et des corps aux territoires et aux surfaces.

La vérité est présente partout et tout le temps, elle est donc accessible en théorie à tout le monde, mais en pratique elle passe par l'acquisition de certaines techniques, elle est accessible à peu de personnes.

> "Les universités, les sociétés savantes, l'enseignement canonique, les écoles, les laboratoires, le jeu des spécialisations, le jeu des qualifications professionnelles, tout cela, c'est une manière d'aménager à propos d'une vérité, posée par la science comme universelle, la rareté de ceux qui peuvent y accéder."

Dans l'asile, les crises sont exclues par le principe disciplinaire même de cette institution qui maintient l'ordre. La vérité (dont on n'a pas besoin) ne se cherche pas chez lea fol, qu'on ne croit jamais vraiment, on va la chercher de plus en plus dans l'anatomie, notamment à travers des autopsies. Enfin, la crise n'est pas souhaitable en psychiatrie car elle peut être dangereuse, lea fol étant un potentiel criminel·le.

> "il y a la volonté chez les psychiatres de fonder leur pratique sur quelque chose comme une défense sociale, puisqu'ils ne peuvent pas la fonder sur la vérité."

Le savoir psychiatrique se place à une échelle différente du savoir médical classique. Le savoir n'a pas pour objet la maladie mais l'existence même de la maladie. La question est de savoir si une personne est fol ou non. Bien sur, dans la lignée de la médecine de l'époque, on voit se développer un recensement de maladies mentales, des enquêtes, une médecine-consultation mais "ceci n'était que la couverture et la justification d'une activité qui se situait ailleurs".

> "L'hôpital psychiatrique est là pour que la folie devienne réelle, alors que l'hôpital tout court a pour fonction à la fois de savoir ce qu'est la maladie et de la supprimer. L'hôpital psychiatrique a pour fonction, à partir de la décision psychiatrique concernant la réalité de la folie, de la faire exister comme réalité."

L'asile en tant qu'institution a pour rôle de supprimer non pas la folie mais ses symptômes et le pouvoir psychiatrique a pour rôle de réaliser la folie. Le dément est ainsi lea fol parfait·e, iel est sa folie, indescriptible tant on en a raboté les symptômes. De l'autre côté, les hystériques s'approprient leurs symptômes, de manière précise, comme une personne symptomatique mais non malade. Ils simulent, fournissent leur crise à la demande, font naitre le soupçon.

> "saluons les hystériques comme les vrais militants de l'antipsychiatrie."

## Leçon du 30 janvier 1974

La psychiatrie s'intéresse dans son diagnostic à savoir si il y a folie ou non. On parle de diagnostic absolu plutôt que de diagnostic différentiel (comparaison des maladies).

La crise médicale, si importante pendant deux mille ans va donc se scinder en deux.

> "D'une part, par l'anatomie pathologique, l'on a substitué, à la crise médicale classique et à son épreuve, des procédures de vérification dans la forme du constat et de la démonstration : ceci a été la postérité médicale. Et puis, la postérité psychiatrique de la crise classique a été autre : il s'est agi, pour la psychiatrie - puisque l'on avait pas de champ à l'intérieur duquel cette constatation de la vérité était classique -, d'instaurer et de substituer à la vieille crise classique, une épreuve, mais non pas une épreuve de vérité - une épreuve de réalité. Autrement dit, l'épreuve de vérité se dissocie ; d'un côté, dans les techniques de la constatation de la vérité : c'est la médecine ordinaire ; \[de l'autre\], dans une épreuve de réalité : c'est ce qui se produit dans la psychiatrie."

L'épreuve psychiatrique va ainsi poser comme maladie la demande d'internement et par la même "fera fonctionner comme médecin" celui qui apporte ce diagnostic. Le psychiatre dit au patient·e :

> "avec ce que tu fais et ce que tu dis, fournis moi des symptômes pour, non pas que je sache quel malade tu es, mais pour que je puisse en face de toi être médecin."

Cela implique donc aussi un grand pouvoir du malade et les hystériques vont dire : regarde, c'est grace à moi, grace aux symptômes que je te fournis que tu es médecin et que tu as du pouvoir.

Comment fonctionne l'interrogatoire ?
Il recherche d'abord des antécédents familiaux. Vu l'impossibilité de trouver une origine de la maladie dans le corps de lea patient·e, on cherche des causes dans un corps plus étendu, celui de la famille.
Ensuite il y a la recherche d'antécédents chez l'individu même. On cherche à replacer la folie dans un contexte d'anomalies, c'est-à-dire les prédispositions qui peuvent mener à la folie.
Il y a également la question de la responsabilité juridique. Lea fol est souvent en psychiatrie car l'on s'est plaint d'ellui. Le psychiatre demande donc en un sens au fol de lui donner des symptômes, de prouver sa folie pour pouvoir lui ôter sa responsabilité juridique.
Enfin bien sûr, il y a l'aveu de la folie par lea fol. Cet aveu permet de dire "oui je suis malade, j'appartiens donc bien à cet institution, et vous êtes bien médecin".

> "l'interrogatoire psychiatrique, premièrement, constitue un corps par le système d'assignations d'hérédité, il donne corps à une maladie qui n'en avait pas ; deuxièmement, autour de cette maladie et pour pouvoir la repérer comme maladie, il constitue un champ d'anomalies ; troisièmement, il fabrique des symptômes à partir d'une demande ; et enfin, quatrièmement, il isole, il cerne, il définit un foyer pathologique qu'il montre et qu'il actualise dans l'aveu ou dans la réalisation de ce symptôme majeur et nucléaire."

Le pouvoir du psychiatre intervient donc principalement sur le thème du discours. Pour renforcer ce pouvoir, il est parfois important de le revigorer, et quoi de mieux qu'un interrogatoire entouré d'étudiants pour cela.

L'usage des drogues a été assez important au XIXè siècle en psychiatrie. Pour certains psychiatres et notamment Moreau de Tours, consommer du haschisch permettait de se plonger volontairement dans un état de folie (délire, hallucinations, etc) et donc de mieux comprendre et étudier la folie, sans même étudier de fols. On peut donc dire que si auparavant la folie c'était ce qui était inaccessible à l'esprit sain du médecin, désormais celui-ci peut expériencer la folie et mieux la comprendre. On parle alors de rêve pour désigner cet état anormal, que l'on peut atteindre de différente façon. Les rêves liés au sommeil ou à la drogue ne sont pas de la folie, la folie c'est quand le rêve se déverse dans la réalité et que les digues rompent. Certaines folies étaient soignées par le haschisch, on pensait que cela accroissait les symptômes et donc menait à une crise salvatrice.

L'hypnose et le magnétisme étaient également utilisés. Ils permettaient un contrôle de lea patient·e, mais aussi de donner une lucidité supplémentaire au / à la malade, l'"intuitive". Cette notion de lucidité disparait avec l'hypnose de Braid et laisse libre cours à la volonté du psychiatre. Lea malade est donc une page blanche, une matière prête à sculptée qui est, de plus, disciplinée, comme sous sédatifs.

> "L'hypnose, c'est ce qui va effectivement permettre d'intervenir sur le corps, non pas simplement au niveau disciplinaire des comportements manifestes, mais au niveau des muscles, des nerfs, des fonctions élémentaires."

On a donc trois instruments cruciaux du pouvoir psychiatrique : l'interrogatoire, l'hypnose et la drogue.

## Leçon du 6 février 1974

Contrairement à l'anatomo-pathologie, la neurologie étudie le corps en terme de stimulus-réponse plutôt qu'en terme de stimulus-effet. Le stimulus-effet c'est par exemple écouter la toux d'un·e patient·e et en déduire un type ou une cause de toux. Le stimulus-réponse ne cherche "des effets qui révèraient la présence de lésions en un point donné, mais des réponses qui montrent des dysfonctionnements". Par exemple, Duchenne de Boulogne étudie les muscles des jambes de personnes tabétiques, qui perdent l'équilibre.

> "on peut, à partir de cette analyse des comportements, des réponses aux différentes stimulations, voir quelle est la différence fonctionnelle, la différence de mise en oeuvre neurologique et musculaire entre un comportement qui est simplement un comportement réflexe \[et\] un comportement automatique, \[et\], enfin un comportement volontaire qui peut se faire sur un ordre venant de l'extérieur."

La neuropathologie montre donc la volonté de la patient·e, son attitude intentionnelle. Et c'est très important car c'est sur la volonté que s'exerce le pouvoir disciplinaire de la psychiatrie.
La neurologie c'est donc un nouveau dispositif, qui donne des ordres, des injonctions et regarde le corps qui répond. Ce dispositif est très pratique dans la mesure ou l'interrogatoire psychiatrique est dépendant de la volonté du fol.

> "L'épreuve de réalité n'est plus nécessaire \[...\] le neurologue dit : obéis à mes ordres, mais tais-toi, et ton corps répondra pour toi, en donnant des réponses que moi seul, parce que je suis médecin, je pourrai déchiffrer et analyser en terms de vérité."

Entre les maladies à diagnostic différentiel et celles à diagnostic absolu se cachent deux catégories de maladies intéressantes. La première concerne les paralysies qui sont vues positivement car il y a un lien supposé direct entre des lésions encéphaliques et des troubles physiques. La deuxième concerne les névroses, vues négativement, incompréhensibles dans leurs expressions et souvent connotées sexuellement. Cela va radicalement changer dans les années 1870 avec l'intervention de la neurologie qui va permettre de pathologiser un certain nombre de ces troubles.

> "Charcot, lui aussi, a fait en sorte que les hystériques puissent être reconnus comme des malades : il les a pathologisés."

Pour pathologiser l'hystérie il faut arriver à en dégager des signes bien précis. D'abord en dehors des crises, et ensuite en comprenant les crises et en les découpant en phases. Et l'hystérique va se faire un plaisir de donner des symptômes au neurologue. L'hystérique va également comprendre le nouveau pouvoir qu'il obtient vis-à-vis du psychiatre devenu médecin (neurologue).

Charcot va donc essayer l'hypnose pour démêler le vrai du faux. C'est également une période ou de nouvelleaux malades viennent à l'hôpital, les malades assuré·es, qui sont souvent des traumatisé·es du monde du travail. Et ces personnes sont parfaites comme sujets d'étude car on considère qu'ils ne sont pas autant simulateurices que les hystériques déjà hospitalisé·es et qu'on peut donc comparer leur symptômes.

Les hystériques vont, finalement, montrer des symptômes à caractère sexuel aux médecins, ce qui les embarrassera beaucoup, Charcot n'en parlera jamais. Il n'y a alors que deux solutions, ou bien disqualifier l'hystérie comme maladie ou bien tacher de reprendre le dessus à travers "la prise en charge médicale, psychiatrique, psychanalytique, de la sexualité."

> "En forçant les portes de l'asile, en cessant d'être des folles pour devenir des malades, en entrant enfin chez un vrai médecin, c'est-à-dire chez le neurologue, en lui fournissant des vrais symptômes fonctionnels, les hystériques, pour leur plus grand plaisir, mais sans doute pour notre plus grand malheur, ont donné prise à la médecine sur la sexualité."
