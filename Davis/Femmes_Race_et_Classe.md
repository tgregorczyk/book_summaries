# Femmes, Race et Classe (Angela Davis)

TW : esclavage, viol, meurtre, racisme, sexisme, N-word

## Chapitre 1 : L'héritage de l'esclavage : éléments pour une autre approche de la condition de femme

L'histoire des femmes esclaves se fait attendre, il semble que personne ne soit pressé de l'écrire.

Une différence notable entre les esclaves noires et leurs "soeurs blanches" est que ces premières travaillaient principalement à l'extérieur, aux champs. Elles étaient de outils de production, comme les hommes et étaient traitées comme tels. Alors que les femmes blanches étaient reléguées hors du monde du travail pour le capitalisme naissant (et reléguées à une condition, inférieure, de maitresse de maison), les femmes noires étaient 'égales' aux hommes dans leur soumission.

> "Lorsqu'il était question de travail, rien ne différenciait la force et la productivité d'un homme ou d'une femme, mais lorsqu'il s'agissait d'exploiter, de punir ou de brimer un être, les femmes étaient renvoyées à des rôles exclusivement féminins."

Après l'abolition de la traite internationale des Noirs, les femmes noires furent choisies notamment pour leur fécondité. Elles étaient "des ventres, du bétail, dont la valeur était fonction de leur capacité à se multiplier". Les enfants pouvaient être vendus à tout age car "les petits des esclaves \[...\] sont traités comme les autres animaux."
Les femmes noires étaient considérées comme plus rentables que les hommes. Elles étaient parfois utilisées comme bêtes de somme.

Il y eut longtemps le mythe du matriarcat dans les foyers noirs. Cela peut venir notamment du fait que le nom des enfants étaient basés sur celui de la mère (le père étant souvent le propriétaire) et du fait que les femmes noires n'étaient pas cloitrées à la maison. Le rapport Moynihan de 1965 affirme que les problèmes sociaux et économiques des familles noires, loin d'être le produit des discriminations, venaient du manque de suprémacie de l'homme dans le foyer !

La vie sociale des esclaves était bien sur très différente de celle des blanc·hes. Par exemple, les femmes n'étaient pas seulement des 'ménagères' car elles travaillaient aussi, et autant que les hommes. La structure familiale était souvent hétérogène (à cause de morts précoces, de ventes, ...).

> "L'égalité des sexes était un élément essentiel de la vie domestique des esclaves."

Les femmes cousaient et s'occuper de la case quand les hommes jardinaient et chassaient (cette séparation des sexes étant parfois floue). Ces activitées étaient essentielles à la survie du groupe et l'on ne considérait pas qu'une soit supérieure à une autre. Les femmes participaient aussi aux actes de défiance, de sabotage et aux tentatives de fugues.

> "Le viol était une arme de domination, une arme de répression dont le but secret était d'étouffer le désir de révolte des femmes et de démoraliser les maris."

Angela Davis compare cette situation avec les encouragements faits aux GI's de violer des vietnamiennes.

## Chapitre 2 : Le mouvement anti-esclavagiste et la naissance des droits des femmes

Les femmes ont pris une part importante dans la lutte anti-esclavagiste. Une de ses figures importantes est Harriet Beecher Stowe qui a publié le très populaire roman *La case de l'oncle Tom*. Sa vision est celle de l'époque : les femmes blanches sont avant tout des mères, et elles sont donc sympathisantes des esclaves qui sont présentés comme des enfants.

La condition des femmes blanches changea drastiquement au XIXe siècle, et leur rôle domestique fut infériorisé. Des protestations éclatèrent dès les années 1830, chez les ouvrières comme chez les bourgeoises (quoique pour des motifs différents), et les femmes commencèrent à se comparer (par provocation) à des esclaves. Elles participèrent massivement aux campagnes abolitionnistes de cette époque. Les femmes bourgeoises (libre du travail salarié) menaient ces luttes et tentaient de tenir tête aux hommes sexistes de la Convention par la même occasion refusant d'être exclue du champ politique et luttant pour leur propre libération.
Un exemple (dans tous les sens du terme) est Prudence Crandall qui accepta des élèves noir·e·s dans son école. Elle dut luter contre les hommes blancs de Canterbury qui se liguèrent contre elle, refusant de la soigner ainsi que ses élèves, refusant de vendre de la nourriture, empoisonnant le puit, mettant le feu au batiment...

La lutte abolitionniste pour les femmes blanches leur permirent donc d'apprendre la lutte politique (organisation de meeting, de réunion, diffusion de textes, pétitions, ...) ce qui leur sera très utile lorsqu'elle se battirent pour leurs droits dix ans plus tard. Si les hommes s'opposaient farouchement à ce mouvement, certains allaient parfois à ces meetings mixtes écouter des oratrices. L'attaque la plus violente de cette lutte des femmes vint de l'église qui fustisgait ces femmes qui reniait leur propre nature (il est dans la nature des hommes, et d'eux seuls, de réformer la société). Ces propres firent prendre conscience aux femmes qu'elles devaient se battre pour leurs droits également, d'une part pour contre l'attitude abjecte des hommes, d'autre part pour ne pas être exclues de la lutte abolitionniste.

## Chapitre 3 : Les mouvements de classe et de race au début du mouvement pour les droits des femmes

Au milieu du XVIIIe, les revendications des femmes blanches bourgeoises essayaient de trouver un écho dans la politique masculine. Elles dénonçaient principalement le mariage (véritable assujetissement des femmes), et certaines allaient jusqu'à réclamer le droit de vote. Ces revendications se retrouvèrent notamment dans la convention de Seneca Falls (1848).
Mais ces revendications féministes oubliaient bien souvent le sort des femmes blanches prolétaires et des esclaves noires. Les femmes prolétaires travaillaient notamment dans l'industrie du textile dans des conditions abominables (journées de 12h, 14h, maladies, ...). Ces postes étaient d'abord occupés par des femmes américaines au début du siècle avant d'être remplacées par des immigrantes. Dès 1820, ces femmes se rebellaient régulièrement et connaissaient la grève.
De la même façon, certaines bourgeoises avaient un angle mort sur le racisme.

Les abolitionnistes blancs voyaient donc l'esclavage comme une horreur morale, mais n'avaient aucun soucis avec l'oppression capitaliste terrible dans le nord, et ne voyaient aucun lien entre le travail des esclaves et celui des prolétaires libres. De la même façon, les bourgeoises réclamant des droits pour les femmes voyaient dans la phallocratie la tare d'une société acceptable.

Après le début de la guerre de sécession, les racistes du nord firent des émeutes. En 1963, ils s'en prirent à la population noire, faisant plus d'un millier de victimes.

## Chapitre 4 : Le racisme dans le mouvement pour le vote des femmes

Dans les cercles abolitionnistes, une question fut l'objet de controverse, le droit de vote des Noirs, question qui allait bien sur se poser après la fin de l'esclavage. Certain·e·s soutenaient qu'il fallait privilégier la question du droit de vote des femmes, et que les noirs pouvaient attendre, voire qu'il était préférable qu'ils attendent longtemps. Certaines blanches comme Stanton pensaient qu'il ne fallait pas élevé d'hommes, même (surtout ?) noirs, au-dessus des femmes.

> "Les capitalites du Nord cherchaient à contrôler toute l'économie de la nation. Leur lutte contre l'esclavagisme du Sud n'englobait ni la libération des Noirs si celle des femmes."

Les femmes blanches se sentirent trahies après la fin de la guerre. Les républicains refusèrent de considérer le droit de vote des femmes, pas celui des noirs (qui leur assurait deux millions de voix supplémentaires). Ces femmes virent donc leur droit leur échapper et refusèrent de militer pour le droit de vote des Noirs, avec une bonne dose de racisme. Cette vision n'était évidemment pas unanime mais elle existait très clairement.
Pour certains, le droit de vote des Noirs était une priorité. Les Noirs, affrachis, restait dans une situation très précaire, notamment au Sud, et certains pensaient qu'ils ne seraient libres qu'avec le droit de vote. Cette vision était soutenue par le climat extrêmement tendu et des émeutes contre les Noirs :

> "À Memphis et à la Nouvelle-Orléans, des Noirs et des Blancs progressistes avaient été blessés ou tués. Au cours de ces massacres, les émeutiers incendièrent les écoles, les églises et les maisons des Noirs. Ils violèrent aussi, seuls ou en groupe, les femmes noires qu'ils rencontrèrent."

Au sein de l'Association pour l'Égalité des Droits, qui militaient un temps pour les droits des Noirs et des femmes, deux clans se firent face. Elizabeth Cady Stanton et Susan B. Anthony voulaient en priorité le droit de vote pour les femmes. Elles prirent un tournant raciste, et s'allièrent, par opportunisme, à des individus peu recommendables comme George Francis Train qui scandait "La femme en premier, le Nègre en dernier, voilà mon programme." De l'autre côté, Frederick Douglass militait en urgence pour le droit de vote des Noirs (sans s'opposer à celui des femmes).

Dans les années 1960, des amendements constitutionnels importants furent adoptés. Le 14e "stipulant que seuls les citoyens males avaient un droit de vote inaliénable". Le 15e interdisait la privation du droit de vote pour "des raisons discriminatoires de race, de couleur ou de condition (mais pas de sexe !)".

## Chapitre 5 : les femmes noires et la fin de l'esclavage

Après la fin de la guerre, les conditions des Noir·e·s au Sud n'était pas glorieuses. Le système en place avait fait en sorte de rendre leur impossible sans endettement. Le code pénal était très dur, et iels pouvaient être arrêté·e·s pour plein de raisons différentes. Les grands propriétaires louaient ensuite les prisonniers pour les faire travailler.

> "États et employeurs avaient donc tout intérêt à augmenter la population de prisonniers."

Les femmes noires étaient toujours autant soumises aux violences sexuelles, et étaient incarcérées si elles résistaient. Elles dans les champs ou comme domestiques. En 1899, 9% d'entre elles n'étaient **pas** domestiques. Les lois Jim Crow (imposant la ségrégation aux gens de couleur) étaient suspendues pour les domestiques qui accompagnaient les Blancs (pour qu'elles puissent faire leur travail).
Les domestiques sont vues comme des imbéciles immorales, leur parole ne vaut rien.
La situation n'était pas meilleure au Nord et resta inchangée pendant des décennies.

> "Dans les années 1940, il existait encore des marchés dans les rues de New York et d'autres grandes villes - versions modernes des ventes d'esclaves - ou les femmes blanches étaient invitées à choisir des domestiques parmi les femmes noires qui cherchaient du travail."

Le travail domestique était peu syndiqué et peu soutenu par les femmes blanches. En 1940, 60% des femmes noires étaient domestiques, 72h par semaine. 26% travaillaient toujours dans les champs, 10% commençaient à vivre hors de la matrice esclavagiste ... à l'usine. Pendant la guerre, 400 000 d'entre elles quittèrent le travail domestique pour aider à l'effort de guerre.

## Chapitre 6 : du côté des femmes noires, éducation et libération

L'éducation des Noir·e·s était un sujet important. Les maitres refusaient l'éducation aux esclaves car ils craignaient les esclaves instruits. Il y eut de nombreuses tentatives pour ouvrir des écoles pour personnes de couleur / intégrer ces personnes à des écoles existantes. Les esclaves essayaient souvent d'apprendre à lire dans la clandestinité.

> "Myrtilla Miner entretenait la flamme que lui avaient léguée les soeurs Grimké, Prudence Candrall et bien d'autres. En prenant de tels risques pour défendre leurs soeurs noires, toutes ces femmes blanches ne s'étaient pas fortuitement engagées dans la lutte pour l'éducation."

On ne peut pas assez appuyer sur l'importance qu'à eu l'éducation.

## Chapitre 7 : Le vote des femmes au début du siècle, la montée du racisme

Dans les années 1890 et 1900, de nombreux États du Sud adoptèrent de nouvelles constitutions pour priver les noirs du droit de vote. À cette période, la situation des noir·e·s dans le Sud devient de plus en plus insupportables. Chez les puissants, la peur du vote noir, le "problème noir", effraye. La solution semble simple, donner le droit de vote aux femmes (blanches) qui sont généralement lettrées. Cette thèse raciste ("Qui hésiterait entre des femmes cultivées et des Noirs illétrés ?") sera reprise par les suffragettes dans une résolution adoptée en 1893, date de l'entrée en vigueur effective des lois Jim Crow.
Cette décennie vit aussi des avancées de l'impérialisme américain à Hawai, Cuba, les Philippines, Porto Rico.

Au tournant du siècle, la suprématie blanche teintée d'eugénisme se renforça. Les femmes n'étaient plus vues que commes des mères blanches. Et les suffragettes y adhérèrent. Le discours d'ouverture de la convention d l'Association Nationale Américaine pour le vote des femmes en 1901 :

> "\[Grace à\] l'émancipation intelligente des femmes, la race sera purifiée."

Trois grands ennemis du droit de vote pour les femmes furent indiqués : la militarisation, la prostitution et le vote des étrangers / Noirs / amérindiens.

La classe capitaliste mis en avant le risque de conflits si les Noirs obtenaient plus de pouvoir, divisant ainsi racialement la société :

> "Selon \[le sénateur Ben Tillman\], les écoles permettaient à ceux qu'il considérait comme 'notre parent le plus proche du singe' 'de rivaliser avec leurs voisins blancs' : elles allaient 'opposer les plus pauvres de notre société et ces futurs concurrents sur le marché du travail'."

> "Il leur fallait briser l'unité de la classe ouvrière pour faciliter son exploitation."

## Chapitre 8 : les femmes noires et le mouvement des clubs

Les années 1890 virent aussi l'émergence de clubs de femmes noires, notamment sous l'impulsion d'Ida B. Wells, et Mary Church Terrell. Un des grands enjeux fut la lutte contre le lynchage. Ces clubs font écho aux clubs de femmes blanches (de la classe moyenne) qui étaient plus des passe-temps que des mouvements politiques.

## Chapitre 9 : ouvrières, femmes noires et histoire du mouvement pour le droit de vote

En 1866, Le Syndicat National du Travail fut créé et il prit en compte les couturières, alors que les femmes étaient alors peu représentées dans un monde ouvrier très masculin. Le syndicat se prononça pour "la syndicalisation généralisée des femmes et l'égalité des salaires".

Les suffragettes, bien que pas contre les syndicats en théorie, voyaient d'un mauvais oeil le fait de promouvoir la lutte de classe. Pour elles, le plus important était la lutte pour les droits des femmes. Pour les ouvrières, le droit de vote était un concept abstrait, elles préféraient se battre pour une amélioration immédiate de leurs conditions de travail. Elles ne réclamèrent le droit de vote qu'au début du XXe siècle quand elles en ressentir la nécessité.

Contrairement aux hommes blancs, les hommes noirs soutenèrent la lutte des femmes pour le droit de vote, malgré le racisme de certaines suffragettes.

Le droit de vote des femmes est acquis par le XIXe amendement qui rentre en vigueur en 1920. Dans les faits, de nombreuses femmes noires sont empêchées de voter par la force.

## Chapitre 10 : les femmes communistes

Les organisations marxistes se développèrent dès 1850 aux USA, d'abord à forte majorité masculine. C'est avec le mouvement pour le droit de vote des femmes au tout début du XXè et que le tout jeune parti Socialiste créé en 1900 se féminisa et accorda un rôle important aux femmes et à leur lutte. Une grande manifestation pour le droit de vote des femmes fut organisée le 8 mars 1908, date qui resta.
Le Parti ne s'occupait cependant pas du problème du racisme, étant concentré uniquement sur la lutte de classe. C'est du côté de l'Internationale Mondiale des Travailleurs (IWW) fondée en 1905 qu'il faut aller chercher pour trouver le soutien aux noir·e·s. À sa première tribune, on retrouvait notamment Lucy Parsons et Mother Bloor. D'abord anarchiste puis communiste, Parsons se batit toute sa vie pour le droit des ouvrier·e·s (arguant que les noir·e·s et les femmes ne subissaient pas d'oppression propre, qu'ils étaient simplement d'une classe inférieure aux travailleurs blancs). Elle fut la femme d'Albert Parsons, martyr d'Haymarket. Ella Reeve Bloor, communiste blanche, fut également une très grande figure de la lutte des ourvrier·es, des femmes, des noir·e·s.
On aussi citer Anita Whitney, secrétaire générale du Parti communiste en Californie.

> "Le savez-vous ? Un Noir dit un jour que s'il possédait l'enfer et le Texas, il préfererait louer le Texas et habiter en enfer."

## Chapitre 11 : le viol, le racisme et le mythe du violeur noir

Le viol a été utilisé depuis longtemps par les hommes blancs pour marquer leur domination sur les femmes noires. Ces viols ne sont quasiment jamais punis devant la loi. Au contraire de nombreuses accusations ont été portées contre des hommes noirs qui auraient violé des femmes blanches. Ces accusations sont généralement fausses et permettent de justifier des lynchages et des répressions.
Et il faut bien voir que ces attitudes des hommes blancs envers les femmes racisées peuvent se propager à toute la société. Il en va de même pour les GIs revenus du Vietnam qui avaient été encouragés à violer des vietnamiennes.
Le mythe du violeur noir s'appuya sur de nombreuses théories au fil des années, biologiques, sociologiques, philosophiques ... Tout était bon pour expliquer que les noir·e·s étaient avides de relations sexuelles : donc les noirs violent les blanches et les noires sont immorales et leurs viols sont acceptables.

Le mythe du violeur noir était une justification parfaite aux lynchages, qui permettaient de menacer et d'opprimer les noir·e·s. Après 1872, on vit l'émergence de milices : Le Ku Klux Klan et les Knights of the White Camelia notamment. Initialement, les lynchages n'avaient pas vraiment besoin de justification dans le sud raciste, il servait simplement à éviter que les noir·e·s ne se soulèvent. C'est avec le temps et après la fin de la guerre qu'il fallut trouver des nouvelles justifications. Et ce mythe fonctionna à merveille et parvint à pénétrer la société en profondeur.
Les femmes blanches participaient généralement aux lynchages, elles désignaient les coupables et prenaient parfois part à l'action violente. Les enfants étaient souvent présents pour voir et apprendre :

> "Au cours d'une enquête sur le lynchage en Floride, une petite fille de neuf ou dix ans déclara 'qu'ils s'étaient bien amusés à voir bruler les Nègres'."

Ce sont les femmes noires qui furent à l'avant-garde de la lutte contre le lynchage, mais c'est avec l'aide des femmes blanches que les choses commencèrent à changer. Le seul problème est que les femmes blanches se réveillèrent avec quarante ans de retard.

## Chapitre 12 : racisme, contrôle des naissances et libre maternité

Le droit à l'avortement aux États-Unis fut acquis en 1973 suite à la décision de la cours suprême *Roe v Wade*.

La campagne de lutte pour obtenir ce droit a principalement été mené par des femmes blanches, notamment à cause du racisme du mouvement pour le contrôle des naissances qui avait soutenu la stérilisation forcée que les noir·e·s associaient à un génocide. Les femmes noires avortaient bien plus que les blanches, avant ou après qu'elles en aient le droit légalement. Elles ne souhaitaient généralement pas élever d'enfants dans la misère. Dans ce contexte, l'avortement n'est acceptable que si c'est réellement un choix, c'est-à-dire qu'il n'est pas forcé par des conditions matérielles trop limitées. Les femmes noires et leurs ancêtres esclaves avortaient non pas parce qu'elles ne souhaitaient pas d'enfants mais parce qu'elles ne souhaitaient pas d'enfants *dans ces conditions*.

> "En 1977, par l'amendement Hyde, le Congrès vota la suppression des fonds du gouvernement fédéral pour l'avortement."

Dans les faits, les femmes pauvres furent privées du droit à l'avortement alors que "la stérilisation, financée par le ministère de la Santé, de l'Éducation et des Affaires sociales, étaiet pratiquée gratuitement sur demande".

Historiquement, les femmes blanches en ville commencèrent à avoir moins d'enfants au tournant du XXe siècle.

> "En 1905, le président Roosevelt termina son discours de 'la journée de Lincoln' en proclamant 'qu'il fallait préserver la pureté de la race'. Et, en 1906, il associa la chute du taux de natalité chez les Américains blancs au 'suicide de la race'. Dans son message aux États de l'Union, Roosevelt accusa les femmes blanches des classes favorisées, du 'péché de stérilité, et les menaça de les juger responsables de la mort de la Nation, du suicide de la race'."

> "En 1932, la Société pour l'Eugénisme pouvait se targuer d'avoir fait voter des lois de stérilisation obligatoire dans vingt-six États et d'avoir fait stériliser des milliers de gens 'indignes' de se reproduire. Margaret Sanger \[du mouvement pour le contrôle des naissances\] se félicita en public de ces résultats. Elle déclara à la radio qu'il fallait stériliser les 'arriérés, les débiles mentaux, les épileptiques, les illétrés, les pauvres, ceux qui ne pouvaient pas travailler, les criminels, les prostituées et les toxicomanes'. Elle ne voulait pas pousser l'intransigeance jusqu'à leur ôter toute possiblité de choix : ils pouvaient donc, s'ils le souhaitaient, opter pour une vie isolée dans des camps de travail jusqu'à la fin de leurs jours."

Cari Schultz du Bureau des Affaires Démographiques estiment qu'en 1972, entre 100 000 et 200 000 stérilisations ont été subventionnées par le gouvernement. Elles concernaient principalement des personnes opprimées, parfois illétrées qui étaient souvent trompées. Certains médecins n'acceptaient ces personnes que si elles acceptaient de se faire stériliser. Les chiffres précédents sont du même ordre de grandeur que ceux du régime nazi.

> "la médecin Connie Uri révela devant un comité du Sénat qu'en 1976, 24% des femmes amérindiennes en age d'être mères avaient été stérilisées."

> "En 1970, selon une étude du Bureau de Contrôle Démographique de l'Université de Princeton sur la fécondité nationale, 20% des femmes noires mariées et à peu près autant de femmes chicanas ont été rendues définitivement stériles. D'autre part, 43% des stérilisations financées par les programmes du gouvernement fédéral ont été pratiquées sur des femmes noires."

Sur Porto Rico :

> "\[En 1939\], le comité interdépartemental du président Roosevelt pour Porto Rico fit paraitre une étude qui estimait que la surpopulation de l'ile était responsable de ses problèmes économiques. Ce comité proposait des mesures permettant d'aligner le taux de natalité sur celui de la mortalité."

On ouvrit des cliniques et on stérilisa. Au milieu des années 1960, on enregistrait une baisse démographique de 20%.

> "Dans les années 1970, plus de 35% des Portoricaines pubères avaient été stérilisées."

## Chapitre 13 : Du côté de la classe ouvrière, vers un déclin du travail domestique

Avant le capitalisme, le travail domestique des femmes consistaient principalement en la production du nécessaire pour vivre (vêtements, bougies, pain, savon etc), ainsi que de connaissances médicinales etc Elles n'étaient en rien des ménagères au sens actuel, c'est-à-dire qu'elles s'occupaient peu souvent du ménage ou des lessives. L'arrivée du capitalisme a sorti ces domaines du contexte domestique pour aller vers la production de masse. Le rôle des femmes (blanches bourgeoises) fut donc dévalorisé et un nouveau récit émerga, celui de la femmes comme mère, protectrice du foyer. Les femmes prolétaires étaient donc vues comme n'obéissant pas à cet idéal universel, elles étaient étrangères dans le monde masculin de l'usine.

Pour le capitalisme, la condition préalable est l'existence de travailleurs exploitables. Et pour la reproduction de travailleurs, le travail domestique est nécessaire. Les femmes travaillent donc gratuitement à la maison pour ce système qui exploite leur travail.

Pour Davis, il est important d'abolir le travail domestique et il est préférable que les femmes travaillent hors de chez elles. Donner une rémunération aux femmes qui travaillent à la maison ne peut pas être une alternative viable. Enfin, il convient de penser une société qui ne se base pas sur ce travail domestique, qui s'organise pour garder les enfants etc, une société socialiste.
