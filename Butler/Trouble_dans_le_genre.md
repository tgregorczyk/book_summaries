# Trouble dans le genre, Judith Butler

## Préface (Éric Fassin)

Dans les années 1980 un débat fait rage entre féministes. D'un côté, certaines voient le sexe comme instrument de libération et d'expérimentation, d'autres y voient un facteur d'oppression (viol, prostitution, pornographie, ...). C'est pourquoi la féministe pro-sexe Gayle Rubin va chercher à définir le genre comme opposition à la sexualité plutôt qu'au sexe. Le débat est donc sur la définition des termes de genre, sexe, sexualité et de comprendre leurs articulations. C'est dans cette prespective que s'inscrit ce livre de Butler.

Au sujet du travestissement, Butler nous dit que ce genre de performances censées renverser les normes, ne sont pas forcément subversives, surtout si celles-ci sont prises dans un rituel, répété et accepté.

> "La performativité est assignation normative, comme on le voit avec l'insulte ou l'injure. C'est ainsi que nous sommes constitués en tant que sujets- : le genre n'est pas notre essence, qui se révèlerait dans nos pratiques ; ce sont les pratiques du corps dont la répétition institue le genre."

On est proche de la notion d'*habitus* de Bourdieu.

Mais cependant, tout n'est pas déterminisme culturel. C'est plus "une pratique d'improvisation, dans une scène de contrainte."

Butler reprend la vision foucaldienne : nous subissons la domination (le pouvoir) mais c'est aussi cette domination qui nous crée, nous fait exister, nous donne une place.
Pour Bourdieu, la domination fonctionne d'autant mieux qu'elle est intériorisée.

Ainsi, se concentrer sur les drags, comme marges, nous permet de mieux comprendre la règle générale. Cette règle est qu'il y a une norme de genre et que chacun essaye de l'imiter. Étre drag est évidemment vu comme une comédie, on joue un rôle, mais en réalité tout le monde joue un rôle. On essaye tous de se conformer aux normes de genre, sans d'ailleurs jamais réellement réussir car c'est impossible.

## Chapitre 1 : Sujets de sexe/genre/désir

### Les 'femmes' en tant que sujets du féminisme

Le féminisme traite bien sur des femmes, mais encore faut-il réussir à définir cette catégorie. On peut penser que la structure dans laquelle nous vivons produit ces femmes :

> "Foucault signale que les systèmes juridiques du pouvoir *produisent* les sujets qu'ils viennent ensuite à représenter."

et donc :

> "croire que ce système permettra l'émancipation des 'femmes' revient à se mettre en situation d'échec."

Les 'femmes' - sujets du féminisme - constituent donc une catégorie produite par des structures de pouvoir. Cette catégorie est dominée par nature, car elle est créée comme telle.
L'idée de sujet (notamment de la loi) rappelle l'idée que le sujet existe avant la loi, qui a de fait été créée pour ce sujet précis alors que c'est en réalité la loi qui crée son sujet.

L'idée d'un patriarcat universel est aujourd'hui obsolète pour bon nombre de féministes car cette notion oublie les différents axes de pouvoir qui traversent une société (classe, race, ...). Pour Butler, la question du terme "femme" comme universel doit donc aussi être remis en question. Les femmes sont multiples et ne peuvent être considérées comme une catégorie homogène. Il est donc important de redéfinir le sujet du féminisme, en gardant à l'esprit que cette catégorie en doit pas être forcément homogène, que sa définition crée négativement ceux qui ne sont pas sujets du féminisme, et se méfiant de cette catégorie comme cristalisant des rapports de genre, etc

### L'ordre obligatoire du sexe/genre/désir

Dans l'acceptation classique,le sexe est une donnée biologique, et le genre est son pendant culturel. Cependant, il ne faut pas oublier que le sexe a une histoire et que sa définition scientifique, est produit justement par les scientifiques qui ne sont pas une classe neutre en dehors de la société. Le sexe aussi s'inscrit dans un contexte culturel. Alors comment repenser la distinction entre sexe et genre dans ce cadre ?
Pour Butler, le genre comprend donc la dimension du sexe comme appartenant au domaine prédiscursif c'est-à-dire cette construction qui place le sexe dans le domaine du naturel : dire que le sexe est uniquement biologique, c'est un fait culturel.

### Le genre : les "ruines circulaires" du débat actuel

Dire que le genre est un aspect culturel du sexe pose question. D'un côté, si le genre est construit par la culture, on a à faire à une construction déterministe. De l'autre si l'on suppose comme Simone de Beauvoir que l'on devient femme, il semble y avoir une part de libre-arbitre, certe conditionné par la culture, mais c'est une construction à laquelle on prend part. D'un côté, le corps est vu comme un instrument neutre sur lequel viennent s'apposer des marques culturelles, de l'autre c'est "l'instrument par lequel une volonté d'appropriation et d'interprétation se choisit une signification culturelle."
On remarque alors que le corps est vu comme un simple instrument qui existe avant le genre, alors qu'en réalité il se co-construit avec le genre.

Pour Beauvoir, l'identité masculine est l'identité universelle, l'identité féminine est donc construite en deçà, et souvent réduite à son sexe. Pour Irigaray, l'identité masculine se traduit aussi par un language phallogocentrique qui ne permet pas à l'identité féminine de s'exprimer en ses termes. Autrement dit le féminin n'est même pas conçu comme l'Autre par rapport à l'homme comme le pense Beauvoir, le féminin ne peut pas être représenté dans une matrice purement masculine :

> "Le rapport entre le masculin et le féminin ne peut pas être représenté dans une économie de la signification ou le masculin forme le cercle fermé du signifiant et du signifié."

Pour Beauvoir, le masculin est l'universel, le féminin est réduit à l'Autre et à une dimension corporelle tandis que le corps de l'homme est "nié".
Butler reproche à Beauvoir de construire le corps contre la liberté (et l'esprit).

### Théoriser le binaire, l'unitaire et l'au-delà

> "à force d'insister sur la cohérence et l'unité de la catégorie 'femme' on a fini par exclure les multiples intersections culturelles, sociales et politiques ou toutes sortes de 'femmes' en chair et en os sont construites."

Pour Butler il faut se lancer dans la lutte sans nécessairement avoir une unité, une cohérence car le dissensus peut être positif et participer à la "démocratisation". Est-il réellement souhaitable d'avoir une définition bien précise, négative, qui rejette certaines personnes ?

> "Définir une identité dans les termes culturellement disponibles revient à poser une définition qui exclut à l'avance la possibilité que de nouveaux concepts de l'identité émergent dans l'action politique."

### Identité, sexe, et métaphysique de la substance

Historiquement, on a souvent imaginé une personne comme préexistant à son contexte, sa culture, son genre au niveau de sa conscience, aptitude au langage etc

> "la 'cohérence' et la 'constance' de 'la personne' ne sont pas des attributs logiques de la personne ni des instruments d'analyse, mais plutôt des normes d'intelligibilité socialement instituées et maintenues. L'identité étant dixée par des concepts tels le sexe, le genre et la sexualité, l'idée même de personne- est mise en question par l'émergence culturelle d'êtres marqués par le genre de façon 'incohérente' ou 'discontinue', des êtres qui apparaissent bel et bien commes des personnes, mais qui ne parviennent à se conformer aux normes de l'intelligibilité culturelle, des normes marquées par le genre et qui définissent ce qu'est une personne."

L'identité de genre se construit dans une société et dans sa culture. Les genres "intelligibles" dans cette matrice sont ceux pour qui il y a continuité entre le sexe, le genre et la pratique / le désir sexuel.

> "L'hétérosexualisation du désir nécessite et institue la production d'oppositions binaires et hiérarchiques entre le 'féminin' et le 'masculin' entendus comme des attributs exprimant le 'male' et la femelle'."

Les genres intelligibles sont donc produits contre tous ceux qui ne rentrent pas dans les clous des normes.

Résumé bref de la pensée de Wittig : il n'y a qu'un sexe, le féminin, car le masculin est l'universel. Il faut détruire le sexe pour permettre aux femmes d'accéder à cet universalité.
Cette vision semble donc célébrer un personne réellement universelle, un corps qui n'a donc pas été marqué par le genre. La liberté humaine a donc un "statut pré-social".

> "L'institution d'une hétérosexualité obligatoire et naturalisée a pour condition nécessaire le genre et le régule comme un rapport binaire dans lequel le terme masculin se différencie d'un terme féminin, et dans lequel cette différenciation est réalisée à travers le désir hétérosexuel."

> "le genre se révèle performatif - c'est-à-dire qu'il constitue l'identité qu'il est censé être. Ainsi, le genre est toujours un faire, mais non le fait d'un sujet qui précèderait ce faire. [...] il n'y a pas d'identité de genre cachée derrière les expressions du genre ; cette identité est constituée sur un mode performatif par ces expressions, celles-là mêmes qui sont censées résulter d'une identité."

### Langage, pouvoir et stratégies de déstabilisation

> "Wittig parle du 'sexe' comme d'une marque qui est en quelque sorte apposée par une hétérosexualité d'institution, une marque susceptible d'être effacée ou altérée par des pratiques qui mettent vraiment en cause cette institution."

En revanche Wittig considère que le langage n'est pas misogyne, c'est un instrument qui est souvent utilisé de manière misogyne. Pour Irigaray, le marque du genre est à comprendre dans le contexte d'une économie de la signification purement masculine. Il faudrait inventer un nouveau langage pour s'en extraire, et la binarité H/F n'est qu'une ruse car tout n'est réellement que masculin.
Pour Wittig, le langage n'est qu'un autre type de matérialité, actuellement marqué par l'hétérosexualité obligatoire qui impose binarité masculin / féminin, mais qui peut être dépassée.

Wittig renverse la vision de Freud qui explique que la sexualité génitale se développe et est supérieure à la supériorité infantile. L'homosexualité est en quelque sorte une sexualité qui ne s'est pas développée.

> "En réalité, on ne peut comprendre l'idée de développement que comme un processus de normalisation au sein de la matrice hétérosexuelle."

Wittig veut donc placer l'emphase sur la sexualité non-génitale, non-reproductrice.

> "Chez Lacan, comme chez Irigaray lorsqu'elle reprend Freud en des terms post-lacaniens, la différence sexuelle n'est pas une simple binarité fondée sur la métaphysique de la substance. Le 'sujet' masculin est une construction fictive produite par la loi qui prohibe l'inceste et impose au désir hétésexuel de se déplacer sans cesse."

Les lois qui définissent le genre sont donc :

> "Le tabou de l'inceste qui barre l'accès du fils à la mère et établit par là le rapport de parenté entre ex est une loi appliquée 'au nom du Père'. De la même manière, la loi qui interdit à la fille de désirer tant sa mère que son père exige qu'elle reprenne le flambeau de la maternité et perpétue les règles de la parenté."

Les positions masculines et féminines sont donc constituées avec un sexualité inconsciente.

Butler détaille les différences entre les positions d'Irigaray comme exemple de l'école féministe post-lacanienne et de Wittig comme exemple des féministes matérialistes (françaises).

> "Les différences entre les positions matérialistes et lacaniennes (et post-lacaniennes) apparaissent dans le cadre d'une controverse normative sur la question de savoir si l'on peut recouvrer une sexualité soit 'avant' soit 'en dehors' de la loi sur le mode de l'inconscient ou, 'après' la loi, sur celui de la sexualité post-génitale."

Il est important de comprendre que la loi, ou les rapports de pouvoir, créent la sexualité aussi bien hétérosexuelle qu'homosexuelle. Penser une sexualité post-génitale en dehors de la matrice hétérosexuelle ne peut pas fonctionner car l'on ne peut pas s'extirper cette matrice, se positionner contre elle c'est aussi se positionner par rapport à elle.

> "Que des cultures non hétérosexuelles reproduisent la matrice hétérosexuelle fait ressortir le statut fondamentalement construit de ce prétendu original hétérosexuel. Le gai ou la lesbienne est donc à l'hétérosexuel·le *non pas* ce que la copie est à l'original, mais plutôt ce que la copie est à la copie."

Il faut donc accepter de se placer dans le cadre de la matrice du pouvoir et cela ne signifie pas renoncer à une critique de cette matrice, ni renoncer à repenser des possibilités subversives pour fragiliser cette matrice. Il convient donc pour cela de comprendre la construction des lois pour essayer de ne pas les reproduire.

> "Parmi les possibilités de faire du genre, lesquelles répètent et déstabilisent ces constructions qui les mobilisent par l'hyperbole, la dissonance, la confusion interne et la prolifération ?"

Pour Foucault, la nature des "pratiques régulatrices" qui reproduisent les rapports de pouvoir / discours sont assez flou, pour Wittig il s'agit de la reproduction sexuelle et de son outil, l'hétérosexualité obligatoire. Pour Butler, ce n'est pas tout, le système de production des normes est complexe ("la complexité de la carte discursive") et parfois contradictoire. Il faut donc espérer que le système produise lui-même des identités qui le déstabilisent, ou plutôt que des identités soient produites de manière contradictoire par différents discours ce qui déstabilise la fiction catégorielle.

> "Si les multiples fictions régulatrices du sexe et du genre sont elles-mêmes autant de lieux de signification ouverts à la contestation, alors la multiplicité même de leur construction offre la possibilité de déstabiliser leur positionnement univoque."

> "Le genre, c'est la stylisation répétée des corps, une série d'actes répétés à l'intérieur d'un cadre régulateur des plus rigide, des actes qui se figent avec le temps de telle sorte qu'ils finissent par reproduire l'apparence de la substance, un genre naturel de l'être."

## Chapitre 2 : Prohibition, psychanalyse et production de la matrice hétérosexuelle

Les féministes ont souvent cherché à remonter à des temps pré-patriarcaux. Cette recherche est partagée par un système qui a bien compris qu'il pouvait en tirer avantage :

> "En n'admettant qu'une seule vraie histoire de ce passé impossible à recouvrer, le récit des origines est une statégie narrative qui fait de la constitution de la loi une nécessité historique."

Chercher un idéal, un modèle dans ce passé fantasmé est donc un cul-de-sac pour Butler qui y voit un récit historique plus qu'une histoire objective. De Engels à l'anthropologie structuraliste, de nombreuses tentatives ont cherché à mettre en lumière le passage au patriarcat à travers une dialectique de la nature et de la culture. Certains anthropologues ont donc eu pour idée de montrer comment une femelle naturelle / biologique devient femme socialement subordonnée (sexe naturel / genre construit). Ce genre de récit a d'ailleurs tendance à féminiser la nature, soumise à la culture. Or le sexe est lui aussi politique.

### L'échange critique du structuralisme

> "Le discours structuraliste a tendance à énoncer la Loi au singulier, suivant en cela Lévi-Strauss qui postulait l'existence d'une structure universelle régulant les échanges propres à tous les systèmes de parenté."

Pour Lévi-Strauss, des relations sont donc créées par l'échange de femmes dans le cadre de l'institution du mariage. Une différenciation symbolique se crée et donne leur identité aux hommes. Les femmes, elles, n'ont pas d'identité propre, elles sont le moyen d'échange entre clans patrilinéaires.

> "En tant qu'épouses, les femmes n'assurent pas seulement la reproduction du nom (but fonctionnel) ; elles établissent aussi des rapports sexuels symboliques entre les clans des hommes."

Pour Lévi-Strauss, c'est le tabou de l'inceste qui crée l'exogamie et donc l'échange des femmes. Pour Irigaray, cette économie phallogocentrique est fondée sur un désir homo-social. La ou l'anthropologue français considère des structures universelles (le tabou de l'inceste qui donne lieu à une certaine économie), la féministe française parle de désir de lien et de réciprocité des hommes pour leurs semblables

> "une sexualité *refoulée* et donc dévalorisée, une relation entre hommes qui porte finalement sur les liens entre hommes, mais qui ne s'établit qu'à travers l'échange hétérosexuel et le partage des femmes."

Lévi-Strauss présente le tabou de l'inceste comme la pierre cardinale de l'économie de la parenté interdisant l'endogamie. Il va jusqu'à la justifier en reprenant Freud et en affirmant que ce tabou fonctionne parfaitement, au mépris des réalités chiffrées. Pour Butler, il faut comprendre comme ce tabou et son érotisation ont des effets sociaux sur la pratique réelle de l'inceste, car ce n'est pas parce qu'il y a prohibition que celle-ci fonctionne.

> "La question est plutôt de savoir comment la prohibition pesant sur de tels fantasmes les produit voire les institue"

Et comment la croyance que le tabou est une prohibition parfaite permet dans la réalité à l'inceste de perdurer en toute impunité.

> "Pour Lévi-Strauss, le tabou de l'inceste hétérosexuel entre le fils et la mère - l'acte de même que le fantasme - est érigé en vérité universelle de la culture. [...] La naturalisation simultanée de l'hétérosexualité et de la sexualité masculine comme sexualité active est une construction discursive dont on ne trouvera nulle explication même si elle est constamment présupposée dans ce cadre structuraliste de base."

### Lacan, Riviere et les stratégies de mascarade

Dans la vision de Lacan, l'ontologie, cad la question de l'être et du néant n'est pas un bon point d'attaque. Il faut plutôt se demander ce qui constitue l'être "à travers les pratiques signifiantes de l'économie paternelle". Lacan distingue ceux (celleux...) qui ont le Phallus et ceux qui sont le Phallus. La position féminine d'être le Phallus signifie incarner le Phallus comme zone de pénétration. Le masculin a donc besoin de l'Autre (féminin) pour que ce dernier le confirme.

> "Mais la production de sens est un processus qui requiert que les femmes reflètent le pouvoir masculin et qu'elle rassurent constamment ce pouvoir sur la réalité de son autonomie illusoire."

Cette dépendance aux femmes est niée et recherchée en même temps

> "car la femme en tant que signe rassurant est le corps maternel déplacé, la promesse, vaine mais non moins persistante, de recouvrer la jouissance qui précède l'individuation."

Cette jouissance, "plaisirs associés au corps maternel", doit être refoulée nous dit Lacan pour que le "sujet accès à l'Être".

> "Pour pouvoir 'être' le Phallus, le réflecteur et le garant d'une position qui est visiblement celle du sujet masculin, les femmes doivent devenir, 'être' (au sens ou elles doivent 'faire comme si elles étaient') ce que les hommes ne sont précisément pas, et c'est par leur manque qu'elles doivent établir la fonction essentielle des hommes."

Le paraitre des femmes est qualifié de *mascarade* par Lacan. Butler analyse ce terme comme étant ambivalent, il signifie à la fois que la position féminine n'est qu'apparences et jeux, mais aussi qu'il existe une nature fondamentale et cachée derrière ce jeu.

> "D'un côté, on pourrait considérer que la mascarade consiste à produire sur un mode performatif une ontologie sexuelle, un paraitre qui réussit à passer pour un 'être' ; d'un autre côté, on pourrait voir dans la mascarade un déni du désir féminin présupposant une sorte de féminité ontologique préalable qui n'est normalement pas représentée dans l'économie phallique."
