# Judith Butler : race, genre et mélancolie (Hourya Bentouhami)

## Introduction

> "La mélancolie caractérise ce qui est perdu sans que l'on puisse identifier clairement l'objet de la perte et en faire le deuil. Dans ce cas, l'objet perdu est incorporé, devient une partie du soi, dont il est impossible de se séparer."

Pour Butler, la mélancolie peut s'appliquer à un corps politique. Dans ce cas, elle concerne toutes les personnes et catégories de personnes qui sont mises au ban de la société, jugées anormales ou déviantes.
Les personnes qui s'écartent de la norme, de ce qu'est un vrai corps, un corps normal, sont stigmatisées et stéréotypées. Chaque individu de la population déviante peu être remplacé par un.e autre. Ce processus d'essentialisation est appelé *fongibilité* et est inhérent au fonctionnement raciste.

Le genre renvoie à la construction sociale qui se base sur le sexe prétendumment naturel. Le genre renvoie aussi à la hiérarchisation de sexualité, avec l'hétérosexualité comme norme et l'homosexualité comme déviance.
Pour Butler, le sexe n'est pas naturel, c'est aussi un peu du genre. En effet, le genre c'est cette croyance d'une construction sociale sur une base naturelle (le sexe). Le discours sur le genre est donc aussi un discours sur le sexe.

> "L'efficacité politique (soit la normalisation des conduites qui résulte des attentes liées au genre) tient à la dimension performative des discours tenus sur le sexe, le genre et les sexualités. [...] Les énoncés [...] sont dits performatifs par la philosophe car, alors même qu'ils se présentent comme des énoncés descriptifs, ils sont en réalité des actions, des interventions dans le monde."

Ces énoncés créent donc le réel en créant des normes, des modèles auquel il faut s'identifier pour être légitimé.es socialement. Ces modèles sont donc reproduits de manière mimétique.

> "Ces répétitions ordinaires façonnent un discours de soi, de la famille et de la nation qui repose sur des exclusions hors du champ symbolique."

Butler nous dit que le sujet du féminisme ne va pas de soi. La catégorie de femme a ainsi été stabilisée autour de la femme blanche dans une matrice hétérosexuelle. Certaines femmes ne sont pas donc pas considérées (autant) comme femmes (que d'autres). En reprenant le terme de forclusion de Freud :

> "Est forclos ce qui échappe à la représentation consciente. Tandis que ce qui est exclu est consciemment mis à l'écart, ce qui est forclos fait l'objet d'une répudiation inconsciente, si bien que l'on ne se représente même pas ce qui est rejeté."

Les lesbiennes tout comme les femmes noires ont pu être exclues de cette catégorie de femme. On voit donc comment une catégorie pensée comme naturelle charie tout un imaginaire qui a été utilisé pour opprimer certaines populations et contrôler leurs corps. Le droit de fonder une famille, le statut juridique, le contrôle de la natalité entre autres sont fondés sur des catégories bien précises d'hommes et de femmes. Féminiser des hommes, masculiniser des femmes, faire sortir des personnes des catégories normatives permet de les faire sortir du droit et de la société.

> "Toute déconstruction passe en effet par un mésusage, une réappropriation détournée de ce qui constitue initialement le site de l'oppression. La catégorie de 'genre' est utile si elle devient le terme qui rend possible un retournement de situation et de signification."

## Chapitre 1 : Performativité du genre et de la race, à propos du mimétisme

### Performativité, *passing / queering*

> "Dans sa formulation butlérienne, la performativité est définie comme le processus circulaire par lequel la loi ou la norme produit ce que, paradoxalement, elle présuppose : le sujet de la loi, de la norme, est dès lors à la fois antérieur et postérieur à l'énoncé de la loi."

*Passer* au sens de *passing*, c'est réussir à passer pour quelqu'un d'une catégorie à laquelle on n'appartient pas normalement, c'est passer de l'autre côté d'une frontière (raciale, genrée, ...) sans être remarqué.e pour profiter de certains privilèges (passer pour un blanc pour obtenir un logement par exemple). Butler prend les exemples de travestissement (drag), transsexualité, travestissement vestimentaire. Les recherches de ces dernières décennies ont permis d'obtenir des biotechnologies (par exemple hormonales) qui facilitent le passing (notamment trans).

Au fond, tout est travestissement, l'hétérosexualité aussi, c'est une imitation de certaines normes. Les drags ne sont donc pas moins travesti.es que les autres, iels remettent simplement en lumière le phénomène même de travestissement qui semble invisible au quotidien.

> "Dans quelle mesure la mise en scène et l'exhibition des normes peuvent-elles constituer une manière de critiquer et de dénoncer le caractère inhabitable, invivable, des normes de genre ? Interpréter, est-ce nécessairement dénaturaliser les normes ?"

Pour Butler, les représentations drags sont parfois réutilisées (au cinéma) comme divertissement haut de gamme qui au fond ne fait que renforcer l'hétérosexualité blanche. En créant une panique homosexuelle, la norme se protège d'une invasion queer tout en en montrant une représentation.

Pour Homi Bhabha, la *mimicry* est une traduction du langage dominant dans le langage du / de la dominé.e.

> "il s'agit de faire entrer l'ordre de la loi coloniale dans le langage indigène."

Pour exister, il faut donc reproduire une norme, une norme qui n'est pas exprimable dans les mots des opprimé.es, elle est "contaminée par la culture locale".

> "La disjonction entre l'ordre et celui qui l'incarne - le subordonné, à la fois objet et sujet de l'ordre - fait vaciller la littéralité du texte."

Mais il faut bien comprendre qu'une norme n'est qu'un idéal, donc "le modèle original fait en réalité défaut". L'imitation vaut aussi bien pour les hétéros que pour les homos :

> "Le gay ou la lesbienne est donc à l'hétérosexuel·le *non pas* ce que la copie est à l'original, mais plutôt ce que la copie est à la copie."

Partant de là, les performances drags mettent en lumière le système car elles reprennent littéralement des normes qui ne sont pas censées être interprétées par ces corps.

> "Ainsi performées littéralement, les représentations idéalisées apparaissent pour ce qu'elles sont : des fantasmes, dont l'original est insaisissable - car qui a jamais vu une 'vraie' femme ?"

Pour les personnes pour qui le passing est vital, l'identité devient celle de l'effacement.

> "On finit par vouloir apparaitre sous la forme d'une disparition, d'un *ne pas être vu·e* comme homosexuel·le, *ne pas être vu·e* comme musulman·e, ce qui peut évidemment induire la dissolution psychique du sujet."

À noter également le risque d'instrumentalisation des outing queer dans un but islamophobe (homonationalisme)

> "Dans le cadre d'une politique néolibérale, il s'agit de l'assimilation / normalisation d'une partie du queer par des États qui trouvent par là l'occasion de légitimer leurs campagnes impérialistes par leur supériorité morale et civilisationnelle, supposée démontrée par leur tolérance"

### Blanchité obligatoire

Dans nos sociétés occidentales, la blanchité est la norme, c'est un capital social, un passe-partout. On parle d'institutions *colorblinds* (littéralement aveugles aux couleurs) pour désigner le fait que le position légal antiraciste fait que la couleur de peau des individus n'est pas censé être pris en compte alors que les individus sont considérés par défaut comme blanc·hes.

Être normal dans cette société, c'est avoir le privilège d'être inattentif à la vie, c'est avoir un corps marqué comme neutre.

> "À l'inverse, les corps marqués sont attentifs à la vie ; ils se surveillent eux-mêmes et investissent de réflexion leurs faits et gestes, jusqu'à la saturation psychique."

Il est souvent souhaitable d'avoir un corps marqué, notamment masculin ou féminin. C'est en effet à travers ce marquage que l'on peut être désiré.e en société. Accepter le stigmate de la différence sexuelle pour les femmes, c'est obtenir une reconnaissance.

On peut aussi s'intéresser à la façon dont le mot "juif" / "juive" a longtemps été associé à des féminités / masculinités moindres, voire dégenré. Et comment l'extrême-droite israélienne se réapproprie cette identité de manière très blanche en oubliant de nombreuses façon d'être juifve, notamment arabe, noire ...

### Désir de race et pulsion de mort

La race est donc pensée comme une différence, un dégout et un rejet de l'Autre, ce qui va de pair avec une fascination et un désir.

> "La marchandisation de l'altérité raciale, ethnique, dans les rêves exotiques de la publicité ou dans la promotion de la nourriture 'ethnique' globalisée, exprime cet appétit pour la différence qui participe de la pensée de la race, plutôt qu'il n'en est le dépassement."

Il en va de même d'un point de vue sexuel : il y a une fétichisation et un désir qui proviennent directement du tabou du mélange racial. On peut voir l'hétérosexualité normative comme une production de ce tabou associé à la répudiation de l'homosexualité. On obtient une norme qui promeut une reproduction sexuelle "racialement pure".

> "ce que l'on répudie dans la mélancolie est maintenu vivant en nous et, surtout, devient *désirable* dans son abjection même."

Les études sur la mélancolie se sont donc intéressées à cette question, comment aimer ce que l'on déteste, est-ce seulement de l'amour, comment concilier ces sentiments antagonistes de désir et de désir de mort.

Il est important de voir que le patriarcat capitaliste, raciste et hétérosexiste s'est consistué historiquement sur l'aliénation (devenir autre), sur l'exposition à la blessure (vulnérabilité) et à la mort. C'est en niant l'humanité de certains individus que l'on construit l'humanité en négatif.

> "En même temps que la vulnérabilité caractérise de façon générale la condition humaine, certaines populations sont bien sur infiniment plus exposées à la violence et à l'annihilation."

Butler parle d'être "marqués pour la mort" en prenant pour exemple les principales victimes du sida, les homosexuels, prostituées et toxicomanes. Il est intéressant de constater que l'impossibilité de faire famille et d'accéder à la parenté peut rendre plus importante la vulnérabilité. De manière analogue, on parle de ligne de souillure pour exprimer comment ce qui est en contact avec des déchets devient déchet et se voit constituer en sujet abject.

> "Selon Hartman en effet, le déchet, la fongibilité des corps en esclavage, c'est-à-dire du fait qu'ils font l'objet d'un échange, pour lesquels ils sont préparés, rendus beaux et appétissants."

Ces corps sont appétissants car ils sont voués à la consommation capitaliste et sexuelle mais ils sont en même temps voués à la mort sociale ou réelle.

On peut donc considérer que tout sujet se constitue par attachements aux autres, mais cet attachement peut être nié par la mélancolie raciale, cad que quelque chose échappe à la représentation consciente. La répudiation inconsciente (forclusion) peut ainsi prendre différentes formes, de l'abhorration au désir.

### Tabou de l'homosexualité et fétichisme racial

Dans les années 70/80, Mapplethorpe est connu pour ses photographies de nus, masculins et souvent homo-érotiques. En 89, un sénateur obtient la censure de l'une de ses expositions. Butler critique la vision fétichisante de Mapplethorpe, ses gros plans sur des parties génitales démesurées (de personnes noires), des mises en scène de fantasmes. Pour la philosophe, présenter certaines parties du corps (pénis, seins, ...) comme sources de plaisir, c'est déjà considérer un corps construit d'un certain genre en le comparant à un idéal normatif.
Il est donc intéressant de penser les organes sexuels, en fonction de la race (la peur de la rencontre du pénis noir et du vagin blanc par exemple), mais aussi en fonction de leur "naturalité". Butler parle de sexes par destination pour parler d'organes naturels ou synthétiques qui ne sont normalement pas sexuels mais qui peuvent l'être dans une situation donnée.

> "Que se passe-t-il en effet si des seins se retrouvent sur un corps qui se dit, se réclame, masculin ? Que se passe-t-il lorsque le pénis est une prothèse accrochée à un corps de femme, et qui plus est non-blanche ? Quel type de disjonction érotique et symbolique ces corps induisent-ils au coeur de l'hétéronormativité blanche ?"

Pour Butler, "l'assimilation du désir au réel" est le syndrome de l'hétérosexualité mélancolique. Le désir (hétérosexuel et centré sur les parties génitales) est compris comme le réel (pénis et vagin comme organes génitaux naturels, la race comme donné biologique ou culturel). Cette assimilation c'est de la mélancolie et les normes qui s'appliquent sont celles de l'hétérosexualité blanche. L'apparence (que voir, que regarder) est donc saturé de représentations inconscientes, Butler appelle cela la "paranoia blanche".

### Paranoia blanche

Un symbole de la paranoia blanche est l'affaire [Rodney King](https://fr.wikipedia.org/wiki/Rodney_King#L'affaire_Rodney_King). Les images de son passage à tabac furent visionnées à son procès et le tribunal renversa les accusations, sa défense fut comprise comme agression. Pour Butler, cela représente typiquement la façon dont le voir peut être modifié par un regard raciste. La défense est vue comme entrave à la force de police, la victime est responsable de la façon dont elle est perçue.

> "Rendre visible un corps implique de lui assigner une marque à la fois raciale et sexuelle [...], avec les qualités et les propriétés afférentes en termes de valence positive ou négative figée ("criminel", "athlétique", "provocant", "aguicheuse", etc.)"

> "Le voir est lui-même un acte performatif essentiel à la reproduction de la matrice hétérosexuelle et raciale."

Le visuel est donc factice, il n'existe que pour une personne donnée, dans un certain cadre d'intelligibilité. Les hommes noirs sont vus par les blanc·hes comme une menace, et c'est ainsi qu'ils sont perçus par les forces de l'ordre.

> "Ces corps sont vus comme dangereux pour les Blancs et devant être humiliés, notamment sexuellement."

D'ou l'intérêt des stratégies de passing, "de détournement du voir", pour échapper à la violence.

> "Il ne suffit pas de ne pas apparaitre soi-même comme non-blanc, il faut, pour apparaitre tel, s'entourer de corps tenus pour blancs ou 'sans couleur'. C'est donc la *relation* qui fait le marquage."

## Chapitre 2 : Biopolitique de la parenté, l'humain en question

> "La reconnaissance comme être humain d'un individu passe aussi selon Butler par la légitimité attribuée à sa famille"

### Mort sociale et pathologisation des familles non-blanches

La question de la famille est intimement liée à celle de l'humanité (création de lignées, contrôle des naissances, importance accordée à telle naissance, transmission culturelle, ...). Les familles esclaves étaient régulièrement disloquées, il était impossible de faire famille pour ces populations, d'ou l'idée de mort sociale.
Faire famille est donc très important pour les populations marginalisées qui peuvent y retrouver un sanctuaire. Ces familles peuvent être assez éloignées des standards que ce soit au niveau de différences culturelles dans la manière d'élever les enfant, au bien par impossibilité d'être dans la norme (personnes queer). Ces familles ont donc une histoire de résistance aux oppressions.

Il est intéressant de constater que ces familles marginalisées ont été stigmatisées / dévaluées, que ce soit avec l'aide d'arguments psychanalytiques (il faut une forte figure paternelle, ...), pour justifier la délinquance des enfants (dégradation des structures familiales dans les banlieux), ou bien en considérant des populations entières comme d'éternels enfants.

> "Que ce soit dans les discours de la loi ou dans des régimes et pratiques biopolitiques qui décident de la valeur de l'enfance, de la minorité et de la majorité morale, médicale et politique, l'obsession de l'enfance à discipliner, réguler et soigner, loin de susciter une authentique politique de protection des enfants et de leur famille, induit une pathologisation, voire une criminalisation des enfances vécues dans des familles qui ne correspondent pas au modèle de la reproduction de l'ordre national."

Il est alors facile de dire que les familles noires manquent d'une figure d'autorité que l'État se doit se remplacer.

### Qui peut être parent ?

Butler critique la théorie structuraliste de Lévi-Strauss pour qui le tabou de l'inceste est au fondement de la culture. Pour la philosophe, il s'agit en réalité du "tabou inavoué de l'homosexualité". En effet, comment expliquer un tabou de l'inceste quand cette pratique est très largement répandue ?
Pour Lévi-Strauss, le tabou de l'inceste et l'injonction à l'exogamie crée un échange de femmes. Pour Butler, ces femmes lient les hommes et leur famille entre elles mais sont en même temps de simple objet de transaction. Et c'est dans cet échange que les femmes sont femmes. La patrilinéarité implique une réciprocité entre hommes et excluent les femmes de ce processus.
Être femme signifie donc dans ce contexte être soumise à l'hétérosexualité et à la conjugalité. On peut donc voir aussi comment certaines femmes n'étaient pas insérées dans la féminité dans un contexte colonial. Les femmes colonisées étaient mises à disposition des colons suivant d'autres modalités, elles n'étaient donc pas femmes de la même façon.

> "En réalité, dit Butler reprenant cette fois Wittig, ce qui est au fondement de l'échange et de la culture, pour assure la reproduction de la nation, c'est l'interdit de l'homosexualité et du mélange racial, et non l'interdit de l'inceste."

> "De fait les relations à l'intérieur des clans patrilinéaires sont fondées sur le désir homo-social [...] une sexualité *refoulée* et donc dévalorisée, une relation entre hommes qui porte finalement sur les liens entre hommes, mais qui ne s'établit qu'à travers l'échange hétérosexuel et le partage des femmes."

Les relations entre femmes sont donc inconcevables dans ce cadre car être femme c'est être une relation entre des hommes. En revanche, pour les femmes non-blanches, accéder à ces relations, à ces liens, être la fille de ou la femme de, c'est se défaire d'une position raciste et cela peut être vu comme un moyen d'émancipation.

### Tabou du métissage

Nous avons l'importance du tabou de l'homosexualité pour la reproduction d'une société. Il semble qu'il faille également prendre en compte une dimension raciale, avec le tabou du métissage. En effet, la production d'enfants n'est pas encouragée de façon neutre, elle est imbriquée dans une histoire coloniale et de classe ou les enfants valorisés sont forts et non-mélangés.
Il faut comprendre le tabou de l'inceste de Lévi-Strauss comme la nécessité de se marier en dehors du clan, mais pour Butler il faut ajouter à cela le tabou du métissage qui limite qui nous dit de ne pas se marier "trop" en dehors du clan. Cela peut être compris notamment dans les nations coloniales par toutes les lois, règles et normes autour des enfants métis. Il est d'ailleurs intéressant de voir que ces lois peuvent être contradictoires. Il faut en effet proscrire le métissage au sein du territoire national pour reproduire une nation blanche, mais le métissage et l'homosexualité pouvait être tolérés voire encouragés dans les colonies comme forme d'exutoire pour les colons, ou comme politique coloniale pour briser les résistances et faciliter "l'acclimatation" comme l'on disait. Les discours sur l'hygiène et l'éducation participent à des politiques de "blanchiment". Enfin, la dépréciation dese enfances marginalisées est aussi un symptôme d'un récit historique national qui présente une nation qui se construit seule, avec pour dégout la dépendance à des populations jugées comme subalternes.

### "Sois mon corps" : une pensée de la fongibilité raciale

Du Bois et Fanon parlent de "double conscience" pour parler du phénomène caractéristique pour les noir·es vivant en territoire blanc de se faire observer et de se voir à travers les yeux des dominant·es. Butler parle de la même façon du dédoublement des corps car les corps se construisent en fonction de corps qui leur sont extérieur. On peut dire de corps dominés qu'ils sont créés hors d'eux-même car ils sont créés par les corps dominants qui leur disent "sois mon corps".
Les corps dominés sont également vidés de leur essence par leur fonction, ils sont au service d'autres corps, à leur soin, leur service, leur plaisir, au détriment de leur propre corps. Ces services et ces corps sont forclos, ils sont oubliés par le mythe de la nation qui se construit elle-même. Pour Butler et sa théorie de la non-violence, il faut donc dès le plus jeune age remettre sur le devant de la scène ces systèmes de domination qui sont effacés.

> "Le corps du maitre et celui du serviteur ou de la domestique sont enlacés, attachés l'un à l'autre, chaque corps étant vécu ailleurs qu'en lui-même. Mais ces extériorisations ne sont pas symétriques et la délégation de corps fait l'objet d'un immense travail d'effacement, de négation, si bien que seul apparait finalement comme humain celui qui est dit 'libre', c'est-à-dire qui se présente comme ne dépendant que de lui-même. Ainsi, ce que suggère Butler, c'est que la déshumanisation des corps est passée historiquement par l'affirmation mensongère de l'indépendance des corps dignes, tandis que la dépendance était définie par le propre de celles et ceux qui sont incapables de liberté, car incapables de subvenir eux-mêmes à leurs besoins."

Ainsi la déshumanisation implique la répudiation des liens sociaux entre maitre et serviteur. Cela implique également que le corps du maitre est pensé comme abstrait et loin de la matérialité (se nourrir, se soigner, effectuer des taches bassement matérielles), ce qui permet de le couper des taches effectuées par ses serviteurs et qui lui sont pourtant essentielles.

## Chapitre 3 : Politiques du nom, injure raciale et politique de la non-violence

> "Reconsidéré à l'aune des politiques raciales et de genre, le pouvoir d'attribuer des noms est l'un des principaux pouvoirs discursifs du gouvernement des corps, mais il qualifie aussi la puissance littéraire et politique, pour qui les noms sont toutjours des fictions mobilisatrices."

Butler dialogue avec les auteurices de la Critical Race Theory pour avancer ses concepts clés : la vulnérabilité comme possibilité d'être blessé·e, la violence et notamment celle du discours, et la performativité comme lien entre dire et faire.
En particulier ce qui intéresse ici est le pouvoir du langage, des mots, leur capacité à blesser comme à réparer.

### Antiracisme politique *vs* antiracisme moral ?

Pour Butler, un énoncé raciste ou homophobe ne peut pas être réduit à une simple mauvaise intention d'un sujet malveillant qui insulte une personne sans défense qui reçoit l'insulte. L'énoncé dépasse l'intention tout comme les personnes présentes. Butler affirme par exemple que l'État joue son rôle dans cette interaction car par ses lois et pratiques racistes, il autorise la violence

> "C'est pour cette raison aussi qu'elle conteste que l'État devrait venir en aide aux *victimes*, supposément sans défense car abandonnées de lui. Comment, en effet, en appeler à l'intervention de l'État alors qu'il sature de sa présence la scène de la violence ?"

Pour Matsuda, la violence des injures raciales est telle que c'est comme si les personnes "racisées" étaient projetées hors de l'État car privées de sa protection. Elle argumente donc en faveur d'une restriction du Premier Amendement de la constitution américaine pour limiter la liberté d'expression aux énoncés qui ne sont pas des injures de ce type.
Butler s'écarte de cette interprétation car pour elle, d'une part, les victimes ne sont pas projetées hors de l'État car c'est bien l'État qui permet et tolère ce genre d'actions, et d'autre part, car cela implique une impuissance de la victime hors du cadre juridique qui lui est défavorable.

> "Pour la philosophe, un mot n'a pas à lui seul la puissance de réaliser ce qu'il énonce : l'arbitraire qui est au coeur du langage peut permettre de retourner les mots contre les agresseurs."

En effet, une injure n'exprime qu'un fantasme, pas une réalité. Pour Butler il faut donc sortir d'un discours victimaire qui recherche la protection par les institutions car celle-ci ne peut pas permettre de combattre le racisme et l'homophobie institutionnelle, bienveillante, implicite.

### Mort linguistique et survie dans le discours : dans les pas de Toni Morrison

Plutôt que de considérer les mots comme les véhicules de l'intentionnalité, il faut donc mieux les voir dans leur contexte et leur histoire. Cela permet de s'emparer de ce contexte plutôt que de simplement le recevoir passivement. L'énoncé, hors de son contexte, renseigne bien peu sur sa capacité à blesser. Il faut voir l'énoncé comme une répétition d'une certaine situation et s'interroger sur le fait que cette répétition aggrave ou soigne la blessure (réappropriation).
Enfin il esst problématique de considérer que des mots sont des actes. Ainsi l'injure blesse, et le coming out peut être instrumentalisé comme un acte dangereux voire contagieux pour certain·es.

### Occuper le genre et la race

Pour Butler il faut partir du lieu de la violence pour se la réapproprier

> "Les collectifs qui se constituent autour de la blessure, de l'injure et de la perte ont la puissance de donner un autre sens à leurs expériences, en partant précisément du déni dont cette violence fait l'objet"

Il faut donc réinvestier les termes de race, de genre, ces termes qui ont été imposés, pour en modifier la signification. Il faut "occuper soi-même ces termes qui nous occupent".

Butler définie de manière particulière la non-violence comme le fait de refuser, pour des corps assujettis, d'être des réceptacles de la violence, notamment policière. C'est donc une forme de contre-violence, être un obstacle, physiquement, à la violence policière. Butler parle de "non-violence aggressive" qui aurait une forme politique et collective du refus du brutalisme politicier et militaire.

La philosophe critique également la notion de modernité qui se base sur des valeurs occidentales et qui est utilisée pour dévaluer certains groupes culturels. Par exemple, l'homonationalisme se base sur une le fait que les nations européennes seraient plus tolérantes des personnes LGBT pour dévaluer les pays musulmans. De la même façon, on va considérer que le voile est une oppression qu'il faut combattre au nom de la liberté. La modernité permet donc une hiérarchie des populations qui se retranscrit au final par le fait que la mort de certaines personnes est moins grave que d'autres.
