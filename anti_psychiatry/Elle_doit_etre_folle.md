# Elle doit être folle (Jennifer L. Reimer)

## 1. Intro

Le manuel diagnostic et statistique des troubles mentaux (DSM) est la bible psychiatrique, elle répertorie dans sa quatrième édition 297 troubles. Dans les troubles de la personnalité (TP), on retrouve trois grands groupes. Le premier (A) parle des "comportement bizarre ou excentrique", il ne fait pas référence aux émotions et comporte par exemple les TP paranoiaque ou schizoide, des troubles largement diagnostiqués chez les hommes. Le groupe B se rapporte aux "comportements dramatiques, émotionnel, ou erratique" et comporte les TPB (borderline) et TPH (histrionique). Enfin le groupe C est pour les "comportements anxieux et craintifs" dont le TPD (dépendante). Ces deux derniers groupes sont très féminins. Pour l'autrice cela fait suite à la socialisation des jeunes hommes et femmes :

> "Tandis que les schémas de la masculinité hégémonique découragent les hommes d'exprimer leurs émotions, les femmes sont associées à l'émotivité, et ici elles sont pathologisées pour en avoir trop exprimé."

## 2. Concepts théoriques

Les troubles psychiatriques représentent les limites de ce que la société accepte. Une femme ne doit pas être trop dépendante (TPD) mais elle ne doit pas non plus s'adonner à trop de relations non sérieuses (TPH). En somme, elle devrait s'installer dans une relation monogame sans être trop dépendante de son partenaire.

On peut voir les maladies (et *a fortiori* les maladies mentales) comme des constructions sociales. Une fois le diagnostic posé, lea patient·e va s'introduire dans la peau du malade et répondre aux attentes sociales associées à ce statut.

> "Après l'assignation d'un diagnostic par une figure d'autorité médicale, iel est déchargé·e de certaines responsabilités et en vient à être perçu·e comme qqun·e qui a besoin qu'on s'occupe d'ellui. Étant donné que la 'maladie' est essentiellement indésirable, l'individu développe un désir 'd'aller mieux', et se tourne donc vers des technicien·es médicalaux, suivant aveuglément les instructions qu'on lui donne dans l'espoir d'aller mieux."

L'autrice introduit deux concepts foucauldiens. D'une part la généalogie qui consiste à faire l'histoire des faits et pensées "qui ont originairement donné leur sens aux mots et aux concepts que nous tenons maintenant pour acquis". D'autre part la "gouvernementalité" qui désigne le fait que des individus se gouverne ellui-même, qu'iels se rendent dociles sans que l'autorité centrale n'ait besoin d'intervenir.

> "Les techniques 'd'autodiagnostic' par lesquelles des individus peuvent 'découvrir' la source de leur mécontentement dans leur propre comportement, pensée et sentiments 'pathologiques', initiant leur propre participation à la psychiatrie, peuvent être considérées comme un exemple frappant du phénomène de gouvernementalité."

La gouvernementalité va être notamment renforcée par le tournant néo-libéral qui fera la part belle à l'individu.

Il y a aussi cet effet de boucle qu'il est important de prendre en compte :

> "Les TP reflètent  non seulement le jugement 'expert', mais les réactions qu'ils ont suscitées lors de leur utilisation, parmi les individus pathologisé·es, leurs relations, et au sein de la culture et du discours populaire. Les stéréotypes, la stigmatisation, et le rejet social ne sont pas seulement déclenchés par les diagnostics psychiatriques, mais avec le temps leurs effets en viennent à définir les diagnostics, validant et perpétuant un système répressif et un environnement oppressif pour les femmes déviants diagnostiquées comme souffrant d'un TP, et pour tou·tes les êtres humain·es qui sont désigné·es comme mentalement malades par un psychiatre."

## 3. Les troubles : borderline, dépendante, histrionique

Trull et Widiger définissent ainsi les TP : "manifestations pathologiques de traits de personnalité normaux". Ce sont donc les psys qui définissent la normalité. Les TP dits "féminins" sont qualifiés de "inappropriés et intenses" par le DSM.

Le TP le plus diagnostiqué est le TPB, le trouble borderline. Il fut originellement utilisé pour désigner une sorte de schizophrénie légère puis fut redéfini dans les années 70 par Kernberg, psy influent. Alors caractérisé par une

> "construction faible du soi, de l'anxiété menant à l'impulsivité, de l'instabilité émotionelle, de perversions sexuelles, et l'utilisation de mécanismes de défense primaires tels que le clivage du moi - percevant les autres comme entièrement bon ou entièrement mauvais, sans zone grise."

Ses sympotômes étaient donc "féminins" et sa cause également, un "maternage inadéquat".

L'hystérie, renommée TPH, est censée représenter des personnages

> "superficiellement dramatiques, manipulateurices, et en demande d'attention constante tels que Blanche DuBois dans la pièce de Tennessee Williams". (Blagov 2013)

L'hystérie est une maladie qui résulte du fait de ne pas être un homme. À partir du DSM-III, côté colérique et manipulateur est assigné au TPB quand le TPH correspond aux "séduction inappropriée" et au "discours impressionniste". La sexualité et plus spécifiquement la non-satisfaction sexuelle a longtemps été pensée comme une des causes de ce trouble qui

> "pendant un temps \[était traité\] par un médecin en appliquant un objet vibrant dans les parties intimes des femmes."

Le TPD est caractérisé par :

- une passivité dans les relations interpersonnelles
- une volonté de subordonner ses besoins à ceux des autres
- un manque de confiance en soi

Bref ces troubles sont bien plus basés sur des caractéristiques sociales que scientifique. On peut retrouver la même chose chez les hommes avec le TPA (antisocial). Les TPH cherchent de l'attention pour obtenir une connexion émotionnelle, les TPA recherchent des bénéfices matériels et professionels. Les TP pathologisent la "colère" des femmes et "l'agressivité" des hommes.

> "Pour être une femme adulte 'normale', il faut être satisfaite de sa position subordonnée, ou au moins sembler l'être."

## 4. L'histoire de l'oppression des femmes : sorcellerie, hystérie, TP

TW : viol

À chaque époque, des hommes "experts" appartenant à des institutions ont statué sur les comportements qui étaient acceptables de la part des femmes, de la sorcellerie aux TP. Au cours du temps ce sont les institutions qui ont changé et les termes utilisés, pas vraiment le rapport de domination engendré.

Les femmes du Moyen-Age dont la sexualité était "étrange" (avoir trop de relations ou aucune) pouvait être considérer comme sorcière, ainsi que "les femmes guérisseuses" qui empiétaient sur le champ naissant et masculin de la médecine.
Le DSM de l'époque est le *Malleus Maleficarum* qui "nommait les signes montrant qu'une femme était probablement une sorcière". À la place du psychiatre, le piqueur. Celui-ci pouvait tester des femmes en piquant le bout de leur doigt et mesurant la quantité de sang perdue. Il avait ensuite la possibilité d'enfermer les femmes qui ne passaient pas le test. Les femmes étaient ensuite guéries (exsorcisées) ou exécutées.

> "Foucault soutient que le concept d'hystérie a émergé au début des années 1800 comme un nouveau terme utilisé par les hommes pour décrire des comportements féminins 'difficiles' en l'absence de 'sorcellerie' et correspondant au changement sociétal passant de la croyance à la religion à la croyance en la science."

À mi-chemin en croyance et science, la "folie morale" est née pour expliquer de manière scientifique (biologie) l'immoralité. Et c'est bien pratique car cela veut dire que l'on explique les vices et les crimes par des problèmes médicaux, individuels et non sociaux. La psychiatrie naissante offre donc pour horizon de supprimer ces tares de manière scientifique, médicale.

Le terme hystérie vient étymologiquement de mot *utérus*. Utilisé à partir du XVIIè siècle, il apparait dans le domaine médical à la fin du XIXè. Si la sorcellerie se focalisait principalement sur les femmes défiant les normes de genre, l'hystérie concernait les douleurs, et faiblesses féminines :

> "Le cliché qui veut que n'importe quel inconfort féminin puisse être expliqué par l'hystérie n'est pas une exagération."

La sexualité des femmes a donc surgit sur le devant de la scène. Avant, personn n'en parlait. À partir du XIXè, on en parle d'un point de vue médical. Le manque de désir devient une maladie. On efface donc un tabou pour le remplacer pour un discours qui contrôle. Au contraire, le désir sexuel "immoral" est associé à l'hystérie.

> "Les femmes victimes d'agressions ou de viols qui étaient traumatisées étaient envoyées à l'asile"

Notons que toujours aujourd'hui, les victimes de VSS sont plus diagnostiquées TPB. Le TPB inclue également tout ce qui est lié à la rage féminine :

> "Lorsque les hommes crient ils 's'affirment', parfois ils peuvent être 'méchants' ; lorsque les femmes crient elles sont 'folles'."

## 5. Pop culture : propager la légitimité

Ces nouveaux troubles se voient renforcer dans leur légitimité par de nombreuses apparitions dans des films et séries. On voit par exemple une femme typiquement borderline dans *Liaison fatale* (Lyne 1987) et **spoiler alerte** la seule façon de se sortir de cette situation est de la tuer.

## 6. Femmes dangereuses dans la société néolibérale: le pouvoir psychiatrique et le *rétrécissement du spectre de la normalité*

La normalité est un concept relativement récent (XIXè siècle) qui pu se développer grace aux techniques statistiques et la définition d'un "homme moyen".

> "Tandis que la psychiatrie se préoccupait à l'origine de l'hygiène publique et de la protection de la société contre la maladie, après l'émergence de l'individu 'anormal·e', qui coincidait avec la vaste extension de techniques et technologies disciplinaires, elle revendiqua rapidement le domaine de l'anormalité."

Les anormal·es étant celleux n'étant pas capables de se conformer aux normes institutionnelles (école, prison, armée, ...)

L'emprise de la psychiatrie sur la société semble de plus en plus importante, et l'on observe des cas extrêmes de gouvernementalité avec par exemple le développement personnel ou les tests de personnalités et à cette mode de l'auto-diagnostic.

## 7. Impasse conceptuelle : la question de l'abus.

La psychiatrie individualise et rend impossible la remise en question d'un système problématique

> "En assignant le mécontentement au domaine de l'anormal, et en localisant sa source dans une pathologie individuelle, son potentiel à inspirer l'action est écrasé."

De plus, les problèmes "personnels" sont couverts de honte ce qui rend plus difficile d'en parler.

Pour les femmes, la question de l'image de soi est très complexe. Elles sont bombardées depuis leur tendre enfance de pub objectifiante et on leur inculque des normes strictes.

> "Ces TP peuvent donc constituer une réponse à deux différentes formes d'abus : 1) l'expérience universelle d'abus que les femmes subissent en vivant dans la société actuelle, ou 2) les cas individuels d'abus physiques, sexuels, émotionnels, ou autres."

Les statistiques prouvent assez facilement la corrélation entre le diagnostic d'un TP (particulièrement TPB) et le fait d'avoir été victime d'abus.

> "Il est cruellement ironique que l'expression de colère - comme cell de type soi-disant 'intense' - des femmes qui ont été victimes d'abus sexuels soit utilisée comme preuve d'une pathologie individuelle, plutôt que d'une réponse 'saine' ou 'normale' au grave traumatisme associé à cette violation des plus intimes."

## Conclusion

La société actuelle tend trop facilement à croire que les hommes et les femmes sont égauxles. À tel point que les mécanismes de régulation et de contrôle présent dans l'administration psychiatrique ne sont pas identifiés comme tels.
La psychiatrie poursuit la longue tendance au contrôle des femmes et de leurs comportements, contrôle précédemment effectué à travers la 'moralité', la religion, la chasse aux soricières...

Les industries capitalisent sur la dépression en vendant des pillules ou en promouvant le développement personnel. Ce côté néolibéral individualisant fonctionne si bien que les individu·es semblent se contrôler elleux-mêmes et ne sont plus dans la lutte collective.
