# [L'émergence du concept de "santé mentale" dans les années 1940-1960 : genèse d'une psycho-politique](https://shs.cairn.info/revue-pratique-en-sante-mentale-2015-1-page-3?lang=fr) (Claude-Olivier Doron)

> "À un moment historique bien précis (les années 1940-1960), la santé mentale devient un champ général, à partir duquel s'organisent des savoirs, des pratiques et des institutions."

Auparavant, les référentiels étaient plutôt l'aliénation ou la maladie. On parlait d'ailleurs plutôt d'hygiène mentale initialement, avant de parler de santé mentale. De manière parallèle à l'hygiène (prophylaxie bactériologique), on va chercher à détecter les anomalies et troubles mentaux le plus tôt possible, dès l'enfance. Avec la santé mentale, on assiste à une institutionalisation des pratiques.

> "le cas anglais de la formation de la National Association for Mental Health (NAHM) en 1946 est exemplaire. La NAHM est née sous l'impulsion d'un rapport établi en 1939 par le parlementaire anglais Lord Feversham, qui constatait : 1) l'augmentation des 'névroses' et des 'inadaptations' à la vie moderne, nécessitant l'organisation d'un système national de protection de la santé mentale ; 2) le coût économique de ces problèmes pour la communauté, tant du côté de la dépense étatique que de la perte de productivité des citoyens ; 3) le manque total de coordination des différentes associations jouant un rôle dans ce domaine."

La santé mentale est donc définie de manière assez vague et est intimement liée à l'action publique.

À cette période, il est également impossible d'oublier le rôle de la guerre, qui, d'une part, va avoir un impact très important sur la "santé mentale" des populations, et d'autre part va donner de nouveaux rôles à la psychiatrie, comme dans l'armée et le recrutement.

> "Une des branches de la santé mentale qui se développera durant cette période sera l'intervention des psychiatres dans les entreprises pour gérer les conflits de travail et améliorer la productivité en travaillant sur les relations humaines."

L'idée d'un positionnement idéologique est également à situer dans un contexte de guerre froide ou les "démocraties" occidentales vont chercher à se démarquer du totalitarisme.  Donc il faut valoriser "*dans une certaine mesure* la non-conformité individuelle". Cela ne signifie pas accepter les révolutions, mais qu'il faut "promouvoir une adaptation harmonieusede l'individu social en évitant les conflits".

> "L'une des fonctions essentielles de la santé mentale est de convertir de potentiels conflits sociaux, que ce soit au niveau de l'industrie ou dans les pays en voie de développement, en conflits 'intrapsychiques' individuels, ou interrelationnels entre individus, qu'il faut 'résoudre de manière harmonieuse'."

Il n'y a pas de consensus sur la définition de la santé mentale. Des définitions de l'OMS des années 1950 parle d'harmonie, de bonheur, de relations constructives. Malgré cela, la volonté politique de promouvoir la santé mentale n'a jamais cessé.

La santé mentale a pour sujet l'individu. Plus précisément, on considère un individu en développement qui va passer par différentes phase (enfance, adolescence etc) et forger sa personnalité. Les facteurs externes sont pris pour acquis, il ne s'agit pas de la modifier mais de les anticiper et de favoriser l'adaptation. Le seul milieu sur lequel l'on agit ce sont les groupes d'individus, au travers de "relations interindividuelles et de liens affectifs".

> "nous avons affaire à un biopouvoir au sens plein du terme : il s'agit de redoubler, par des dispositifs de pouvoir qui vont du conseil, de l'orientation, à l'enfermement, *toute la vie des sujets*, c'est-à-dire toutes les phases de leur développement. \[...\] il s'agit aussi d'entrer dans tout un ensemble de milieux humains : famille, travail, etc. pour améliorer les relations humaines."

> "Il s'agit, en quelque sorte, de repérer tout un ensemble de facteurs qui sont susceptibles d'influer sur le développement des individus puis non pas de modifier ces facteurs mais d'aider, de préparer l'individu à les affronter et/ou, le cas échéant, de prendre en charge au plus tôt leurs répercussions négatives. \[...\] En fait, c'est une sorte de psychanalyse générale des phases du développement et des rapports entre sujets qui est recommandée."

Si ces facteurs peuvent être l'adolescence, la maternité, la ménopause etc ils peuvent aussi bien être des changemenents économiques, l'industrialisation ou autre. Ces facteurs ne sont jamais interrogés ou remis en question, l'important est d'étudier les répercussions sur les individus, sur leurs relations affectives. Tous les secteurs (école, maison, hôpital, usine, justice, ...) se posent donc une question :

> "comment une relation, une manière de se rapporter à l'autre, peut effectivement influer sur son développement affectif et contribuer à sa santé mentale"

Dans le monde du travail, on parle relations humaines, souffrance au travail, on psychologise les rapports de force, on pacifie les conflits sociaux. On veut humaniser les soins à l'hôpital. Les soins doivent d'ailleurs pouvoir s'adapter, être présent à différents niveaux, et ne doivent pas couper l'individu de la communauté.

> "l'hôpital psychiatrique doit être considéré comme un centre de traitement transitoire. Sa tache est de réapprendre au malade, dans l'atmosphère de la communauté thérapeutique, à subir les tensions normales qui caractérisent la collectivité ordinaire."
