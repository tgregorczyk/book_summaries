# L'amazone et la cuisinière, Alain Testart

## Chapitre 1 : D'étonnantes constantes

D'abord un rappel, les femmes ont toujours travaillé, et si l'industrialisation les a sorties de la sphère domestique, il en fut de même pour les hommes.

Des études ont été faites sur la répartition des travaux des hommes et des femmes dans de nombreuses sociétés. Il en ressort que :

- au niveau de la fabrication des objets, les matières premières dures (bois, acier, ...) sont l'apanage des hommes et inversement pour les matériaux plus mous (tissu, fibre, ...)
- les femmes sont exclues des travaux liés à la mer, notamment le métier de marin
- la chasse est gardée par les hommes pour faire un jeu de mots un peu nul

Ces constantes étonnent, et, même s'il semble que ces différences vont s'atténuer, elles restent très présentes aujourd'hui.

## Chapitre 2 : Faillite des explications naturalites

Certains ont tenté d'expliquer pourquoi les femmes allaient à la cueillette et les hommes à la chasse. Une explication possible est que les femmes ne sont pas assez mobiles car elles s'occupent des enfants. Pour Testart, cette explication est naturaliste (elle explique un fait social par la physiologie) et rationaliste (elle justifie ce choix).
En réalité on a des exemples de femmes qui chassent. Par exemple, elles peuvent chasser de petits animaux ou servir de rabatteuses dans des chasses collectives. Dans ce dernier cas, elles forment le groupe le plus mobile de la chasse. On a d'ailleurs aussi des exemples de chasses ou il faut rester immobile (par exemple pour attendre qu'un phoque vienne respirer dans un trou), chasse également masculine.
De plus les femmes n'ont besoin de rester avec les enfants que pendant des périodes bien précises, elles pourraient chasser à d'autres moments.

Il semble que le principale différence soit liée à l'utilisation des armes, lesquelles étaient reservées aux hommes.

## Chapitre 3 : Du sang des animaux et du sang des femmes

Il semble que l'interdiction d'utiliser les armes est ce qui exclut les femmes de la chasse (comment chasser sans armes?). Pourtant les femmes peuvent utiliser des gourdins ou des filets.

> "Les armes que n'utilisent pas les femmes sont celles qui font couler le sang des animaux."

De nombreux tabous entourent le sang des femmes, notamment lors de périodes menstruelles. Comme si on ne pouvait pas cumuler deux sangs. Les femmes réglées ne doivent avoir aucun rapport avec la chasse sanglante (y compris toucher les armes ou avoir des rapports avec leurs maris chasseurs).

La seule "exception" c'est Artémis / Diane, déesse de la chasse, mais elle est vierge et semble ne jamais devoir saigner.

## Chapitre 4 : Équarissage, tanage, etc

Il semble que ce qui pose problème réellement est le jaillissement du sang, les femmes peuvent être en contact avec le sang animal. Elles interviennent plus facilement, moins il y a de sang. C'est-à-dire que si les femmes participent au dépeçage, alors elles participeront au travail des peaux. A d'autres endroits, elles travailleront uniquement les peaux. Ou parfois pas du tout. Tout cela dépend de la répartition des charges de travail. Vers l'Arctique, les femmes ont peu à faire avec les végétaux et la cueillette en général, il faut donc qu'elles participent avec le produit de la chasse. Si il y a beaucoup à faire avec la cueillette, alors elles peuvent ne jamais être au contact du sang animal.

## Chapitre 5 : Une exclusion universelle

On précise bien que les conclusions précédentes sont universelles, pas seulement chez les chasseurs-cueilleurs.

## Chapitre 6 : Le sang du Christ, et autour

Les femmes sont par la même raison écartées de pas mal de religion car elles ne peuvent pas faire de sacrifices. Dans la religion catholique, lors de chaque messe, le vin se transforme en sang du christ, ce qui explique que les femmes ne puissent être prêtres. Le contre-exemple vient des églises protestantes ... qui rejettent la transsubstantation cad la transformation du vin en sang.

L'Église elle-même dit qu'elle a horreur du sang. Elle interdit à ses membres la chasse, la guerre. Donc de manière plus générale, c'est le cumul de sang qui posent prolème, ceux-ci pouvant être :

- le sang des femmes
- le sang du Christ
- le sang de la guerre
- le sang des animaux

Il existe toujours une gradation. Les femmes ne sont en général exclut des temples que pendant leur période de règles. Ce qui semble toujours interdit c'est le cumul des sangs.

## Chapitre 7 : Façons féminines, façons masculines de mettre fin à ses jours

Les femmes utilisent des techniques de suicide qui n'ouvrent pas de plaies dans leurs corps : pendaison, empoisement, saut, noyade. Les hommes se suicident plus souvent par armes à feu ou de manière sanglante.
Il était d'ailleurs considéré commme dégradant d'être pendu.

## Chapitre 8 : La mayonnaise, le sel et le vin

Des croyances racontent que les femmes (notamment pendant la menstruation) ratent la mayonnaise, ou peuvent faire tourner le vin, ou tout ce qui est conservé dans le sel. Testart nous indique que c'est par ce qu'on peut relier ces éléments au sang.
Pour le vin c'est évident, c'est le sang de la vigne, le sang du Christ.
Pour le sel, il est très lié au sang dans beaucoup de religion aussi.
Enfin pour la mayonnaise, elle est faite avec des oeufs, et les oeufs sont très lié au Christ (et à sa résurrection à Paques) et à la couleur rouge.

## Chapitre 9 : Le fer et le feu

> "pour les forgerons primitifs \[...\] métallurgie et sidérurgie se confondent."

De part sa forme, les fours sont associés aux femmes. Il y a une bouche (gueulard), ou l'on enfourne le minerai, un ventre ou il chauffe et le creuset par lequel le résultat sort. En Afrique, le travail de fonderie était explicitement mis en parallèle avec un coit. Dans ce cas, que signifie le métal rouge en fusion ? C'est soit un placenta, soit les règles.

> "quand une tache technique évoque trop fortement le corps de la femme dans ses dérèglements, les femmes en sont exclues."

Efficacement, tous les forgerons étaient des hommes, les femmes faisant la poterie.

## Chapitre 10 : La mer

Alors quel lien entre la mer et une femme ?
La déesse de la mer est souvent une femme, l'eau salée est liée à l'infertilité comme la femme qui a ses règles, les tempêtes sont une forte perturbation interne des océans, comme les règles d'une femme.
De la même manière, les prêtres sont de mauvais signes pour les marins ainsi que les lapins (qui symbolisent une acitivité sexuelle débordante)

## Chapitre 11 : Creuser

Les femmes ont souvent été éloignées de la terre également. Plus précisément des mines, des carrières etc. En fait on associe la terre à une terre-mère, fertile, que l'on ne souhaite pas lier aux femmes.

## Chapitre 12 : Couper

De la même manière, il existe des tabous sur les femmes qui coupent des choses. Par exemple, chez les Berbères, les femmes font le tissage sauf une action qui consiste à couper un fil. Elles demandent alors l'aide des hommes en disant "viens égorger mon mouton", pour couper ce fil.

Il semble que, plus qu'un problème avec le sang comme substance, les tabous qui entourent les femmes tiennent plus à tout ce qui à attrait rendre visible une intériorité, comme le sang qui jallit. On peut don établir la règle suivante :

> "la femme ne peut perturber de façon soudaine les corps en leurs intérieurs parce qu'elle est sujette à de telles perturbation en son intérieur"

## Chapitre 13 : Moudre, pilonner, écraser : le geste technique féminin

On observe une différence entre l'utilisation des outils par les femmes et par les hommes. On parle de percution posée (raclage, le fait de moudre, ...) contre une percution lancée (avec des outils comme des javelots, haches, harpons, ...). Les femmes ont plutôt tendant à écraser, à avoir un effet diffus, répétitif, surfacique, les hommes à transpercer, à avoir un effet précis, net, linéaire, brusque.

> "L'homme ne fait la chasse que pour autant qu'il utilise des armes de jet coupantes et perforantes. La femme participe aux autres formes de chasse."

On modifie à nouveau la conclusion générale :

> "La femme, étant sujette à de graves perturbations qui l'affectent en l'intérieur de son corps, elle évitera de produire de telles perturbations dans l'intérieur des corps qu'elle travaille."

## Chapitre 14 : Le laboureur et la semeuse

Le travail de la charrue est le domaine des hommes. C'est logique car la charrue ouvre la terre fertile avec un objet tranchant.
De manière générale, les hommes s'occupent plus des animaux et les femmes des plantes.

## Chapitre 15 : Moissoner, variations sur le thème de la coupure

Différence entre faux et faucille. La faux nécéssite un grand geste tournoyant. Pour la faucille, une faut d'abord réunir une poignée, puis l'instrument vient couper au contact.

- "percussion linéaire lancée = domaine masculin"
- "percussion linéaire posée = domaine féminin"

> "Les hommes tranchent, les femmes rassemblent. Et c'est la même opposition que chez les chasseurs-cueilleurs."

## Chapitre 18 : Quand l'agriculture est aux mains des femmes

On observe que les femmes font parfois la majeure partie des travaux d'agriculture, parfois le travail est relativement équilibré, parfois les hommes font tout. Comment l'expliquer ?
On observe aussi que plus l'agriculture tend à être complexe et plus il semble qu'elle soit l'apanache des hommes. Pour certains cela conforte l'idée que les femmes ont inventé l'agriculture.

> "L'agriculture est entièrement aux mains des femmes là ou les hommes sont entièrement voués à l'art martial."

Comme par exemple chez les Iroquois.

## Chapitre 19 : Pourquoi le potier supplanta la potière

Aux endroits ou l'on connait la roue, on connait le tour de potier, et la potière devient potier.
Il semble que la mécanisation vienne avec l'apparition de métier, on sort du cadre domestique. Et les hommes, chefs de famille, s'approprient les métiers.

## Chapitre 20 : Le puzzle du tissage

> "partout ou l'émergence d'un pouvoir économique, même limité, d'une classe d'artisans est envisageable, les outils et le travail ont été accaparés par les homme ; partout ou l'émergence d'un tel pouvoir est impensable, outils et travaux ont été laissés aux mains des femmes. On voit combien on est loin de la thèse du sous-équipement féminin car ce n'est pas de technique qu'il s'agit, mais bien de pouvoir."

## Chapitre 21 : Pourquoi ?

Testart tente un résumé ;

> "Pendant des millénaires, et probablement depuis la préhistoire, la division sexuelle du travail provient de ce que la femme a été écartée des taches qui évoquaient trop la blessure secrète et inquiétante qu'elle porte en elle."

Il semble que ces interdits, qui ne concernent que les femmes, viennent de loin dans la préhistoire.

On peut essayer d'expliquer ces faits par la croyance qu'il faut "éviter la conjonction du même avec le même." L'exemple classique concerne le tabou de l'inceste. La rapport intime entre personnes du même sang risquerait de provoquer des catastrophes.

On tente un résumé, un peu plus spéculatif :

> "La mise en présence de deux êtres pareillement affectés par le sang risque de déclencher des catastrophes."

> "ce sont des croyances \[...\] qui expliquent le plus gros de la division sexuelle du travail depuis les origines à nos jours. Que par ailleurs elles aient contribué à maintenir les femmes dans une position subordonnée, c'est une évidence."
