# Les chasseurs-cueilleurs ou L'origine des inégalités, Alain Testart

La préface semble être un bon résumé du livre. La thèse principale de celui-ci est que la séparation chasseurs-cueilleurs / agriculteurs ne peut expliquer l'émergence d'inégalités. Il faut plutôt aller voir du côté du stockage.

## Introduction

Être chasseur-cueilleur c'est vivre de ressources sauvages, non domestiquées. Notons que la domestication est un concept qui peut être flou et que certains peuples ne sont pas forcément 100% chasseurs-cueilleurs, tout comme les agriculteurs peuvent continuer d'aller chercher des baies dans la forêt. D'ailleurs la domestication peut être connue pour des fins non-alimentaires, on pense par exemple au chien.

Historiquement les chasseurs-cueilleurs ont été le sujet d'étude des matérialistes (qui partent de la base matérielle ie subsistance, production écologie pour expliquer une société) et les évolutionnistes qui essayent d'expliquer que les sauvages sont les chasseurs-cueilleurs, les barbares les agriculteurs (ou bien les potiers car la poterie était souvent associée à l'agriculture).

C'est Childe qui va parler de révolution néolithique, ie que l'adoption de l'agriculture va complètement changer le mode de vie des sociétés humaines (fini le nomadisme, la faible densité démographique, égalitarisme, etc)

## Chapitre 1 : Des sociétés réputées exceptionnelles

De nombreux peuples sont être des exceptions parmi les chasseurs-cueilleurs. Et quand il y a beaucoup d'exceptions, peut-être faut-il commencer à remettre en question la théorie sous-jacente.

### La côte nord-ouest américaine

> "L'existence de villages permanents, d'une très forte densité démographique, d'inégalités sociales systématisées en termes de rangs, de l'esclavage, de sociétés secrètes et d'une certaine division du travail autre que sexuelle suffit à les distinguer des autres chasseurs-cueilleurs"

Ils vivaient essentiellement de la pêche saumon, particulièrement abondant. Et c'est cette abondance que certains anthropologues ont utilisé pour justifier cette société de chasseurs-cueilleurs hors normes. Ils sont parfois comparés à des agriculteurs qui récoltent le poisson et le stocke.

### Le sud-est sibérien

Sud-est de la Sibérie, "du bas Amour au Kamtchatka", mais aussi aux rives du Baikal. Ces peuples sont chasseurs-cueilleurs sédentaires, et connaissent des inégalités très marquées.

### La Californie

De la même manière, les peuples californiens ne rentrent pas dans les cases de la théorie des chasseurs-cueilleurs. Leurs villages avec des greniers et leur mode de vie ressemble à ceux de peuples agriculteurs ... mais sans l'agriculture.

### Le Delta de l'Orénoque

Les Warrau ont une stratification sociale basée sur la religion qui rappelle des populations andines.

### Les premiers villages

Les premiers villages du natoufiens (Syrie, Palestine) ne présente aucun cas d'agriculture. D'ailleurs, on peut aussi dire que souvent, l'agriculture précède au contraire la sédentarité.

### La première poterie

Il en va de même avec la poterie. On l'avait souvent associée à une conséquence de l'agriculture mais en réalité les plus vieilles poteries du monde ont été retrouvées au Japon, datant de 10 000 av JC, bien avant toute forme d'agriculture.

## Chapitre 2 : Un système techno-économique fondé sur le stockage alimentaire

### La sédentarité

Le nomadisme est nécessaire si les ressources alimentaires dont dépendent la survis ne sont pas présentes à un endroit précis tout au long de l'année. Mais si le stockage existe, cela change tout, en effet les agriculteurs aussi font face à des récoltes périodiques.

> "la sédentarité est possible lorsqu'une société, exploitant des ressources alimentaires saisonnières présentes en abondance suffisante pour constituer la nourriture de base, les récolte en masse et les stocke sur une large échelle."

Notons que cette définition ne présume pas du caractère sauvage ou domestiqué des ressources. Par contre la sédentarité implique une certaine technicité, il faut pouvoir récolter en masse, éventuellement produire en masse, et stocker.
On pourrait étendre ces questions aux ressources non-alimentaires mais celles-ci sont en général moins limitantes.

D'autres facteurs peuvent impliquer la sédentarité. Par exemple si les ressources naturelles sont abondantes tout au long de l'année. Cela peut être faciliter par des moyens de transports rapides qui permettent d'aller chercher la nourriture dans un rayon plus important, ou part des échanges avec d'autres populations.
Les conditions naturelles peuvent aussi amener à la sédentarité, pensons aux régions arctiques et aux maisons qui sont construites pour protéger du froid.

La sédentarité peut bien sur être relative, saisonnière, ...

### La densité démographique

Une vieille idée est que la densité démographique des chasseurs-cueilleurs est faible. Or ce n'est pas toujours le cas.

Les femmes chez les chasseurs-cueilleurs nomades ont une double tache: la cueillette et l'éducation des enfants (qu'il faut transporter sur son dos ...). Il est donc plus facile d'avoir plus d'enfants chez une population sédentaire. De plus, le stockage permet de compenser la loi de Leibig (qui dit que le facteur limitant de la taille d'une population n'est pas la quantité de ressources disponibles mais cette quantité en période de pénurie).

L'accroissement des densités de population peut donc d'expliquer par la sédentarité plutôt que par l'agriculture.

### Les inégalité socio-économiques

Encore une fois, les vieilles idées sont que l'agriculture mène aux inégalités dans la société car les chasseurs-cueilleurs n'ont pas de quoi produire un surplus. C'est évidemment faux, voir les théories de Sahlins sur l'abondance.

On peut déjà dire que les populations nomades ont en général peu de possessions matérielles car c'est de la masse à transporter. Il faut donc se tourner vers les peuples sédentaires pour voir des possessions matérielles, que ce soit alimentaires ou autres.

Pour Testart, un des moyens d'acquérir du prestige est de faire des dons alimentaires. Chez les nomades, le chasseur revient au camp et offre la nourriture à tout le monde et peut en retirer du prestige. Chez les sédentaires cela peut être accentuer la des dons plus massifs liés au stockage.

Cela a évidemment de grands impacts culturels. Celui qui stocke fait moins appel à la générosité collective, il met plus l'accent sur le travail humain de conservation que sur la générosité de la nature. Chez de nombreux peuples nomades, stocker est un acte impie qui renie ce qui fait la société. Il existe bien sur des stocks communautaires, ce sont les stocks individuels qui tendent à créer des inégalités.

> "La sédentarité signifie aussi l'exploitation exclusive ou privilégiée du territoire ou le groupe s'est fixé. Les différences régionales dans l'abondance des ressources et l'adoption d'une structure sociale plus rigide conduisent à des pression démographiques trop grandes pour certains groupes, entrainent des différences de richesse d'un groupe à l'autre. La sédentarisation limite aussi laa possibilité de résolution des conflits par fission du groupe, d'ou la nécessité de médiateurs, occasion pour les anciens leaders de consolider leur position."

Le stockage des denrées alimentaires éloignent le consommateur du producteur, éloignement qui peut être renforcé en cas de vol (raids) ou échange.

> "l'extorsion d'un surproduit peut être réalisé à une tout autre échelle que chez les non-stockeurs."

Notons qu'en général, les sédentaires auront tendance un produire un peu plus que nécessaire pour pallier aux imprévus.

### Autres aspects

Il existe une corrélation statistique entre la sédentarité et la poterie. Les poteries sont en effet fragiles et peu adaptées à un mode de vie nomade. Cependant ce n'est pas une règle absolue, il existe des chasseurs-cueilleurs utilisant la poterie, des sédentaires ne connaissant pas la poterie. Quoiqu'il en soit, un lien existe plus entre la poterie et la sédentarité qu'entre la poterie et l'agriculture.

De même la division du travail est plus probable chez les sédentaires. En effet chez un peuple nomade (de chasseurs-cueilleurs) la focale est mise sur la production de nourriture, il y a donc peu de place pour d'autres productions (mais aussi peu de place pour des échanges intérieurs qui bénéficierait d'inégalités, ou du moins de personnes riches). La seule division du travail est donc en général la division genrée.

En ce qui concerne la guerre, on ne peut pas dire en général que les chasseurs-cueilleurs nomades soient non-belliqueux. En revanche certaines situations que l'on retrouve plus facilement chez les sédentaires (dont les inégalités voire la hiérarchie) peuvent favoriser des attitudes guerrières.

Enfin les fêtes et les variations saisonnières sont plutôt l'apanage des sociétés à stockage car il faut pouvoir accumuler pour pouvoir faire une fête bien évidemment. De plus, ce genre de fête peut faire office de grande redistribution et d'accroissement du prestige des chefs

## Chapitre 3 : Les sociétés de chasseurs-cueilleurs sédentarité stockeurs, exemples typiques

### La côte nord-ouest de l'Amérique du nord

Alaska - nord-ouest de la Californie

Les ressources halieutiques sont très importantes notamment les saumons. Mais les saumons viennent effectuer leur migration, les ressources sont dons limitées dans le temps. La question est donc plus de savoir comment stocker la nourriture, ce qui est en l'occurence possible grace aux techniques de séchage et de fumage, qui sont relativement complexe. Tout au nord, on peut observer également de la congélation d'aliments.

Le séchage est en général effectué à l'automne, le soleil étant trop direct l'été. Dans certaines régions trop humides on sèche avec l'aide d'un feu.

Dans cette aire, les processus ce conservation sont au moins voire plus long que la pêche elle-même. La conservation est un travail féminin, la pêche est masculine.

En fonction des régions, la pêche peut être diverse : truite, hareng, flétan, morue, baleine.

Les maxima d'activité sont au printemps et en automne, l'hiver est une saison de loisir. L'été les activités sont réduites, c'est un peu une période mixte.

En général, le poisson maigre se prête mieux à la dessiccation. Le poisson gras, bien que préféré, est plus difficile à conserver. On extrait l'huile des poissons gras et on la stocke. On sèche aussi des algues, des mollusques, des oeufs de poisson et on garde baies et racines dans l'huile.

Outre en Californie, des systèmes de rangs sont très élaborés et importants. Les nobles ont de nombreux privilèges comme ne pas effectuer le travail manuel pénible. Ils méprisent les gens communs.
Le rang est attribué par la naissance mais ne peut être conservé qu'en prouvant sa richesse lors des potlach. Les potlach sont des fêtes dans lesquelles une grande quantité de biens sont donnés ou détruits. Ces biens sont en partie alimentaire, les potlach étaient en partie une grande redistribution de nourriture (dont l'huile chez les Kwakiutl), mais aussi des biens manufacturés comme des plaques de cuivres, ou, plus tard avec le contact avec les européens, les couverture de la Compagnie de la baie d'Hudson, des machines à coudre, des canots à moteur ... Enfin les biens englobent également les esclaves.

Les chefs sont riches de la richesse de ceux qui travaillent pour eux, de cette main-d'oeuvre :

> "Le chef Nootka est propriétaire du sol et en particulier des rivières à saumons ; le produit de la première pèche lui revient de droit, et, même plus tard, lorsque le poisson a été péché et séché, le chef peut envoyer ses hommes percevoir le 'tribut' pour lui"

> "L'exploitation reste limitée en vertu d'une certaine fluidité de l'organisation sociale : les mécontents peuvent toujours aller s'installer ailleurs, s'affilier à d'autres maisonnées et donc à d'autres chefs \[...\] seule l'ossature du système, consitutée de chefferies patrilinéaires liées à des droits exclusifs sur la terre, est rigide, tandis que les gens du commun peuvent choisir leur affiliation à l'une ou l'autre de ces chefferies"

Le prestige du chef rejaillit sur le groupe, et les redistributions rendent cette exploitation semble-t-il supportable. De plus, travailler pour un chef tend à renforcer les liens de parenté avec cette famille.

Les chefs peuvent avoir 10-20 esclaves chez les Chinook, ceux-ci peuvent être parfois mis à mort lors des potlach.

Dans le sud (vers ke nord-ouest Californie), "l'accent est mis sur la richesse." Ces peuples sont obsédés par la richesse, antinomique des femmes. Il existe une monnaie de coquillage, et tout à un prix. On peut même évaluer la valeur d'homme en fonction notamment du prix de sa fiancée. Le potlach n'existe pas.

> "La richesse est accumulée comme une fin en soi. Les dons sont rares et sont toujours faits sur une petite échelle."

La société est beaucoup plus individualiste, l'exploitation d'hommes n'existent (mais le travail des femmes de la famille est exploité). L'esclavage pour dette existe.

Une certaine division du travail existe, certains sont spécialisés dans la pêche, d'autres dans la fabrication de canots par exemple. La guerre a pour but le pillage des biens, la capture d'esclaves et l'annexion de territoires. Dans cette aire, on fabrique des armures, on érige des palissades.

> "L'ooposition hiver / été avec sa connotation sacré / profane est clairement marquée chez les Kwakiutl. Changement des noms portés par les individus d'une saison à l'autre ; changement complet de la structure sociale organisée, en été, en fonction des groupes de parenté et, en hiver, des sociétés secrètes ; présence des esprits pendant la seule saison d'hiver, etc."

### Le sud-est sibérien

"sur le bas Amour et au Kamtchatka", ainsi que la mer d'Okhotsk, l'ile de Sakhaline et le nord du Japon (Hokkaido), voir carte et tableau p128-129

Les points communs de l'aire sont la sédentarité, la pêche et l'absence d'élevage de rennes.

> "L'absence de domestication du renne a pour corrélat un élevage important des chiens à traineau : une maisonnée chez lez Nivkh dispose de 30 à 40 chiens en moyenne. Le chien sert aussi à l'alimentation, à la confection de vêtements ; il est un objet de sacrifice et les courses de chiens fint partie des fêtes de l'ours."

Les peuples de cette aire pratiquent de plus en plus la chasse au fur et à mesure que l'on s'éloigne des côtes.

Sur l'ile d'Hokkaido, les pratiques sont un peu différentes. On pratique un peu d'agriculture et moins la cueillette. Il n'y a pas d'habitats d'été et d'hiver et moins de constructions sous-terraines.

Cet aire est relativement comparable à la région américaine voisine. Quelques différences comprennent le découpement géographique de la côte, les courants (chauds du côté américain, froids du côté asiatique), et la présence de traineau à chiens et de skis en Asie et non en Amérique.
Une autre différence fondamentale est que les peuples asiatiques sont soumis au désagréable et dangereux voisinage d'États puissants.

> "La fin du XIXe siècle est contemporaine de l'exacerbation de la rivalité des colonialismes russe et japonais dans l'ile de Sakhaline : colonisation de peuplement, installation d'une colonie pénitentière, massacres et transferts ed populations. \[...\] En 1869 \[...\] l'ile \[d'Hokkaido\] est proclamée territoire japonais et ceci marque le début de l'afflux de colons japonais ; en 1883 le gouvernement japonais décide un programme pour introduire l'agriculture chez les Ainou."

Ce voisinage fait émerger des charactéristiques particulières pour des chasseurs-cueilleurs, comme la métallurgie du fer, le tissage et la présence de nombreux échanges marchands.

Les ressources en poisson et notamment en saumon sont très importantes même si elles sont parfois limitées dans le temps. Le *youkola*, poisson séché et légèrement fumé et la nourriture de base.

En retrouve généralement des villages d'été et d'hiver, les premiers souvent construits sur pilotis, les seconds sont semi-souterrains.

La richesse est une quête importante. Notons que le commerce avec d'autres peuples et États permet d'acquérir des objets précieux qui permettent de "convertir tout surplus alimentaire en richesse durable dont la thésaurisation est une fin en soi." Cela a aussi pour effet de modifier certains modes de vie, on va par exemple préférer la chasse d'un animal pour revendre ensuite sa fourrure.

Chez les Nivkh et les Ainou, la richesse a un aspect éminemment religieux car il permet de faire des offrandes. En même temps, celui qui est riche est le chasseur habile qui a su transformer le surplus en richesse. Et cet habile a besoin de chance ou autrement dit d'aide des dieux. Il est donc riche car il est bien vu des dieux.
Les systèmes de rang semblent en tout cas moins strict qu'en Amérique. Ils s'expriment notamment lors des fêtes des ours par de grandes redistributions de nourriture et par la démonstration de la richesse des individus (les riches élevaient des ours, ce qui coutait pas mal d'argent).

> "Les inégalités supposent une certaine appropriation privée des ressources. Chez les Oultches, la chair des animaux est seule objet du partage, ce qui signifie que les peaux sont appropriées individuellement. Chez les Nivkh la terre en général est propriété du clan, mais certains sites de pêche, des cours d'eau à zibelines ou des trous à renard semblent avoir été assignés à des individus."

Notons que les trésors individuels "peuvent être réquisitionnés par le clan", la frontière entre propriété clanique et individuelle n'est pas si claire.

On note entre autres chez les Nivkh un "exemple classique de mariage asymétrique avec échange généralisé : le clan A donne ses femmes au clan B qui donne les siennes à C et ainsi de suite".

> "La guerre est liée dans toute l'aire à l'existence de l'esclavage. Dans la région du bas Amour, elle semble peu importante, elle ne vise jamais à la destruction des propriétés de l'ennemi, les femmes et les enfants ne sont pas massacrés, et une seule nuit représente la durée maximale des combats ; par ailleurs il existe de nombreuses méthodes pacifiques de règlement des conflits inter-claniques. À l'inverse, on mentionne un état de guerre endémique au Kamtchatka."

### La Californie

L'alimentation était princpalement basé sur les glands qui devaient être conservé à l'abris des rongeur et de l'humidité. Il s'agit d'une région densément peuplée, de l'ordre de 43 habitants / 100 km2 (les chasseurs-cueilleurs nomades ont une densité presque toujours inférieure à 10h/km2). La population totale de l'État de Californie était d'environ 100 000 à 300 000 personnes.

> "il existait dans toute la région une inégalité sociale très marquée : une chefferie généralement héréditaire liée à la richesse, un chef collaborant étroitement avec les chamanes, et servi par toute une 'bureaucratie' de sous-chefs, administrateurs et conseillers"

Les chefs étaient associés aux stocks de nourriture.

Il existait une importante division du travail :

> "chamanes, chasseurs, pêcheurs, joueurs, chanteurs, fabriquants de perles, de pipes, de ceintures de plumes de pie, de filets pour la pêche ou pour la chasse aux canards, de monnaie de coquillages, de forets pour la monnaie, de chapeaux en filet, de canoes, de paniers, d'arcs et de flêches"

Ces métiers et leurs secrets étaient souvent hérités ce qui contribuaient aux inégalités.

Le commerce intertribal était très actif, et la monnaie et coquillage incarnait la richesse. La nourriture étant facilement convertible en nourriture, on peut dire que les stocks de monnaie étaient très importants. Les échanges étaient même parfois complètement indispensable comme dans le cas des Chumash qui acquéraient leur nourriture (glands) unniquement par échange, les conditions naturelles sur les iles ou ils étaient n'étant pas réunies.

> "La terre et ses ressources semblent rester en général propriété commune \[...\] il semble difficile de parler de propriété privée."

La société semble relativement belliqueuse, principalement pour des motifs économiques.

## Chapitre 4 : Cas probables et cas limites

Quelques autres exemples de différentes régions et périodes

### La préhistoire de la côte péruvienne

Entre -2500 et -1700, société de chasseurs-cueilleurs sédentaires mais on ne sait pas trop si cela est du à une très grande abondance en nourriture ou bien à si c'est lié au stockage.

### Les Warao

Delta de l'Orénoque (Venezuela).

> "immense région marécageuse périodiquement inondée ou pousse abondamment le palmier 'moriche'"

Ce palmier fourni la base de l'alimentation avec le produit de la chasse. On mange sa fécule et ses fruits qui ne sont disponibles que pendant la saison sèche car "la fécule est consommée par la plante pendant la floraison". La fécule est transformée en farine et conservée plusieurs mois.

La société est fortement stratifiée et les personnes importantes n'ont pas à faire de taches physiques. Le travail de la pierre et la céramique sont inconnus (avant introduction extérieure), tout est fait en bois.

> "le spécialiste religieux contrôle à la fois la production, le stockage et la distribution de l'aliment de base."

### L'est de l'Amérique du nord

#### Les Calusa

Sud de la Floride, à la période de la découverte de l'Amérique.

Chasseurs-cueilleurs, société stratifiée, "travaux collectifs gigantesques", sédentaires, "importance de l'art céramique pratiqué depuis longtemps".
Contrairement aux peuples de la côte ouest, les Calusa étaient entourés par des chefferies et des États. Leur particularité était l'absence d'agriculture. On en sait pas si ils pratiquaient (abondamment) le stockage

#### La préhistoire de l'est des États-Unis

Les charactéristiques précédentes sont relativement courantes pour les peuples de cette zone. À l'époque du Woodland (-1500/+500ou+1000), on retrouve le Hopewell, les grands tumulus de Poverty Point dans le bas Mississippi et le Grand Serpent dans l'Ohio. Ces populations étaient sédentaires, l'agriculture existait (mais) sans être très importante. Il semble que le principal de l'alimentation végétal soit lié aux noix.

#### Vue d'ensemble sur l'est

Au sud-ouest de la région des grands lacs, les Ojibwa récoltent le riz sauvage et connaissent la conservation du poisson et du gibier. Ils reste cependant plutot mobiles.

### L'ouest de l'Amérique du nord

Le Grand Bassin qui prolonge la Californie au Sud lui ressemble à ceci près que la région est plus désertique. Le récolte des pignons est imprévisible et les habitants ne se basent pas uniquement sur cette ressource. La plupart des peuples de la région sont chasseurs-cueilleurs et nomades même si certaines maisons en dur ont pu être retrouvées par les archéologues.

Les sociétés du Plateau ("partie ouest du domaine sub-arctique") ressemble aux sociétés de la côte nord-ouest. On peut noter des villages d'hiver basé sur le stockage de racines, de poissons et sur la cahsse hivernale.

### Eskimos et Aléoutes

#### Le domaine eskimo

Dans les régions arctiques, la conservation est rendue aisée par la glace et notamment le permafrost. On retrouve des campements permanents. Les variations saisonnières sont très importantes, la cueillette est très marginale, l'essentiel de l'alimentation vient des animaux.

> "le travail est long et difficile, l'infanticide et la mise à mort des vieux sont pratiques courantes"

L'apparition d'une société de stockage est donc possible mais pas évident. On retrouve un cas par exemple chez les Tareumiut au nord de l'Alaska qui entrepose le produit de la chasse (baleines chassées au printemps, caribous, poissons et autres mimmifères marins) dans de grandes caves dans le permafrost.

> "Les Tareumiut sont parmi les rares chasseurs à posséder la poterie. Les inégalités économiques sont assez développées. Les caves sont propriétés privées et leurs grandeurs différentes traduisent des différences de richesse. La possession d'un bateau est un autre signe de richesse, et seuls les plus influents parmi ceux qui en possèdent pourront organiser une expéditioin importante de chasse en mer. La pauvreté existe et constitue un thème récurrent du folklore local."

#### Les Aléoutes

Les Aléoutes se rattachent aux Eskimos avec la particularité qu'ils occupent des iles entre les côtes américaines et russes. Cinquante ans après leur découverte par les russes en 1741, leur population était réduite à peau de chagrin (maladies + massacres).

La société était stratifiée entre gens importants, gens communs et esclaves. Parmi les personnes importantes de cette société, on retrouvait encore les baleiniers (qui détenaient la connaissance pour empoisonner les lances à l'aconit). La guerre était importante (fortifications...). Les Aléoutes sont sédentaires comme les peuples "environnants". Leur méthodes de stockage sont cependant moins performantes (sèchage compliqué en raison du climat humide et pas de fumage)

### Le Japon à l'époque Jomon

Les premières poteries datent du XIe millénaire. La floraison des poteries a lieu à l'époque Jomon (jusqu'en +400) puis époque Yayoi avec une nouvelle tradition céramique + riziculture.
L'exploitation des produits de la mer (notamment des coquillages) a donné lieu à une tentative d'explication pour l'invention de la poterie : cela permet de faire bouillir facilement les bivalves.
L'agriculture était connue mais semble secondaire à l'époque Jomon.
Le peuple est sédentaire (céramiques fragiles, maisons semi-souterraines, meules lourdes et encombrantes, ...)

### L'eurasie du nord

On considère la toundra et les forêts de conifères, du Baikal à la Suède.
Certaines des différences entre l'Amérique, et, notamment la Sibérie : cette dernière région est plus froide (ce qui permet un recours accru aux techniques de conservation) et aussi plus enneigée (développement des skis et élevage de rennes = inconnu en Amérique, et déplacement en traineau à chiens). La poterie est connue depuis longtemps mais à connue une régression au fil du temps.
L'intérieur de la Sibérie est réputée plus nomade que la région de l'Amour riche en poisson mais on peut noter que certains lacs et rivières étaient propices à la pêche et à la sédentarité.

> "La netteté de la démonstration fournie par l'archéologie du lac Baikal peut se résumer ainsi : d'abord accroissement du rôle de la pêche qui conduit à une économie probablement semi-sédentaire avec stockage, puis, sur cette base, développement des inégalités sociales."

> "Dans la taiga de la Sibérie occidentale, de l'Ob à l'Ienissei, on trouve plusieurs peuples chasseurs-pêcheurs semi-sédentaires"

Ces peuples ont généralement des résidences d'hiver et d'été et domestiquent peu le renne.

Au nord de l'Europe, les lapons n'élèvent les rennes que depuis le Ve siècle de notre ère. Avant et après, on compte de nombreux pêcheurs. Des divisions sociales sont présentes. Cela se retrouve aussi autour du lac Onega (des nécropoles avec des centaines de tombes ont été découvertes.)

Époque mésolithique européenne :

### Le proche-orient : le natoufien

Cueillette de céréales sauvages pendant une courte période, sédentarité.

## Chapitre 5 : Répartition géographique et contraintes naturelles
