# Worshiping Power: an anarchist view of early state formation - Peter Gelderloos

## Introduction

> "According to dialectical materialism, the State is a product of class divisions in society: government is an organizing tool of the owning class, and different forms of governance are determined by a society's economic production."
> Pierre Clastres: State often precedes such divisions ...

> "Christopher Boehm, in an extensive survey of stateless societies, demonstrated that the key factor allowing a society to be stateless was not its mode of production or geographic conditions but an ethical and political determination to prevent the emergence of hierarchy: what he referred to as 'reverse dominance hierarchy'."

Environmental determinism: basically they say that the environment is a major factor that influences the political outcome of a society. But this is no exact science and is not deterministic. Environment is really important but taking only this factor into account erases the will of the people in one society. Jared Diamond: any society given the same environment as the West will develop a state and commit the same atrocities as we need.

Primitivism: can be interesting for some aspects but cannot give correct answers for the roots of domination.

For Gelderloos, there is a cruel lack of knowledge on horizontal societies. They have too long been considered brutal and archaic.

> "Anarchists define the State as a centralized, hierarchical system of political organization based on coercion and alienation, the primordial alienation being the theft of each person's ability to decide over their own lives, the suppression of self-organization so that power could be centralized, delegated, and institutionalized."

> For social scientists, there are several different factors:

- a least three levels of hierarchy (capital, provincial capital, town)
- "unitary decision-making and an explicit chain of command, which ideally do not permit contradiction, even if contradictions are regularly produced in practice"
- "the administration of a redistribution of resources, from the toilers to government functionaries"
- authority is institutionalized and flows from a centralized abstract point (God, the law, the people)
- "identity and authority are territorial rather than kinship-based"
- ...

This is a very precise definition which leaves many societies in a grey zone where they are x-% state. The definition of anarchists is broader because they refuse any form of domination, so everything approaching the form of a State is bad.

# Chapter 1: Take me to your leader, The Politics of Alien Invasion

A first quite widespread scenario: colonization. One country wants to deal with the colonized territory and it is easier if this territory has a chief to talk to and some administration. If not, this can (should, will) be imposed.
Gelderloos notes that a hierarchical society is organized in order to be ruled whereas an egalitarian one is organized in order not to be ruled. This is not a distinction of complexity as some would think.

> "The British tried to appoint chiefs among the horizontal Chin of Southeast Asia, and to increase their prestige and authority, the chiefs, subsidized by their powerful allies, threw lavish feasts, in accordance with the feasting culture prevalent in their society. In response, a new cult arose among the Chin that 'repudiated community feasts while continuing the tradition of individual feasts that served to increase personal, not chiefly, status'."

Imposing a chief will only work well if the society already has a tradition of hierarchy. If this is not the case the colonial state could have a policy of genocide, resettlement or ask for a tribute and foster trade this way (which could be like a slow option to softpowerise the colonized)

Trade plays an essential role in state formation through colonization. Trading companies are usually quite wealthy and have the abilities to blackmail or invade (militarily) the society they want to colonize. In presence of a stateless society that doesn't want to be in close relationship with the colonizer, there is always some trading going on. This can lead to the rise of some native merchants who want to get some good deals with the State. Trading with some people is effectively making them some intermediaries between the two societies which can crystallize some forms of power. These merchants become also really powerful in comparison as before the trade, they can acquire firearms or some prestigious wealth.

> _reluctant client state_: "the formation of a new state that often flows out of a strategy of resistance of the stateless society"

Two examples with Indian nations in the US. Basically we see a part of the population taking arms against the invaders and launching guerrilla warfare (only to get killed) and another part of the society with casual people pursuing their way of life while leaders make concessions with the Europeans until acculturation. (important points: treason of the elite, division of the society and accepting compromise when the other side wants to kill you).

> "True independence from states can only come through warfare."

Note that colonization can also produce some anti-authoritarian societies, fleeing away from colonizers. It doesn't only produce states.
Last examples in the Caribbean, and most importantly Haiti. Different forms of contests arose, some centralized and others not. The most long-lasting were usually the decentralized ones, and they were also the only ones to let women have a role. Centralized revolts (hierarchical army of Toussaint l'Ouverture for instance) can in fact recreate the dominating State people wanted to destroy.

# Chapter 2: Ze Germans, a State-making technology

> _imitative state_: "Local elites within the preexisting autochthonous hierarchies were impressed by the greater power amassed by the elites in neighboring societies, and sought to copy them."

Having powerful neighbors also poses the question of self-defense, and militarization is often leading to more hierarchy and a stronger state.

> "There is another mechanism for stable state-formation: the _progressive state_, a lesser of two evils that takes advantage of the popular rejection of a more onerous form of state."

The German tribes were actually very important with regard to state formation in Europe after the fall of the roman empire. Even though they were only tribes and not states, their invasions were aimed at imposing states, and they often succeeded.

> "A politogen [term coined by Gelderloos] is a cultural group that constitutes a state-making technology and often takes the form of an ethnicity or proto-ethnicity. It is like a virus that will form a state once it acquires a host population"

We then talk about _conquest_ or _colony state_. The characteristics of such a politogen will be a patriarchal and hierarchical society even in its mythology. This group will also know the concept of state. For example the Germans with the Romans.
The economic mode and agricultural techniques are imposed by conquerors, the religion is mixed.

The Germans were actually different groups, with similar dialects and culture. They were not unified, the term "German" mostly comes from the Roman.
They were a "free people", relatively egalitarian, uncapitalistic, hospitable, decentralized. So how did they ended up a politogen?
For Gelderloos, this lies in their _militarization_. Labor was a woman's field, war was for men. And when men started conquering other peoples (previously under Rome), they placed their value, war, on top of the hierarchy and labor at the bottom.

> "David Graeber makes a similar argument with respect to the ancient Greeks, asserting that democratic organization was typical to state-forming warrior societies whereas consensus-based organization was typical to states societies."

Early Rome: relatively egalitarian society (for free men).

> "All men could vote, and they divided land won by conquest on an egalitarian basis. [...] Incidentally, extended kinship bonds were weak and communities of commoners were united by neighbor bonds, with each household led by a man."
> "such conditions of alienation and relative individualism were likely the result of displacement, slaving, and turmoil - basically refugee crises - caused by expansive states." => "land alienation: given the atomized family structure, land was doled out as individual property."

Not very clear to me.

> "Militarisation, in sum, is a culturally driven, non-inevitable process by which the exigencies of warfare - either socially manufactured or imposed by bellicose neighbors - are exploited by an endogenous proto-elite to create a pathway for increasing social discipline and hierarchy."

It also seems that militarization comes generally with a diminution of the social power of women and of the acceptation of gay people.

// TODO: wtf did i wrote here
In Kula state (Congo basin), judges were really important. The justice was done through secret fraternities. These anonymous people were allowed to commit violence and even blood crimes (although it was not authorized in the society). So only the State is able to produce this violence. Furthermore, begin anonymous can make great impression and even symbolise some divine aspects.

# Chapter 3: Save me from yourself, The statist spread of salvation religions

Catholic church used the Roman statecraft to spread, and overcame its fall.

> "Even where it was too weak to constitute an imperial state, it spread a common cultural language that favored state formation and preached network to mobilize resources for state formation, until the power of those states increased to a point where they either monopolized the resources of the Church or rebelled against it, creating their own autonomous churches with a doctrine modified to legitimize the transformed basis of their power."

> "A major cause of the peasant rebellions that began to occur in Western Europe in the Late Middle Ages was the restoration of Roman law by local elites, which allowed land to be bought and sold as a commodity. As a direct result and parallel, the condition of peasants - who were much better off in the Middle Ages or under the earlier German tribes than their Roman homologue, the slave - began to deteriorate gravely"

> "The epidemics, poverty, starvation, servitude, intolerance, witch hunts - basically all the features assigned to the stereotypical image of the Dark Ages - were in fact characteristics of the end of the Middle Ages, the Renaissance, and the beginning of the Enlightenment."

> "The Dark Ages were only dark for states."

> "When state power was restored, it was accompanied by another Roman practice that might have slipped into obsolescence were it not for the diligence of the Catholic Church: usury. Contrary to its own doctrine, the Church encouraged finance - leading with interest - in order to fund its own power pursuits."

Even the Protestants didn't consistently criticize this. Only Anabaptists did and they were slaughtered by them.

Example of Islam: new professional class (a religious one), new social order / hierarchies (Arab Muslim > non-Arab Muslim > non-Muslim), focus on genealogy implies finding common ancestors and extending the *ethos*

> "Islam has succeeded in centralizing symbolic power and creating a statist imaginary, such that state-building was henceforth the goal of nearly all political activity, ineffective as it might be."

Also Islam fostered trade and

> "Trade expansions often go hand in hand with state expansion, as trade creates networks that states can exploit, and states create infrastructures and impose conditions of alienation that make the accumulation of wealth possible. While trade occurs anarchically and can be organized without states, a state may decrease its costs and risks, allowing it to be pursued purely in the interest of profit, and also create dependent populations vulnerable to exploitation."

Furthermore, trade creates a new class, merchants, which first can see state positively, but can clash with it also. State doesn't want to lose its power but is also reliant on trade and merchants. It is useful that state is based on an universal religion because, as trade supersedes borders, there could be huge tensions with different states having local religions.
Yet this can be complicated for a state because being internationalism makes it much more harder to mobilize and militarize.

Salvation religions are mutually exclusive and can be used for war mobilization. Furthermore, they can be used to shake up a society and install a new proactive government.
These religions are often associated to a certain culture which will be referred as civilization.
They foster state because they can be used to impose one certain culture and homogenization fosters states. They can also bring with them some state-like institutions like the temple to pray or some tribunals (as we have to judge according to the moral principles of the religion)

People who want to stay away from state power have often chosen an heterodox version of the dominant religion like Anabaptism against regular Christianity. But this is also a way for the state to justify repression against heretics.

> "Polytheistic states largely described the fickle, shifting fortunes of nature or geopolitics (e.g. invasions, declines in trade) as the result of wars or conflicts between the gods; in other words, society and the states were peripheral to extremely important changes in the world around them. Such a view, in which human society is at the mercy of fortune, created a crucial barrier to the expansion of state power. The monotheistic state of Judah, on the contrary, could develop a cratocentric worldview in which divine will and state interests were completely fused. Any misfortune, whether a plague or the supposed division of the United Kingdom, was a result of the wrath of an extremely demanding God. Any good fortune was the result of obedience to God's will."

# Chapter 4: Sleeper States and Imperial Imaginaries, Authority's Afterlife and Reincarnation

Would deserve a re lecture, but basically after the fall of the Roman Empire the statist mentality persisted among the ruling classes. The peasants were a source of innovation and class of _improvers_ rose up little by little 

# Chapter 5: The Modern State: a Revolutionary Hybrid

Capitalism is difficult to accept for the population because of its brutality so the State had to adapt and became more repressive, stronger.
The lack of strong cultural opposition to this new regime lays in part in the absence of strong family ties. This let possible a "culture of personal advancement" typical of "the bourgeois supersession of the nobility".
This system also thrived on repression based on patriarchy and the old tradition of repression by the church.

> "Punishment of heresy was systematically designed to discipline, to gain repentance."

On the witch hunt:

> "the majority of the victims of the witch hunts were probably not healers, and in fact a disproportionate number of state's witnesses were officially licensed midwives and nurses."

> "the with burnings were complemented by a process of increased state regulation of women and of feminine professions. The newly professionalized male doctors were appearing on the scene and attempting to monopolize healing practices. A place was reserved for women who were willing to subordinate themselves to the doctors as nurses."

On the importance of patriarchy and the Church:

> "First Christian philosophy and then, in a seamless transition, Enlightenment philosophy, served to coordinate the offensive that restored and absolute patriarchy utilizing a broad array of state and non-state institutions, from the municipal to the continental level. "

The creation of the nation-state is also something to consider:

> "These two classes of people, students and traders, played a dynamic, essential role in the subsequent, creation of nation-states."

> "Contrary to nationalist mythologies, nations in the West did not arise as part of a slow process of linguistic and cultural unification, nor did they even appear within their own borders."

> "Progressive movements systematically redirect popular rage, which is inspired by nothing less than the violence of begin governed [...] The goal (at least of those who command such movements) and the result is to make power more powerful. Progressive's favorite whipping boy has long been corruption, but corruption is the last vestige of humanity in an increasingly inhuman system."

> "The Enlightenment, and the philosophy of scientific rationalism that accompanied it, were also a _sine qua none_ for the development of modern state."

> " There is also much to be said about the restoration of Roman law favoring private property and allowing land to be alienated, the development of new productive techniques and modes of production,  and new forms of economic and political organization."

# Chapter 6: Zomia: A Topography of Positionality

Geographical determinism could be interesting were it not for the fact that social organization is a political choice. A place could be inhabited by a hierarchical or anarchic people, with a variety of mode of production and agricultural practices.
As state formation was not that easy, it developed certain techniques (crop plantation...) and stateless societies developed again this culture. And state had to adapt to keep its population. 

> "On the organizational place, warfare was one of the first and most common measures deployed to achieve and maintain the labor-power states needed for their existence. This involved "forcibly resettling war captives by the tens of thousands ad by buying and/or kidnapping slaves." But as captives ran off, states had to develop complementary methods. Subsistence activities and living outside of sanctioned villages were prohibited, and state planners developed legal and economic mechanisms, with the aim of forcing their subjects to choose between grain cultivation and starvation. [...] The destruction of wild places was another obvious tactic, if we accept the pathological mentality of states and their agents. "Cut the forests, transform the forests into fields, for then only will you become a true king," as a ruler in ancient Mali was instructed."

States can try to control everything, from space (shaping nature and cities) to economical practices (through law, erasing alternative sources of living) in order to enforce certain political agenda (like capitalism) and erase opposition.

> "Religion, history, citizenship, nationality, and identity as we know it all train us to be incapable of imagining our lives outside of state authority."

> "States and their leaders are to us as the protagonists of history, and when the stateless cannot be symbolically suppressed as primitive, savage, obsolescent, ignorant, evil or terrorist, they are relegated to the shadowy backdrop of a stage the State clearly commands."

> "the fundamental purpose of education is to civilize children, and a large part of this means filling their heads with the lies that are necessary to make them always view history and society from the perspective that privileges state power."

> "What would change if everyone grew up knowing that states built walls to keep their subjects in, that a great motor of history was the state need to coerce people into being its subjects, that all of us, in one way or another, are the descendants of slaves, and that the mechanisms of our enslavement have never disappeared only been elaborated?"

# Chapter 7: Chiefdoms and Megacommunities: On the Stability of Non-State Hierarchies

Chiefdoms have been proven way more stable than thought before, for example by the false assumption of the neo-evolutionist sequence ending up with the state (chiefdom being the previous state). Having a leading figure has not always meant coercion and lots of societies have ended up growing in complexity by forming Megacommunities and not going the State-way.

Example of the Haudenosaunee (Iroquois)

> "Warfare does not cause state formation, as I have already argued; rather, preexisting hierarchies can use warfare to justify and accelerate state formation."

> "All the nomadic empires of the Eurasian steppes - those of the Turks, the Huns the Mongols, and the Uighurs - used a similar system. Lacking a bureaucratic principle of authority, paramount rulers sent their comrades-in-arms or their relatives to govern provinces, dependent tribes, and allied chiefdoms. Each of these groups was largely self-sufficient and autonomous, with their own structures of internal organization, but they presumable tolerated the representative of the central authority as a way to maintain their alliance and avoid hostilities. On the whole, the duties required by the central authority were mutual self-defense  and military discipline: limiting raiding and offensive warfare to the enemies of the central polity. Such an alliance tended to come with few obligations beyond ritual gifts and obeisance, and provided the benefits of protection from powerful state neighbors and the booty from successfully raiding."

> "The extended family can better approach economic subsistence than a nuclear family, therefore not leaving _organizational gaps_ that emerging administrative institutions could fill. On the other hand, it is not so large as a clan, and not so suited for a redistributive economy that would allow the concentration of wealth."

Example of Benin which was composed of lots of different chiefdoms. At some point a (foreign) supreme leader emerged but:

> "The function of the paramount ruler remained religious, and his power was primarily symbolic. It symbolized the unity of all the chiefdoms and independent communities, therefore lending itself to a political stability that was spiritual rather than bureaucratic."

Interesting thought:

> "It is characteristic of state evolution that the emerging center of state authority must wage a bloody conflict against the power-holders in the preexisting political organization. Those prior leaders are generally complicit with politogenesis (sometimes unwittingly so) but in many specific instances will fight against state formation in order to non-state power-holders as conduits of the logic of power, which conditions them to push for innovations and forms of organization that encourage the further accumulation of power; at the same time they wish to hold onto power, even though the conditions that allow them to be power-holders must be superseded for power to accumulate further. Thus they favor the accumulation or power even as they oppose the organizational progress that higher intensities of power require." 

For Gelderloos, it is clear that the collapse of a great state is always multifactorial and we cannot usually extract one cause. However:

> "I propose that we would attain a far more accurate view of history if , every time a state collapsed, we assumed rebellion was a principal cause, unless evidence existed for another cause. [...] Too often, historians and archaeologists fabricate cheap mysteries, 'Why did this great civilization suddenly collapse?', because they refuse to accept the obvious: that states are odious structure that their populations destroy whenever the get the opportunity, and sometimes even when they face impossible odds. "

> "In fact, the disappearance of writing is a likely result of an anti-authoritarian revolution in a society in which written language is controlled by the elite."

On the Sea Peoples, which warred against big states (modern day Turkey, Syria, Egypt and Palestine). Bronze Age.
> "The relative strength of the values of anti-authoritarianism, on he one hand, and militarism, on the other as well as the presence or absence of patriarchy, probably determine whether a liberated people become a new politogen."

Greece. The compatibility test.

> "Tyrants were like the paralegal Batman that every system of law and order needs to function; condemned and externalized to a marginal space of illegitimacy, but clearly present both in the imaginary and in the historical operation of the idealized model they shadow."

> "To me, this interchangeability - take out the assembly, put the tyrant in its place, and the society keeps functioning - constitutes a important test: compatibility. The fact that the poleis pass this test proves that the assembly and the dictator function within the same democratic logic. On the contrary, truly stateless societies, saddled with a president, a chief, or an assembly of male, slave-owning citizens, will either ignore the addition (probably, the people would kill or exile them if they attempted to assert their authority) or the enter social fabric would have to be reengineered, because it contains no features or interfaces compatible with the operation of the statist institution."

> "The fact of compatibility between dictator and assembly in the ancient Greek poleis demonstrates what most social scientists and historians, thanks to their conditioning by democratic mythology, will never e able to see: that these two institutions constitutes part of the same political system, just as they do in parliamentary democracies today."

# Chapter 8: They Ain't Got no Class: Surpluses and the State

We will focus on how States can form "primarily" ie without having an example nearby or having already experienced statecraft. On common theme is economical accumulation but "The very notion of understanding (as distinct from analyzing) the economy as a distinct sphere of social life is problematic from the start." 

> "It becomes apparent, on a long timeline that surpasses Marxism's narrow optic, that economic accumulation is inconceivable without the hierarchical structures and spiritual values that states and proto-states create. I believe all the available evidence confirms that economic accumulation is never a cause of state formation, although developing an effective strategy of economic accumulation can allow a state to grow rapidly."

> "In a gift-based economy, bounty and abundance never manifest as surplus, since anything that is not used by an individual or her family is given away."

Characteristics of early States: weak parasitism (protection racket...), social reengineering (push for production specialization, network of distribution controlled by the State, "relocating people and developing new architectures", "they captures slaves and manufactured ethnic or religious barriers to solidarity"). Choices made for the State are usually made in order to increase its power more that for some efficiency.

> "They are rarely models of efficient or sustainable agriculture, but they are, and they are intended to be, models of legibility and appropriation." Scott on Southeast Asia.

> "Comparing early city-states in the Americas with those in Asia, Berezkin points out that hyper-urbanization was not efficient for production but was necessitated by administrative limitations of the state that attempted to monopolize and reorganize economic activity."

It seems important to control religion and trade.

Example of a stateless civilization, the Indus Valley (Mohenjo-daro etc). Gelderloos insists on the stability of this civilization over long period of times, its egalitarian features and its scientific development.
Another example, the Banda islands in the Maluku archipelago, "one of the most intensive, high-value trade networks in world history" (spice trade). Horizontal society though there existed  a council of wealthy male elders. "Islam arrived peacefully to the islands in the fourteenth century" but then "the VOC (the Dutch East Indian Company) arrived in the seventeenth century and tried to impose monopoly with unfavorable terms, [and] the islanders revolted and killed the interlopers. [...]". When the Dutch returned they "instituted a policy of genocide, killing or expelling the natives and repopulating the islands with slaves or forced immigrants. This was the beginning of state authority in the archipelago."
So trade does not lead but State but States can profit form it.

About Tarumanagara:

> "many hierarchical but non-state societies in the area developed states as a way to protect themselves from more aggressive neighbouring states

The regular change in capital city is a sign that power changes frequently and the society is probably not a state.

About the VOC:

> "No longer content to profit off of or even monopolize trade, they began to reengineer the terrain, destroying and remaking entire societies, in the symmetrical interests of social control and productivity. The Dutch East India Company was peerless in this regard."

> "it constituted a politogen, even a state in itself. By 1669, it commanded over two hundred ships, a quarter of them warships, and ten thousand soldiers. The Dutch state had granted the VOC law-making, war-making, and judicial authority."

In the 16th and 17th, Europe produced nothing interesting for the powerful Asian states so the VOC "initiated intra-Asian trade circuits in order to fund its spice shipments to Europe".

> "To summarize, a growing population and the interrelated improvements in agricultural and metallurgical techniques, which probably arrived in Central Europe as decentralized (one might say "anarchic") innovations, together with a context of neighborly warfare, provided an opportunity for hierarchical, non-state chiefdoms in Central Europe to develop a state. The pathway, following well-known models practiced by the neighboring Germanic states, involved convincing preexisting nobility and local leaders to centralize their power, first in some form of confederation, to administer construction, production, and warfare. This state-building activity spurred a cycle of accumulation and a paradigm shift in the economic mode of production, seen not only in the quantitative increase in trade and population (outstripping the earlier population increase) and the qualitative improvement in agricultural and metallurgical techniques, but also in the (re)appearance of such significant and game-changing elements as currency."

On the difference between the Polanie state and other societies nearby that didn't end up as states: "an ambitious plan to reorganize society. State-formation was a strategic act of elite will."

> "in its origins and in its essence, the economy is but one moment in the war the State constantly wages against society. "

# Chapter 9: All in the Family, Kingship and Statehood.

> "Throughout a broad swathe of human history, the accumulation of status was more feasible than the accumulation of material wealth".

Basically, status led to the access of important position in a society or to the right to do and / or to possess stuff.

Examples might include who has access to better places in spiritual rituals.
Another example from Kropotkin's history of the French Revolution. In rural villages there was a distinction between citizen and inhabitant. Better-off farmers identified as the citizens, which has a connotation linked to the bourgeoisie from the cities and who preferred the privatization of lots of lands where the inhabitants, modest farmers, preferred to keep the commons. The former are associated to the founders of the society, the latter to newcomers. 
This can of distinction is prompt to lead to inequalities within a society. Historical figures of a society can easily choose to accept or not newcomers and at which cost. They could allow them to establish themselves nearby but without normal rights or with the obligation to work more than "historical" inhabitants.
For Gelderloos, such a distinction is present in our "western democracies" which have been historically exclusionary (only the privileged ones were citizens). Even today, immigrants are excluded.

> "Through the unequal accumulation of hereditary status, an authoritarian society can subdivide into multiple ranked lineages, forming a complex hierarchy. With outsiders being given permission to live on the society's territory on the condition that they accept fewer rights, a caste of dependents could also arise, giving even more privileges, status, and economic benefits to the higher-ranked lineages."

What seems important here is how families are organized. Lots of anti-authoritarian societies live with extended families which means that people are aware of their big families and superior lineage is not likely to arise. 

> "lineages are ranked; they jockey for status; and part of the jockeying rests on claims to superiority based on different, and fabricated, origin myths and genealogies." Scott

A counter-example might be the Mapuche where life is organized around the nuclear families. But important roles (knowledge of plants, peace mediation, diviner, medicine man or woman) are distributed and aren't "powerful roles" but instead roles that demands extra work for the community.

> "Among nomadic Mbuti communities, Colin Turn bull described the habit of claiming kinship relations with every other member of the community, as well as with neighboring communities, regardless of whether common ancestors or blood relations were a genetic 'fact'. Traditionally, kinship relations were deliberately ambiguous, all the adults of a community were parents to all the children, and all the children of the same age group were siblings."

> "societies without an anti-authoritarian ethos, we can surmise, treated relations not as an opportunity for everyone's mutual enrichment, but as a resource to be controlled. And for someone to gain power from access to a resource, they first must make that resource artificially scarce. **In fact, the whole Western concept of an 'objective' kinship based on shared genetic material, or in earlier terms, 'blood', is little more than a massive mythical justification for limiting familial relations**."

Gelderloos makes the distinction between a ranked society, with different lineages having different status, and a stratified society, in which the concept of nobility arises, where the status is not based anymore on merit (we must not prove that we deserve a status). How to go from ranked to stratified? For Gelderloos, it supposes breaking the solidarity between members of the society, which can only be done through "warfare", "cold, calculated murder".

We can then define the *royal court state*, a model of state formation where the dominant lineage can maybe crystallize around a charismatic figure (good orator, good warrior and organizer) and then unify different territories to end up with a monarchy.
Cementing the power of such a lineage can also be by means of finding or inventing some glorious ancestors.

> "as far as we know, all states and proto-states have arisen from patriarchal societies."

> "Ancestor worship creates an important circuit in the concentration of power."

Gelderloos suggests that this could lead to people could exaggerate their ancestors' prestige, leading in some cases to god-like status. This is interesting in the light of the theory of Sahlins and Graeber in *On Kings* which goes that divinity comes from Gods, men can behave like Gods, ie Gods *precedes* kings.
So another way of taking advantage of a dominant lineage to create a proto-state is the *holy father state*: by monopolizing a divine ancestor and encouraging a cult to him (which requires some control of the "religion", priests, ...), one could create an important distinction between "the divine and the profane". Society is not necessarily stratified, the division is in nature different.

> "in all the well documented examples I am aware of - such as the Congo basin states, the early Mayan states, and the Shang and Zhou states in ancient China - religion and warfare both played fundamental roles"

This could be interesting to compare with the theory developed by Wengrow and Graeber in *The Dawn of Everything*.

> "Symmetrical to the hierarchy of priests and spiritual courtiers who attend to the god-king, a hierarchy evolves among the divine ancestors and natural spirits, such that the supreme divinity gains his own court that serves to naturalize the earthly political institution. The result is a simultaneously mystical and bureaucratic pantheon and social apparatus that transforms power from charismatic, ceremonial performances into a distributive mechanism that delinks power from a set of localized and limited familial relations and binds it to an expansive, mobile, and monopolized claim to the divine." 

Note that having a divine position is also having people put high expectancies on you. Weak kings could be killed, and they were also wrapped by sacred rules, taboos which limited their power (see again *On Kings*).

> "charismatic authority may be said to exist only in the process of originating. It cannot remain stable, but becomes either traditionalized or rationalized, or a combination of both." Weber

> "A state that did not wisely reinvest its spiritual capital in military, administrative, and infrastructural capital, would most likely collapse."

Some interesting pages on Hawaii and then the Aryans, eg:

> "Before the caste system opened the door to state formation, Aryan law was purely customary, with disputes mediated by elders or leaders, and reconciled with reparations (i.e. the payment of cows), even in cases of murder. The death penalty came later, as did codified laws in which people were punished differently according to their caste."

Gelderloos compares this to our democracies in that we are "equal before the law" but it's not really interesting if the law is made for the rich.

> "written law (as opposed to community mediation) signifies the professionalization of justice and an authoritarian transformation of power"

# Chapter 10: Building the Walls Higher: From Raiding to Warfare

> "The accumulation of population by war and slave-raiding is often seen as the origin of the social hierarchy and centralization typical of the earliest states." R.L. Carneiro

Again, for Gelderloos, "the intentions and the projectuality of [...] political actors" is the engine towards keeping a stateless society or developing into a state. There have been stateless peoples using raids as well as hierarchical ones. The raids are usually aimed at collecting resources (including humans) and / or controlling new territories.
One example with the Zapotec:

> "Previously 'the Canada [with a tilde on the *n*, we are in Mexico] was occupied by 12 Perdido phase villages', which were burned to the ground [by the Zapotec], replaced by new sites dedicated exclusively to intensive agriculture and lacking the rich ceremonial life of the earlier society, and by a heavily fortified complex of seven sites that controlled the northern entrance of the Canada."

The story goes: people doing exploits in raids get privileged status, so people are becoming more bellicose, raids are more violent, more and more resources are stolen and a class of warrior is getting higher status.

# Chapter 11: Staff and Sun: A New Symbolic Order

> "Just as we have seen that status accumulation was more important than economic accumulation in the social hierarchies that developed into states, symbolic power was a question of vital importance for proto-states. Lacking a reliable degree of coercive power, the earliest states and the non-state hierarchies that preceded them had to concern themselves with the centralization and expansion of symbolic production, in order to unify and pacify their subjects supplant the social practice of reciprocity, engineer a rupture with the old model or order, and establish a new model of authority."

> "The idea that 'political power flows from the barrel of a gun' is largely true in a modern state, but in early states, though examples abound of political power descending from the edge of a blade, is most commonly flowed from the gods. Symbolic production was, above all, a religious activity, to the point that is becomes hard to imagine an early state without religion. A well developed, hierarchical religion seems to be almost as important as a patriarchal social organization and value system in the role of companion and motor to state formation."

First we can note that worshiping gods is not happening only with states:

> "Numerous stateless societies that have practiced sedentary agriculture, from Crete to Southeast Asia, have worshiped gods, though heterodox beliefs, female deities, or non-hierarchical pantheons distinguish them from the ordered, patriarchal religions of states."

But the spirituality of hunter-gatherers seems different in that we can find many spirits (basically for all living stuff), we could even say that this spirituality is more decentralized?
Furthermore, the hierarchy of these spirits is not necessarily as strong as "real religions" although they could be. Again see *On Kings* chapter 1.

> "The godless, nature-worshiping spirituality of hunter-gatherers, therefore, is not the only egalitarian belief system, but it is the most state-resistant."

Production of symbolic power could also be seen architecture-wise with the construction of big monuments and temples (pyramids). The creation of closed temples is also important to develop a class of priests that monopolizes a part of religious knowledge and in the same time of political power.

Examples of China and the roman empire where religion is associated with bureaucratic and administrative stuff.

Other examples show that a religion can be used to break with preceding beliefs. For example after conquering the indian Naga tribe worshiping snakes, Indo-Europeans generally held snakes to be evil (religious schimosgenesis?). People could also include previous gods in the new religion as did the Catholic Church turning some pagan gods into saints.

> "Symbolic inversions relating to femininity and maternity also have a special place in processes of state formation. As mentioned in Chapter IX, divine ancestors who were worshiped in order to legitimate earthly hierarchies were nearly always men. The father as founder and creator in a symbolic replacement for the mother creator commonly worshiped in stateless societies (though is must be mentioned that, contrary to the assertions of essentialist feminist historians, earth mothers and great spirits were frequently hermephroditic, in the first case, or impersonal and genderless in the second). Given the frequency with which divine male ancestors are creators of some kind (founders of cities, progenitors of lineages, discoverers of fire or metallurgy), patriarchal ancestor cults allow men to co-opt and monopolize the symbolic realm of creation."

> "In any case, the gender complementarity [this is how Gelderloos calls the antithesis of patriarchy] of many African societies was eroded by European colonial influence. British, French, Portuguese, and other colonizers saw patriarchies where none existed. By only dealing with male institutions and leaders, and ignoring or even annihilating female institutions and leaders, they gradually created the very societies they had projected."

Interesting paragraphs on foreign kings in Africa : many African societies have kings coming from foreign lands. Gelderloos suggests that being external to the society helps would-be rulers to not be under the influence of the current rules and that even local leaders can view this as a good point in that they could hope gaining status from a friendship with the new king (while they could not stand another adversary becoming king). For more info see *On Kings* by Graeber and Sahlins.
This can of society generally have one spiritual leader (coming from the community) different from the king. For Gelderloos this could be a problem for state formation, which would benefit from a centralized spiritual power.

> "Changes in the organization of political power were often accompanied by changes in cosmologies and divine pantheons. [...] From a diverse and potentially horizontal pantheon of deities, the Sun God emerges as a supreme god, parallel to multiple local and spiritual processes : the centralization of state authority with supreme rulers dominating lesser rulers; the monopolization of spirituality by a professional priestly class, who need to streamline the pantheon and dilute the importance of localized deities outside their control; and the alienation of spirituality, with the ascendance of a supreme god who is apart from and above the earth and its worshipers."

Example of Buddha.

> "Written languages and number systems play a special role in state formation."

They are not always linked to state but some anti-authoritarian societies would reject literacy associated with bureaucracy and the state (see Scott).
Yet they were primarily used for religion and trade. They were of course useful but they had also the particularity of creating two categories : those who master letters and numbers and those who cannot. Furthermore, mathematics were first used to construct temples or create calendar (Mayan, China, ...), symbols of power.

> "I believe that what may be the most common model of primary state formation is based on the consolidation and centralization of interregional networks in which no practical distinction existed between spiritual and material commerce. This is the sacred commerce state. In such a model, commerce cannot be characterized as a mercantile concern motivated principally by profit. To understand the model, we have to imagine a world in which the pilgrimage and the trade venture are potentially indistinguishable; in  which priests are also scribes, accountants, surveyors, architects, insurance agents, and brokers; in which holy sistes are also meccas of artistic and artisanal production"

> "The necessary evoluion for state formation, then, was from spiritual commoning, in which everyone has access to sacred acts of creation, artistry, and gift-giving, to fixed spiritual values allowing for a centralized process of spiritual production. Some of the most important trade goods circulated in the network eventually dominated and intensified by the Tiwanaku tate were ritual items, which, by the very nature of their being ritualized, possessed a fixed rather than a subjective spiritual value. By changing the value paradigm, the emerging priests could make themselves indispensable to trade by controlling the very rites by which good were fixed with a value. The new spirituality, therefore, had to negate the subjective - what is useful, what is beautiful (to me) - and manufacture a social consensus on what was objectively valuable. Value, in its earliest iterations, is not the product of a free market, but a professional conspiracy."

see Graeber : toward an anthropological theory of value & The Dawn of Everything (spiritual knowledge)

# Chapter 12: A Forager's Mecca: Dreams of Power

For Gelderloos, agriculture does not lead (always) to state formation but:

> "agriculture does seem to be a clear precondition for state formation"

Agriculture was mastered in many different places around the same time. Yet lots of societies decide not to use this mode of food production or at least not exclusively. The reason is simple, farming is hard (and becomes harder with the process of domestication of seeds, see Scott). So one hypothesis is that:

> "Probably most of the hunter-gatherer societies that have adopted agriculture and sedentary living in the last three hundred years - societies that covered a significant portion of the globe - were forced to do so by processes of colonialism, and some still resist deep in the Amazon or the Kalahari desert."

> "We know the while all hunter-gatherer societies are stateless and in material terms egalitarian, we know that they cover a great range from mildly patriarchal and gerontocratic to resolutely anti-authoritarian. A society of the former type could potentially develop kinship and religious hierarchies once agriculture gave them the opportunities for denser populations, fixed territoriality, craft specialization, and inheritance."

> "sedentary living preceded full-scale agriculture"

For Gelderloos there must some link between spiritual power and the development of extensive agriculture. Choosing agriculture is a big leap, also concerning spiritual values and the view of the world. 

# Chapter 13: From Clastres to Cairo to Kobane: Learning from States

Different state formation: primary (the society develops as states without any previous knowledge of what a state is), secondary (with knowledge of state), and tertiary via the action of other states.


| Primary | Secondary | tertiary |
| ----------- | ----------- | ----------- |
| royal court state | imitative state | progressive state |
| holy father state | rebel state | colony state |
| raider state | reluctant client state | neo-colonial state |
| sacred commerce state | conquest state | revolutionary state |
| | projectual state | settler state |
| | true believer's state | |

> "My interest in studying the state flows directly from my lived experiences as a state subject, which I could politely characterize as unfavorable, unhealthy, involuntary, and antagonistic. These experiences leave me and many others with an unambiguous desire to destroy the State, and this book is one among many initiatives intended to sharpen the struggle against all domination."

All states are fundamentally the same, similarities between present-day states far outweigh differences. A welfare democratic state is not that different from an authoritarian state, and the change could happen really fast.
Against reformism: we must destroy the state,

> "By continuing to trust states as the potential solvers of climate change and mass extinction rather than spreading information and tools designed to enable grassroots direct action (most of which would be illegal), climate scientists are complicit with catastrophe."

> "We must stop thinking of the State as a potential vehicle for emancipation. It has never been anything other than a tool for the accumulation of power, and accumulation of power is harmful for individuals, for communities, and for the entire planet. [...] The State is incompatible with our freedom and well-being."

Gelderloos proposes to think of the particularities of the State and to go against every one of them. For example, refuse to comply to the law, to pay taxes, to help the police, refuse all the forms that helps legibility and control such as social media and control technologies.

> "A study of history show that the state authority hs a religious aspect: the adoration of leaders and the worship of power itself. People in rebellion must cultivate the contrary, viewing not only politicians but all power-holders and the professionals of para-state institutions (the media, the universities) as torturers, murderers, exploiters, liars, parasites, bullies, snitches, mercenaries, or at best, desperate and unethical opportunists."

> "We need to learn how to trust again in our own histories and our own capacities for problem solving. An anti-authoritarian history arises from an affinity with the egalitarian "Dark Ages" and not the Golden Ages of opulence above and misery below; with the barbarians and savages and not with the civilizations and empires. It arises from a sympathy with the outlaws and the slaves rather than with the rulers and generals; with the Indians and not with the cowboys, with the witches and the heretics, and not with the Inquisitors of the reformers. Every historical gaze posits a center; if we truly value freedom over hubris, ours must be rooted among he underclasses who have born the weight of grandeur, and among the marginalized who have fled the State's expanding borders."

But anti-authoritarian have always fought back, and Gelderloos is proud to be part of the anarchist tradition which, not only in Western countries, "have killed kings" / "have put more than a few genocidal butchers into early graves".

Anarchy works.

> "Avoiding the logic of militarization is key to preventing authoritarianism or politogenesis in our present-day struggles."

> "We have the chance, however, to secure one point of common ground that might allow a thousand different worlds to flourish side by side. Just as we refuse to be ruled, we refuse to rule over anyone else. The State is an idea that has been given one chance after another for thousands of years, and the result has always been disastrous. Let us raise our voices against ignorance and complicity until the refusal of domination becomes the common sentiment of all humankind: never again!"
