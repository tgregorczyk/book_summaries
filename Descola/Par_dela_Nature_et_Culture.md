# Par-delà nature et culture, Philippe Descola

## Chapitre 1 : Figures du continu

On commence chez les Achuar entre le Pérou et l'Équateur. Comme de nombreux peuples améridiens, ils croient que les animaux, notamment, sont des êtres humains, des parents.
Il en résulte que le "savoir faire technique" dépend d'une conservation entre personnes humaines, personnes non-humaines et esprits.

> "les femmes s'adressent aux plantes cultivées comme à des enfants qu'il convient de mener d'une main ferme vers la maturité. \[...\] Les hommes, eux, considèrent le gibier comme un beau-frère, relation instable et difficile qui exige el respect mutuel et la circonspection. Les parents par alliance forment en effet la base des coalitions politiques, mais sont aussi les adversaires les plus immédiats dans les guerres de vendetta."

On retrouve aussi entre humains et non-humains, l'opposition entre consanguins et affins "qui gouvernent la classification sociale des Achuar."

> "Parents par le sang pour les femmes, parents par alliance pour les hommes, les êtres de la nature deviennent de véritables partenaires sociaux."

On peut alors raisonnablement se demander si la nature existe dans ce contexte ?

Les Achuar classent les personnes par leurs rapports à la communication. Ils se voient en haut d'une pyramide, d'autres peuples humains avec lesquels ils peuvent communiquer étant proches. Les animaux et plantes sont en-dessous car même si on peut communiquer avec eux la réponse n'est pas directe, elle intervient dans les songes. Il y a aussi un classement parmi les animaux, par exemple ceux qui bafouent les règles d'exogamie sont en dessous des autres, les solitaires (grands prédateurs) étant tout en bas.

> "ces êtres solitaires sont les familiers des chamanes, qui les emploient pour disséminer l'infortune ou combattre leurs ennemis. Établis aux lisières de la vie commune, ces êtres nocifs ne sont aucunement sauvages puisque les maitres qu'ils servent ne sont pas hors de la société."

Les seuls êtres qui sont hors de la sphère sociale sont la plupart des insectes et des poissons, les herbes, les mousses, les fougères etc

En Colombie orientale, les Makuna ont une vision encore plus radicale. Tous les êtres vivants sont des humains qui se distinguent par leurs origines mythiques, leur régime alimentaire et leurs modes de reproduction mais partagent en commun la vie sociale et cérémonielle, l'intentionalité, la connaissance ...
Cela est notamment possible par l'existence de la métamorphose (entre humains et animaux ou entre animaux). Les animaux habitent aussi des maisons longues, naviguent en pirogue etc, leur apparence n'est qu'une parure qu'ils portent en sortant de leurs demeures.

Ce genre de cosmologie peut se retrouver un peu partout dans les zones forestières d'Amérique du sud. On utilise le terme de 'perspectivisme', car on attribue aux animaux par exemple, des pensées de type humaine. Ainsi le pelage pour les animaux est un vêtement, le sang que boit le jaguar est vu par celui-ci comme de la bière de manioc etc

Certaines théories racontent que ce continuum entre nature et culture vient de la diversité écologique de l'Amazonie. Or on peut retrouver le même genre de cosmologie près de 6000km au nord, dans une forêt boréale qui a des charactéristiques quasiment opposée à celle de l'Amazonie. Il en va de même en Sibérie orientale.

La chasse dans ces sociétés est donc une relation sociale. Il faut traiter la proie avec respect, ne pas chasser plus que nécessaire, respecter la dépouille, etc Les relations entre animaux d'une même espèce sont vues comme les relations entre les individus d'un même clan.
Manger des animaux revient donc en soi à une sorte de cannibalisme, ce dilemme, difficile à résoudre, nécessite un certains nombres de rite pour, en général, se faire pardonner. Surtout que les animaux peuvent se venger s'ils trouvent une conduite inacceptable. On emploie donc souvent des euphémismes pour parler de la chasse, ou bien l'on dira que la personne qui a mise un animal à mort est un fait d'un autre clan, ou encore on utilisera des masques pour cacher son identité et éviter des représailles ...

Les fêtes qui sont données après une chasse peuvent être vues comme des rites funéraires. L'aspect corporel n'étant qu'une enveloppe, le chasseur ne fait que prélever la chair de l'animal, qui continue à vivre tant que ses os existent. Il est d'usage de faire un petit tas des os et de les laisser en forêt pour que l'ame de l'animal puisse ensuite rejoindre le stock des animaux.
Il convient donc aussi de rendre de la chair aux animaux, ce qui peut se faire de différentes façons. Les morts humains sont en général laissé à la disposition des animaux sauvages, les petits animaux peuvent être recueillis, on peut aussi faire des offrandes.

Les premières descriptions de transes chamaniques datent de la Sibérie orientale à la fin du XVIIe siècle. La théorie diffusioniste laisse penser que cette "religion" se serait diffusée à divers endroit et qu'elles est la base des points communs que nous avons vus. Il faut objecter à celui que les rapports entre humains et non-humains étaient des rapports entre personnes, des rapports souvent individuels, le chaman n'ayant qu'un role limité voire négligeable. De plus, le dépassement des frontières de l'humain peut se retrouver aussi bien en Asie du sud-est et en Nouvelle-Calédonie.

En Nouvelle-Calédonie, par exemple, le *kamo* est un "prédicat indiquant la vie". Il ne définit pas un corps vivant, mais plutôt un ensemble de relations. Les autochtones voient donc l'introduction de l'individualisme par les colonisateurs comme l'arrivée du concept de corps plutôt que de celui d'esprit.

Il semble qu'il n'y a au'en Afrique (et en Europe) qu'on ne retrouve pas ce schéma.
Au travers des exemples de l'Inde brahmanique et du Japon, on peut voir que la disjonction de l'Europe moderne entre nature et culture n'est pas quelque chose qui vient avec la civilisation. En fait c'est même quelque chose d'assez peu courant dans le monde. La tendance est donc rarement à l'objectivation de la nature, à la création d'une sphère naturelle séparée de la sphère culturelle. On tend plutôt à accorder aux êtres non-humains des qualités humaines ce qui rend possible la communication.

## Chapitre 2 : Le sauvage et le domestique

### Espaces nomades

Les chasseurs-cueilleurs sont généralement en mouvement perpétuel. Ils parcourent un vaste territoire qui n'est pas considéré comme sauvage (par opposition à un domaine domestique qu'il faudrait définir). Ils savent et voient bien que le territoire a été subtilement modelé au fil des générations. L'opposition entre sauvage et domestique n'a pas grand sens, "la totalité de l'environnement parcouru est habité comme une demeure spacieuse et familière".

### Le jardin et la forêt

Revenons aux Achuar et à la forêt amazonienne. Il reste en général au même endroit entre 10 et 15 ans, puis doivent se déplacer car le gibier alentour s'amenuise et que les maisons ont besoin d'être reconstruite. Les Achuar ont des jardins (gérés par les femmes) qui peuvent contenir une centaine de plantes différentes, ce sont de très bons jardiniers.
De part l'organisation en village - jardin contre la forêt, on peut s'attendre à voir émerger une distinction domestique / sauvage.

Les plantes du jardin, hors mauvaises herbes, sont *aramu*, 'ce qui a été mis en terre'. Cela comprend des espèces domestiquées mais aussi des espèces simplement transplantées.
Les plantes dites *ikiamia* "de la forêt" sont aussi cultivées, mais par un esprit nommé Shakaim.
En somme, les Achuar ne font que prendre les plantes cultivées par Shakaim pour les planter dans leurs jardins, qui, dans leur structure, semblent copier la forêt.
Le couple *aramu* / *ikiamia* ne représente donc pas une différence entre domestique et sauvage mais entre cultivé par les Achuar ou par l'esprit.

La même argument peut être fait pour les animaux.

On peut voir les habitations des Achuar en cercle concentrique. D'abord la maison, puis le jardin cultivé par les femmes et terrain et jeu / chasse des enfants. Ensuite il y a la forêt. Dans un rayon d'une à deux heures de marche, la forêt est bien connue par tous, on y fait la cueillette et s'y promène. Au-delà c'est le terrain de chasse ou les hommes s'aventurent quotidiennement. Les femmes et enfants n'y vont qu'avec les hommes. Les chasseurs connaissent bien ce territoire et passent du temps à essayer de soustraire les animaux des esprits, en les séduisant et en les cajolant. Des résidences secondaires peuvent être construites dans cette forêt profonde, sortes de maisons de campagne.

Sur une plus longue durée, les anciens essarts (jardins cultivés) abandonnés restent deux fois plus riches que la forêt en général. Ces zones sont connues. Elles attirent les animaux et l'on peut ainsi dire que, même si ce n'est pas forcément voulu, une certaine partie de la forêt change sous les effets humains, même si ces changements sont imperceptibles à un oeil non averti.

En prenant des exemples de Nouvelle-Guinée on peut voir que l'argument précédent peut être valide dans des contextes très différents avec parfois une forte densité démographique et des jardins très denses.

### Le champ et la rizière

> "il est difficile de repérer en Chine, en Inde ou au Japon une dichotomie du sauvage et du domestique analogue à celle que l'Occident a forgée. Qu'une différence soit faite en Asie entre les lieux habités et ceux qui ne le sont pas, cela n'est guère surprenant ; mais que cette différence recouvre une opposition tranchée entre deux types d'environnement, deux catégories d'êtres et deux systèmes de valeurs mutuellement exclusifs, voilà qui parait plus douteux."

En Chine et au Japon par exemple, la montagne est le lieu inhabité ou, du moins, loin de la civilisation. Elle est associée à des puissances spirituelles, importantes dans la cosmologie. En Chine par exemple, ces forces sont liées au fengshi, et peuvent dicter certains choix dans les villes. La montagne et la ville font donc parties d'un tout.

### *Ager* et *Silva*

> "Est sauvage, on le sait, ce qui procède de la *silva*, la grande forêt européenne que la colonisation romaine va peu à peu grignoter"

En opposition, nous avons le domestique représenté par le *domus* :

> "milieu de vie, exploitation agricole à l'origine ou, sous l'autorité du père de famille et la protection des divinités du foyer, femmes, enfants, esclaves, animaux et plantes, trouvent les conditions propices à la réalisation de peur propre nature. Travaux des champs, éducation, dressage, division des taches et des responsabilités, tout concourt à ranger humains et non-humains sous un même registre de subordination hiérarchisée dont les relations au sein de la famille étendue offrent le modèle accompli."

Cette opposition se changera en le couple sauvage / civilisé par la suite. Et des gens comme Rousseau iront jusqu'à retourner les sens pour penser un sauvage bon, et un civilisé mauvais. Cela on le voit bien, par du principe que le sauvage existe en tant que tel, alors qu'il n'est présent que dans les yeux de celui qui se dit civilisé.
Par ailleurs, dans un contexte civilisé, une zone sauvage est nécessaire. Elle est le lieu notamment de la chasse (qui se rapporte à la guerre) et contraste avec le vivre-ensemble qui englobe les animaux domestiques.

### Paysage romain, forêt hercynienne, nature romantique

> "Rome se dégage peu à peu du modèle de chasse héroique pour ne plus voir dans la traque du gibier qu'un moyen de protéger les cultures. \[...\] Les animaux sauvages \[...\] sont des nuisibles."

Les "ressources" en gibier sont régulées de manière "rationelle".

C'est finalement cette vision romaine qui nous est restée, adoucie par la vision germanique de la campagne. C'est une vision européenne qui ne découle pas de tel ou tel avancée technologique, mais bien de la culture romaine. Et ce n'est que vers le XIXe siècle que la nature sauvage sera remise à l'honneur avec le romantisme.

## Chapitre 3 : Le grand partage

### L'autonomie du paysage

En peinture, il faut probablement attendre Durer en 1490 pour commencer à observer des paysages qui sont le sujet principal de l'oeuvre (et non plus l'arrière-plan d'une église). La peinture de la nature a beaucoup bénéficié de l'invention de la perspective linéaire. Cette perspective a d'ailleurs un rôle très important : elle rend objective (par des règles mathématiques) un point de vue particulier, celui de l'artiste, homme.
Notons que les croquis de Durer étaient restés sans réelle connaissance publique, il faut attendre Bruegel l'Ancien ou Savery (XVI/XVIIe) pour voir ce genre plus reconnu.
Les techniques artistiques vont se développer pour atteindre de hauts sommets. La représentation de la nature est aussi fonction des découvertes scientifiques de l'époque, comme l'invention du microscope (1590), du téléscope (1605), des avancées de Copernic, Galilée, Descartes, Newton ...

> "Car la dimension technique de l'objectivation du réel est bien sur essentielle dans cette révolution mécaniste du XVIIe siècle qui figure le monde à l'image d'une machine dont les rouages peuvent être démontés par les savants, et non plus comme une totalité composite d'humains et de non-humains dotée d'une signification intrinsèque par la création divine."

Et c'est ainsi que fut créée la distinction entre l'homme et la nature chez les Modernes. Et bien plus tard, cette distinction servira à délimiter le champ d'étude de l'anthropologie par exemple.

### L'autonomie de la *phusis*

La *phusis*, la nature, est d'abord vue comme la nature propre d'une chose, et non commme un ensemble différent de l'homme, la Nature.
En Grèce, on commence à avoir cette idée, parmi certaines élites, que le monde peut être compris, qu'il suit des lois régulières qui ne demandent qu'à être découverte (et non simplement des caprices divins). Aristote est un des grands penseurs de la Nature, comprise comme l'ensemble des êtres soumis aux lois naturelles. Pour lui, la Cité doit essayer de suivre au mieux les lois de la Nature (et ses éventuelles hiérarchies). On peut alors classifier les espèces en fonction de leur nature (leur essence, leur composition interne). Par exemple beaucoup d'espèces peuvent se déplacer, on peut donc les ranger suivant si elles ont des nageoires, palmes, pattes, ailes, ... Et à l'intérieur de ces catégories, on peut distinguer le degré de développement des organes moteurs.
Un pas crucial est alors franchi : les espèces sont décontextualisées, elles sont arrachées à leur milieu naturel et à leurs interactions.

### L'autonomie de la création

Pour Aristote, l'homme fait donc encore partie de la nature, il manque une étape pour arriver aux Modernes:

> "C'est au christianisme que l'on doit ce second bouleversement, avec sa double idée d'une transcendance de l'homme et d'un univers tiré du néant par la volonté divine. \[...\] De cette origine surnaturelle, l'homme tire le droit et la mission d'administrer la terre"

### L'autonomie de la nature

Le changement de l'idée de Nature a permis la révolution scientifique du XVIIe. La nature est vue comme un mécanisme soumis à des lois déterministes. L'homme est vu comme séparé de cette nature, comme le berger du troupeau. L'idée de nature humaine se développe en réaction et l'on finira, au XIXe par opposer nature et société.

### L'autonomie de la culture

Les premières définitions de la culture au XIXe siècle dans l'anthropologie naissante suppose que le progrès est continu chez tous les humains. Il y a donc une culture humaine (qui n'est pas différente du concept de civilisation) et l'on peut classer les peuples en fonction de leur degré de perfectionnement. Cette idée sera abandonnée notamment par Franz Boas qui voit dans les cultures (au pluriel) des réalisations particulières qui se valent.

### L'autonomie du dualisme

En anthropologie, on observe toujours une distinction entre nature et culture, la question étant de comprendre l'interaction entre les deux pour telle ou telle société. Certaines théories supposent que la culture est un produit de la nature (contraintes génétiques, géographiques), d'autres que la nature n'est qu'un "réservoir de signes et de symboles ou la culture vient puiser". Si certains essayent de trouver un compromis sur cet axe, il parait évident que c'est une situation précaire tant l'axe a peu de sens.

> "\[Le rôle de l'anthropologie\] est de comprendre comment des peuples qui ne partagent pas notre cosmologie ont pu inventer pour eux-mêmes des réalités distinctes de la nôtre, témoignant par la d'une créativité qui ne saurait être jaugée à l'aune de nos propres accomplissements."

Prenons l'exemple du surnaturel. On peut penser que, dans un monde gouverné par les lois, le surnaturel est la partie que l'on ne peut pas expliquer (des crues, des éclairs, ...) et à laquelle on prête des explications irrationnelles. Mais en réalité, il faudrait déjà que le peuple concerné connaisse le concept de nature, qu'il voit le monde comme gouverné par des lois, qu'il ait conscience que certaines explications sont moins 'rationelles' que d'autres (qu'il existe un concept de rationalité ...).

## Chapitre 4 : Les schèmes de la pratique
