# Homo Domesticus (James Scott) :

L'idée générale est de remettre en cause le rôle de l'Etat dans le développement des sociétés. En gros société nomade, anarchiste, contre société sédentaire, agricole et étatique.

La première chose est de dire qu'on ne sait pas situer le début des sociétés agraires et que leur développement a pris beaucoup de temps. De plus les premières sociétés sédentaires n'étaient pas forcément liées à l'agriculture dans un premier temps.
Le biais qui consiste à penser que l'Etat est incontournable, y compris dans l'histoire des civilisations vient en partie du fait qu'il est plus facile d'étudier les traces de civilisations qui ont laissé des ruines, des écrits, … plutôt que des sociétés de chasseurs-cueilleurs.

> " Une estimation généreuse nous amènerait à affirmer que jusqu’il y a environ quatre siècles, un tiers du globe était encore occupé par des chasseurs-cueilleurs, des cultivateurs itinérants, des peuples pastoraux et des horticulteurs indépendants, tandis que les États, essentiellement agraires, étaient largement confinés à la petite fraction des terres émergées propice à l’agriculture. Une bonne partie de la population mondiale n’avait sans doute jamais été confrontée à ce personnage emblématique de l’État : le collecteur d’impôts. "

> " Je suis pratiquement convaincu – et je ne suis pas le seul dans ce cas – que les maladies étaient un facteur fondamental de la fragilité des premiers États. "

**Sur les origines, la modification de l'environnement et l'agriculture** :

Le feu a été déterminant. On sait que chez certains « sauvages », il était utilisé pour façonné l'environnement et maximiser la cueillette et la chasse.

> " Par le biais d’une forme d’horticulture assistée par le feu, la flore et la faune désirables ont été relocalisées à l’intérieur d’un cercle restreint autour des campements humains, ce qui facilitait grandement la chasse et la cueillette. C’est ainsi que le « rayon du repas » a diminué : les ressources alimentaires étaient désormais plus proches, plus abondantes et plus prévisibles. "

La cuisson des aliments a également eu une importance capitale :

> " Cette décomposition chimique externe des aliments crus – sans laquelle les chimpanzés, par exemple, doivent avoir un intestin environ trois fois plus grand que le nôtre – a permis à Homo sapiens de se contenter d’une quantité bien moindre de nourriture et de dépenser beaucoup moins de calories afin d’en extraire la substance nutritive. "

**Sur la sédentarité** :

> " Pourtant, la sédentarité fut largement antérieure à la domestication des céréales et du bétail et a souvent persisté dans des milieux où la culture céréalière était faible ou inexistante. "

> " Le point de vue dominant selon lequel le processus consistant à « faire fleurir le désert » par le biais de l’agriculture irriguée est au fondement des premières communautés sédentaires de grande taille s’avère en réalité presque entièrement erroné. Comme nous allons le voir, les premiers grands établissements sédentaires sont apparus en zones humides, pas en milieux arides ; c’est dans leurs ressources naturelles, et non dans les cultures céréalières, qu’ils trouvèrent leurs principaux moyens de subsistance ; et ils n’avaient nul besoin de systèmes d’irrigation au sens conventionnel du terme. "

Exemple du détroit de l'Euphrate, alors extrêmement riche en faune et en flore. Les hommes s'y sont installés, se déplaçant légèrement en fonction des saisons mais en restant chasseurs-cueilleurs.

> " Les chasseurs mésopotamiens, de même que leurs congénères partout dans le monde, devaient l’essentiel de leur approvisionnement annuel en protéines animales à une semaine – ou à peine plus – d’efforts intenses consacrés à la capture du plus grand nombre possible de têtes de gibier migrateur. "

On commence à voir les grandes d'être sédentaire. Le choix des zones humides se justifie également par l'importance de la possibilité de transport par voie maritime.

> " Une dernière raison, plus hypothétique, de l’obscurité des sociétés des zones humides pourrait être leur incompatibilité écologique avec la centralisation administrative et le contrôle par le haut. Ces sociétés reposaient en effet sur ce que l’on appelle aujourd’hui des « biens collectifs » ou des « communs » – plantes, animaux et espèces aquatiques sauvages auxquelles toute la communauté avait accès. "

**Sur les débuts de l'agriculture** :

Ecart de 4000 ans environ entre la domestication des animaux sauvages et des céréales et la formation d'Etats (sociétés agropastorales).

Il fut une période où la plupart des sociétés cummulaient l'agriculture, l'élevage et la chasse / cueillette. En fait, il valait mieux assurer une diversité de sources d'alimentation pour faire face aux différents aléas climatiques etc.
Le type d'agriculture qui demande le moins d'efforts à l'humain : jeter des graines dans le limon fertile d'une fleuve (apporté par une crue).

La domus de l'éleveur. On choisit des espèces que l'on souhaite élever, ce qui attire nombres d'autres espèces autour de la domus. L'élevage tout comme l'agriculture fait évoluer les plantes et les animaux. Les plantes deviennent adaptées à un environnement aisé, sans mauvaises herbes etc et les animaux sont choisis pour leur docilité, les plus aggressifs étant abattus. Les animaux domestiqués ont donc muté génétiquement du fait du travail de l'homme.

> " Par rapport à leurs ancêtres sauvages, les moutons ont connu une réduction de 24 % de la taille de leur cerveau au cours des dix mille ans d’histoire de leur domestication"

Il y a aussi eu des changements physiques (cornes et mâchoires plus petites, moins de différences entre mâles et femelles, ... ).
Effet négatif du confinement sur la santé des bêtes, notamment forte mortalité chez les nouveau-nés. Augmentation de la fécondité spectaculaire.

Il semblerait que ce mode de vie nous ait nous aussi profondément changé. Cependant les preuves sont difficiles à avancer concernant notre cerveau. De plus il ne s'est passé qu'environ 200 générations depuis que l'agriculture est développée. C'est l'dée de l'auto-domestication, l'homme est lui aussi au service de son champ, de ses animaux.

Idée que la chasse-cueillette était beaucoup plus variée intellectuellement que la routine agraire.

> " Une fois qu’Homo sapiens a franchi le Rubicon de l’agriculture, notre espèce s’est retrouvée prisonnière d’une austère discipline monacale rythmée essentiellement par le tic-tac contraignant de l’horloge génétique d’une poignée d’espèces cultivées – en particulier le blé et l’orge en Mésopotamie. "

Petite note écologique intéressante :

> " le mergule nain mangeur de poissons est parvenu, en colonisant massivement le Groenland septentrional, à engendrer à partir de ses excréments une couche de sol suffisant à créer un habitat propice aux petits mammifères dont la présence a, à son tour, attiré des grands prédateurs tels que l’ours polaire. "

Il semble que devenir agriculteur est un très mauvais choix, alors pourquoi le faire ?

> " Pourquoi des cueilleurs dotés d’un minimum de bon sens auraient-ils opté pour l’énorme augmentation de la quantité de travail pénible exigée par l’agriculture sédentaire et les soins du bétail, à moins d’y être contraints par une menace létale ? Nous savons que même les chasseurs-cueilleurs contemporains, réduits à survivre dans des environnements pauvres en ressources, ne consacrent que la moitié de leur temps à ce que nous pourrions appeler un travail de subsistance. "

Agriculture → ville → épidémies, à cause de la densité de population et de la baisse de diversité génétique (« Plus leur similitude génétique est grande – moins ils présentent de variation –, plus forte est la probabilité que tous les individus d’un même groupe soient vulnérables au même pathogène. ») Pas mal des maladies viennent notamment de la proximité avec les animaux.

> " On ne surestimera jamais assez l’importance de la sédentarité et de la concentration démographique qu’elle a entraînée. Cela signifie que presque toutes les maladies infectieuses dues à des micro-organismes spécifiquement adaptés à Homo sapiens ne sont apparues qu’au cours des derniers dix millénaires et nombre d’entre elles depuis seulement cinq mille ans. Elles constituent donc un « effet civilisationnel », au sens fort du terme. Ces maladies historiquement inédites – choléra, variole, oreillons, rougeole, grippe, varicelle et peut-être aussi paludisme – n’ont émergé qu’avec les débuts de l’urbanisation et, comme nous allons le voir, de l’agriculture. "

Premiers Etats vers -3300, avec notamment Uruk. Hypothèse de Nissen : une grande sècheresse a favorisé le développement des Etats.

Pour la formation d'Etats, il y a un besoin d'excédent des récoltes pour nourrir les travailleurs qui ne faisaient pas parti du domaine de l'agriculture. Donc mise en place d'impôts sur le céréal en nature et besoin de terres suffisamment fertiles pour ce genre de pratique : loess ou alluvial : « sans alluvions, pas d'Etat ». En pratique : Tigre, Euphrate, Nil, fleuve Jaune (Chine), vallée de l'Indus.
Céréales : blé, orge, et millet pour la Chine.

De plus le transport fluvial et maritime a permis la facilitation d'échanges de marchandises indispensables à des Etats non-autosuffisants.

> " On peut illustrer ce contraste par la constatation frappante qu’en 1800 – c’est-à-dire avant l’invention du bateau à vapeur ou du chemin de fer –, il était pratiquement aussi rapide de se rendre en bateau de Southampton, en Angleterre, au Cap de Bonne-Espérance que d’aller en diligence de Londres à Edimbourgg. "

Lien entre État et céréale :

> " Je crois que la clé du lien entre l’État et les céréales, c’est le fait que seules ces dernières peuvent servir de base à l’impôt, de par leur visibilité, leur divisibilité, leur « évaluabilité », leur « stockabilité », leur transportabilité et leur « rationabilité ». "

Contrairement aux légumineuses, tubercules et autres plantes sur lesquelles jamais un Etat ne s'est construit.

Construction de murailles qui servaient à définir les limites du contrôle politique. Elles pouvaient aussi bien servir à se protéger des intrusions qu'à éviter une fuite des contribuables (comme dans le cas de la Grande Muraille de Chine).

L'Etat se construit également avec l'écriture qui permet de tenir des archives, de faire de la comptabilité, ce qui est indispensable quand la population grandit, si l'on veut la controler.

> " À l’origine, ni en Chine ni en Mésopotamie l’écriture ne fut conçue comme un moyen de représenter le langage. "

Pouvant être comprise à différents endroits où différents dialectes étaient parlés, l'écriture se veut universelle.
Elle a été développée par certains Etats très portés sur le recensement, la centralisation, ... (dynastie Qin (221-206 av JC), Ur III)

On note aussi une envie d'universaliser également les poids et mesures. De manière générale, voilà autant de façon pour l'État de recenser et de contrôler.

> " Il s’agissait désormais de créer un État centralisé plutôt qu’une simple cité-État se contentant de prélever un tribut occasionnel sur une constellation de villes satellites quasi indépendantes. "

> " Dans le contexte de rivalité militaire régionale avec les petits États concurrents, il était important de tirer le maximum de profit des ressources du royaume. "

> " L’une des caractéristiques de l’art de gouverner des premiers royaumes agraires était de maintenir la population sur place et d’empêcher tout mouvement non autorisé. La mobilité physique et la dispersion ont toujours été les fléaux du collecteur d’impôt. "

> " Si les États archaïques n’ont pas inventé l’irrigation et le contrôle des eaux, ils en ont étendu l’usage et ont construit des canaux visant à faciliter le transport et à accroître la surface des terres céréalières. "

> " Sous les Qin, cette importance de la question démographique est reflétée par le fait que non seulement l’État interdisait l’exode de ses sujets, mais aussi qu’il instituait une politique nataliste, avec des allègements fiscaux en faveur des femmes les plus fécondes et de leurs familles. "

> " La Troisième Dynastie d’Ur, avec sa logique d’enrégimentement extrême de la société, dura à peine un siècle, et les Qin seulement quinze ans. "

On note donc que les États misent sur les céréales et ont besoin d'hommes pour les cultiver. Ils extraient ensuite une partie de cette production via des taxes. Cela nécessite un certain travail de standardisation pour savoir qui possède quoi, par exemple. Enfin, ces conditions (en y ajoutant les maladies et les risques de famine lors de mauvaises récoltes) peuvent être difficiles pour la population qui n'a généralement pas trop de mal à s'enfuir.

> " Pourquoi toutes les diverses communautés de la périphérie ont-elles rejeté l’usage de l’écriture alors qu’un si grand nombre de cultures documentées par l’archéologie ont été exposées à la complexité de la Basse Mésopotamie ? On pourrait soutenir que ce rejet de la complexité était un acte conscient. Quelle en était la motivation ? \[...\] Peut-être que, loin d’être moins qualifiés intellectuellement pour affronter la complexité, les peuples de la périphérie furent suffisamment intelligents pour se soustraire pendant au moins cinq siècles de plus aux structures de commandement oppressives qui lui étaient associées, dès lors qu’elles leur étaient imposées par la conquête militaire. \[...\] Chaque fois, la périphérie a commencé par rejeter l’adoption de la complexité même après y avoir été directement exposée \[...\] et, ce faisant, elle a évité la cage de fer de l’État pendant un autre demimillénaire. " Algaze, « Initial Social Complexity in Southwestern Asia », p. 220-222 ; l’auteur cite C. C. Lambert-Karlovsky. Voir également Scott, Zomia, ou l’art de ne pas être gouverné

Avant l'apparition de l'Etat, les ressources nécessaires à la subsistance comme des champs étaient ouvertes à tous les membres de la communauté. La production était consommée et donc personne ne produisait plus que nécessaire ; le surplus, n'est nécessaire qu'aux Etats et à leur classe dirigeante non productrice.

> " C’était le principe directeur du colonialisme espagnol dans le Nouveau Monde, aux Philippines et ailleurs. Les reducciones, ces regroupements (souvent forcés) de populations autochtones autour d’un centre d’où rayonnait la puissance espagnole, étaient conçues comme faisant partie d’un projet civilisateur, mais elles remplissaient aussi la fonction, ce qui n’est pas tout à fait anodin, de servir et de nourrir les conquistadores. Les centres chrétiens d’évangélisation des populations dispersées – qu’ils fussent catholiques ou protestants – commencèrent de la même manière, en rassemblant une population productive autour de missions d’où rayonnaient les efforts de conversion. "

cf A. V. Chayanov

Une population importante était vitale pour ces premiers Etats, c'était là leur vraie richesse. Subissant une exode permanente, il leur fallait compenser, en annexant des communautés non-étatiques, en achetant des esclaves ou encore en faisant la guerre à d'autres Etats. Ces guerres concernaient principalement les petits Etats. Notons quand même que « l’esclavage n’a pas été inventé par l’État. »

> " On ne saurait surévaluer la centralité de la servitude, sous une forme ou une autre, dans le développement de l’État jusqu’à une époque très récente. Comme l’a observé Adam Hochschild, autour de 1800 encore, près des trois quarts de la population mondiale pouvaient être considérés comme asservis "

> " Si le but de la guerre était notamment l’acquisition de captifs, il est dès lors plus logique de considérer que ces expéditions militaires ressemblaient plus à des raids de trafiquants d’esclaves qu’à une guerre conventionnelle. "

Les esclaves proviennent principalement de la guerre.

> " À Uruk, la seule institution esclavagiste importante et documentée semble avoir été les ateliers textiles supervisés par l’État, qui employaient jusqu’à neuf mille femmes. "

> " D’après diverses estimations, Uruk aurait compté entre quarante mille et quarante-cinq mille habitants en l’an 3000 avant notre ère. Neuf mille ouvrières du textile auraient donc constitué à elles seules plus de 20 % de cette population, nombre qui n’inclut pas les autres prisonniers de guerre et esclaves travaillant dans d’autres secteurs de l’économie. L’approvisionnement de cette main-d’oeuvre servile et d’autres travailleurs dépendants de l’État en rations de céréales exigeait un formidable appareil d’évaluation, de collecte et de stockage "

> " Le principe du recours à une classe de serviteurs socialement déracinés – janissaires, eunuques, Juifs de cour – a longtemps été considéré comme une technique permettant aux dirigeants de s’entourer de personnel qualifié mais politiquement neutralisé. Mais au bout d’un certain temps, si cette population servile était nombreuse, concentrée et si ses membres étaient ethniquement apparentés, l’atomisation souhaitée ne jouait plus. Les nombreuses révoltes d’esclaves en Grèce et à Rome en sont le symptôme, même si la Mésopotamie et l’Égypte (du moins jusqu’à l’avènement du Nouvel Empire) ne semblent pas avoir connu l’esclavage à une échelle aussi vaste. "

Dans le monde gréco-romain, les mines, l'exploitation forestière, les équipages de rameurs étaient indispensables pour l'armée et les ambitions urbanistiques de l'Etat. Cela avait aussi l'avatange de se passer loin des villes, donc moins visible et moins de risque d'insurrection. De plus, on épargne aux populations ce genre de tâches ingrates qui pourraient les mener à la révolte.

> " Un signe indubitable de l’obsession démographique des États archaïques, que ce soit dans le Croissant fertile, en Grèce ou en Asie du Sud-Est, est le fait que leurs chroniqueurs ne vantaient que rarement leurs gains territoriaux. "

> " Trois raisons évidentes expliquent que les sociétés mésopotamiennes du troisième millénaire passent pour moins esclavagistes qu’Athènes ou que Rome : leur petite taille, le faible nombre d’archives qu’elles ont laissées derrière elles et leur territoire relativement réduit. Athènes et Rome étaient de formidables puissances maritimes qui importaient des esclaves des quatre coins du monde connu et presque toute leur population servile venait de sociétés lointaines non hellénophones et non latines. \[...\] Les cités-États mésopotamiennes, en revanche, raptaient leurs captifs beaucoup plus près de chez elles. De ce fait, ces derniers étaient plus susceptibles d’avoir d’importantes affinités culturelles avec leurs ravisseurs. Si l’on part de cette hypothèse, il est probable que les captifs, si on les laissait faire, s’intégraient plus rapidement à la culture et aux moeurs de leurs maîtres et de leurs maîtresses. Dans le cas des jeunes femmes et des enfants, qui étaient souvent les captifs les plus prisés, mariage mixte et concubinage ont sans doute contribué à occulter leurs origines sociales en quelques générations. "

> " Dans l’Empire néo-assyrien (911-609 av. J.-C.), déportation massive et réinstallation forcée étaient systématiquement imposées aux territoires conquis. La totalité de la population et du bétail de ces contrées étaient déplacés de la périphérie du royaume vers des régions plus proche du centre, où ils étaient réinstallés de force, généralement dans des colonies agricoles. \[...\] la déportation et la réinstallation forcée se caractérisaient par le fait que la majeure partie de la communauté captive voyait préserver son intégrité et était déplacée collectivement sur des sites où sa production pouvait être plus facilement contrôlée et accaparée. Là aussi, on voit fonctionner à plein l’appareil de centralisation de la main-d’oeuvre et des céréales, mais à une échelle massive, dans laquelle des communautés agraires entières étaient transformées en modules au service de l’État. Même si l’on tient compte des exagérations des scribes, l’ampleur de ces transferts de population était sans précédent et pour tout dire assez stupéfiant. Ainsi, plus de deux cent mille Babyloniens auraient été déplacés jusqu’au coeur de l’Empire néoassyrien. \[...\] Une fois réinstallés, avec le temps, et surtout s’ils n’étaient pas très éloignés sur le plan culturel, leurs membres pouvaient parfaitement devenir des sujets ordinaires, difficiles à distinguer d’autres sujets pratiquant l’agriculture. "

Caractéristique des États :

> " Nous savons que l’État n’a pas inventé l’esclavage et la servitude ; ceux-ci sont observables dans un nombre considérable de sociétés pré-étatiques. Mais ce que l’État a certainement inventé, ce sont des sociétés de grande taille reposant systématiquement sur le travail forcé et une main-d’oeuvre asservie. Même lorsque la proportion d’esclaves y était bien inférieure à celle d’Athènes, de Sparte, de Rome ou de l’Empire néo-assyrien, le rôle de la main-d’oeuvre captive et de l’esclavage était tellement vital et stratégique pour la préservation du pouvoir de ces États que l’on imagine mal qu’ils aient pu subsister longtemps en leur absence. "

**Sur les fragilités des États**

> " il ne fait aucun doute que c’est une logique de ruralisation plutôt que d’urbanisation qui a prévalu en Mésopotamie pendant plus de mille ans après la chute d’Ur III, apparemment en raison des incursions de peuples pastoraux "

> " il est indispensable de prendre en compte la vulnérabilité structurelle fondamentale du complexe céréalier sur lequel reposaient tous les États archaïques. La sédentarité a émergé dans des niches écologiques très spécifiques et circonscrites, en particulier, on l’a vu, les régions alluviales et les sols de loess. Plus tard – bien plus tard –, les premiers États centralisés ont eux-mêmes vu le jour dans des contextes écologiques encore plus circonscrits, caractérisés par une vaste superficie centrale de sols riches et bien arrosés et par la présence de voies navigables, seules à même de soutenir l’existence d’une grande quantité de sujets cultivant des céréales. En dehors de ces sites rares et propices à l’émergence de l’État, chasse, cueillette et pastoralisme continuaient à prédominer "

> " La première et la plus importante de ces vulnérabilités structurelles était leur très forte dépendance à une seule récolte annuelle d’une ou deux céréales de base. Si la moisson échouait à la suite de sécheresse, d’inondation, de parasites, de dégâts causés par les tempêtes ou de maladies des cultures, la population était en danger mortel, de même que ses dirigeants, qui dépendaient de l’excédent produit par leurs sujets. "

> " En Asie du Sud-Est, par exemple, il était rare qu’un royaume dure plus de deux ou trois règnes et sa survie était à la merci de toute une série de problèmes qui échappaient souvent à son pouvoir. "

Etat comme Uruk = nécéssite bcp de ressources, parfois qui viennent de loin (pierres précieuses, étain, cuivre, ...) ce qui a contribué à étendre le commerce sur des zones géographiques importantes, ce qui a favorisé le transport de maladies. De même avec la guerre qui a entraîné des mouvements de populations.

**En particulier sur les problèmes écologiques** :

> " Parmi les contraintes environnementales les plus susceptibles de menacer l’existence de l’État, deux, sur lesquelles on dispose de témoignages écrits depuis les temps les plus reculés, se sont révélées particulièrement importantes dans le monde antique : la déforestation et la salinisation "

> " En ce sens, les premières sociétés agraires étaient fortement dépendantes de l’apport des nutriments transportés vers l’aval par ces fleuves \[Tigre et Euphrate\] depuis des millénaires. Mais avec la croissance des centres urbains, et à mesure qu’augmentait la demande de bois d’oeuvre, de chauffage et de cuisson absent des zones humides de Basse Mésopotamie, ce processus entra dans une nouvelle phase. C’est ce dont témoignent les nombreux indices de déforestation des régions du cours supérieur de l’Euphrate en amont de Mari au début du troisième millénaire av. J.-C., déforestation due à une combinaison de surexploitation des ressources forestières et de surpâturage. Les États archaïques avaient un appétit dévorant de bois qui allait bien au-delà des besoins d’une communauté sédentaire même importante. En plus du défrichage des terres destinées aux cultures et au pâturage, du bois de chauffage et de cuisson, du bois de construction et du bois destiné aux fours à poterie, l’État archaïque avait besoin d’énormes quantités de bois destiné à la métallurgie, la fonderie, les fours à briques, la salaison des viandes, les galeries de mines, la construction navale, l’édification de grands monuments et la production d’enduit à chaux – cette dernière exigeant de très grands apports en combustible. Compte tenu des difficultés de transport du bois sur de longues distances, un centre étatique avait tôt fait d’épuiser les modestes réserves situées à proximité de son siège principal. Mais s’il était situé, comme presque tous les premiers États, au bord d’une voie navigable, généralement un fleuve, il pouvait tirer profit de la flottabilité du bois et du sens du courant afin d’exploiter les forêts situées en amont. "

> " Un signe presque infaillible qu’une cité-État a dû faire face à une pénurie de bois facilement accessible est la proportion de sa consommation de charbon. Bien que le charbon de bois fût indispensable aux activités exigeant de hautes températures telles la poterie, la fabrication de chaux et la fusion des métaux, il était rarement utilisé à des fins domestiques, sauf en cas d’épuisement des réserves locales de bois de chauffage et de cuisson. L’avantage spécifique du charbon de bois, c’est qu’il fournit beaucoup plus de chaleur par unité de poids et de volume que le bois brut et que son transport sur de longues distances revient donc moins cher. L’inconvénient, bien entendu, c’est qu’il doit être brûlé deux fois et consomme ainsi beaucoup plus de matière ligneuse. Ainsi, moins on avait de bois à portée de main, plus on avait recours à du charbon de bois venant de loin. "

Déforestation → risque d'érosion et d'envasement.

> " Comme les premiers États étaient établis sur des plaines à très faible gradient, leurs cours d’eau se déplaçaient lentement durant la majeure partie de l’année ; ainsi le limon tendait-il à se décanter lorsque le courant ralentissait. Si la cité-État dépendait fortement de l’irrigation, ses canaux commençaient à être asphyxiés par l’envasement, ce qui ralentissait encore plus le courant. Il fallait alors a minima mobiliser la population en corvées de dragage, sans quoi les champs cessaient de produire. "

> " La couverture forestière – il s’agissait essentiellement de chênes, de hêtres et de pins en Mésopotamie antique – avait pour effet de retenir l’eau des pluies de la fin de l’hiver et de diffuser lentement leur humidité par percolation à partir du mois de mai. Sous l’effet de la déforestation ou du défrichage agricole, le bassin versant relâchait beaucoup plus rapidement l’eau de pluie et le limon qu’elle transportait, ce qui engendrait des crues plus rapides et plus violentes "

> " Toutes les eaux d’irrigation contiennent des sels dissous. Comme les plantes ne les absorbent pas, ils finissent par s’accumuler dans le sol et, à moins que les canaux ne soient lessivés, par les tuer. Mais le lessivage des canaux n’est qu’une solution à court terme qui élève le niveau de la nappe phréatique et fait remonter le sel à la surface, où il pénètre les racines des plantes. Étant donné que l’orge tolère mieux le sel que le blé, une façon de s’adapter à la salinisation croissante consiste à planter de l’orge au lieu du blé, malgré la préférence générale pour ce dernier. Mais même avec de l’orge, si la nappe phréatique – et donc le sel – le rapproche de la surface, le rendement des cultures en est considérablement affecté "

**Sur le manque de moyens et de contrôle** :

> " Avec des animaux de trait et des chariots circulant sur une plaine alluviale, les États archaïques n’avaient guère la possibilité de réquisitionner les céréales au-delà d’un rayon d’environ quarante-huit kilomètres. "

> " les premiers États n’avaient qu’une connaissance assez rudimentaire de l’étendue réelle de leurs surfaces cultivées et de leurs rendements probables et effectifs, secteur par secteur, en blé et en l’orge. Même si l’État en savait beaucoup plus sur le noyau central que sur les régions périphériques, il courait toujours le risque de réquisitionner une trop grande quantité de céréales lors d’une mauvaise année, laissant ses sujets au bord de la famine. "

> " Pour reprendre l’expression d’un de mes collègues, ils étaient si malhabiles que « c’est comme si leurs mains n’avaient eu que des pouces ». "

> " incapacité à contrôler sur le terrain la cupidité de leurs propres collecteurs d’impôts "

tendance à exploiter le noyau central d'agriculteurs plutôt que la périphérie, car plus proche (moins de coûts de transports) et on connaît mieux ce qui s'y passe.

Egypte, même problème de surexploitation surtout que le Nil étant entouré de déserts, la population n'avait nul part où s'enfuir

**Sur les périodes sombres** :

> " Il existe des documentaires splendides et instructifs sur la Grèce archaïque, l’Ancien Empire égyptien et Uruk au milieu du troisième millénaire \[beaucoup de traces archéologiques\], mais on cherchera en vain un portrait des périodes sombres qui leur ont succédé : les « siècles obscurs » de la Grèce, la « Première période intermédiaire » de l’Égypte et le déclin d’Uruk sous l’empire akkadien. On pourrait pourtant fort bien soutenir que de telles périodes « creuses » ont en fait suscité un net gain de liberté pour de nombreux sujets des États antiques et une amélioration générale du bien-être humain. "

> " le bien-être d’une population ne doit jamais être confondu avec la puissance d’un centre étatique ou palatial. "

effondrement, chute d'un Etat etc, cela ne signifie pas (forcément) une perte de population ou de culture, mais une redistribution de celles-ci, une décentralisation.

> " l’abandon de l’État pouvait être vécu comme une émancipation. Il ne s’agit certainement pas pour autant de nier que l’existence en dehors de l’État était souvent à la merci de toutes sortes de violences et de prédations, mais plutôt d’affirmer que nous n’avons aucune raison de supposer que l’abandon d’un centre urbain entraînât, ipso facto, une plongée dans la brutalité et la violence. "

> " En guise de conclusion, je souhaiterais aussi mentionner brièvement un autre âge sombre méconnu et non documenté, à bonne distance des centres étatiques. À l’époque des premiers États, la majeure partie de la population mondiale était constituée par des groupes de chasseurs-cueilleurs dépourvus de structures étatiques. William McNeill a émis l’hypothèse que ces groupes ont sans doute subi une véritable saignée démographique au moment d’entrer en contact avec les maladies inédites engendrées par la concentration de la population dans les régions céréalières centrales – maladies qui, parmi les populations urbaines, étaient de plus en plus endémiques et donc moins meurtrièresc. Si tel fut bien le cas, il est possible que la plupart de ces peuples sans État aient péri sans que nous n’en sachions rien, faute de documentation ; l’histoire n’a pas enregistré leur trace, pas plus que celle des hécatombes épidémiologiques auxquelles succombèrent les populations du Nouveau Monde, frappées par des virus qui, souvent, se sont propagés à l’intérieur des terres bien avant l’arrivée des premiers témoins européens. Si nous ajoutons au bilan meurtrier de ces maladies la réduction en esclavage de nombre de ces peuples sans État, pratique qui s’est poursuivie jusqu’au XIXe siècle, nous sommes face à un « âge sombre » de proportions épiques ayant affecté des populations « sans histoire » passées inaperçues aux yeux de la « grande » Histoire. "

**L'âge d'or des barbares**

> " En 2500 avant notre ère, les tout premiers États de Mésopotamie, d’Égypte et de la vallée de l’Indus (comme Harappa) auraient été pratiquement invisibles à un observateur extraterrestre. Vers 1500 av. J.-C., environ, ce dernier aurait constaté l’existence de quelques centres étatiques supplémentaires (les Mayas et la région du fleuve Jaune), mais leur présence géographique globale avait en fait diminué. Même à l’apogée de l’Empire romain et des premiers « super-États » Han, cet observateur aurait vu que l’étendue de leur contrôle véritable était étonnamment modeste. Il aurait enfin remarqué que la population mondiale, tout au long de cette période (et vraisemblablement au moins jusqu’en 1600), est restée constituée en son immense majorité par des peuples sans État : chasseurs-cueilleurs, collecteurs de produits de la mer, horticulteurs, agriculteurs itinérants, pasteurs et un grand nombre de cultivateurs qui échappaient largement au contrôle administratif et fiscal d’un quelconque État "

> " la catégorie « barbares » renvoyait le plus souvent à des populations pastorales hostiles qui constituaient une menace militaire mais pouvaient, en certaines circonstances, être intégrées par l’État ; les « sauvages », en revanche, étaient perçus comme des bandes de chasseurs et de cueilleurs impropres à servir de matière première à la civilisation, et que l’on pouvait donc ignorer, tuer ou asservir. Lorsqu’Aristote décrivait les esclaves comme des « outils », il avait sans doute à l’esprit les « sauvages », et non pas tous les barbares (comme par exemple les Perses). "

> " Mais je souhaite insister ici sur le fait que la menace des barbares était peut-être le principal facteur ayant limité la croissance des États "
> " Nombre de ces peuples qui pratiquaient la razzia étaient d’ailleurs eux-mêmes semisédentaires : c’est le cas des Pachtounes, des Kurdes ou des Berbères, par exemple. "
> " Aux yeux de ces groupes mobiles, les communautés sédentaires constituaient un terrain idéal de prédation concentrée. "

> " Il me semble que l’on peut décrire la période qui s’étend entre l’émergence initiale de l’État et sa conquête de l’hégémonie sur les peuples sans État comme une sorte d’« âge d’or des barbares ». Ce que je veux dire par là, c’est qu’à bien des égards, la condition barbare était « meilleure » du fait même de l’existence des États. Tant que ceux-ci n’étaient pas trop puissants, ils constituaient des cibles alléchantes de pillage et de prélèvement de tribut. "

> " Mais du point de vue des barbares, le principal bénéfice de l’existence des États n’était pas tant leur fonction de sites de prédation que celle de comptoirs de commerce. Étant donné les limites de leurs ressources agroécologiques, les États dépendaient de toute une série de produits absents des plaines alluviales. Populations assujetties à l’État et peuples sans États étaient des partenaires commerciaux naturels. La croissance démographique et économique d’un État entraînait celle de ses échanges commerciaux avec les barbares des régions voisines. Le premier millénaire avant notre ère connut ainsi une véritable explosion du commerce maritime en Méditerranée, qui entraîna une croissance exponentielle du volume et de la valeur des échanges. "

> " Jusque vers 500 avant notre ère, soit jusqu’à l’apparition d’embarcations à voile de plus grande taille capables de transporter des cargaisons volumineuses sur de longues distances, le territoire de l’État était assez largement limité à son noyau céréalier. En revanche, la géographie et l’écologie des barbares sont beaucoup plus difficile à résumer ; il s’agit d’une ample catégorie englobant « tout le reste » et qui embrasse toutes les aires géographiques inadaptées à la formation de l’État. Les régions barbares les plus fréquemment mentionnées sont les montagnes et les steppes mais, dans les faits, toutes les régions difficiles d’accès, indéchiffrables, infranchissables et ne se prêtant pas à l’agriculture intensive pouvaient être définies comme barbares. Selon le discours de l’État, forêts denses et non défrichées, marécages, deltas fluviaux, tourbières, brandes, landes, déserts, étendues arides et même la mer appartenaient tous potentiellement à cette catégorie. "

> " Le « mur de la terre », un ouvrage défensif de deux cent cinquante kilomètres de long construit vers 2000 av. J.-C. entre le Tigre et l’Euphrate sur ordre du roi sumérien Shulgi, fut la première grande muraille de ce type \[frontière physique barbare / Etat\]. "

> " Si l’on en croit Christopher Beckwith, les nomades étaient souvent bien mieux nourris que les habitants des grands États agricoles et leur vie était moins dure et plus longue. On observait un flux constant de peuples qui fuyaient la Chine vers les royaumes de la steppe orientale où ils n’hésitaient pas à proclamer la supériorité du mode de vie nomade. De même, de nombreux Grecs et Romains rejoignaient les Huns ou d’autres peuples de l’intérieur de l’Eurasie, auprès desquels ils vivaient mieux et étaient mieux traités que chez eux13 "

> " Au VIe siècle de notre ère, les Goths se montrèrent tout aussi attirants que l’avaient été les Huns auparavant. Non seulement Totila (roi des Ostrogoths, 541-552) acceptait les esclaves et les colons romains dans son armée, mais il les incitait à se retourner contre leurs maîtres impériaux en leur promettant terres et liberté. « Ce faisant, il offrait aux classes subalternes romaines le prétexte à faire ce à quoi elles aspiraient depuis le IIIe siècle » : « se faire Goths pour échapper à leur situation économique désespérée ». "

> " Le fait que les peuples barbares qui sont restés dans l’histoire en raison de la menace qu’ils représentaient pour les États et les empires portent des noms distincts – Amorites, Scythes, Xiongnu, Mongols, Alamans, Huns, Goths, Dzoungars, etc. – laisse une impression de cohésion et d’identité culturelles souvent amplement démentie par la réalité. Ces groupes étaient tous des confédérations assez lâches de populations disparates brièvement rassemblées autour d’objectifs militaires et dès lors décrites comme des « peuples » par les États qu’elles menaçaient. Les peuples pastoraux, en particulier, avaient des structures de parenté remarquablement flexibles qui leur permettaient d’intégrer ou d’exclure des membres du groupe en fonction des pâturages disponibles, du nombre de têtes de bétail et des tâches à accomplir – y compris les tâches d’ordre militaire. À l’instar des États, ils étaient souvent en manque de main-d’oeuvre et fort enclins à incorporer réfugiés et captifs dans la structure de leurs lignages. "

> " Dans des circonstances prémodernes, et peut-être jusqu’à l’ère des canons, les armées mobiles des peuples pastoraux l’emportaient généralement sur les armées d’aristocrates et de paysans mobilisés par l’État. "

> " Le fameux dicton berbère selon lequel « les razzias sont notre agriculture », que je cite dans mon introduction, est lourd de sens. Il pointe, selon moi, une vérité essentielle sur le caractère parasitaire du pillage. Les greniers d’une communauté sédentaire représentaient parfois deux ans ou plus de labeur agricole que les pillards pouvaient s’approprier en un éclair. De la même manière, les enclos à bétail fonctionnaient comme des greniers vivants susceptibles d’être accaparés. Et comme le butin d’une razzia comprenait aussi généralement des esclaves qu’il était possible de libérer contre rançon, de garder ou de vendre, ces captifs formaient eux aussi une réserve de valeur et de productivité – entretenue à grands frais – pouvant être confisquée en une seule journée. Mais si l’on adopte une perspective plus ample, on peut aussi dire qu’un parasite venait ainsi en remplacer un autre, dans la mesure où les pillards saisissaient et disséminaient les biens accumulés sur un site concentré d’appropriation jusqu’alors exclusivement réservé à l’État. "

> " À la fin du deuxième millénaire avant notre ère, les États du bassin méditerranéen n’avaient pas tant à craindre les incursions en provenance des steppes et des déserts que celles qui venaient de la mer. Tout comme les milieux steppiques ou désertiques, la mer offrait aux prédateurs maritimes des opportunités uniques d’attaquer par surprise et de piller les communautés du littoral, voire, dans certains cas, de les conquérir. La très forte croissance du commerce méditerranéen offrait aussi aux nomades de la mer la possibilité de pratiquer la piraterie, tout comme les pasteurs s’attaquaient aux caravanes terrestres. "

On retrouve aussi des alliances avec des Etats et des logiques mafieuses chez les barbares.

> " Un exemple très éclairant d’arrangement politique de ce genre nous est fourni par l’interaction entre la dynastie des Han orientaux et ses voisins prédateurs nomades, les Xiongnu, aux environs de l’an 20 de notre ère. Les Xiongnu se livraient à des incursions éclairs et se réfugiaient dans les steppes avant que les troupes de l’État n’aient eu le temps de répliquer. Au lendemain de ces incursions, ils dépêchaient des émissaires à la cour, proposant la paix en échange de conditions avantageuses octroyées au commerce frontalier ou de paiements directs. L’accord était scellé par un traité censé faire des nomades des tributaires de l’Empire et mettant en scène les marques de leur allégeance en échange d’une contrepartie substantielle. Cette forme de tribut « inversé » destiné à acheter les nomades ne mobilisait pas moins d’un tiers des dépenses du gouvernement. "
> " Sept siècles plus tard, sous les Tang, l’administration impériale livrait chaque année aux Ouïghours un demi-million de rouleaux de soie dans des conditions similaires. Sur le papier, les nomades pouvaient passer pour des tributaires subordonnés à l’empereur Tang, mais le flux réel des revenus et des marchandises montre bien qu’il en était tout autrement en pratique. De fait, les nomades percevaient des pots-de-vin de la part des Tang en échange de leur non-agression. "

> " Avec le recul, on peut percevoir les relations entre les barbares et l’État comme une compétition pour le droit de s’approprier l’excédent du module sédentaire « céréales/main-d’oeuvre ». Ce module était en effet le fondement essentiel tant de la construction de l’État que du mode d’accumulation barbare. C’est lui qui en constituait l’enjeu principal. Le pillage ponctuel risquait de finir par éliminer sa proie, tandis qu’un racket de protection stable imitait la logique d’appropriation étatique et s’avérait compatible avec la productivité à long terme du noyau céréalier. "

Etat comme grandes sociétés barbares avaient un but, le commerce.

> " Au IXe siècle de notre ère, du fait de l’essor des liens commerciaux entre la Chine et l’Asie du Sud-Est, la chasse et la cueillette connurent une expansion explosive dans les forêts de Bornéo. D’aucuns affirment que l’île, jusqu’alors pratiquement inhabitée, connut un afflux de populations se consacrant à la collecte de produits sylvestres. Ces nouveaux venus espéraient tirer profit des débouchés commerciaux du camphre, de l’or, de l’ivoire de calao, des cornes de rhinocéros, de la cire d’abeille, des épices rares, des plumes, des nids d’oiseaux comestibles ou des carapaces de tortues. On pourrait aussi mentionner le phénomène bien plus tardif de la demande mondiale d’ivoire – destiné essentiellement à la fabrication de touches de piano et de boules de billard en Europe et en Amérique du Nord –, qui déclencha une myriade de guerres entre groupes tribaux aspirant à en contrôler le commerce et, surtout, provoqua l’extermination d’une bonne partie des éléphants. Le commerce des peaux de castors en Amérique du Nord en est un autre exemple. De nos jours, la demande des marchés chinois et japonais de racines de ginseng, de champignons chenilles ou de matsutake a fait de leur cueillette une activité commerciale qui n’est pas sans évoquer la ruée vers l’or du Klondike "

> " Les barbares, au sens large du terme, étaient sans doute en position idéale pour tirer profit de l’explosion des échanges commerciaux – et, dans bien des cas, pour y participer directement. En fin de compte, en raison de leur mobilité et du fait qu’ils chevauchaient plusieurs zones écologiques, ils constituaient une espèce de tissu conjonctif reliant entre eux une série d’États sédentaires à base céréalière. Avec l’essor de ces échanges, les peuples sans État plus ou moins nomades en vinrent à contrôler tout le réseau vasculaire du commerce et à prélever un tribut sur les échanges. Cette mobilité jouait un rôle probablement encore plus crucial dans le contrôle du commerce maritime en Méditerranée. D’après un archéologue, les nomades de la mer avaient sans doute été d’abord des marins qui participaient aux activités commerciales « légitimes » en louant leurs services aux royaumes agraires de l’époque. Mais au fur et à mesure de l’expansion de ces activités et des opportunités qu’elles offraient, ils se transformèrent en entités de plus en plus autonomes capables d’imposer leur domination politique aux régions littorales et de pratiquer le même type de prédation, de commerce et de prélèvement du tribut que leurs homologues continentaux. "

> " Peuples étatiques et peuples sans État, agriculteurs et cueilleurs, « barbares » et « civilisés » sont en réalité des jumeaux, tant au niveau réel qu’au niveau symbolique. Chacun de ces termes engendre son double. Cependant, malgré l’abondance des démentis apportés par l’histoire, les peuples qui se perçoivent historiquement comme appartenant au pôle prétendument plus « évolué » de chacun de ces couples d’opposés – sujets des États, agriculteurs, « civilisés » – définissent leur identité comme essentielle, permanente et supérieure. La plus contestable de ces dichotomies, celle qui oppose barbares et civilisés, repose en fait sur une gémellité originaire. "

> " Chasseurs, cueilleurs et cultivateurs itinérants pouvaient éventuellement grappiller les miettes de la richesse étatique, mais c’étaient les grandes confédérations politiques de pasteurs à cheval qui étaient le plus à même d’extraire les ressources des États sédentaires ; elles étaient elles-mêmes des « proto-États » ou, selon la formule de Barfield, des « Empires fantômes ». En ce qui concerne les cas les plus emblématiques, tels l’État itinérant fondé par Gengis Khan – le plus vaste empire terrestre d’un seul tenant de l’histoire – ou encore l’« Empire comanche » dans le Nouveau Monde, il est plus pertinent de les décrire comme des « États équestres ». "

> " Deux autres « solutions » étaient possibles, chacune d’elles éliminant de fait la dichotomie. La première était que les barbares nomades conquièrent l’État ou l’empire et se transforment en nouvelle classe dirigeante. Ce fut le cas au moins deux fois dans l’histoire de la Chine – avec la dynastie mongole des Yuan et la dynastie mandchoue des Qing ; ce fut aussi le cas d’Osman, le fondateur de l’Empire ottoman. Les barbares devenaient alors la nouvelle élite de l’État sédentaire, habitant la capitale et administrant l’appareil d’État. Comme dit le proverbe chinois, « on peut conquérir un royaume à cheval, mais il faut en descendre pour le gouverner ». "
> " La seconde option, bien plus fréquente mais moins souvent mentionnée, voyait les nomades devenir mercenaires de l’État, cavalerie chargée de surveiller les marches de l’empire et d’endiguer les incursions des autres peuples barbares. De fait, rares étaient les États ou les empires ne recrutant pas de troupes parmi les barbares, souvent en échange d’avantages commerciaux et d’autonomie locale. C’est essentiellement grâce à l’aide de troupes gauloises que César put pacifier la Gaule. Au lieu de conquérir l’État, les barbares intégraient les forces armées d’un État existant, à l’instar des Cosaques ou des Gurkhas, par exemple. "

> " On pourrait, je crois, définir une très longue période couvrant non pas des siècles mais des millénaires – entre l’apparition des tout premiers États et, vraisemblablement, l’aube du XVIIe siècle – comme un « âge d’or des barbares » et des peuples sans État en général. Pendant la majeure partie de cette longue période, le mouvement d’« enclosure politique » représenté par l’État-nation moderne n’existait pas encore. Ce qui prévalait, c’étaient la fluidité des circulations physiques, la perméabilité des frontières et des stratégies de subsistance mixtes. "

> " Nous n’avons guère besoin d’insister sur cette date quasi arbitraire. Le début du XVIIe siècle marque grosso modo la fin des grandes vagues d’invasions barbares eurasiennes : les Vikings du VIIIe au XIe siècle, le grand royaume de Tamerlan de la fin du XIVe siècle les conquêtes d’Osman et de ses successeurs immédiats. À eux tous, ils ont détruit, pillé et conquis des centaines d’entités politiques de toute taille et déplacé des millions de personnes. "

> " Avec l’essor des États et des Empires modernes équipés d’armes à feu, la capacité de prédation des peuples sans État et le contrôle qu’ils étaient susceptibles d’exercer sur les petits États voisins déclina plus ou moins rapidement selon les régions et leur géographie. "

> " En reconstituant systématiquement les réserves de main d’oeuvre de l’État grâce aux esclaves qu’ils lui livraient, ou bien en mettant leur savoir-faire militaire au service de sa protection et de son expansion, les barbares ont délibérément creusé leur propre tombe. "
