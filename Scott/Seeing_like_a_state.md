# Seeing like a state : how certain scemes to improve the human condition have failed (J.C. Scott)

## Introduction

> "much of early modern European statecraft seemed similarly devoted to rationalizing and standardizing what was a social hieroglyph into a legible and administratively more convenient format."

For Scott, many plans to improve the human condition have failed in tragic fashion : the Great Leap Forward, sovietic collectivization etc
For them to fail, he notes four main conditions :

- This top-bottom rationalization which provides the capacity for large-scale social engineering
- a high-modernist ideology, meaning a blind faith in scientific progress and in the power to model nature and social conditions accordingly. This ideology provides the desire for the change.
- an authoritarian state, provides the determination to act
- a society that lacks the capacity to resist these plans

> "I am \[...\] making a case against an imperial or hegemonic planning mentality that excludes the necessary role of local knowledge and know-how."

## Part 1: State Projects of Legibility and Simplification

### Chapter 1. Nature and Space

#### The State and Scientific Forestry: A Parable

In the early modern European state, there were two very different visions of the forest. People were using the forest products in a lot of different ways (fruits, branches, roots, sap, ...). The state was only interest in the revenue it could get from the forest. It replaces the differents sorts of timber by a "an abstract tree representing a volume of lumber or firewood".

This utilitarian discourse can be seen in the vocabulary :

> "Thus, plants that are valued become 'crops', the species that compete with them are stigmatized as 'weeds', and the insects that ingest them are stigmatized as 'pests'."

This logic continues with "timber", "underbrush", "livestock" ...

In Prussia and Saxony was developped scientific forestry between 1765 and 1800. Its goal was to produce the maximum constant volume of wood. To this end, mathematicians worked with the concept of Normalbaum, a standard tree, to calculate estimated revenues, and the foresters reproduced this abstraction in reality : large forest were turned into monoculture of same-age *normal trees*. No diversity, straight rows, easy management. These forests were also used as laboratories : their controlled environment make it easy to test different fertilizers and so on.
Most german forests were converted in the XIXth century. In the short run, it was a success for the state and became a popular forestry model everywhere in the world. It was of course a nightmare for the population which was deprived of a lot of natural resources.
After the second generation of trees, production was reduced significantly (20-30%). The soil became poorer and the lack of diversity became a lack of resilience. The german then tried to reintroduce some selected species to counter-balance the lack of diversity ... Boxes were installed for birds (instead of their natural tree habitat that were removed), some ant species were raised and reintroduced ...

#### Social Facts: Raw and Cooked

In, for example, absolutist France, the State had few information about the population so taxation was mostly uniform. Then, locally, people were only paying a part of the tax that was being asked. So the state had to make those local practices legible, to assimilate them into an administrative grid.

#### Forging the Tools of Legibility: Popular Measures, State Measures

Popular measures are local. They are meant to be understood easily by the locals so they refer to well-known time, distances, ... If everyone knows how much time it takes to cook the local rice, then a "rice cooking" is a perfectly valid measure for time.

> "The *pinte* in eighteenth century Paris, for example, was equivalent to 0.93 liters, whereas in Seine-en-Montagne it was 1.99 liters and in Percy-sous-Thil it was an astouding 3.33 liters."

Local measures also depend on the context : for clothes, a good measure can be the distance between the thumb and the elbow whereas when planting onions, hand-breadths are more useable.
These measures are also quite unprecise, because they usually don't need to be precise.

> "The cultivator who reports that his rice yield from a plot is anywhere between four and seven baskets is conveying more accurate information, when the focus is on the variability of the yield, than if he reported a ten-year statistical average of 5.6 baskets."

Another example can be seen with agriculture :

> "Telling a farmer only that he is leasing twenty acres of land is about as helpful as telling a scholar that he has bought six kilograms of books."

The local measurements could be the number of days of work necessary to sow or weed the land, the number of people a field could feed or the number of seed sown for example. A lot of information more useful than the area of a field.

> "Every act of measurement was an act marked by the play of power relations."

Indeed, measurements were especially useful for the payments of taxes between feudal lords and peasants. Instead of increasing the taxes, it was often wiser for the lords to just change the measures. They were indeed the providers of the baskets in which they got their task so it was easy to increase their size.

The State wanted to uniform measurements for legibility (for example food shortages was a huge concern so to know the resources / yields in all the provinces was important) and for control (to enforce a state tax uniformly for example).

Three factors helped to fulfill the State's project: The Enlightment spirit (because of the *réaction féodale* \[lords abusing their power\] which meant people wanted the State to react + idea to create one people), the growth of a global market and the Napoleonic State.

> "Even as late as 1828 the new measures were more a part of *le pays légal* than of *le pays réel*."

#### Land Tenure: Local Practice and Fiscal Shorthand

The main sources of income of the pre-modern state were taxes on land and commerce.

Rules on land were highly local, complex and plastic. Land was managed by the community and the different families and the situation could change rapidly in case on crises (food shortages).

Codification of these complex local codes seems impossible. However nowadays, the law enforces land rules at the State level. This code is relatively simple compared to the local reality of a few centuries ago, even though it takes trained bureaucrats to decipher it.

> "The crowning artifact of this mighty simplification is the cadastral map."

In France, there was a law, mostly in the XIXth century whose aim was to tax houses depending on their size. The measure which was decided upon was not the area but the number of doors and windows. Peasants then started to renovate their houses with fewer windows. The State, losing money, increased the tax, and the health problem for the population lasted more than a century.

### Chapter 2. Cities, People, and Language

Medieval cities were pretty chaotic in appearance. In reality they were pretty legible to their inhabitants and virtually unfathomable to strangers.

> "Delivering mail, collecting taxes, conducting a census, moving supplies and peple in and out of the city, putting down a riot or insurrection, digging for pipes and sewer lines, finding a felon or conscript \[...\], and planning public transportation, water supply. and trash removal are all made vastly simpler by the logic of the grid."

> "The Enlightment fostered a strong aesthetic that looked with enthusiasm on straight lines and visible order."

Of course, these plans of regular cities are only made at toy's scale, they are designed to facilitate the authority's work and don't take into account the needs or the reality on the ground. More than that: the new, planned city is not a populated city, it is engineer's work.

#### The creation of Surnames

In order to have a legible population, names are important. The State needs them in order to identify people and keep information on them (have they payed their taxes ??).

> "Many of the subjects whose "surnames" appear in the documents were probable not aware of what had been written down, and, for the great majority, the surnames had no social existence whatever outside the document."

Names were usually inspired by the place of living, the profession or some physical characteristics. They became mandatory for all the administrative paperwork, and so were more used with the gradual increase of state power.

#### The Directive for a Standard, Official Language

The same argument can be made for languages. They are strongly related to a culture, a community. The fact that French was imposed in Brittany and Occitanie for example imposes a hierarchy of cultures.

#### The Centralization of Traffic Patterns

Traffic (routes and railways) were also concentrated in Paris. The goal was to link Paris to provinces, again to make them more legible, to ease the access of the state (Paris) to control those regions.

#### Conclusion

The State needs simplifications in order to rule directly because it cannot comprehend all the subtleties of local lives.

> "Indirect rule required only a minimal state apparatus but rested on local elites and communities who had an interest in withholding resources and knowledge from the center. Direct rule sparked widespread resistance and necessitated negotiations that often limited the center's power, but for the first time, it allowed state officials direct knowledge of and access to a previously opaque society."

> "An illegible society, then, is a hindrance to any effective intervention by the state, whether the purpose of that intervention is plunder or public welfare."

> "The aspiration to such uniformity and order alerts us to the fact that modern statecraft is largely a project of internal colonization, often glossed, as it is in imperial rhetoric, as a 'civilizing mission.' The builders of the modern nation-state do not merely describe, observe, and map; they strive to shape a people and landscape that will fit their techniques of observation."

## Part 2: Transforming Visions

### Chapter 3. Authoritarian High Modernism

> "What is high-modernism then? It is best conceived as a strong (one might even say muscle-bound) version of the beliefs in scientific and technical progress that were associated with industrialization in Western Europe and in North America from roughly 1830 until World War I. As its center was a supreme self-confidence ans technical knowledge, the expansion of production, the rational design of social order, the growing satisfaction  of human needs, and, not least, an increasing control over nature (including human nature) commensurate with scientific understanding of natural laws."

#### The Discovery of Society

> "Society became an object that the state might manage and transform with a view toward perfecting it."

It seems like for the state, society is becoming an object along side the forests and the industry. Industry was evolving tremendously rapidly in the XIXth century, so why progress couldn't be applied to society as well. All of this was of course a very authoritarian way of thinking.

#### The Radical Authority of High Modernism

While old practices were coming from everyday life, myths, religion ... some started thinking: science is now giving great results in many fields, why not in social field! This has one main implication which is that only those with scientific knowledge are eligible to ruling the society.
This was also eclipsing politics and ideology. Scientific solutions are seen as apolitical. All political ideologies could adopt high-modernism.

#### Twentieth-Century High Modernism

Times of crises (war, depression, colonization) are perfect for high-modernist planning as the population may not be able to oppose it and can even sense the need for it.
Individual people or groups of people are then seen as tools, machines. In the industry, Taylor uses people as tools to do minute-timed repetitive operations. In the army, there is a need to plan inputs (food, arms) in order for the system to work ...

Scott presents Lenin as being influenced by Rathenau, German's great economic planner during WWI, and Taylor. He was of course against the use of Taylor's organization to exploit workers but found the idea very interesting scientifically speaking and wanted to study it to apply it to Russia. We can see how state planning technologies were interesting for high-modernist figures, whatever their political side.

High-modernism has been opposed for several reasons. One is the idea that some realms were private and that the state shouldn't interfere in them (see Foucault's work on health, sexuality, mental illness, ...). Another point is that democratic processes can bring some local point of views which might oppose planning.

### Chapter 4. The High-Modernist City: An Experiment and a Critique

Le Corbusier is a perfect example of modernist urbanism.

#### Total City Planning

His projects were immodest, big, extravagant. He planned entire cities for millions of inhabitants.

> "None of the plans makes any reference to the urban history, traditions, or aesthetic tastes of the place in which it is to be located. The cities depicted, however striking, betray no context; in their neutrality, they could be anywhere at all."

Le Corbusier loved geometry, straight lines, order, grandeur and bird's eye views. These were for him a precondition to efficiency. And efficient his creations were not particularly.

> "the great architect was expressing no more, and no less, than an aesthetic ideology"

He was into simplicity and stardardization: he wanted one design to be universal and answer the needs of everybody anywhere in the world. He was also a strong advocate for functional separation.

Basically, urbanism is a problem, and Le Corbusier can provide its (unique) solution, the truth, because he is a genius.

#### Brasilia: The High-Modernist City Built - Almost

Brasilia was planned in the late 1950s. The project of the popular leader of Brazil, Kubitschek, was to create a new capital for the country from scratch. Architects influenced by Le Corbusier imagined this grandiose city, based on a "monumental axis" and respecting one of Le Corbusier's principle: "death to the street".

> "Brasilia was designed to eliminate the street and the square as places for public life."

And indeed, streets and squares for social life are inexistant. The only square is so large that it's not convivial (it dwarves the Red Square or Tiananmen). Public life is restrained to places designed for this purpose (theatres, concert halls, restaurants, ...). Streets are for motorized traffic.

It was also a utopian city in which modern healthcare and sanitation were provided:

> "Twenty-five square meters of green space per resident reached the UNESCO-designed ideal."

Brasilia is a city without crowd:

> "it is almost as if the founders Brasilia, rather than having plaaned a city, have actually planned to prevent a city."

And life in Brasilia isn't fun:

> "the daily round in bland, repetitive, austere Brasilia must have resembled life in a sensory deprivation tank."

But, of course, Brasilia didn't go exactly as planned. The workers, who were suppose to live in the city, started squatting additional space and demanded public services.

> "In the end, by 1980, 75 percent of the population of Brasilia lived in settlements that had never been anticipated, while the planned city had reached less than half of its projected population of 557,000"

But, of course, Brasilia didn't go exactly as planned. The workers, who were suppose to live in the city, started squatting additional space and demanded public services.

> "In the end, by 1980, 75 percent of the population of Brasilia lived in settlements that had never been anticipated, while the planned city had reached less than half of its projected population of 557,000."

> "The unplanned Brasilia - that is, the real, existing Brasilia - was quite different from the original vision. Instead of a classless administrative city, it was a city marked by stark spatial segregation according to social class."

#### Le Corbusier at Chandigarh

Le Corbusier was invited by Nehru to finish the plans of Chandigarh, the new capital of Punjab, and created something similar to what we just laid out.

#### The Case Against High-Modernist Urbanism: Jane Jacobs

*The Death and Life of Great American Cities*, Jane Jacobs, 1961.

One of her critique of modernist planners is that visual order doesn't guarantee functional order. Order should be understood as a purpose not aesthetic.
For Jacobs, order, social order, comes from a dense life in the streets, in trust between the inhabitants.

> "Where Le Corbusier began with formal, architectural order from above, Jacobs begins with informal, social order from below."

> "The absence of trust is a disaster to a city street. Its cultivation cannot be institutionalized. \[...\] An urban space where the police are the sole agents of order is a very dangerous place."

Safe, resilient neighborhood are then the results of crowded streets with a diverse population, the contrary of segragated one-function neighborhoods.
Walking in the street has no one purpose, it may be for shopping be also saying hi so neighbors, just to have a walk or to go somewhere. Hence the neighborhood must have a diversity of purposes / activities.

So a city cannot be a work of art because it has to be concrete, and planning risks damaging the very complex social tissue. For Jacobs, the development of a city is analogous to the development of a language. People keep making evolving it despite state's attempts to control and centralize it.

### Chapter 5. The Revolutionary Party: A Plan and a Diagnosis

> "Lenin's design for the construction of the revolution was in many ways comparable to Le Corbusier's deisgn for the construction of the modern city."

#### Lenin: Architect and Engineer of Revolution

Lenin's views (especially in books like *What is to be done?*, 1903) seem high-modernist to Scott. But we shall not forget that Lenin was on opportunist, easily understanding population's mood and being able to adapt its strategy accordingly.

Lenin wanted to rely on a vanguard party, a group of people bringing revolutionary politics to otherwise disorganized proletarians. It is the brain of the "masses", who are the body. The force ans spontaneity of the working class is needed, but more than that it should be concentrated to achieve the revolution.

> "The party is to the working class as intelligence is to brute force."

He viewed the vanguard party as trained army officials trying to organize inexperienced recruits alreading combatting. The term he chose, "the masses", seem to refer to a chaotic crowd which definitely needs the theoritical analysis of scientific socialism. It needs it because otherwise it will just reproduce bourgeois ideology. To be able to produce a socialist model, one must therefore have a solid theoretical training (the vanguard party) and it cannot accept criticism (which would be deviance to bourgeois ideology).

> "Another metaphor occasionally replaces those of the army and classroom in Lenin's discourse. It is the image of a bureaucratic or industrial enterprise in which only the executives and engineers can see the larger purposes of the organization."

The politics is then reserved to the socialist intelligentsia, the professional revolutionaries, which is not even politics anymore but a scientific discourse, and absent from the masses' sphere.
One of Lenin's worries was that the vanguard party was influenced by the proletariat. For him, the intelligentsia should elevate, shape the proletariat without being influenced by it. It is a scientific project from above which should shape the raw material.

In pratice, in 1917, the Bolsheviks succeeded to an empty throne and played a marginal role in the Revolution. They however had a huge impact in the aftermaths of October, fighting the Whites as well as cracking down on all independent sources of power (e.g. the soviets).

*State and Revolution* was written by Lenin in august and september 1917. As a turning political point, he prefers encouraging popular revolt to destabillize the provisional government than explaining how mass should be controlled. This is only a a tactical shot. When he describes his vision for Russia in this book, he affirms once again the necessity of state power to lead the masses.

Lenin was inspired by the newest technologies. For him, small family farms should disappear and be replaced by large-scale production with the use of electricity.

#### Luxemburg: Physician and Midwife to the Revolution

Rosa Luxemburg was assassinated in 1919 in Berlin "at the behest of her less revolutionary allies on the left". She was a strong critic of Lenin although she was also a Marxist so shared a common ideological ground with Lenin.

> "For Luxemburg, the party might well be more farsighted than the workers, but it would nevertheless be constantly surprised and taught new lessons by those whom it presumed to lead."

Luxemburg has the instinct that no revolution can be planned because they are a too complex process. For her, a strike for example, will imply a change in work conditions and the relations between workers and their chiefs leading hopefully to a new strike and so on. The revolutionary process changes the proletariat which changes the process.
Luxemburd had faith in the proletariat's creativity and thought it was "unrealistic and morally distateful" to turn it into some obedient army.

> "Lenin comes across as a rigid schoolmaster with quite definite lessons to convey - a schoolmaster who senses the unruliness of his pupils and wants desperately to keep them in line for their own good. Luxemburg sees that unruliness as well, but she takes it for a sign of vitality, a potentially valuable resource; she fears that an overly strict schoolmaster wil destroy the pupils' enthusiasm and leave a sullen, dispirited classroom where nothing is really learned."

She view political processes of uprisings as organic phenomenon, which, in the end, could barely be influenced let alone controlled. And for her, after the revolution, many new problems of organization arise and that experience, tests and creativity provided by a healthy and free working class could solve. This is her vision of the 'dictatorship of the proletariat', the rule of the whole proletariat and of course not the vision of Lenin and Trotsky of an elite ruling as dictators.

#### Aleksandra Kollontay and the Workers' Opposition to Lenin

Kollontay was Luxemburg's equivalent among the Bolsheviks. She criticized Lenin and the Party in the same way before being silenced. She was especially critic of the fact that the Pary viewed unions as merely instruments to give orders to the workers, in a one-way communication.

> "Kollontay proposed that a host of specialists and officials, who had no practical factory experience and who had joined the party after 1919, be dismissed - at least until they had done some manual labor."

Because you simply cannot decide something correctly on a subject you have no pratical knowledge on.

## Part 3. The Social Engineering of Rural Settlement and Production

> "Most states, to speak broadly, are 'younger' than the societies that they purport to administer. States therefore confront patterns of settlement, social relations, and production, not to mention a natural environment, that have evolved largely independently of state plans."

In precolonial Southeast Asia, the main problem for the State was to fix a population which was very prone to flee if conditions became too harsh. The Thai kingdom even tattooed people.

The state-controlled spaces are located in the valleys and agriculture is priviledged whereas the mountains are usually populated by non-state people. Stateless people, have always been considered a threat to states, and if possible, they were settled (in what was nearly concentration camps) in villages.

For more information on Southeast Asia see *The Art of Not Being Governed* by J.C. Scott

### Chapter 6: Soviet Collectivizatiom, Capitalist Dreams
