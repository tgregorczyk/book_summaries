# Zomia, The Art of not being governed, J.C. Scott

swidenning: agriculture sur brulis. Burn a part of the forest, grow something on it and then leave.
*padi*: malay word for rice plant

## Preface

Zomia: Southeast Asian mainland massif, at the periphery of nine states (including Vietnam, China, Cambodia, Laos, Thailand, Burma).

> "I argue that hill peoples are best understood as runaway, fugitive, maroon communities who have, over the course of two millennia, been fleeing the oppressions of state-making projects in the valleys - slavery, conscription, taxes, corvée labor, epidemics, and warfare."

Scott argues that all the cultures of these peoples are made of political choices. They are barbarians as neighboring states have told over centuries, they have chosen to live as they do, in order to keep states "at arm's length".

![Zomia_1](Zomia_1.jpg)
![Zomia_2](Zomia_2.jpg)

## Chapter 1: Hills, Valleys, and States. An Introduction to Zomia

Scott starts off with geographical records of the region of interests. They are described by "state people" like impassible jungles and mountains. For Scott, what will be important is to compare the people from the states, the valley people, or downstream people, and the people from the mountain / jungle, upstream people. It appears that for most of its history, especially in Southeast Asia, people lived in small bands based on kinship:

> "living in the absence of state structures has been the standard human condition."

> "The founding of agrarian states, then, was the contingent event that created a distinction, hence a dialectic, between a settled, state-governed population and a frontier penumbra of less governed or virtually autonomous peoples. Until at least the early nineteenth century, the difficulties of transportation, the state of military technology, and above all, demographic realities placed sharp limits on the reach of even the most ambitious states."

Scott compares this frontier to an homeostatic device: "the more a state pressed its subjects, the fewer it had."

States only in the last century have tried to conquer hill peoples. Albeit there have been many different types of states in the region, this goal was always present which "suggests that such projects of administrative, economic and cultural standardization are hard-wired into the architecture of the modern state itself."

> "In mainland Southeast Asia, where the first states appear only around the middle of the first millennium of the common era, their mark on the landscape and its peoples is relatively trivial when compared with their oversized place in history books. Small, moated, and walled centers together with their tributary villages, these little nodes of hierarchy and power were both unstable and geographically confined. To an eye not yet hypnotized by archaeological remains and state-centred histories, the landscape would have seemed virtually all periphery ad no centers."

> "What we know of the classical states such as Egypt, Greece, and Rome, as well as the early Khmer, Thai, and Burmese states, suggests that most of their subjects were formally unfree: slaves, captives, and their descendants."

For states, surrounding "barbarians" were at the same time trading partners (having resources not found in valleys), possible human resources (captives), but also a threat. They were "fiscally sterile" and would even raid settled communities (the state did encourage agriculture for being easy to pick...), or even overthrow states.

Those who fled the states took refuge in the highlands creating shatter zones with incredible cultural, ethnic and linguistic diversity.

> "Most, if not all, the characteristics that appear to stigmatize hill peoples - their location at the margins, their physical mobility, their swidden agriculture, their flexible social structure, their religious heterodoxy, their egalitarianism, and even the nonliterate, oral cultures - far from begin the mark of primitives left behind by civilization, are better seen on a long view as adaptations designed to evade both state capture and state formation. They are, in other words, political adaptations of nonstate peoples to a world of states that are, at once, attractive and threatening."

On the power of the grain:

> "Fixed-field grain agriculture has been promoted by the state and has been, historically, the foundation of its power. In turn, sedentary agriculture leads to property rights in land, the patriarchal family enterprise, and an emphasis, also encouraged by the states, on large families. Grain farming is, in this respect, inherently expansionary, generating, when not checked by disease or famine, a surplus population, which is obliged to move and colonize new lands. by any long-run perspective, then, it is grain agriculture that is nomadic" and aggressive, constantly reproducing copies of itself, while, as Hugh Brody aptly notes, foragers and hunters, relying on a single area and demographically far more stable, seem by comparison "profoundly settled"."

Zomia: 2.5 million square kilometers, 80 to 100 million inhabitants. The name is coming from several Tibeto-Burman languages in which it means highlander. 200m to 4000m altitude.

> "As late as 1740, it took no more time to sail from Southampton to the Cape of Good Hope than to travel by stagecoach from London to Edinburgh."

It seems that altitude is a key factor here. Up in the hills, religions, languages, and much more change dramatically than what we can observe in the valleys. It feels like there is another world in the heights.

> "Distinctions of status and wealth abound in the hills, as in the valleys. The difference is that in the valleys they tend to be supralocal and enduring, while in the hills they are both unstable and geographically confined."

> "Their political structures are, with extremely rare exceptions, imitative in the sense that while they may have the trappings and rhetoric of monarchy, they lack the substance: a taxpaying subject population or direct control over their constituent units, let alone a standing army. Hill polities are,  almost invariably, redistributive, competitive feasting systems held together by the benefits they are able to disburse. When they occasionally appear to be relatively centralized, they resemble what Barfield has called the 'shadow-empires' of nomadic pastoralists, a predatory periphery designed to monopolize trading and raiding advantages at the edge of an empire."

Scott's argument: the history of Zomia is one of runaways hence the culture of its peoples "can be best understood as a technique to make good the evasion". This can of shatter zones if a state effect: each state-making project created such a zone where some people fled.

And as such, the history of valley and hill peopled are intertwined and cannot be written separately. In scholar research, what we find is usually that the history of hill people is associated with the one of valley people, but the inverse is not true. Great civilization are often talked about in a self-contained way, without periphery nor the need to have one.

> "Many valley people are, as it were, 'ex-hill people', and many hill people are 'ex-valley people'. \[...\] Depending on the circumstances, groups have disengaged themselves from a state, and then, later, sought to affiliate themselves (or been seized by!) the same or another state. A century or two later, they might again be found outside the state's grasp, perhaps because they had moved away or perhaps because the state in question had itself collapsed. Such shifts were accompanied by a shift in ethnic identity, broadly understood. I will argue for a radically 'constructionist' understanding of the so-called hill tribes of mainland Southeast Asia."

Whereas the hill people are often called 'barbaric' or 'uncivilized', as if they just didn't take the train of progress (namely agriculture), these marginalized people were born at the same time as the state peoples, because they are its effects. Scott gives a similar examples from Morocco with the opposition of the Arabs and tribes of Berbers in the Atlas mountains.

> "Such tribalism is politically marginal. It knows what it rejects." (Gellner)

It is also quite interesting that although the first valley states were quite short-lived and represented a very few percentage of the population of the local area, history concentrates on them. For Scott, there are two main reasons for that: first, because states tend to be more present in the archaeological record, and second, because this history can be useful for contemporary nationalist stories.

It is important also to mention that for a state, the main goal is to extract wealth, and to achieve that, the most crucial resource is human power. But until recently a valley state had no 'hard' power beyond a very limited circle around the court. This means that it wasn't able to extract wealth beyond a limited circle, and "only during the dry season". However, its 'soft' power, made from cosmologies, costumes, rituals, religion and so on traveled way further.

> "One challenge for a non-state-centric history of mainland Southeast Asia consists in specifying the conditions for the aggregation and disaggregation of its elementary units. The problem has been succinctly put by one observer of a somewhat comparable flux between states and their autonomous hinterlands: 'There comes a time when one realizes that one is dealing, really, with molecules which sometimes unify in the form of a vague confederation, sometimes, just as easily, disaggregate. Even their names offer no consistency or certainty.' If the fluidity of the molecules themselves is an inconvenience for anthropologists and historians, imagine the problem it poses for the dynastic official or would-be state-builder, the colonial official, and the modern state functionary."

## Chapter 2: State Space: Zones of governance and appropriation

The importance of wet-rice cultivation cannot be understated. It permits (and requires) high-density population and is legible for appropriation, both important for state creation. And yes, the state rarely did create this kind of agriculture, it usually predated it.

Another important point is that maps we are accustomed to, representing places according to their relative distance is not really relevant as far as pre-19th century (or often even at later periods) societies are concerned. The important parameter is travel time. And as such societies trading via the sea were much closer to one another than mainland / hills societies. We can also note that the time to go from a point A to a point B can depend on the direction, one could go uphill more slowly than downhill for instance.
Two examples could be the Mediterranean Sea with its numerous trading partners, which although not on the same continents had lots of relations. Another example is the one of the Malay 'state' which actually more a coalition of trading ports which cultural influence ran from Swahili speaking Africa to Easter Island in the Pacific (15-16th century).

Scott goes on explaining how States were constrained by geography.

> "The necessary, but by no means sufficient, condition for the rise of a substantial state was the existence of a large alluvial plain suitable for the cultivation of irrigated rice and hence capable of sustaining both a substantial and concentrated population."

> "Because of the generally north-south direction of mountain ranges and major rivers in the region, virtually all of the classical states were to be found along the great north-south river systems."

Political power is limited by these geographical constraints:

> "As a rule of thumb, hilly areas above three hundred meters in elevation were not a part of 'Burma' proper. We must therefore consider precolonial Burma as a flatland phenomenon, rarely venturing out of its irrigation-adapted ecological niche."

We might think of the region as having many centers of power, their reach decreasing with 'distance' to the center. A metaphor for this might be light bulbs with varying intensity representing the political reach of each 'state'. This implies that several regions could be under one or many rulers, but we might note also that the power of each center varied a lot with years passing. Furthermore one important parameter was seasonal weather:

> "The traditional period for military campaigns in Burma was from November to February; it was too hot to fight in March and April, and from May through much of October it was too rainy."

## Chapter 3: Concentrating Manpower and Grain: Slavery and irrigated rice

### The State as Centripetal Population Machine

It seems that manpower was really precious to a state, even more than land in our context. States tried to lure people in by giving them lands, by feasting. They also 'acquired' manpower through slavery, raids, and wars. People of power were often called by how many men they control ('Lord of Thousand Men') instead of classical European 'Lord of this place'.
Manpower is useful because in these years, war and defense boiled down to how many men were fighting on each side. Moreover, one needs men to cultivate rice.

### The Shaping of State Landscape and State Subjets

"In a premonetary setting", states were not interested in gross domestic product but more in "state accessible product":

> "the premodern state attempts to so arrange its subjects and to sculpt the landscape around it in order to make it a legible field of appropriation"

creating the "paddy-state".
The padi rice indeed has the ability to concentrate man and grain. It is legible and fixes people in space.

"Monoculture fosters uniformity at different levels." The value of lands is relatively the same, the rhythm of production doesn't change.

> "The uniformity in the field, in turn, produced a social and cultural uniformity expressed in family structure, the value of child labor and fertility, diet, building styles, agricultural ritual, and market exchange."

For Scott, this can also be seen in collective farms of the ex-USSR, in "postbellum US South": agriculture was not meant to be highly productive but models of legibility and appropriation.

### Eradicating Illegible Agriculture

The major alternative to grain cultivation was and is shifting cultivation. This method has been attacked by states for centuries because it produced non-legible source of food, inaccessible to the state. Many different reasons were invoked, one example is:

> "Perhaps the most long-standing violent campaign against shifting cultivation, designed to force the people who practice it into what amount to concentration camps around military bases or, failing that, to force them over the border into Thailand, is the Burmese military regime's campaign against the Karen. Armed columns are sent out before swidden fields are harvested to burn the grain or to beat it down and to lay land mines in the fields."

### E Pluribus Unum: The Creole Center

It seems that manpower was so important to these padi states that state culture developed around this concept. If the goal was to concentrate as much men as possible, then the culture had to be open to include everyone. Population was often polyglot and had different religions and ethnical background because it was composed of peasants, slaves, merchants, ...

> "The manpower imperative was, everywhere, the enemy of discrimination and exclusion."

### Techniques of Population Control

Manpower was necessary to the state and couldn't be achieved without slavery so all these states were slaving states. Raids can be described as

> "the systematic removal of captives from nonstate spaces, particularly the hills, in order to deposit them at, or nearby, state spaces."

It was possible to become a slave to pay a debt or as a punishment for criminal offense but

> "the majority of slaves, here and elsewhere, appear to come from culturally distinct hill populations and to have been taken in slave raids as prizes of war."

The slave market was so lucrative that some hill peoples took part in it. Scattered hill peoples were hunted like wild cattle.

> "As is so often the case with a major commodity, slaves became virtually the standard of value by which other goods were denominated. A slave in the Chin Hills in the late nineteenth century was worth four head of cattle, a good gun, or twelve pigs. 'Slaves were current coin in the hills, and passed from hand to hand as easily as a bank-note in more civilized regions.' The tight association between hill peoples and the social origin of most slaves is strikingly indexed in the fact that the terms for slaves and hill peoples were often interchangeable."

These histories of abduction are still quite present in the culture of hill peoples:

> "When Karens want to make their children behave, they scare them by saying that a Thai will come and carry them off."

Note that the raiding armies were not really organized or bureaucratized. Loot was a personal objective of soldiers. For example, only

> "elephants, horses, arms and ammunition were reserved for the Burmese crown."

Men, cattle, gold were the property of soldiers. These contributed to their status.

#### Fiscal Legibility

> "An efficient system of taxation requires, first and foremost, that the objects of taxation (people, land, trade) be made legible."

The states therefore tried keep valid registers of these objects. They also tried to enforce laws to prevent land and people from changing too much, which would make registers obsolete. So people were often allowed to move only with a permission.

> "Both the Thai and Burman crowns, in the era before internal passports and identity papers, hit on the device of tattooing much of the male population to indelibly mark one's status."

But at that time, control was not working very well, many things were not legible. And those subjects to taxes tried as much as possible to avoid or reduce them as much as possible. Nobles omitted to declare land and property, commoners fled. The only solution to this loss for the crown was then to start raiding.

#### State space as self-liquidating

The core population, in the plains, was the main source of income through taxation for the crown. However a part of this population worked for the nobility so the other was often highly pressurized by taxation. The state could kill its income with these high taxes if it couldn't prevent exodus.

To conclude, padi states were fragile, they needed to constantly engage in raids to ensure a certain level of manpower. The also needed to extract resources as much as possible from the population without making them flee or rebel. This required a work on legibility and a fine level of information which these states couldn't have.

## Chapter 4: Civilization and the Unruly

For Scott, the classical "civilizational discourse" is still present nowadays with concepts like "development, progress and modernization". And it still persists while it has been known for ages that state ("civilized") and non-state ("uncivilized") peoples are complementary, they form one economic unit and borrow many things from each other.

### Valley State, Highland Peoples: Dark Twins

The states dictate what is begin civilized and what is not. For padi states, living in altitude is being barbarian. For Muslim states it is being nomadic:

> "In civilizational terms, nomadism was to the Arab state what elevation was to the padi state."

Being *legible* is embedded in the concept of civilization. People that move too often or who doesn't produce for the state are marginalized.

> "For the Burmese, as for the Chinese, itinerant people were civilizationally suspect by default."

The forest still has a bad image nowadays in Burma, people associated with it are seen as violent, and to compared to wild animals.

States define the metric for what civilization is. Peripheral peoples are more or less civilized according to how much their culture is similar to the center's.

> "As Richard O'Connor observes in the Tai context, 'Mainlanders link religion to agriculture agriculture to ritual and ritual to ethnic identity. When hill farmers like Karen, Lawa, or Kachin take up valley wet-rice, they find its proper cultivation requires Tai rituals. In effect, agricultural choices are bundled into ethnic whiles that function as competing agro-cultural complexes. A pragmatic shift between complexes thus begins ritual adjustments that may end as an ethnic shift.' O'Connor writes 'ethnic shift' but as he might have written 'religious shift', inasmuch as the two are inseparable in this context."

### The Economic Need for Barbarians

Most of the time, states and hinterland peoples needed each other for trade. It can be said that states couldn't thrive without these partnerships.

> "For mainland Southeast Asia states, particularly smaller states in or near the hills, the same symbiosis between hill and valley prevailed, though it might not be so neatly mapped onto a single watershed. It is no exaggeration to say that the prosperity of such states was largely dependent on its capacity to attract to its markets the products of the surrounding hill peoples, who often outnumbered the population at the state core."

The hill peoples provided a very broad array of goods, lots of which were part of the global luxury market. And valley states were in fact quite dependent from having these useful uncivilized neighbors.

### The Invention of barbarians

The states created the frontier between them and "barbarians", by physical means, walls (especially China), and cosmological means, creating a universal story for the state which ends up right at the hills.
Religion played an important role to create a unified culture and civilization, eg Islam for Malay.

### The domestication of borrowed finery: all the way down

About Cambodia and Java:

> "Sanskritized personal and place names were substituted for the vernacular. Monarchs were consecrated by magical Brahminical rites and given mythical genealogies tracing a divine origin. Indian iconography and epics were introduced, along with the complex ceremonies of South Indian court life. It appears that Sanskritization did not penetrate very deeply into lowland cultures beyond the immediate precincts of the court."

Borrowing the culture of already important states were a mean to be part of that history by creating a charismatic center of a region. And by "elevating" themselves, state leaders created the barbarians, against them.

We can note that the great states of the time (Han-Chinese, Indic, Burmese) were often source of inspiration for little states. They did act as source of inspiration.

### The civilizing mission

The barbarians are defined by what they lack compared to the civilizations that surround them. Civilization can then be expressed as the image of incorporating the barbarians into the "luminous, magnetic center". This is even easier if peripheric peoples are like state ones, just a bit underdevelopped.
And it was in fact usual that hill peoples were considered as ancestors, "'unpolished gems' capable, if carefully shaped and burnished, of becoming fully civilized".

> "As in any colonial or imperial setting, the experience of the subject was wildly at odds with the ideological superstructure that aimed at ennobling the whole entreprise/"

> "Following a Hmong/Miao rebellion in northern Thailand in the late 1960s, General Prapas not only deployed all the counterinsurgency techniques at his disposal - including napalm and aerial bombing - but undertook to 'cilivize' the rebels with schools, resettlement, clinics, and sedentary agricultural techniques."

### Civilization as a rule

Distinction civilized / barbarians in the words "cooked" and "raw".

> "While all 'primitives' were presumptively raw, not all developped barbarians were cooked."

In the end, begin cooked is being subject to the state. Note that different ethnic groups were created to designate people from the state and other peoples. But it is more a political distinction than a real culture one.

### Leaving the State, Going over to the Barbarians

Bennet Bronson's definition of a barbarian:

> "simply a member of a political unit that is in direct contact with a state but that is not itself a state."

This means that the barbarians were often quite advanced technologically and sometimes more powerful than their neighboring states. They were actually weakening their neighbors through raiding.

Through the political lens of states, acculturation was only ever one-sided: from raw to cooked. However it appears that all states always had a barbarian periphery which could be seen as a temptation for some states subjects. Migration towards the barbarians was pretty common.

## Chapter 5: Keeping the State at a Distance: The Peopling of the Hills

### Other Regions of Refuge

Scott takes some examples from other historical and geographical periods where we can also observe some peripheric zones settled with people running away from big central powers. Examples include but are not limited to South America with refuges from the Inca empire and after that the spanish invaders, North America with refuges from the French and English colons and their local allies or refuges from the contour of the Mediterranean Sea trying not to get captured as galley slaves.

Another example is the Tengger highlands (Java). Its inhabitants were Hindu fleeing the islam state and its Dutch allies. What is interesting here is that they didn't really keep the distinguishing Hindu features such as castes, courts and an aristocracy. Instead:

> "Their distinct tradition, despite its Hindu content, is culturally encoded in a strong tradition of household autonomy, self-reliance, and an antihierarchical impulse."

### The Peopling of Zomia: The Long March

We start we the description of Chinese expansion which puched many people to Zomia:

> "Between 1700 and 1850 some three million Han settlers and soldiers entered the southwest provinces, swelling the Han proportion of the twenty million inhabitants of this zone to 60 percent."

(Han "=" chinese)

People fled south and in the mountains and adapted to their new environment. Han settlers ended up mixing with the population and started to see hill tribes as under-developped and unfitted for civilization.

### The Ubiquity and Causes of Flight

This subchapter reminds us that most hill tribes settled 'recently', they were by no means descendents of dark ages people but refugees from the State. Reasons for fleeing include:

- escape from corvée labor,
- wars ("States make wars and wars - massively - make migrants"). Wars include conscription (and people fleeing conscription), desertion, raids on villages nearby the army's way, and of course the dispersion of the loser's side ...
- raids. Hill peoples were often raiding valleys (principally to capture people). Some racket-cum-blackmail partnerships took place were hill peoples protected the valley while extracting a tribute. But competition between hill peoples could lead to attacks of other protected groups, retaliation, and vast depopulated areas.

> "The wet-rice valleys and the level plains of the typical valley state are not merely topographically flat; they can also be thought of as having been culturally, linguistically, and religiouly flattened. The first thing that strikes any observer is the relative uniformity of valley culture compared to the luxuriant diversity of dress, speech, ritual, cultivation, and religious pratice to be found in the hills. This relative uniformity is, to be sure, a state effect."

- As far as religion is concerned we can see the same pattern: a religion orthodoxy at the core, and heterodoxy in the hills.

> "As Zomia became a place of refuge for lowland rebels and defeated armies, so also it became an asylum for banned religious sects. \[...\] Zomia came to resemble something of a shadow society, a mirror image of the great padi states - albeit using much of the same cosmological raw material."

- Epidemics: due to the highest population density and proximity with animals (pigs, ...), a lot more zoonotic diseases appeared within states and led to catastrophic epidemics. Living in the hills was safer in this respect. On the same note States relied heavily on agriculture which could fail due to droughts or crop diseases which could led to famine. Hill people had several sources of food.

Except from the hills, swamps and marshes were also historically places of "low-stateness". They could also be more dangerous to the states than hills because of their potential proximity to the state's core. It was not uncommon for these places to be drained. We could also talk about creeks, mangroves.

Finally Scott ends this chapter by saying that one of the most important characteristic of people living in these non-state regions is precisely the refusal of the state. Geoffrey Benjamin proposed the term *dissimilation* to propose the "creation of cultural distance between societies". As an appetizer is presented the case of the Akha:

> "the Akha insist that they believe in no higher god and that, literally, they do not bow their head to anyone."

## Chapter 6: State Evasion, State Prevention: The Culture and Agriculture of Escape

For Scott, if one were to design a place at the opposite of padi-state, one would end up with Zomia.

> "an agro-ecological setting singularly unfavorable to mapower - and grain - amassing strategies of states."

This strategy is state-repelling in two ways, first it makes states less likely to be interested in conquering this territory (as it would not be easy to extract grain and manpower) and second, it makes the rise of an indigenous state unlikely.

So for Scott, the social arrangement of Zomia is a deliberate choice.

### An Extreme Case: Karen "Hiding Villages"

The context is the war the Burmese state is waging against the Karen. In what they called "peace villages", people are registered, given ID cards, and crops are registered as well, for taxes. As these villages are often near the front, people are being exploited and plundered. Villages are sometimes unified to concentrate manpower.

On the contrary, fleeing Karens scatter in the hills and rely and foraging in order to leave as few traces as possible.

### Location, location, location, and mobility

> "Inaccessiblity and dispersal are the enemies of appropriation. And for an army on the march, as for a state, appropriation is the key to survival."

> "It does not follow however, that the extreme forms of dispersal are the safest. To the contrary, there is a small minimum group size below which new dangers and disavantages loom."

They are basically linked to the hability for the group to defend itself.

basically, as the title goes, choice of location and mobility are keys.

### Escape Agriculture

The classical narrative of progress goes like : foraging / hunting-gathering -> pastoral nomadism -> horticulture / shifting cultivation -> sedentary fixed-field agriculture -> irrigated plow agriculture -> industrial agriculture. And the same idea goes for social structures : small bands -> hamlets-> villages -> towns -> cities -> metropolises.

This is the kind of story the state is telling about itself. There is no way back, you can only get more and more integrated into the state.

> "Its stages of civilization are, at the same time, an index of diminishing autonomy and freedom."

This narrative is wrong. Some counter-examples include people would fled something, disease, raids, slavery. Their main defensive system was mobility and so agriculture has to fit this lifestyle by being hardly seized and destroyed, so roots and sometimes crops.

> "Shifting cultivation is the most comme agricultural practice in the hills of mainland Southeast Asia."

> "Shifting cultivation was a fiscally sterile form of agriculture: diverse, dispersed, hard to monitor, hard to tax or confiscate. Swiddeners were themselves dispersed, hard to monitor, hard to collect for corvée labor or conscription. The features that amde swidenning anathema to states were exactly what made it attractive to state-evading peoples."

The choice of agricultural pratice is political. In some areas it is easier to go with one rather than another of course. But most importantly, rice agriculture can be subject to state appropriation, swidenning is not. Maybe is rice agriculture 'more efficient' but it comes with corvée labor and taxes.

> "It is not uncommon for shifting cultivators to plant, tend and encourage as many as sixty or more cultivars. \[...\] Add to this the fact that mearly all swidden cultivators also hunt, fish, and forage in nearby forests. By pursuing such a broad portfolio of subsistence strategies they spread their risks, they ensure themselves a diverse and nutritious diet, and they present a nearly intractable hieroglyphic to any state that might want to corral them. This is a major reason why most Southeast Asian states were reduced to capturing the swiddeners themselves and removing them forcibly to an already established, state space."

> "In general, roots and tubers such as yams, sweet potatoes, potatoes, and cassava/manioc/yucca are nearly appropriation-proof. After they ripen, they can be safely left in the ground for up to two years and dup up piecemeal as needed."

Escape agriculture was transformed with the introduction of New World crops such as maize and cassava. They originally were disease-resistance, had a good nutritional value, were tasty and could be grown at higher elevations

> "it is important to recall that no matter how isolated a hill people or maroon community was, they were never entirely self-sufficient."

Hence they traded 'trade crops' at local markets to get cotton, coffee, tobacco, tea and opium.

### Social Structures of Escape

Tribes, and small social structures of the hills, are moving political constructs, choices that evolve with time and according to their powerful neighbors. A Berber slogan says "Divide that ye be not ruled".

For example, to be leaderless is good because it prevents the state from having an interlocutor. And this is why states have always tried to create chiefdoms in these regions.

> "Stable, indirect rule of anarchic 'jellyfish' tribes was well nigh impossible."

> "Such populations do not so much change identities as emphasize one aspect of a cutural and linguistic portfolio that emcompasses several potential identities. The vagueness, plurality, and fungibility of identities and social units have certain political advantages; they represent a repertoire of engagement and disengagement with states and with other peoples."

this diversity

> "provide a kind of practical experience, or praxis, in several forms of social organization. A mixed portfolio of subsistence techniques yields a mixed portfolio of social structures that can easily be invoked for political as well as economic advantages."

The British tried to created a chief in the 'democratic' Chin, and "sponsored lavish community feasts" which were very popular. In reaction, they became less popular but continued on an individual level, as long as they were not in order to augment a chiefly status.

So a response to theses state attempts is egalitarianism, or a simulacrum of chiefly authority (to please a state) but without any real power.

Scott notes that if hill political choices were deeply influenced by states, that was reciprocal.

The main escape route from states where to flee, not to rebel in Zomia:

> "Flight, not rebellion, has been the basis of freedom in the hills; far more egalitarian settlements were founded by runaways that by revolutionaries."

Small pretexts (like not liking a chief or a neighbor, or inauspicious signs, or even a dream) could lead for a household to go live elsewhere, so these peoples have a good praxis of mobility.

> "The utter plasticity of social structure among the more democratic, stateless, hill peoples can hardly be exaggerated. Shape-shifting, fissioning, disaggregation, physical mobility, reconstitution, shifts in subsistence routines are often so dizzying that the very existence of the units beloved of anthropologists - the village, the lineage, the tribe, the hamlet - are called into question"

## Chapter 6.5: Orality, Writing, and Texts

Scott wants to continue the same argumentation as before but for literacy. Maybe hill peoples are not *preliterate* but *postliterate* in the sense that they lost (while fleeting for example) are even let behind writing and embraced orality.
Scott notes that even in towns, writing was more an elite than popular thing.

### Oral Histories of Writing

Hill peoples often have the same kind of history on why they do not write.

> "the poeple in question once did have writing but lost it through their own improvidence, or would have had it had they not been cheated of it by treachery."

But if most hill peoples were padi-state peoples before fleeing, why didn't they keep writing?

### The Narrowness of Literacy and Some Precedennts for Its Loss

> "in all premodern societies the vast majority of the population was illiterate and ived in an oral culture, inflected though it was by texts."

> "Not only was \[literacy\] confined to a tiny elite, but the social value of literacy, in turn, depended on a state bureaucracy, an organized clergy, and a social pyramid where literacy was a means of advancement and a mark of status. Any event that threatened these institutional structures threatened literacy itself."

So few people knew how to write, and this was mostly useful in order to gain status because writing was mostly used for (tax) records or by the clergy. It is quite clear then that it was not of practical use for hill peoples.
Writing is important for states:

> "The elementary form of statecraft if the population list and household census: the basis for taxation and conscription."

From Lévi-Strauss: "\[Writing\] seems rather to favor the exploitation than the enlightment of mankind." So:

> "To refuse or to abandon writing and literacy is one strategy among many for remaining out of reach of the state."

Furthermore, orality can have several advantages. First it is inherently more democratic than writing because most people can tell tales. Second, it is less fixed in time and can evolve, it cannot be 'serve as the gold standard of orthodoxy.'

> "From the moment a spoken text (a particular performance) is frozen in writing as preserved speech, it effaces music and dance, audience reaction, bodily and facial expression - any one of which might be essential to its original meaning."

This means that for oral stories, there are no original, there are only performances. This is true for all stories includind history.

> "With the community's implicit agreement, details extraneous to the present slipped away from legend to be replaced by newly relevant elements that were incorporated as ancestral lore, thus rendering the past continually meaningful."

But oral traditions could also be learnt word for word and passed on for centuries pretty much unchanged. Or they can be changed to justify some political choices, some alliances ...

> "The Berbers are said to be able to construct a genealogical warrant for virtually any alliance of convenience necessary to politics, grazing rights, or war."

Note that these arguments are also valid for written texts. A text about genealogy, politics, alliances, history of a group is not necessarilly more accurate than an oral tradition. It is also emphasizing certain points, forgetting others. But this time, the tradition is written into stone and it is more difficult to change it. This kind of stories is of course associated with states, which choose them. Remember that winners are writing history.

A more radical approach has been taken by the Lisu, who decided to not have a history at all! The common practice is to forget it. This means that there is no ground for domination of certain lineages over others for example, as there is no genealogy. This also

> "leave no space for the active role of supra-household structures, such as villages or village clusters in ritual life, social organization, or in the mobilization of people"s attention, labor or resources."

## Chapter 7: Ethnogenesis, A Radical Constructionist Case

### The Incoherence of Tribe and Ethnicity

In the beginning of the XXth century, the British tried to carry on a census of Burma. When they got to the hills, it became a hell. The distinction between 'tribes' (which they also called races) was not easy, and they went with the idea that language defines cultural groups with similar history. But in the hills, people are always bilingual or trilingual and a small village can have easily 5-6 native languages.

> "Some of the races or 'tribes' in Burma change their language almost as often as they change their clothes."

In fact it seems that an identity (which is sometimes only defined by outsiders) cannot always be defined with specific traits. Being a Karen for example could mean very different things in different places. Also some people could be Karen at home and behave like Thais at the market or change their cultural identity overnight if needed.
Finally identity differences could be very smooth between villages so no clear demarcation could be established. And these identites often changed with time ...

For Scott, identity must be a political choice. Indeed fleeing a state is changing its status from peasant for a state to being in a small stateless community. Identity then is defined vis-à-vis neighboring states and hill peoples.

### State-Making as a Cosmopolitan Ingathering

We have also to note that states being made mostly of slaves (by raiding) of people attracted to this center, the population is basically ex-hill peoples. Having the identity of the state was in fact pretty easy, the state needing manpower it coudn't be too demanding on the cultural identity of the population. This made the population of padi-states pretty cosmopolitan and rapidly evolving.

> "This process of petty state-making, of turning hill people into valley people, could, it seems clear, just as easily be reversed."

### Valleys flatten

Uniformity of valleys religiously, linguistically, ethnically are pretty much the opposite of hills. It seems that the uniformity / diversity follows from the political structure (one state meaning uniformity, several villages meaning diversity.)

A case can be made for agriculture uniformity inducing some cultural uniformity. People grow crops in the same way, they have the same problems to manage water for irrigation, they end up having the same diet.

### Identities: Porosity, Plurality, Flux

Ethnic identities were also plural in the hills. People describe themselves depending on their interlocutor and can name themselves in a variety of ways. Hill people are also multicultural in the sense that it is pretty easy for them to change their identities and to fall in a certain ethnic group. This can be a choice made out of opportunities. In this sense, it is very hard to categorize them.

### Radical Constructionism: The Tribe is Dead, Long Live the Tribe

> "'Tribes' in the strong sense of the word have never existed. By 'strong sense', I mean tribes as conceived as distinct, bounded, social units."

Tribes are basically inventions of states.

> "States and empires create tribes precisely to cut through the flux and formlessness that characterize vernacular social relations."

To be more concrete, if one were to draw maps with say hair cut styles, languages, religion, clothing, rituals, mode of subsistence, form of villages, political institutions etc then these maps never really match one another. There are always gradients (smooth or sharp) and it is really difficult to draw a line and say: this is a tribe, this is another one.

Colonial empires were really keen on drawing these lines (on certain chosen criteria) ie naming tribes and setting chiefs to discuss with.

### Tribe-Making

> "it is striking how often a tribal or ethnic identity is generated at the periphery almost entirely for the purpose of making a political claim to autonomy and/or resources."

Scott takes the example of the Cossack, a population of runaway serf and fugitive with nothing in common who fled and then formed a society against the Tsar's state. Their culture got some influence from Tatars, Circassians and Kalmyks.

> "Ecological niche, because it marks off different subsistence routines, rituals, and material culture, is one distinction around which ethnogenesis can occur."

> "Many identities are crafted, it is clear, with similar purposes in mind: to defend a strategic trade route location, to assert an exclusive claim to water, minerals, or valuable land, to claim ownership of a particular commodity, to defend fishing or hunting rights against competition, to guard entry into a lucrative occupation, or to claim ritual privileges. \[...\] It serves, for such societies, essentially the same purpose that the formation of trade union, a corporation, or a craft guild would serve in a more contemporary society."

> "Once launched, the 'tribe' as a politicized entity can set in motion social processes that reproduce and intensify cultural differences. They can, as it were, create the rationale for their own existence."

### Genealogical Face Saving

Lineages were also often fluid in the hills. First it was necessary to be able to accept strangers in the community (they were usually integrated at a higher rank than in the valleys). Second, lineages could be rewritten in order to augment one's status:

> "Social climbing then is the product of a dual process. Prestige is first acquired by an individual by lavishness in following ritual obligations. This prestige is the converted into recognized status by validating retrospectively the rank of the individual's lineage"

Scott explains that the hill people have options on how to organize themselves which can be represented along two axes: equality-versus-hierarchy and statelessness-versus-stateness

### Positionality

Here we explore the relations of cooperations between padi-states and hill peoples.

### Egalitarianism: The Prevention of States

Some hill peoples chose an egalitarian way of life to avoid states:

> "what we encouter here is the pratice of disavowing status-building histories in the name of preventing hierarchy and its frequent companion, state formation. The Lisu are without history not because they are incapable of history but because they choose to avoid its inconveniences"

There were also common stories among egalitarian peoples that those who wants power, those who wants to be headmen will be killed.

> "One of the key material conditions of egalitarian structure - necessary but not sufficient - is open and equal access to subsistence resources."

## Chapter 8: Prophets of Renewal

In history there were thousands of uprisings "against encroaching states".

> "These uprisings, usually led by people styling themselves (and/or taken to be) wonder-working prophets, shoulder their way to the front of the historical record by virtue of how large they loom in the archives."

### A Vocation for Prophecy and Rebellion: Hmong, Karen, and Lahu

The Miao/Hmong have a very long history of rebellion against China and then, later, against the French in Indochina. The first historical record is a defeat against the Yellow Emperor of the Han in the third millunium BCE.

> "they were led by prophetic figures appealing to millenarian expectations, and they tended to appeal, as well, to other, neighboring upland peoples."

The Karen are located at the Burmese/Thai border. They number roughly 4.5 million. They can be Buddhists, Christian or animist but :

> "it is a trait in the national character of the Karen to have prophets rising among them."

> "The pervasive idea of a reversal of fortunes, of a world turned upside down, of the conviction that it is the Karen's turn to have power and the wealth, places, and cities to go along with it, animates a long tradition of rebellions."

The Lahu are located in the southwestern corner of Yunnan. They are a very egalitarian people and number 650,000. They have "little or no political unity beyond the hamlet", but they have a deep tradition of prophets.

They used to have Tai overlords with a pretty light rule, everything went harsher with the "aggressively expansionist early Qing Dynasty". They imposed new taxes to this people which they view as being "born with tails" which led to a series of revolt after 1725.

Prophets usually emerge to eliminate political domination and to restore a peaceful and ethical world. After being recognized as a good healer outside his community and starting to have many followers, one can claim a divine nature. His role is then to push for "reforms (diet, prayer, taboos) to cleanse the community and prepare for a new order". Then, finally, the god-man "proclaims a new order and unites his followers in defiance of the lowland state."

### Theodicity of the Marginal and Dispossessed

Scott stresses the "insistence among so many hill peoples on reading the world in their favor, on believing in their imminent emancipation".

> "virtually all popular struggles for power that today would qualify as 'revolutionary' were, before the last quarter of the eighteenth century, generally understood in a religious idiom. Popular mass politics was religion, and religion was political. To paraphrase Marc Bloch, millenia revolt was as natural to the seigneurial (feudal) world as strikes, let us say, are to large-scale capitalism. Before the first two avowedly secular revolutions in North America and France in 1776 and 1789, virtually all mass political movements expressed their aspirations in religious terms. Ideas of justice and of rights and, indeed, what we might today call 'class consciousness' were religiously phrased."

### Prophets Are a Dime a Dozen

For Scott there usually were lots of potential would-be prophets, and from time to time, one would emerge.

> "One imagines that in early-first-century Roman Palestine there were innumerable would-be messiahs, each believing himself the fullfillment of the ancient Judaic prophecy. Yet the cult of only one, Jesus of Nazareth, later became institutionalized as a world religion.

For Scott there usually were lots of potential would-be prophets, and from time to time, one would emerge.

> "One imagines that in early-first-century Roman Palestine there were innumerable would-be messiahs, each believing himself the fullfillment of the ancient Judaic prophecy. Yet the cult of only one, Jesus of Nazareth, later became institutionalized as a world religion.

For Scott there usually were lots of potential would-be prophets, and from time to time, one would emerge.

> "One imagines that in early-first-century Roman Palestine there were innumerable would-be messiahs, each believing himself the fullfillment of the ancient Judaic prophecy. Yet the cult of only one, Jesus of Nazareth, later became institutionalized as a world religion.

For Scott there usually were lots of potential would-be prophets, and from time to time, one would emerge.

> "One imagines that in early-first-century Roman Palestine there were innumerable would-be messiahs, each believing himself the fullfillment of the ancient Judaic prophecy. Yet the cult of only one, Jesus of Nazareth, later became institutionalized as a world religion."

Being charismatic is a function of space, culture and time. The society has expectations and the prophets is the good man, at the good moment and at the good place to represent them.

### "Sooner or Later..."

> "Any social hierarchy creates, by its very existence, a stratification of wealth, privilege, and honor."

And the disprivileged class always has interests in seeing the world upside down, to reverse the hierarchy. Then emerges the hope that sooner or later a prophet will come.
Prophets were often monks from the forest practicing heterodox Buddhism. This is also in opposition with well-establish lowland Theravada Buddhism. States were aware of this threat and withdrawing into solitary places was forbidden for monks.

From padi-states to highlands there seem to be a gradient of Buddhist practices. The Buddhist clergy from the states and its institutions and rites are mimicked and opposed the farther away we are to the center of states. Thus the monk in the monastic school becomes the hermit in the forest. "In place of central pagodas and shrines, there are the local nat pwes (spirit ceremonies)".
And these nats are to honored people who died early (and so left a powerful spirit behind them). These people were usually rebels or victims of an unjust king, martyrs.

> "Mendelson believes that the pantheon represents an effort to appropriate a series of local cults under a monarchical Buddhist umbrella, much as the cults of certain saints in Catholic countries are often associated with pre-Christiam deities."

So these nats keep a democratic spirit and were often forbidden or repressed by state-power.

### High-Altitude Prophetism

Prophetism exists also in valleys but it is different. Prophetism is always associated with a kind of catastrophe, and hill peoples have seen many kind of catastrophes in their position of state adversaries. So their prophetism is against oppressions.

> "Holy-man revolts in the hills might well be considered one of several tehcniques for thwarting state incorporation."

### Dialogue, Mimicry, and Connections

Hill socities view themselves as descending from peoples which were equal to valley peoples. This equality has often been broken / stolen by treachery and one day someone will come and restore this unbalance.

So we can see hill societies as a group of runaways (poor and 'lumpen intelligentsia' from the valleys) who fled. This still keep some relations with the hills. Part of the culture is kept, they gladly accept the valley's cosmology but refuse the political authority. To this end, they organize as in a decentralized way, with no agriculture, oral history etc. In times of danger, prophetic figures help unify hill people to rebel.

Charismatic leaders usually have travelled a lot, know many languages and serve as translators between their community and outside cosmologies. But this kind of external knowledge is also taken seriously as a potential threat. In the Lahu communities, Chinese guests were received by someone speaking fluently chinese which was "one could say, a village-level minister of foreign affairs whose job was to placate the guests, minimize their demands, and serve as a kind of cultural barrier between the Han and the internal affairs of the village." At the same time, those "hosting and feeding Han officials" were expected to act as pure Lahu, wearing Lahu-clothes, eating Lahu food, speaking their language etc.

> "Knowing that a powerful and cosmopolitan local intermediary could as easily be a liability as an asset, the Lahu took pains to split the two roles and thus minimize the danger."

### Turning on a Dime: The Ultimate Escape Social Structure

The prophet is a direct continuation of shamanic practices:

> "If the shaman is concerned with the health and well-being of individual patients and their families  ... the messianic prophet is ultimately concerned with the salvation of Hmong society as a whole." Tapp

For example a shaman (a prophet) could have a dream that suggests that it is time to move the village a bit further. This kind of event happened frequently. The main difference with prophetism as described earlier is the magnitude of the change and the fact that the new world will be different. Prophetism aims at constructing a new society, turning the social order upside down. There is no going back: taboos can be broken, there can be changes in agricultural practices, religion and social hierarchies.

### Cosmologies of Ethnic Collaboration

Prophetic movements were spreading across different ethnicities quite easily, enabling collaboration.

### Christianity: A Ressource for Distance and Modernity

Christianism was quite popular in the hills, contrary to the valleys. It was indeed no associated with the valley peoples and it had its own millenarian cosmology.

> "where we find Hinduism in the valleys, we might find animism, Islam, Christianity, or Buddhism in the hills. Where, as in Java, we might find Islam in the valleys, we are likely to find Christianity, animism, or Hinduism in the hills. In Malaysia, where the rulers are Muslim, many of the minority hill peoples are Christian, animist, or Baha'i."

> "Now Christianity offered the opportunity to be modern, cosmopolitan, literate, and still Hmong."

It is interesting in the light of the legends which were telling people that writing was lost or has been stolen by the Chinese.
So of course, Christianity was mixed with local religions.
