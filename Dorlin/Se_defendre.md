# Se défendre: une philosophie de la violence (Elsa Dorlin)

## Prologue : ce que peut un corps

On commence sur l'histoire d'un supplicié, en 1802. Il est condamné à être dans une cage jusqu'à ce que mort s'en suive. Ses pieds sont enchainés entre eux et il surplombe une lame. Chaque faiblesse, chaque mouvement de protection risque de le blesser. Dorlin compare cette mise à mort avec celle qui ouvre *Surveiller et punir*. Dans le livre de Foucault, le condamné ne peut rien, il est objet de supplice. Dans l'histoire coloniale de Dorlin, le condamné peut se défendre, et c'est cette défense ou sa faiblesse qui le tue. Le dispositif de supplice réprime la défense : plus tu résistes, plus tu souffres.

> "Cette économie de moyens, qui fait du condamné et plus généralement du corps violenté son propre bourreau, dessine de façon négative les traits du sujet moderne."

Le 3 mars 1991, à Los Angeles, Rodney King était tabassé par la police. La scène a été filmée et le monde entier a été témoin de la violence de la police. Devant le tribunal composé d'un jury blanc, les quatre policiers accusés sont jugés, avec pour seul preuve la vidéo. Ils seront acquittés, donnant lieu à de violentes émeutes et à la mort de 53 personnes. Dans les yeux du jury blanc, les gestes de défense de King ont été vus comme une violence, une potentielle agression. Comme si le corps noir de King ne pouvait pas produire autre chose que de la violence, comme si se défendre n'était donc pas une option.

> "Puisque les corps minoritaires sont une menace, puisqu'ils sont la source d'un danger, agents de toute violence possible, la violence qui s'exerce en continu sur eux, à commencer par celle de la police et de l'État, ne peut jamais être vue comme la violence crasse qu'elle est : elle est seconde, protectrice, défensive - une réaction, une réponse toujours légitimée."

On s'intéresse donc au *dispositif défensif*, à ces technologies du pouvoir qui rendent certains corps sans défense, qui retournent leur élan contre eux. Ce dispositif peut s'étudier dans une généalogie de l'autodéfense.

## Chapitre 1. La fabrique des corps désarmés.

### Brève histoire du port d'armes

En France, le droit de porter une arme est historiquement associé à la noblesse et au droit de chasse qui lui était associé. Au Moyen-Age, de nombreuses populations sont armées et certaines dans des lieux stratégiques sont encouragées à rester armer. Ce qui est de plus en plus réglementé, c'est d'être armé hors de chez soi :

> "L'espace public est alors défini par référence à la notion de sureté du roi ; le désarmement des groupes d'individus sur les chemins royaux et voies publiques étant la condition de la circulation en paix et en sécurité du roi."

Le principal danger pour l'État vient des mouvements séditieux nobiliaires. Après le Fronde, la mainmise de l'État sur les armes se fait plus grande, qui contrôle leur fabrication, commerce et stockage.

Au XVè siècle, on voit l'apparition d'une armée de métier. Les armes pour se battre (armure et bouclier inclus) sorte du cadre civil.

> "Au-delà de la coercition, l'invention des "armes prohibées" apprend aux sujets à venir trouver la justice pour éviter la violence." Romain Wenz

L'État cherche donc à encadrer les armes d'un point de vue offensif. Les armes défensives vont devenir l'apanache de l'aristocratie (après le XVIè siècle), pour qu'elle puisse se défendre *corps et biens*.

> "Arquebuses et pistolets sont ainsi exclusivement réservés aux gentilshommes."

De même, la rapière, utilisée "plus pour piquer que pour trancher" se porte dans l'espace public.

> "Cette épée civile est proprement destinée à l'autodéfense."

> "En pratique, la frontière entre l'espace privé et l'espace public - civil - est désormais marqué par le fait de *sortir armé* ou de *déposer son arme avant d'entrer quelque part*."

Il faut aussi savoir utiliser son arme, d'ou l'importance de l'escrime dans les milieux bourgeois. D'abord autodéfense, cet art basculera dans le sport par une ritualisation des pratiques et des techniques quitte à perdre en efficacité. L'important est de se distinguer des techniques brouillonnes des gueux.

### Désarmer les esclaves et les indigènes : droit de tuer contre subjectivité à "mains nues"

> "En 1685, l'article 15 du Code noir français défend 'aux esclaves de porter aucune arme offensive, ni de gros batons' sous peine de fouet."

Le code noir espagnol va jusqu'à interdire l'usage de la machette agricole sauf "aux seuls quarterons, métis et 'au-delà'."

> "Elijah Gree, ancien esclave né en 1843 en Louisiane, rapporte qu'il était strictement interdit à un Noir d'être en possession d'un crayon ou d'un stylo sous peine d'être condamné pour tentative de meurtre et pendu."

Au contraire, les colons sont incités à s'armer, à se regrouper en milices, à faire la police. Cette distinction fondamentale entre colons et esclaves vient du fait que par définition, l'esclave ne se possède pas lui-même et dépend donc de son maitre pour sa sécurité.

> "Les esclaves n'ont plus de vie ; ils n'ont qu'une valeur."

Les colonisé·es sont aussi sans défense face à la justice. Tous leurs actes sont assimilés à de l'agression, ils sont présumés coupables et ne peuvent pas se défendre juridiquement.

### Ascèse martiale : culture de l'autodéfense esclave

Dorlin décrit d'après Fanon le corps du colonisé comme un ressort comprimé, tétanisé par la colonisation, qui ne peut se libérer qu'en rêve, et est prêt à se libérer réellement dès que le colon flanchera. Et les colons ont peur :

> "On soupçonne un pas de danse d'être déjà un engagement au combat."

La nuit, clandestinement, les esclaves se réunissent et mêlent danse, musique, spiritualité et gestes : acrobatiques ou de lutte. Ce sont des danses martiales. L'impossibilité de se défendre à armes égales

> "les maintient dans une position de tension permanente qui attise les conflictualités autodestructrices ; mais cette tension constitue aussi, de fait, une forme d'entrainement en conditions 'réelles' (sans effet de symbolisation), une capitalisation de violence extrême, explosive, une forme de sociabilité martiale radicale. Ainsi cette posture doublement polarisée incarne une autodéfense sans modalité de préservation de soi qui annonce une entrée dans la lutte défensive ou la peur de la mort ne constitue ni une limite ni même un noeud dialectique."

### La force noire de L'Empire : "Vive le patriarcat, vive la France !"

Les populations noires, notamment d'Afrique de l'Ouest dans le contexte français, sont perçues comme naturellement guerrière, mais aussi comme des peuples hiérarchiques, qui savent obéir voire qui sont fait pour cela. Ces qualités en font donc de parfaits petits soldats qu'on peut envoyer se battre à la place des blancs, "un bras armé qui ne réfléchit pas", car, pour les racistes, les peuples africains sont en dehors de la civilisation, ils ne savent encore rien.
L'idée que les hommes sont polygames et oisifs (ce sont les femmes qui travaillent la terre) est également évoquée pour justifier à la fois une mission civilisatrice et le fait que l'homme est disponible pour la guerre. Ces soldats indigènes ne sont pas citoyens français et, pour essayer de gagner leur loyauté, l'Empire distille le racisme dans ses troupes et les envoie conquérir des plus *barbares*, car "on est tous le barbare de quelqu'un".

## Chapitre 2. Défense de soi, défense de la nation

### Mourir pour la patrie

La juste défense des individus présente dans le droit romain s'effacera à partir des XVII et XVIIIè siècle au profit de la légitime défense des États : ce qui prime c'est la défense de la nation. La défense de la patrie devient le devoir des citoyens et permet de former un peuple. Dans les faits, les peuples colonisés seront toujours largement utilisés comme chaire à canon sans obtenir le titre de citoyen·es et les classes aisées trouveront toujours un moyen de se soustraire aux conscriptions.

### "Femmes, armons-nous" : les bataillons d'Amazones

Le corollaire de la partie précédente est que seulement certaines parties de la population ont accès à une formation martiale. Les autres restent sans armes, désarmé·es :

> "plutôt que de 'monopole de la violence légitime', on pourrait parler de *gestion sociale de la martialité*."

Un exemple peut être les femmes qui souhaitaient être armées et formées pendant la Révolution françaises pour ne plus être des "citoyennes passives", "patriotes infirmes".
Les femmes en armes sont présentées comme défiant l'ordre sexuelle. Ce ne sont pas des atouts mais des menaces, on craint que les femmes soient possédées et leurs mouvements infiltrés.

Viendra alors la vision de la femme défendant la patrie de l'intérieur pendant que les soldats hommes luttent hors de leurs frontières. Mais lutter, pour les femmes, revient aussi à se hisser au niveau des hommes.

> "Mourir pour la patrie, c'est mourir pour rester libre ou, plutôt, pour le devenir : c'est mourir *pour soi*."

### Armée citoyenne ou défense du Capital ?

Avant la première guerre mondiale, en France, la grande question est de savoir comment organiser l'armée. Certains aimeraient une avant guarde entrainée dans l'armée, soutenue par une masse de soldats. Pour Jaurès, la question est celle de la défense : il faut armer la population. C'est évidemment un pari dangereux pour l'État et le Capital car c'est alors l'armée qui est envoyée pour réprimer les manifestations et briser les grèves.

### Le ju-jitsu des suffragettes : combat rapproché et antinationalisme

À Londres, des écoles enseignaient au début du XXè siècle, des arts martiaux inspirés du ju-jitsu. Ils avaient pour but d'être concrets, détournant parfois des objets du quotidien (canne, parapluie) pour une défense réaliste. Des militantes féministes (suffragettes) se sont formées dans ces écoles ouvertes à toustes et commencent à former des services d'ordre, notamment pour se protéger en manifestation.

> "L'autodéfense n'est donc pas un moyen en vue d'une fin - acquérir un statut et une reconnaissance politiques - elle politise des corps, sans médiation, sans délégation, sans représentation."

Ces arts martiaux se basent sur le retournement de la force de l'adversaire et les effets de ruse et de surprise.

## Chapitre 3. Testaments de l'autodéfense

### Mourir en combattant: l'insurrection du ghetto de Varsovie

L'autodéfense du ghetto passa par la cache d'armes et l'organisation de l'espace en un champ de bataille contre les SS, les gendarmes polonais et leurs alliés. En septembre 1942, Menachem Kirszenbaum fait passer le message hors du ghetto : celui-ci déclare la guerre à l'Allemagne. Pour lutter contre l'agonie lente, ce dernier combat perdu d'avance, un appel à mourir les armes à la main. Il s'agissait de choisir sa mort :

> "Il s'agissait alors de préferer le combat au suicide : pour la plupart des résistants, le suicide gaspille des balles qui auraient du être destinées aux nazis."

Et les résistances se battirent quand les nazis entrèrent liquide les survivants du ghetto le 19 avril 1943.

### L'autodéfense comme doctrine nationale

Dès la fin du XIXè siècle, l'autodéfense est vitale pour le juif d'Europe de l'Est, notamment en Russie ou les progroms font rage attisés par la propagande et encouragés par la police. De nombreux groupes d'autodéfense se constituent dont le plus connu est surement le Bund.

> "l'une des techniques de combat communément pratiquée en Russie à cette période est une forme ultra-violente de boxe à poings nus, combat sans règle ni arbitre appelée *kulachnyi boi*. Les groupes [affiliés au Bund] sont aussi armés de batons, pieux, haches, barres de fer, armes blanches, entrainés au maniement des armes à feu, à la construction d'engins explosifs, mais aussi à l'organisation d'assassinats visant des agents infiltrés de la police du tsar (Okhrana)."

Des courants différents existent : le Bund est socialiste révolutionnaire quand d'autres groupes sont sionistes, parfois socialistes, parfois fascisants. Si ces mouvements s'entraident un temps après les progroms de Kichinev en 1903, le Bund va tenter de rester actif, notamment en Russie, quand les groupes sionistes vont commencer à s'allier aux anglais pour conquérir la Palestine.

Certains transposent alors l'autodéfense à ce contexte particulier : menacé en Europe par les populations, ils sont tout autant menacés en Palestine par le "terrorisme arabe". Les juifs doivent donc se tenir près à intervenir à tout moment. La violence doit être continue et permanente et les rapports sociaux plus complexes peuvent être oubliés :

> "Cet appauvrissement du monde au profit d'une 'cosmologie de la guerre totale et de la terreur' enferme alors l'individu défensif dans une phénoménologie du corps-arme, corps létal, transformant l'autodéfense en politique, c'est-à-dire en véritable gouvernement de l'intensité de la violence à l'échelle du corps propre."

### Généalogie du *krav maga*

Imi Lichtenfeld, inventeur du *krav maga* a longtemps vécu à Bratislava. Il participe à la défense contre les pogroms en inventant différents styles de combat dans un processus de "désportivisation" ie en prenant des concepts venant de sport de combat. Il part pour la Palestine ou le *krav maga* se développera (à partir de 1949), cette fois-ci de manière plus offensive :

> "L'hypothèse que vous voudrions poser ici est qu'une certain conception tactique du combat rapproché a été la base d'une politique stratégique militaire de plus grande ampleur ou, du moins, a inspiré le champ lexical de sa propagande. Le krav maga symbolise ainsi cette idéologie nationale de la défensive offensive, d'une guerre de conquête qui s'est menée dans un contexte ou une armée s'est autodéfinie comme une nation en situation d'autodéfense contre tous pour assurer son existence."

Un certain nombres d'organisation militaires existent alors en Palestine et seront rassemblés dans Tsahal en 1948 (forces de défense israélienne). Le manque de moyen implique l'apprentissage de nombreuses techniques de combat au corps à corps. De ces expériences paramilitaires, Israel devient le modèle d'une société de sécurité. Le krav maga n'est pas seulement système de combat, c'est une culture nationale, une pratique citoyenne.

Ces techniques de combat sont présentées comme non-léthales, définissant par la-même des nouvelles méthodes de maintien de l'ordre. La diffusion du krav maga à toute la société crée une situation particulière ou chacun peut se défendre à n'importe quel moment et ou la peur du terrorisme est omniprésente et justifie le schéma de défense offensive.

## Chapitre 4 : L'État ou le non-monopole de la violence légitime

### Hobbes ou Locke, deux philosophies de la défense de soi

Pour Hobbes, la défense de soi relève d'un devoir naturel. Mais ce devoir implique un *état de guerre* qui n'est pas supportable. Il n'est pas possible qu'une personne renonce à sa protection car elle se transformerait alors en proie, elle nierait sa propre humanité. Il faut donc que toustes, collectivement, nous renonçions, déleguions ce devoir au Léviathan.
Pour Locke, brièvement, nous avons le droit absolu, naturel, de nous défendre et de défendre nos biens (ce qui revient un peu au même car nous nous possédons nous-même). Chez Locke, la question du droit est centrale et l'autodéfense est donc légitime défense. Elle n'a lieu que si notre propriété est (sur le point d'être) menacée. En découle une guerre contre les voleurs, qui par leurs actions, s'excluent de l'humanité et doivent être considérés comme des bêtes sauvages et punis.

> "se conserver, c'est punir"

Le sujet préexiste au droit de conservation. En effet, le sujet possède son corps et ainsi un droit naturel à le défendre. Or il y a une différence radicale entre ceux qui se possèdent et les autres :

> "Pour les autres, les Indiens jouisseurs des bienfaits de la nature, les esclaves, les domestiques, les femmes et les enfants, les indigents, les criminels et les scélérats ... il n'y a personne dans de tels corps dépossédés d'eux-mêmes."

Les relations entre ces deux catégories peuvent tourner à la guerre, il faut donc instituer une juridiction qui servira à faire respecter le droit de propriété et prononcera des peines. Les sujets possédants délèguent leur droit naturel de défense à cette entité.

Les pouvoirs qui sont déléguer à l'État peuvent ne l'être qu'en partie. L'État peut décider d'assurer son devoir de sureté avec des milices citoyennes ou en réglementant le port d'armes par exemple.

### Se faire justice soi-même : milices et "coopératives judiciaires"

Cette vision Lockéenne de l'autodéfense est rentrée dans le droit anglais puis américain avec pour argument phare le droit de s'armer.
Parallèlement, les frais de justice pour les plaignant·es sont si colossaux que seuls les riches peuvent entreprendre des démarches judiciaires. Se créent alors des *coopératives judiciaires* de citoyens qui se regroupent pour se promettre de l'aide (non-agression, soutien financier, ...). Ces sociétés (regroupant des personnes possédantes) ont agit en "appareil parajudiciaire" d'autodéfense, comme auparavant les associations médiévales mercantiles. Elles ont largement supporter les intérêts des classes possédantes. Leur nombre en 1839 est estimé à plus de 500.

> "Si l'État a laissé libre cours à ces coopératives judiciaires, ce n'est donc pas par faiblesse, mais plutôt dans le cadre d'un processus continue de rationalisation de son exercice. L'histoire du droit à l'autodéfense armée est inséparable de celle de ces organisations judiciaires privées et participe d'une généalogie de l'État libéral. Elle est aussi constitutive d'une certaine définition de la subjectivité moderne dominante, centrée sur une figure de citoyen modèle caractérisé par une capacité martiale et judiciaire autonome à défendre sa propriété comme lui-même."

Aux États-Unis, le port d'arme devient le symbole de cette nouvelle nation :

> "L'arme individuelle est en un sens l'incarnation prototypique de la 'main invisible' d'Adam Smith : elle fait société."

Les porteurs d'armes sont les pionniers, les *frontiersmen*, les défricheurs et constructeurs de la nouvelle nation. Ce sont aussi les maitres de l'ordre colonial et raciste.

### Le vigilantisme et la naissance de l'État racial

Le groupes de *vigilants* (justiciers) se sont développés aux USA au XIXè siècle comme des groupes majoritairement non-mixtes de riches propriétaires blancs.

> "Le vigilantisme est, de fait, l'une des expression les plus massives de l'histoire des actions directes extralégales, de l'antiabolitionnisme comme de la criminalité et du terrorisme raciste américain."

Les comités veulent, en cette veille de guerre de Sécession, purger leur terre.

> "En cas de crime, les comités ne prévoient qu'une seule défense : la corde. La plupart des comités de vigilance à la fin du XIXè siècle utiliseront le fouet, le bannissement et la pendaison et chasseront sur le territoire de leurs États tous les hommes considérés comme indésirables et constituant une menace pour la société coloniale blanche."

Les justiciers ne sont pas des juges, ils posent leur propre cadre qu'ils estiment légitime. Ce sont les ennemis des juges et leur justice tient en fait de la guerre. Et les justiciers finiront par renverser la justice pour la remplacer, accomplissant ainsi la genèse d'un État racial, de la suprématie blanche.

> "Le vigilantisme est devenu un modèle de citoyenneté - tout bon citoyen américain est un citoyen vigilant."

## Chaptre 5. Justice blanche.

TW : torture, meurtre, racisme, sadisme, ...

### Du lynchage à la légitime défense : "un mensonge cousu de fil blanc"

Le concept de lynchage vient du blanc-seing octroyé par l'État de Virginie à Charles Lynch pendant la révolution américaine pour éradiquer les voleurs et bandits. Cette loi qui autorise à ne pas respecter la loi s'est répandue aux États du sud contre les vaganbonds, étrangers, etc

Si les lynchages ont pu être commis par des groupes comme le KKK, ils étaient généralement l'oeuvre de la population encouragée par la propagande raciste.

> "les lynchages survenus entre 1880 et jusqu'après la Seconde Guerre mondiale constituaient des scènes de la vie quotidienne et témoignaient de comportements sociaux qui étaient considérés comme *normaux* : la population d'une ville ou d'un village se rassemblait autour d'un homme qu'on s'apprêtait à torturer, à mutiler, à bruler vif ou à pendre. Les écoles fermaient pour l'occasion afin de laisser les enfants assister au spectacle. On laissait jouer ces derniers avec la dépouille. Les familles pique-niquaient à l'ombre des arbres ou les corps suppliciés étaient pendus."

La plupart des hommes noirs lynchés l'étaient à cause de soupçon de viol sur des femmes blanches. En réalité ce n'était qu'une excuse, la grande majorité des mis à mort étaient innocents. De plus, rien de spécial n'était fait pour protéger les femmes blanches. Cette excuse est évidemment également raciste car les viols contre des femmes noires et / ou par des hommes blancs ne faisaient jamais l'objet de poursuites. On voit donc bien qu'il s'agit pour le vigilantisme de remplacer la justice par une autodéfense raciale : les noirs sont placés dans la catégorie des *tuables* par la catégorie des blancs. On voit bien la construction d'une société purement raciste avec la participation active de la population sous la forme du vigilantisme. Les fausses accusations qui sont alimentées par le mythe du violeur noir permettent de faire tomber n'importe quel personne noire, ennemi, concurrent, ...

### "Il faut défendre les femmes"

Cette société raciste fonde donc sa justification sur le violeur noir et sur le fait que les hommes blancs doivent défendre les femmes blanches. On voit ici la construction de l'image de la femme blanche, la *lady*, chaste, fragile, docile, soumise, vulnérable et donc redevable aux hommes pour sa sécurité. Cette position (d'infériorité) exclut les femmes noires et les militantes abolitionnistes.

Par l'imaginaire de femmes vulnérables et chastes, le patriarcat blanc justifie de rendre *tuable* les hommes noirs. Remettre en cause la condition féminine revient donc à faire bouger la vision raciale de la société.

De manière plus récente, l'argument de la défense des femmes est toujours invoqué mais dans un contexte impérialiste. Il s'agit de défendre les femmes des hommes, mais dans d'autres pays, considérés comme moins progressistes. Il s'agit de récupérer l'argument progressiste à des fins coloniales. L'archétype de cela est la présence de femmes dans l'armée américaine que l'ont utilise à la fois comme caution du progressisme mais aussi pour aller défendre d'autres femmes de leurs maris barbares.

## Chapitre 6. Self-defense: power to the people!

### En finir avec la non-violence: "Arm Yourself or Harm Yourself"

Avant la deuxième guerre mondiale déjà un certain nombre de militant·es (dont Ida Wells) recommandaient aux noir·es de s'armer pour pouvoir se combattre dans cette lutte raciale. Après la guerre, le gouvernement américain commencera à tenir une position de plus en plus difficile entre sa position hégémonique de puissance démocratique et sa propre "démocratie Jim Crow". Après la guerre, on peut aussi noter que la critique de la ségrégation se renforce avec des comparaisons à un colonialisme intérieur et de nombreuses personnes noires ont été formées à la guerre et savent manier les armes. L'une de ces personnes est Robert Williams qui va proner l'autodéfense armée face au KKK et notamment après le [Kissing Case](https://fr.wikipedia.org/wiki/Kissing_Case). Il sera ostracisé dans le NAACP (National Association for the Advancement of Colored People) mais sera soutenu par W.E.B du Bois et sera actif plus tard dans le mouvement Black Power.

> "Selon lui, l'autodéfense intervient lorsque la non-violence arrive à ce point critique ou persister dans cette tactique se muerait en suicide."

Williams considère que seule l'action violente pourra changer profondément cette société raciste et rétablir la justice. Il se décrit comme internationaliste (marxiste) et non comme nationaliste noir.

### Les Black Panthers : l'autodéfense comme révolution politique

Dans les années 60 et particulièrement après 1966, des appels à rejeter la non-violence alors en vogue se font entendre. En effet, les marches non-violentes sont régulièrement l'objet d'attaques et d'agressions ce qui les rend insoutenables. Le révérend King continue à prôner la non-violence sans s'opposer à l'autodéfense, il refuse le port d'arme ostensible en manifestation notamment ce qui constitue selon lui un permis de tuer pour la police.
Pour Malcolm X de la *Nation of Islam*, la seule injonction à la non-violence acceptable est celle entre noir·es, dans la communauté. Il critique King, qui, en "désarmant" les noir·es est devenu le seul interlocuteur que les Blancs jugent respectables.

Les actions non-violentes supposent une logique longue de lutte. On veut montrer la violence de l'agresseur, la rendre visible, l'exposer. Mais cette forme de lutte use les corps et suppose un temps long que l'action violente veut renverser.

La théorie de l'action violente est notamment influencée par la lecture des textes de Fanon et sa "défense-explosive". Il s'agit de restaurer une lutte à armes égales, ainsi que la dignité des agressés.
Novembre 1966, création du Black Panther Party for Self-Defense. Ses membres sortent armés dans le but de défendre la vie des noir·es. L'usage de l'arme n'est autorisée qu'en cas de crainte pour la vie de qqun·e. L'ennemi désigné est la police. Le parti deviendra BPP en 1968, laissant de côté l'autodéfense de son nom, pour plusieurs raisons. La première est que les media ne parlait que de ça et que la répression par le FBI était violente. Or le BPP oeuvrait aussi pour l'autodéfense sociale, en ouvrant des dispensaires, cours du soir, en offrant des petits-déjeuners aux enfants pauvres etc L'image d'autodéfense créait aussi une division sexuelle du travail avec des militants viril(ist)es armés dans les rues.
Le BPP se voulait aussi avant-guarde de la révolution sociale et organisateur du *lumpen* prolétariat, les personnes noir·es.

De nombreuses critiques des dérives masculinistes du BPP seront faites, notamment par des femmes qui trouveront inenvisageable de faire la révolution sans elles, et en portant le "masque blanc" ("avoir du pouvoir c'est être un homme, être un homme c'est être blanc"). Cela peut s'expliquer aussi par la vision "class-first" des marxistes pour qui la lutte contre le patriarcat est une lutte au pire bourgeoise, au mieux secondaire.
Pour les militants noirs, cette tentative de revanche sur la masculinité dont ils ont été si longtemps exclus, émasculés, revient à se hisser dans la norme blanche. Dans leur discours macho, ils demandent aux femmes de ne pas être des révolutionnaires et de rester à la maison. Ils se sont condamnés à devenir de pales copies des normes blanches.

## Chapitre 7. Autodéfense et sécurité

### SAFE!

À la fin des années 60 / début des années 70, plusieurs luttent se rapprochent autour d'un ennemi commun, le flic. Après les émeutes de Stonewall en 1969, le Gay Liberation Front (GLF) participe à des actions avec ou en soutien du BPP. Les voix des anti-impérialistes aussi se font entendre.
Les mouvements gai critiquent néanmoins violemment le machisme de certains groupes qui les oppressent et les violentent au quotidien. En 1973, le Gay Activists Alliance crée une branche d'autodéfense, les Lavender Panthers, groupe armé de "justiciers" dont le but affiché est de répondre aux agressions (la police est très lente et peu fiable) et de "nettoyer" les quartiers de l'homophobie (cela inclut des punks, des gangs, les dealers et donc en particulier les communautés noires et sino-américaines).

> "Sept ans plus tard [...], le "nettoyage", dont les Lavender Panthers sont le fer de lance, sert directement les intérêts des promoteurs et des bailleurs privés qui soutiennent une communauté gaie "policée" condition idéale pour investir et spéculer dans des quartiers historiques."

Pour la Butterfly Brigade, le but est de se réapproprier la défense de la communauté et que tout le monde soit vigilant. Malgré la volonté d'inclusion et le non-port d'un uniforme, les membres sont assez homogènes :

> "produisant une norme de la masculinité gaie blanche : corps athlétique, cheveux courts et moustache, jeans, tee-shirt et veste en cuir, sifflet deviennent l'uniforme de la communauté gaie de Castro."

D'un point de vue politique, ce sont les pratiques "safe" qui sont vues positivement. Le calme et la sécurité participe à la qualité de vie et trace la ligne entre les gais qui sont acceptables et ceux qui ne le sont pas. Les pratiques jugées unsafe (trans, prostitution, drogue, errance, ...) sont du mauvais côté de la barrière politique ainsi que ceux qui ne respectent pas les "bons gais" :

> "renforçant le préjugé selon lequel toustes les homosexuel·les seraient blanches, et toustes les homophobes seraient noir·es. Ce processus se matérialise par la surveillance policière, la répression de la délinquance sexuelle (jugée obscène) et "raciale" (jugée violente), la suppression des politiques sociales et le déplacement géographique des "anormaux" et des minorités raciales appartenant à la classe laborieuse paupérisée vers d'autres zones de la ville."

### Autodéfense et politique de la rage

> "Constituer la 'sécurité' comme norme de vie n'est possible qu'à la condition de produire des insécurités contre lesquelles l'État apparait (et se présente) comme le seul recours."

La politique sécuritaire de l'État a pu être reproduite à l'intérieur de collectifs militants par l'injonction à être safe.

> "Dans endroits prétendumment safe, ou l'on se retranche entre pair·es, ces dernier·es seraient, par définition, sans danger. L'entre-soi safe est alors défini par opposition à une extériorité insecure, suscitant la peur ou la haine ; ce qui rend proprement impensable ou inacceptable de considérer que les rapports de pouvoir, la conflictualité, les antagonismes subsistent inévitablement à l'intérieur et s'exercent sans discontinuité."

Être safe implique de faire la police dans sa propre communauté

> "Plus on se protège contre l'insécurité, plus on épuise le pouvoir de ce que signifie une 'communauté', solidaire, coalisée, de laquelle puiser la puissance de la rage ; plus on réalise une forme de biopolitique à l'échelle des luttes, un biomilitantisme."

## Chapitre 8. Répliquer.

### Sans défense

Dorlin analyse les campagnes de prévention / sensibilisation aux violences conjugales en France. Elles ont ceci de particulier qu'elles mettent toujours en scène des femmes visiblement violentées, faibles, prostrées. Pour l'autrice, de part la nature figée de la photographie, on ne présente qu'une situation de fait : voici des femmes ayant subi des violences. On présente cette situation comme la réalité et donc les femmes sont associées à ces êtres faibles et l'on reproduit ainsi la vision omniprésente de ce à quoi ressemble les violences de genre.

> "Les campagnes publiques sont un tribut offert aux agresseurs."

Les victimes sont représentées comme telles (et qui éprouverait autre chose que des sentiments négatifs à une telle vision de soi-même ?) et les agresseurs contemplent leur puissance d'agir.

> "Nous ne sommes donc pas mis·es devant la souffrance d'un objet, mais devant la puissance d'un sujet. Ces campagnes sont tragiques parce qu'au fond elles ne traitent que d'une surpuissance prêtée aux 'hommes' ; une puissance que l'on présente comme celle des corps masculins rarement montrés, rarement mis en scène, excepté dans la mise en scène - de l'efficacité, de la brutalité et de licéité de leurs coups."

### Phénoménologie de la proie

On parle ici du livre *Dirty Week-end* de Helen Zahavi qui raconte comment une femme lambda est devenue une serial killeuse (en gros). Du point de vue de Bella, l'héroine agressée, la normalité est définie par un tiers, par l'homme à la fenetre, par son agresseur. Elle doit donc faire un effort monumental pour vivre au quotidien selon une normalité qu'elle ne décide pas car elle "rend indigne tout ce qui relève de soi". Bella va apprendre à renverser la situation, à se faire confiance

> "Bella n'a pas appris à se battre, elle a désappris à ne pas se battre."

### Épistémologie du soucis des autres et care négatif

Le renversement qu'opère Bella a donc objet de faire voir aux oppresseurs ce que c'est que d'être opprimé. Elle renverse leur point de vue. Cela n'est possible qu'après avoir conscientisé le fait que les violences du quotidien, ce point de vue atrophié, sont justement des violences, et qu'elles sont politiques.

Dorlin parle de *dirty care* ou *care* négatif pour désigner le fait que les femmes (et les dominé·es en général) sont placées dans cette position d'empathie permanente avec autrui, mais que ce processus les épuise et les traumatise. Car il se s'agit pas simplement de prendre soin des autres mais de faire attention aux autres, de ne pas les énerver, de justifier leurs actions au détriment de soi-même etc
Cette position pousse les dominé·es à étudier et à connaitre très précisément les dominants.

> "L'objet [le dominant] devient le centre du monde que le sujet appréhende depuis *nulle part*."

Les dominé·es s'épuisent donc à connaitre les dominants et leur puissance d'agir en oubliant complètement la leur. Les dominants, en miroir, paissent dans l'ignorance. Ils peuvent se permettre de nier complètement les points de vue tiers en posant le leur comme universel. Mais pour être dominant il faut connaitre un minimum les dominé·es. En réalité il faut s'y connaitre en chasse et être capable de faire des dominé·es des proies.
