# La matrice de la race : Généalogie sexuelle et coloniale et la Nation française (Elsa Dorlin)

> "*Une différence physique réelle n'existe que pour qutant qu'elle est désignée, en tant que signifiant, dans une culture quelconque*. Ces signifiants varient d'une culture à l'autre. Cette différence se manifeste donc comme pur signifiant, porteur des catégorisations et des valeurs d'une société." Colette Guillaumin

Colette Guillaumin a cherché à montrer que le sexe, tout comme la race, ne sont que des catégories créées, qui réclament une réalité biologique sans en avoir, et qui se construisent en stigmatisant un groupe.

Elsa Dorlin veut proposer dans son livre une généalogie des catégories du sain et du malsain (une "nosopolitique"), en regardant particulièrement l'imbrication des rapports de pouvoir (sexe, race) et les crises de ces mêmes catégories, cad comment leur rationalité a pu être remise en question et comment leurs justifications ont pu s'adapter.

## Partie 1 : Les maladies des femmes

### Chapitre 1 : Le tempérament

Au XVIè siècle et au début du XVIIè,

> "Moralistes, théologiens, philosophes, médecins et courtisans s'empoignent pour déterminer le tempérament qui caractérise chaque sexe, c'est-à-dire leurs vertus et leurs vices - physiques et moraux -, le 'naturel' qui les différencie."

Autrement, l'anatomie n'est pas le seul concept utilisé pour définir le sexe.
Pour Galien, il y a isomorphisme des sexes, cela signifie que les corps de l'homme et de la femme sont identiques en tous points, sauf que les organes sexuels de l'homme se trouve au dehors quand ceux de la femme sont à l'intérieur. Pour Galien, les organes sont les mêmes, seule leur disposition change. Cela est du au fait que le tempérament des femmes est plus froid et n'a pas permis l'extériorisation des organes.

Pour Thomas Laqueur dans *La Fabrique du sexe*, l'on peut donc affirmer que la différence H/F n'était donc pas fondé sur la biologie, c'était une distinction qui portait sur le genre. Or comme le fait remarquer Elsa Dorlin, Galien pense bien les différences H/F de manière ontologique, il y a une différence naturelle, en soi, qui s'appuie sur un concept médical, celui du tempérament.

Le tempérament est un concept qui vient de l'Antiquité. Le corps serait composé d'humeurs de différentes qualités (chaude, froide, sèche, humide). Une disposition intérieure parfaite du corps consiste en le mélange équilibré de ces humeurs, les maladies sont des excès de certaines humeurs, et les caractères (colérique, mélancolique) peuvent aussi s'expliquer dans ce cadre.

Les hommes sont chauds et humides, comme le sang, signe de vitalité, les femmes sont froides, rigides, imparfaites, c'est Aristote qui l'a dit. La chaleur permet de transformer les aliments en sang, puis de cuire le sang et d'en faire du sperme. Les femmes en sont incapable, leur semence (sic) sont donc du sang, leurs règles.

> "À partir de la fin du XVIè sicècle, il est clair pour tous les médecins que les femmes sont affligées d'un tempérament froid et humide qui les exclut tendanciellement de la santé. [...] le corps féminin est un corps malade et tout corps malade est par définition un corps efféminé."

Des critiques de ce système arrivent au XVIIè siècle. François Poullain de la Barre écrit un livre dans lequel il annonce que les femmes et les hommes sont égaux. Pour lui, le tempérament existe mais ne permet pas de classer les sexes ni de faire des jugements moraux ou de hiérarchiser les individus. Les personnes sont bien plus influencées par leur condition et les coutumes que par le tempérament. Pour Gabrielle Suchon, le médecins naturalisent les caractéristiques H/F alors que celles-ci sont construites. La santé dépend de beaucoup de facteurs, donnez les meilleures conditions de vie aux femmes et vous verrez qu'elles ne sont pas de pauvres petits êtres malades.

### Chapitre 2 : La maladie a-t-elle un sexe ?

Les traités sur les maladies des femmes font partie d'un genre très courant, avec une riche production jusqu'à récemment.

Pour Hippocrate, les maladies des femmes sont très spécifiques. Le corps des femmes gèrent mal les différentes humeurs qui vont se fixer dans le corps et doivent être évacuées. Le point central est l'utérus qui évacue des humeurs sous la forme de règles. La question est donc de rétablir l'équilibre du corps des femmes, pour un médecin qui n'a pas accès à ce corps. Les traitements conseillés sont souvent des fumigations, des rapports sexuels, des attouchements, des grossesses.

Au Moyen-Age, cette théorie est revue sous le prisme de la religion. Les maladies sont vues comme des altérations de l'ordre naturel, que ce soit à la suite d'un péché ou d'une intervention divine. L'humeur laisse la place au tempérament, et le tempérament froid des femmes peut s'expliquer par le péché originel. Le lieu des troubles est la matrice (l'utérus) qui semble avoir sa propre volonté :

> "L'image d'un animal enragé, errant, 'avide de procréation', se ruant contre les parois du ventre symbolise durablement la crise hystérique, caractérisée par les sensations d'étouffement, les vomissements et les convulsions ; comme si l'utérus remontait effectivement jusqu'au diaphragme."

C'est donc l'utérus qui expose les femmes aux maux. Les femmes les plus à risque sont celles qui n'évacue plus leurs règles (sic) et celles qui ne les fécondent pas, c'est-à-dire les veuves, les femmes vierges et les femmes ménopausées. Globalement l'hystérie est la "maladie des femmes sans hommes", des femmes qui s'abstiennent de rapports sexuels.
A la Renaissance, les mouvements de la matrice laissent la place à la possession par le diable. Les corps faibles des femmes sont propices aux possessions. Les médecins sont souvent appelés aux procès en tant qu'experts.

> "Comme l'écrit Sarah Kofman, 'parce que la femme, en effet, n'a pas le droit à la parole, elle peut seulement avoir des secrets'. C'est précisément ce secret qui est en définitive compris comme la cause de la maladie. Or, le corps malade est censé rendre le secret douloureux et, partant, public. Ainsi, le corps apparait comme l''instrument de l'ordre restauré' : exorcisé, soigné, engrossé, il témoigne, de par son silence, non pas de la restauration de la santé, mais bien au retour de l'ordre social."

L'hystérie a longtemps été une maladie dite féminine, qui a été analysée en fonction de la matrice. Malgré de nombreuses incohérences dans les théories avancées, il faut attendre le début du projet nosographique pour que d'autres explications commencent à être avancées. Ce projet a pour but de décrire le plus précisément possible les maladies, comme l'écologie le fait avec les sytèmes vivants. Mais l'hystérie semble résister à cette étude, débutée par Sydenham.

> "Nous avons donc la réponse : l'hystérie est la seule maladie pour laquelle la méthode nosographique serait inefficace."

Les femmes ont donc longtemps continué d'être définie, dans leur différence par rapport aux hommes, par leur humeur et par le pouvoir médical.

### Chapitre 3 : Des corps mutants - prostituées, Africaines et tribades

TW : viol

Entre ces théories médicales et les théories théologiques, autre principe d'organisation majeur de la société, on peut observer des différences. La femme est décrite par la médecine et la philosophie comme frigide, faible alors qu'elle est décrite par l'Église comme lubrique, et plus forte qu le diable.
Certains types de femmes vont être considérées comme déviantes par la médecine, notamment les prostituées. Leur tempérament chaud est associé à la stérilité, bien loin des femmes caractérisées par leur tempérament froid et leur capacité reproductrice. À noter que les prostituées étaient assez régulièrement stérile à cause de leurs conditions de vie, de leurs nombreuses maladies et avortements.

La fécondité est largement théorisée. Beaucoup de choses influent sur la fécondité : position sexuelle, compatibilité de tempéraments, sentiments partagés, tempérament de la femme (trop chaud, il brule le sperme par exemple), ...
Ainsi une femme qui tombe enceinte après un viol doit avoir apprécié le moment. L'adultère est également vu comme une pratique non-reproductive.
Ces considérations sont encore très influentes au XIXe. Les femmes qui ne sont pas froides, sont donc exclues par la médecine de la normalité, de la féminité, elles sont virilisées.

> "En ce sens, le corps des prostituées est ce que j'appelle un 'corps mutant'."

Les catégories dominantes, normatives de la médecine qui assigne aux femmes un tempérament froid, va donc réussir à dépasser ses contradictions en inventant des corps mutants. Ces exceptions à la théorie dominante, ces marges vont donc être intégrées, justifiées. Dorlin parle de

> "l'extrême malléabilité des logiques dominantes qui se ménagent une véritable palette catégorielle, permettant de subsumer tous les corps et de s'adapter à toutes les configurations de pouvoir."

Reste une question, le tempérament chaud des prostituées règle-t-il leur sexualité, ou bien est-ce leur sexualité qui définit ce tempérament chaud ? À partir du XIXè siècle, la question ne se pose plus. Toutes les femmes sont initialement froides, c'est bien la sexualité déviante des prostituées qui définit leur tempérament. Elles sont donc malades, et désormais toutes les femmes sont potentiellement soumises à une pathologisation de leurs pratiques sexuelles.

Les femmes africaines ont été décrites par les explorateurs comme des femmes lubriques. L'excision était donc une pratique logique pour réfréner leurs ardeurs.

> "Pour les médecins européens, l'inversion des rôles sexuels et l'homosexualité font des habitants de l'Afrique des corps monstrueux, mais aussi fascinants."

À travers l'exemple africain, les médecins européens ont donc pu consolider, en négatif, la norme : formes des parties génitales (un clitoris hypertrophié n'est pas normal), désir, pratiques. L'homosexualité féminine, caractérisée par un clitoris hypertrophié, semble particulièrement problématique, de même que les personnes "hermaphrodites". Des procès (pour imposture, travestissement, sodomie) et chirurgies sont préconisées. Les médecins s'intéressent donc particulièrement aux origines de ces excroissances clitoridiennes chez les européennes.

> "nombre de pratiques sexuelles vont être définies comme pathologiques chez les Européennes, alors qu'elles seront comprises comme des traits anthropologiques chez les Africaines."

La masturbation féminine est vue comme une maladie proche de la fureur intérine à partir du XVIIIè siècle. Les femmes en se masturbant (et par l'orgasme) deviennent viriles et sont donc plus enclintes au tribadisme (lesbiannisme). Leur clitoris devient plus important et les femmes sont déviées de leurs fonctions reproductrices.

> "La masturbation est donc une initiation aux sexualités, à la liberté et, par là même, une débauche politique. On retrouve ici toute la symbolique de l'inversion de l'ordre sexuel qui figure le renversement de l'ordre social et politique."

### Chapitre 4 : Fureur et chatiments

Le pouvoir médical intervient donc au plus près des corps, en établissant des normes liées directement à la sexualité, sous couvert de moralité. Une des maladies particulièrement étudiées est la nymphomanie, archétype de l'immoralité. Foucault parle de "sensualisation du pouvoir" pour évoquer ces médecins à la fois fascinés et horrifiés par des sexualités qu'ils jugent étant contre la nature féminine.
La norme a longtemps été liée au mariage et à la reproduction. À partir du XVIIIè/XIXè siècle, on observe un litérature prolifique concernant les sexualités hérétiques (comme par exemple la sodomie). On parle moins de la norme et plus des marges.

> "Bienville assure que les femmes les plus prédisposées à la nymphomanie sont les jeunes filles nubiles, les filles débauchées, les femmes mal mariées, les juenes veuves, les femmes dont la véhémence naturelle du tempérament les porte à lire des romans luxurieux, à écouter des chansons passionnées, à boire des liqueurs et du café, à manger du chocolat ou des repas piquant, etc."

Les femmes nymphomanes sont également décrites comme étant plus souvent sveltes, poilues, très brunes, musclées, autrement dit à l'exact opposé des standards de beauté de la bourgeoisie.

Commen soigner les nymphomanes ? En refroidissant leur tempérament (sangsues, bain glacé) et par la morale (religion, mariage). Les médecins essaient de reféminiser les corps en les humidifiant. Ils refusent l'homomédication féminine (masturbation, relation homosexuelle) et prédisent des stimulations médicales. Ils prennent en charge ces stimulations. Plus clairement, ils médicalisentla masturbation, privent les femmes du pouvoir sur leur propre corps et les assignent à la déviance.

> "Certes, le corps féminin est 'saturé de sexualité', mais d'une sexualité médicalisée, dans laquelle le désir et le plaisir féminins sont désormais étroitement liés à la passivité, à l'hétéronomie, à l'ignorance et surtout à la douleur."

Ce pouvoir médical va servir de base au racisme :

> "Selon moi, au milieu du XIXè siècle, le processus de pathologisation du corps noir s'est déjà totalement converti en un processus d'anthropologisation. [...] Au XIXè siècle, les 'curiosités' sexuelles des autres nations africaines ne sont plus des objets licites d'excitation sexuelle, elles sont considérées commes les marques d'une humanité à peine sortie de l'enfance."

## Partie 2 : L'engendrement de la nation

### Chapitre 5 : Les vapeurs de la lutte des classes

Au XVIIIè siècle, la théorie des humeurs, liquides, se modifient et devient une théorie des fibres et des tensions nerveuses. Le trop humide relache les fibres (hystérie), le trop sec les rend cassantes (fureur utérine). La normalité féminine se situe donc entre deux pôles morbides. Pour Joseph Raulin, le juste milieu, le naturel féminin peut être trouvé chez les femmes de la campagne. Les bourgeoises sont trop oisives et donc vaporeuses, susceptibles à une mauvaise santé. De manière plus générale, pour Raulin, les bourgeois, par leur pratique, dénaturent leur corps. Les maladies féminines et masculines et cette classe tendent à se confondre car c'est n'est pas tant le tempérament qui importe que la façon de vivre.
Dans le cadre de la théorie des fibres, Bienville s'intéresse à la nymphomanie. Une des composantes qui le dérange le plus est le fait que les de leurs "fureurs", les nymphomanes en oublient jusqu'aux frontières de classe et se jettent sur les domestiques. Bienville et d'autres accusent les domestiques d'initier les bourgeoises à ces pratiques déviantes :

> "la domestique la plus froide est plus chaude que l'aristocrate la plus chaude."

À la veille de la Révolution, la médecine commence donc à imputer à certaines classes sociales certains tempéraments et certaines prédispositions à des maladies.

> "la vapeurs des gens de lettres, la mélancolie des courtisans, l'hyponcondrie des seigneurs, l'hystérie des aristocrates..."

Quant aux maladies féminines, l'hystérie est liée aux femmes de la noblesse, la nymphomanie aux femmes du peuple. On peut d'ailleurs noter que si l'hystérie de la noblesse est vu comme un trait social, comme issue d'un trop plein de féminité qui est correctible, la nymphomanie du peuple est un trait naturel, anthropologique. Ce sont donc les nobles qui peuvent, en modifiant leur comportement, devenir un "juste milieu" et symboliser la santé. Pour certains, l'hystérie peut être compris comme un signe de la nature qui indique qu'une jeune fille vierge est prête à marier. Dans ce cas, il ne s'agit même plus d'une maladie mais d'un signe permettant de continuer une vie ordonnée et policée.

### Chapitre 6 : La naissance de la "mère"

La médecine a pour objet de rétablir un corps sain, or le corps féminin a longtemps été vu comme naturellement malade. Même la grossesse a été pensée comme une maladie par les médecins hommes qui commencent à remplacer les sages-femmes. Au début du XVIIIè siècle, on commence à penser la normalité en terme de rythme de la matrice que l'on compare à un mécanisme régulier (ou non s'il est malade).

> "La santé féminine n'est donc conceptuellement possible qu'à la condition que le corps féminin puisse être pensé comme un corps en ordre, un corps bien réglé et, par conséquent, mesurable, prévisible et manipulable. Or, les phénomènes qui se prêtent le plus à cette quantification des phénomènes physiologiques sont la menstruation et la grossesse."

À partir de 1760, les traités sur la grossesse changent notablement. La grossesse n'est plus une maladie mais rentre dans le domaine de la santé. Pour Dorlin, il faut mettre ce phénomène en regard de la politique nataliste destinée au renouveau de la Nation française.

> "La 'mère' devient une norme de la santé féminine, car elle incarne un type physiologique à même de donner naissance aux enfants d'une Nation forte."

La grossesse est donc représentée comme une sorte de travail. Si les médecins, auparavant, en se méprenant, pouvaient la comparer à une maladie, c'est parce qu'ils avaient affaire à des bourgeoises douillettes.
La grossesse est également vue comme un bouleversement dans l'anatomie féminine qui doit avoir une grande souplesse / faiblesse pour s'adapter et tenir le coup. Au milieu du XVIIIè, s'instaure donc une nouvelle norme féminine dont la faiblesse est la force, physiquement comme psychologiquement.

> "Du tempérament flegmatique unique, qui fonde un déterminisme somatique non différencié (*toutes les femmes sont malades*), on passe donc à un échelle des tempéraments qui hiérarchisent les femmes entre elles, selon la distinction du sain et du malsain, lequel oscille entre l'excessive lacheté des fibres et leur excessive tension. Au sommet de cette échelle, la 'mère' est dont le type féminin de la santé, l'incarnation de la féminité même, laquelle s'autonomise progressivement de toute définition pathologique."

Le constat de Rousseau dans *L'Émile*, partagé par un grand nombre de médecins, est que la Nature a fait en sorte que les humains s'en sortent aisément. La médecine affaiblit les hommes car ces derniers ne craignent plus les douleurs et la mort. Les femmes bourgeoises sont culpabilisées car leurs maux viendraient du fait qu'elles vivent de manière urbaine et trop coupées de la Nature. De nombreux traités font l'éloge de l'allaitement et s'en prennent aux femmes qui font appel à des nourrices.

On assiste en parallèle à une augmentation des discours féministes. La société est en effet vue comme corrompue par une espèce de modernité, il fait revenir à la nature, vertueuse par défaut. On parle d'égalité et de droits naturels des hommes et des femmes. Cette "égalité", qui ne remet pas en question une distinction des sexes, n'est bien sur valide que pour les femmes, les mères, qui affrontent courageusement leur maternité.

### Chapitre 7 : Épistémologie historique des savoirs obstétriques
