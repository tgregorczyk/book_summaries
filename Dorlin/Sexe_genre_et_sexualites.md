# Sexe, genre et sexualités (Elsa Dorlin)

## Chapitre 1 : Épistémologies féministes

Le mouvement de libération des femmes des années 1960, avec pour emblème Le Deuxième Sexe de Simone de Beauvoir, consiste en la conscientisation de l'oppression des femmes et la mise en place d'un savoir féministe. D'abord sur l'origine et la reproduction de cette oppression, avec la théorisation de la catégorie politique des femmes comme de la famille patriarcale, et ensuite un savoir qui remet en question la science établie et masculine, aveugle aux enjeux féministes. Il convient donc, à la fois, de remettre en cause l'histoire, la politique, l'anthropologie etc avec un prisme féministe, mais aussi de constituer des savoirs trop longtemps négligés comme au sujet du corps des femmes. On assiste donc à une politisation du privé et de l'intime.

Le féminisme est souvent inspiré du marxisme qu'il critique, notamment dans son incapacité à rendre compte de l'oppression des femmes sauf à dire que le patriarcat est issu du capitalisme. D'ou la théorisation de la division sexuelle du travail ou les femmes sont assignées au travail reproductif, familial, invisibilisé quand les hommes participent à la sphère productive, valorisée. Les hommes sont donc coupés de tout un tas d'activités, ce qui influence leur vision du monde :

> "Les conditions de vie sont aussi des conditions de vue." Maria Puig de la Bellacasa

La féministe matérialiste Nancy Hartsock a ainsi essayé de créer une nouvelle figure dans les protagonistes du Capital, en plus du prolétaire et du capitaliste, voici la féministe. À partir de "point de vue" (*standpoint*) féministe, l'on peut créer un savoir qui part directement des conditions matérielles de femmes et permet de politiser la division sexuelle du travail. Ce savoir, politiquement situé et engagé, l'est tout autant que le savoir scientifique dominant considéré comme neutre.

> "Qu'il n'y ait pas de connaissance neutre est un lieu commun. Mais de notre point de vue cela a un sens très précis. Toute connaissance est le produit d'une situation historique, qu'elle le sache ou non. Mais qu'elle le sache ou non fait une grande différence ; si elle ne le sait pas, si elle se prétend "neutre", elle nie l'histoire qu'elle prétend expliquer \[...\]. Toute connaissance qui ne reconnait pas, qui ne prend pas pour prémisse l'oppression sociale, la nie, et en conséquence la sert objectivement."

On peut citer comme exemples Nicole Claude-Mathieu qui critique de nombreuses études enthographiques et anthropologiques qui invisibilisent les activités féminines et leur place dans la société. Ou encore Gilligan et les travaux sur l'éthique du *care* qui, tout en reconnaissant que les femmes ont, par leur position dans la société, une tendance accrue au *care*, refusent d'accepter la position hiérarchique inférieure du *care* par rapport à un idéal très masculin de la justice abstraite.

Sandra Harding parle d'objectivité forte pour désigner une science qui fait place aux points de vue minoritaires, et qui permet de mettre en lumière la fausse neutralité de la science dominante. Il s'agit donc de créer une science plus "objective" en remettant en cause certains de ses fondements.

## Chapitre 2 : L'historicité du sexe

Le concept de genre intervient dans les années 1950/60 dans le milieu médical au niveau du traitement des personnes intersexes. Celles-ci sont considérées comme anormales ne se conformant pas à la binarité sexuelle. Il convient donc de les opérer pour les réassigner à un sexe plus conventionnel. On commence à comprendre en parallèle que le sexe, c'est plus que la simple sexuation des corps. On comprend à travers les réassignations médicales que qu'il y a une construction sociale de la personne en tant qu'homme ou femme, qui n'est pas directement déterminée par le sexe "biologique", on commence à parler de genre.

> "il y a toujours déjà, dans ce que nous appréhendons communément comme le 'sexe biologique' des individus, du *genre* et les traces d'une gestion sociale de la reproduction, c'est-à-dire, une identité sexuelle (de genre et de sexualité) imposée, assignée."

Le terme de genre intervient pour la première fois dans la littérature féministe en 1972 avec Ann Oakley. Dans les années 1980, le focus se fait sur le concept de sexe, qui est donc différent de la sexuation des corps. En effet, la dualité genre / sexe avait eu tendance à naturaliser le sexe. Le sexe sera critiqué comme concept : il n'existe pas deux sexes, catégoriser chaque individu comme "homme" ou "femme" n'a rien de naturel.

Aujourd'hui encore, les opérations réalisées sur les personnes intersexes ont pour but une sexualité normale. Lorsqu'on crée un vagin (en vidant un micro-pénis du corps caverneux et en le retroussant par exemple), la principale préoccupation est sa pénétrabilité, pas sa capabilité à donner du plaisir. De nombreuses ablations sont généralement réalisées (glande mammaire, pomme d'Adam, utérus, ...) sur les personnes intersexes et constituent des violences inouies. Les personnes intersexes (2% des naissances diagnostiquées) sont étudiées avec des critères si draconiens, qu'une étude a montré que la moitié la population adulte pourrait être considérée comme "anormale" avec la même étude médicale. On mutile (ce mot est le mien) donc les personnes intersexes pour produire une personne se conformant à un genre "normal". On produit des hommes et des femmes. Le fondement de cette bicatégorisation n'est donc plus le "sexe" mais le genre. C'est par rapport à une idée normale des rôles H/F que sont opérées et suivies médicalement les personnes intersexes.

Pour Dorlin, le système de définition que l'on utilise est en crise perpétuelle : on invoque le sexe puis le genre pour justifier de la bicatégorisation H/F. Le système se reproduit en évoluant au gré des critiques mais sans jamais se remettre en question :

> "Le genre peut être défini comme un rapport de pouvoir qui assure sa reproduction en partie grace aux mutations du système catégoriel qu'il produit et sur lequel il s'adosse. \[...\] La capacité normative du genre, le fait que ce rapport social parvienne à substantialiser le processus de sexuation en deux sexes biologiques, et finalement civils, en dépit d'une normativité naturelle polymorphe, tient donc à sa capacité à maintenir un régime théorique et pratique en crise."

Dorlin critique donc les "dispositifs matériels et discursifs" qui permettent de créer des concepts qui se veulent la reproduction de réalités naturelles / biologiques / normales. Autrement dit, comment des concepts comme le genre naturalisent le fondement de l'organisation patriarcale. Ou plus simplement, comment des descriptions qui se font passer pour juste, scientifique, biologique, justifient la catégorisation H/F qui est à la base du système de domination patriarcale.

Disclaimer : chapitre un peu compliqué à comprendre pour moi, j'espère que je ne suis pas trop à côté de la plaque dans mon explication.

## Chapitre 3 : "Nos corps, nous-mêmes"

Les catégorisations H/F sont également liés à l'idée de l'hétérosexualité obligatoire ou comme la nomme Wittig, la pensée *straight*. C'est parce que l'hétérosexualité est vue comme une norme qu'il faut des hommes et des femmes. Cette idée, cette norme a influencé nombre de théories, comme le complexe d'Oedipe de Freud ou le "syndrôme d'aliénation parentale" qui crée l'image de la "mère folle" et qui fut utilisé par les tribunaux comme argument et fut retiré de la classification des maladies par l'OMS en 2020.

Il existe plusieurs critiques de l'hétérosexualité. Pour le féminisme radical (étatsuniens),  l'hétérosexualité est par défaut oppressive, créant une catégorie d'homme dominant. Pour les essentialistes, l'hétérosexualité se contredit quand elle affirme une relation entre deux personnes différentes. L'oppression d'une partie sur l'autre rabat autoritairement "l'Autre sous l'Empire du Même". Autrement dit, lors de la relation, les femmes sont niées. Une autre critique s'attaque à l'hétérosexualité en tant que pratique, au fond peu importe avec qui l'on fait du sexe, c'est le "comment" qui est important et regulé. Une dernière critique soulève la question de la gestion sociale de la reproduction.

> "\[Paola Tabet\] montre que l'hétérosexualité, monogame et reproductive, fonctionne comme une domestication de la sexualité des femmes les exposant *maximalement* au coit reproducteur."

Par exemple, l'hétérosexualité légale, le mariage, a longtemps été un moyen de s'approprier le corps des femmes et a constitué un permis de violer. Le viol conjugal n'a été reconnu en France qu'en 1992. Le droit à l'accès aux techniques contraceptives et à l'avortement est donc primordial. Le concept de "posséder son corps" et d'en jouir librement a été avancé mais peut poser problème.

> "Le droit des femmes à disposer de leur corps suppose une politique de justice sociale et d'égalité réelle. Si tel n'est pas le cas, ce droit des femmes peut toujours être conditionné par des impératifs populationnistes, nationalistes ou impérialistes et capitalistes sous couvert de slogans libéraux qui déclinent les bienfaits de l'individualisme possessif."

Silvia Federici a beaucoup travaillé pour actualiser le marxisme. Elle considère que le travail reproductif, "créer" des prolétaires, est un travail indispensable au capitalisme bien qu'invisibilisé par ce système prétend utiliser des ressources gratuites et omniprésentes. Les femmes sont donc des machines-outils à produire. Lors de l'accumulation primordiale du capital, il y a eut une augmentation des différences de genre qui ont permis cet asservissement des femmes. Les rapports non reproductifs ont été criminalisés (cf la chasse aux sorcières).

> "La famille devient une machine à discipliner et exploiter le travail reproductif et donc le sexe, le corps, le temps et la vie des femmes."

L'exploitation a aussi bien évidemment une composante raciale. Dans le Nouveau Monde, les champs de coton nécéssitaient une forte population d'esclaves qui constituait également une monnaie d'échange. Tout est donc lié, colonisation, capitalisme, patriarcat.

> "Ainsi, la radicalité - et le féminisme radical - consiste en une seule et même lutte antipatriarcale, anticapitaliste, écologique et décoloniale."

À ce féminisme radical, Federici oppose le féminisme libéral qui vise une meilleure égalité en droit H/F, des salaires, égaux etc

> "Son principal personnage est une héroine multitache, sponsorisée par l'Oréal, et partie à l'assaut du plafond de verre."

Pour Wittig, "c'est l'oppression qui crée le sexe", l'hétérosexualité et la différence des sexes ne sont pas naturelles. Et l'on peut parler, comme Guillaumin, de sexage, dans le sens ou les femmes sont une classe plus proche des serfs ou des esclaves que des prolétaires, car ce n'est pas seulement leur travail qui est accaparé mais tout leur corps. On peut parler de phénoménologie de la domination dans le sens ou la société organise, légalement ou moralement, un caractère féminin faible et disponible, "pour en faire des corps constamment 'chassés'."

Pour Wittig, une solution politique est le lesbiannisme. Si les catégories H/F ne sont pas naturelles mais créées par la norme hétérosexuelle, alors sortir de ces catégories, être lesbienne, c'est ne pas être une femme. Évidemment ce séparatisme lesbien présente des limitations dans le sens ou il ne considère qu'une forme d'oppression, le patriarcat, quand nous avons vu que les oppressions de classe et de race lui étaient inextricablement reliées. Les lesbiennes ne sont donc pas magiquement libres.

## Chapitre 4 : Le sujet politique du féminisme

Kimberlé Williams Crenshaw a proposé le terme d'intersectionnalité pour parler des intersections entre les différents rapports de domination. Pour prendre un exemple, le sujet du féminisme est souvent la femme blanche de classe moyenne, la lutte est donc centrée sur cette figure vue comme universelle. Or, d'autres femmes existent et sont invisibilisées par ce féminisme blanc.
Les oppressions ne sont pas additives, une femme noire ne subit pas simplement une oppression en tant que femme et une oppression en tant que noire. Elle subit une oppression en tant que femme noire, oppression qui a une histoire, qui est fondamentalement différente de celles subies par les femmes blanches et un racisme fondamentalement différent de celui subie par les hommes noirs, qui passent par des représentations, des stéréotypes et des conditions matérielles spécifiques. L'idée d'oppression additive sépare les oppressions, ouvre la porte à leur hiérarchisations ("commençons par s'attaquer au patriarcat, on verra après pour le racisme"), elle uniformise la diversité des oppressions subies par les femmes en ne considérant qu'un point de vue blanc, et donc en invisibilisant l'oppression que des femmes blanches ont pu faire subir aux femmes noires.

Aujourd'hui, le féminisme est utilisé pour dénoncer des pratiques patriarcales dans des pays du Sud. On dira que tel pays est barbare car les femmes y sont oppressées, forcées à porter le voile etc La norme féministe occidentale s'érige comme avant-garde éclairée. Cela pose la question de l'invisibilisation de patriarcat des pays du Nord et de la domination d'un féminisme occidental. La question de la décolonisation du féminisme se pose donc à cet égard.

Pour Butler, il est dangereux de partir du principe que le féminisme a pour objet la femme, c'est-à-dire que la catégorie femme est définie en amont du féminisme. Toute catégorie se définit par la négative, par exclusion, hier des esclaves noires, aujourd'hui d'un certain nombre de leurs descendantes. Il faut en permanence effectuer un travail de décentrement, ne pas s'intéresser seulement à l'oppression que l'on subit mais aussi à celle que l'on peut faire subir.

> "nous devons nous concentrer sur cette partie de l'oppresseur enfouie au plus profond de chacun de nous" Audre Lorde

## Chapitre 5 : Résister à la police du réel

La question que la praxis queer pose est celle de la subversion des normes, des catégories H/F, de la causalité anatomie/sexe/genre, et de l'hétérosexualité.
La théorie Queer est une expression de Teresa de Lauretis de 1991.

Pour Butler, le corps est un lieu ou s'exercent des techniques de pouvoir, des disciplines. Le corps est modelé et ainsi nait le genre :

> "Le genre constitue ainsi le corps en identité intelligible au sein de la matrice hétérosexuelle"

Le genre doit être recréé en permanence par des performances. Ces performances "font ce qu'elles disent", ce sont ces actions qui créent le genre.

> "Butler montre que l'énoncé de l'agent de l'état civil ou plus encore celui de l'échographe au cinquième mois de la grossesse - 'c'est une fille !' ou 'c'est un garçon !' - est un performatif. En effet, dans ces circonstances ou l'echographe est placé en situation d'autorité dans un lieu institutionnel qui est le cabinet médical ou l'hôpital, l'échographe, donc, fait du foetus, un individu genré, au sens ou le genre participe intrinsèquement à la définition dominante de l'individu - ici en devenir. Dès lors, on n'attend plus un 'enfant', on attend une 'fille' ou un 'garçon'."

Le genre est donc

> "un rituel que l'on nous enjoint d'effectuer."

> "Le genre c'est la stylisation répétée des corps." Butler

D'ou l'intérêt des drag show ou drag balls qui sont de troublantes performances. Des performances qui mettent à jour certains mécanismes enfouis, mais des performances au même titre que le genre de tout un·e chacun·e. Pour Butler, les drags ne sont pas subversifves en soi, ielles ne sont pas la parodie du genre, ielles sont une performance comme une autre, mais une performance dont on se rend compte. À ce titre, les drags kings sont particulièrement intéressants car ils performent la masculinité, censée être neutre et lisse et qui apparait donc comme une posture, au même titre que la féminité plus facilement caricaturable.

La performance vise à la copie d'un original et les dominants sont forts pour dénoncer les imposteurices.

> "Le sujet colonisé, subalterne imite, donc il est, mais puisqu'il imite, il ne sera jamais véritablement. Les masculinités subalternes sont toujours contraintes à une imitation défaillante de la masculinité policée, lisse, dominante, poussées dans un excès viriliste qui les rend suspectes."

Butler invoque donc la puissance d'agir pour créer des nouvelles formes, des sujets subversifs qui remettent en cause le Sujet. Ces nouveaux sujets doivent se constituer en sujets politiques pour supporter la répression de la matrice dominante. Encore une fois, l'identité ne doit pas être figée, sinon on revient à la discussion précédente sur les dangers de créer un sujet qui prééxiste à la lutte.

## Chapitre 6 : Patriarcat

Discussion intéressante sur le fait que les sexualités lesbiennes ont parfois été critiquées sur le fait qu'elles peuvent recréer des éléments hétérosexuels comme au travers des fem/butch ou de l'utilisation de godemichets. On peut aussi voir dans l'utilisation de nouvelles techniques de nouvelles opportunités :

> "Le gode permet ainsi de modifier la géographie érogène du corps, en la détachant de sa référence phallocentrique."

De nombreux débats ont également lieu dans les milieux féministes au sujet de la pornographie. Hétérosexiste de manière inhérente, le pornographie produit une sorte de vérité du sexe, donc une norme problématique. Des débats ont lieu entre les abolitionnistes et celleux qui souhaitent une subversion de ce système.
Les corps sont stéréotypés tout comme les comportements, les pratiques sont codifiées et fantasmées de manière raciste et sexiste.

> "\[Linda Williams\] analyse comment la vérité de la jouissance féminine est produite sous la forme de la confession involontaire, passive ou de l'aveu arraché à ses dépens."

Les scènes sont construites autour de l'homme (le début avec l'érection, la fin avec l'éjaculation), le viol est souvent mis en scène.

> "Le plaisir est comme produit mécaniquement, aux dépens de tout consentement - comme est produit mécaniquement le rictus par l'électricité, comme la crise hystérique est stimulée par la manipulation de Charcot - il est saisi dans sa naturalité et provoqué, stimulé à l'infini comme une expérience qui n'en finit pas de confirmer une vérité."

Il est enfin également intéressant de regarder de plus près la masculinité. La masculinité dominante, blanche, exclut celle des colonisés. Ils sont à la fois dévirilisés (ils sont indolents, laches, efféminés, sensibles, doux, idiots) et survirilisés (violents, bestiaux, libidineux, cruels, immoraux).
Cela est parfois utilisé pour justifier la colonisation, comme pacification et introduction de moeurs et de civilisation. Mais, comme le montre Frantz Fanon, les comportements des colonisés sont profondément des résultats de la colonisation. Les colonisé·es ont de bonnes raisons d'avoir peur ou d'être violent·es.

La psychanalyse et notamment le complexe d'Oedipe de Freud ont fait beaucoup pour justifier l'ordre patriarcal, avec la figure du Père :

> "L'idée que les violences sont essentiellement l'expression des dispositions pulsionnelles constitutionnelles convient parfaitement à ceux qui se représentent que l'ordre qui régit les générations et le rapport des hommes aux enfants et aux femmes est dépourvu de violence, mais revêt la figure même de l'ordre des choses." Michel Tort

"Le discours psychanalytique hégémonique" est donc une source pérenne de gouvernementalité c'est-à-dire de stratégie disciplinaire inconsciente et intériorisée.
