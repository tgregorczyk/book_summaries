# Dette : 5000 ans d'histoire

## Chapitre 1 : L'expérience de la confusion morale

David Graeber commence son livre, comme souvent, par une petite anecdote. Il en profite pour nous parler du FMI. En bon militant altermondialiste, il dénonce le fait que l'argent prêté aux nations du Tiers-Monde est soumis à des intérêts importants et à une contrepartie économique: la politique du pays doit être libérale *made in Washington*. Il milite donc contre le FMI (qui vérifie le bon remboursement des dettes) et pour l'annulations de ces dettes.

En effet, les dettes ne doivent pas toujours être remboursées (c'est pour ça qu'il y a des intérêts). Et pourtant le FMI est là pour garantir les remboursements, quitte à imposer des plans d'austérité, qui touchent principalement les services publics. Par exemple, Madagascar a rogné sur la prévention du paludisme, une épidémie s'est déclarée et a fait 10 000 morts.

On peut remarquer aussi que la condition du débiteur n'est pas universelle. En effet, les États-Unis sont extrêmement endettés mais personne le leur dit rien car "ils ont le pistolet".

La guerre entre créancier et débiteurs existent depuis bien 5000 ans. Et à chaque révolte, le même programme:

> "annulation des dettes et redistribution des terres"

Graeber parle de la façon dont il est mal vu d'être débiteur (notamment aux États-Unis). Ne pas payer ses dettes, ce n'est pas moral. Pourtant si la crise de 2008 a eu lieu c'est en partie parce que des banques ont prêté de l'argent de manière complèment inconsidérée, sachant pertinemment qu'elles ne seraient pas remboursées par le débiteur mais que les lois et les institutions les soutiendraient / rembourseraient.

## Chapitre 2 : Le mythe du troc

On commence par le grand mythe des économistes qui vient d'Adam Smith: au début il n'y avait pas de monnaie. Les gens s'étaient spécialisés dans certains domaines et produisaient des objets qu'ils troquaient (parce que c'est dans la nature humaine d'échanger des trucs, c'est une pulsion voyez vous ...). Or le troc, ce n'est vraiment pas pratique parce qu'il faut que ce que possède une personne soit exactement les besoins de l'autre et inversement. Donc on a inventé la monnaie pour remédier à ce problème. Ensuite sont nées banque, crédit etc

Le soucis c'est que cette histoire n'a jamais été observée dans la réalité. Personne n'a jamais trouvé le pays du troc. On peut avoir des exemples de trocs entre différents peuples mais jamais au sein d'une société. Et cela pour une bonne raison. Lors d'un troc, chaque partie essaye en général de tirer le meilleur profit de l'échange. Imaginons donc une société dans laquelle, à chaque échange, les parties prenantes essayent d'avoir l'autre. Cela ne peut mener qu'au conflit.

Ce qui est plus réaliste serait d'imaginer que si, entre deux voisins, l'un a besoin de quelque chose, l'autre lui fournira sans problème. La situation inverse se reproduira bien un jour ou le premier voisin pourra s'acquitter de sa dette (dette!?).

> "En réalité, tout porte à croire que le troc n'est pas un phénomène particulièrement ancient, et qu'il ne s'est vraiment répandu qu'à l'époque moderne. Il est certain que, dans la plupart des cas que nous connaissons, il a lieu entre des personnes auxquelles l'usage de la monnaie est familier, mais qui, pour une raison quelconque, n'en ont pas beaucoup."

En effet, historiquement la quantité de monnaie était relativement faible et ne suffisait pas. L'histoire (vraie) a donc débuté par le crédit. On faisait les comptes de qui devait quoi à qui, et l'on pouvait se rembourser avec globalement n'importe quoi. Les pièces de monnaie sont arrivées bien plus tard et sans jamais remplacer les crédits. L'histoire qu'a écrite Adam Smith est donc dans le mauvais sens.

## Chapitre 3 : Dettes primordiales

La vision chartaliste / théorie de la monnaie de crédit :

La monnaie mesure la dette.
Imaginons qu'Alice donne quelque chose à Bob. Bob en retour, donne à Alice une reconnaissance de dette. Alice peut effacer cette dette une fois qu'elle est remboursée ou bien l'utiliser pour payer autre chose. La reconnaissance de dette circule, c'est de la monnaie. Le plus important est qu'il faut avoir confiance en Bob, sinon ce papier ne vaut rien. Et la monnaie ne fonctionne que si Bob ne rembourse jamais sa dette.

Et c'est ainsi que la banque d'Angleterre est née : par un prêt au roi qui n'a jamais et ne peut pas être remboursé.

Mettre en place une monnaie peut être un moyen de créer des marchés. Par exemple on peut exiger que l'impôt soit uniquement payé dans la monnaie battue par l'État. Admettons que l'État paie son armée avec cette monnaie, alors tout le monde va vouloir commercer avec eux pour se procurer de quoi payer les impôts.

> "Les sociétés sans États sont aussi, très généralement, sans marchés."

Bien que les libéraux suiveurs de Smith veulent croire le contraire.

La dette primitive :

Les humains endettés. Auprès des dieux, des élites, de l'humanité toute entière, et il faut donc faire des actions tout au long de sa vie pour rembourser cette dette. Cela va de l'adoration de dieux, à l'hospitalité (pour rembourser sa dette auprès de l'humanité), etc

De là on peut imaginer que les États ont récupéré cette dette primordiale pour en faire la base de leur système fiscal (l'impôt pour rembourser la dette).

Le problème de cette vision est qu'une dette est censée pouvoir être rendue, or la dette que l'on a auprès des dieux ou de l'humanité ne peut être qu'infinie. Ce genre de dette ne peut être remboursée, et même si c'était possible, serait-ce souhaitable ? Imaginons que l'on soit endetté auprès de ses parents pour notre naissance, notre éducation, ... Est-il souhaitable de vouloir être quitte avec ses parents ? Ou cela signifie-t-il que la relation est brisée ?
Pour Graeber la notion d'endettement auprès de la société est un concept étrange car il ne peut s'appliquer que pour des nations modernes. Avant la plupart des gens ne savaient pas forcément à quel pays / société ils appartenaient. C'est au final au vision très nationaliste, on doit quelque chose à notre État.

## Chapitre 4 : Cruauté et rédemption

La monnaie est donc à la fois une marchandise et une reconnaissance de dette. Une marchandise dans le sens ou il s'agit parfois de pièces d'or ou d'argent, qui ont une valeur intrinsèque, mais aussi une reconnaissance de dette: si un État accepte cette pièce comme paiement par exemple, la valeur de la pièce devient sa valeur nominale qui était souvent supérieure à sa valeur en matériau.

L'analyse de Nietzsche: si l'on part du principe que les relations humaines sont basées sur l'achat et la vente, on peut arriver à la conclusion que lorsque des sociétés se forment, la relation à ces sociétés et à leur cosmos sont pensées en terme de dettes. Les axiomes de Smith sur le fonctionnement de l'être humain, autrement dit, mènent à la théorie de la dette primordiale. Or si l'on regarde les récits anthropologiques, oui, l'homme (et la femme et les autres) à tendance à comparer et à mesurer, mais les relations égalitaires sont basées sur le fait que l'on refuse de calculer. Aider quelqu'un, faire un cadeau, c'est normal, c'est nécessaire et l'on n'a pas besoin de considérer que le receveur a une dette envers nous.

Pour beaucoup de personnes dans l'histoire, la monnaie et les dettes étaient synonymes de grand malheur. Le non-remboursement des dettes menaient souvent à l'esclavage des fils et des filles des débiteurs.
Or un contrat de dette est censé être passé entre personnes égales. De nombreuses crises de la dette ont eu lieu pendant lesquelles on s'indignait du sort réservé aux débiteurs malchanceux. Comment accepter une telle cruauté entre personnes égales ? Les esclaves, par exemple, étaient *intrinsèquement* inférieurs et l'on s'intéressait peu à leurs conditions de vie.

> "toutes ces religions - du zoroastrisme à l'islam - sont nées au milieu d'intenses polémiques sur le rôle de la monnaie et du marché dans la vie humaine."

et toutes religions justifient les arguments pour une annulation des dettes (Dieu libère \[liberté était souvent égal à la libération d'une dette\] les Juifs de l'Égypte, les vieilles lois juives demandent une annulation des dettes tous les sept ans, Jésus se sacrifie pour racheter les péchés des hommes, ...), la libération des asservis, la redistribution des terres.

## Chapitre 5 : Bref traité sur les fondements moraux des relations économiques

Une question importante est de définir la dette. Il semble qu'elle soit partout, même dans les dons. Dans son *Essai sur les dons*, Marcel Mauss présente un certains nombres de sociétés dans lesquelles les dons créent une dette qui doit être rendue de peur de représaille sévère des esprits.
De nombreuses théories ont été créées pour donner aux échanges humains une base de *réciprocité*. C'est en effet une vision plus optimiste de l'humanité que de penser les rapports humains comme étant basés sur des dettes.
Par exemple avons-nous une dette envers nous parents ? Si une dette doit toujours être rendue, alors non, car payer cette dette signierait que l'on ne veut plus avoir affaire à ses parents, que l'on veut être quitte.

Graeber essaye de cartographier "trois grands principes moraux susceptibles de fonder les relations économiques".

### Le communisme

> "De chacun selon ses capacités, à chacun selon ses besoins"

On parle là d'échanges qui sont présents dans toutes les sociétés, ce sont les échanges pour lesquels on en compte et même plus, il serait considéré comme blessant de compter. Cela peut aller, dans une grande ville, à demander du feu, à des échanges beaucoup plus importants dans des groupes de personnes plus intimes.

### L'échange

Il s'agit la d'un échange plus impersonnel ou l'on s'intéresse d'abord aux valeurs des choses échangées. Contrairement aux relations communistes qui partent du principe que la société durera toujours, les échanges peuvent être continus ou n'avoir lieu qu'une fois. Les échanges sont censés être relativement égaux. S'ils le sont parfaitement, cela signifie que les parties prenantes sont libres, quittes. Sinon, l'échange continue.

> "Tout cela peut devenir très proche du troc, de l'échange direct d'un objet contre un autre - qui existe, nous l'avons vu, même dans le cadre de ce que Marcel Mauss appelle les 'économies de don', bien qu'il qu'il ait lieu essentiellement entre étrangers. Au sein des communautés, il y a presque toujours une réticence, comme l'illustre admirativement l'exemple des Tiv, à laisser les choses 's'annuler' - c'est l'une des raisons pour lesquelles, s'il y a de l'argent qui circule, les gens refuseront souvent de l'utiliser avec les parents et amis"

### La hiérarchie

La hiérarchie intervient là ou l'on ne peut pas avoir d'échange, parce qu'il n'y a ni égalité ni réciprocité possible. Cela peut être le cas avec les rois par exemple, ou avec des enfants, ou des mendiants. L'échange dépend des relations antérieures (on s'attend à payer des impôts d'un montant relativement stable au fil des années, un enfant qui a reçu u  cadeau peut s'attendre à en recevoir à nouveau un autre plus tard, ...).

> "Nous pouvons écrire ici une formule simple : une action répétée devient coutumière ; elle en vient donc à définir l'essence même de celui qui l'accomplit."

S'il y a "échange" dans ce cadre, les choses échangées ne peuvent pas être comparées, comme par exemple du blé et une protection militaire.

> "C'est précisément ce critère qui permet d'évaluer le degré d'égalitarisme réel d'une société : les personnes en position de d'autorité manifeste sont-elles de simples canaux de redistribution ou peuvent-elles utiliser leur position pour s'enrichir ?"

### Le passage d'une modalité à l'autre

Un exemple classique est le passage du communisme à la hiérarchie. Si l'on considère de grands chasseurs, alors il peut arriver que le groupe qui se fait nourrir se ressente endetté, dépendant, dans une relation asymmétrique. Il est donc nécessaire de créer des garde-fous pour que les chasseurs ne deviennent pas supérieurs. Cela passe généralement par l'autodérision, et tout chasseur qui se vante de ses captures sera raillé.

Un autre exemple intéressant est celui du contrat du travail dans lequel, deux parties *égales*, deux personnes, signent et rompent l'égalité car dans le travail, l'un sera subordonné à l'autre.

Pour qu'il y ait une dette il faut donc que les deux parties soient plus ou moins égales, et qu'il y ait un moyen de rembourser cette dette.

> "Tant que la dette n'est pas remboursée, c'est la logique de la hiérarchie qui s'applique. Il n'y a aucune réciprocité."

> "Une dette n'est donc qu'un échange qui n'est pas allé jusqu'au bout. Par conséquent, la dette est strictement une créature de la réciprocité."

> "La dette c'est ce qui se passe dans l'entre-deux : quand les deux parties ne peuvent pas encore se séparer, parce qu'elles ne sont pas encore égales. \[...\] Cependant, puisque atteindre cette égalité détruit la raison même d'avoir une relation, tout ce qui est intéressant se passe dans l'entre-deux. Tout ce qui est humain, en fait, se passe dans l'entre-deux - même si cela signifie que tous les rapports humains de ce genre comportent, fut-ce à dose infime, un élément de criminalité, de culpabilité ou de honte."

(la raison étant que dans beaucoup de langues européennes l'étymologie du mot dette est proche de "faute", "péché", "culpabilité").

## Chapitre 6 : Jeux avec le Sexe et la Mort

L'histoire économique avec ses petites histoires censées expliquer comment tout a commencé omet énormément de choses, et par exemple les femmes :

> "Dans l'Irlande antique, les femmes esclaves étaient si nombreuses et si importantes qu'elles ont fini par servir de monnaie."

De la même façon, un homme endetté pouvait parfois (rarement) vendre sa femme et ses enfants en esclavages.

Un autre point important est que les monnaies appelées primitives ne servent pas forcément à acheter des choses :

> "On les utilise en réalité pour créer, maintenir et réorganiser autrement les relations entre des personnes : arranger les mariages, établir la paternité des enfants, consoler les endeuillés lors des funérailles, demander pardon en cas de crime, négocier des traités"

Graeber parle alors de "monnaie sociale" et "d'économie humaine", "soucieux non d'accumuler des richesses, mais de créer, détruire et redisposer des êtres humains."

### La monnaie comme substitut inadéquat

Graeber explique la théorie de Philippe Rosbapé :

> "Selon sa thèse, la "monnaie primitive" n'était pas, à l'origine, un moyen de payer ses dettes, quelles qu'elles fussent. C'était une façon de reconnaitre l'existence de dettes impossibles à rembourser."

Par exemple, ce genre de monnaie servait beaucoup pour les mariages. Pourtant personne n'ira revendiquer qu'on peut acheter une femme avec de la monnaie. Par exemple chez les Tiv, il est clair que le seul prix pour une femme est une autre femme, s'en est construit un système ou celui qui donne une femme de sa famille à marier acquiert le droit de prendre une femme de l'autre famille à marier etc Si la monnaie entre en jeu, elle ne peut pas régler la dette.

Dans le cas d'un meurtre, la compensation financière permet d'appaiser la famille endeuillée mais elle n'efface pas les rancoeurs et ne compense pas la vie volée. Chez les Nuer, cette compensation est le prix "d'achat d'une femme" (40 têtes de bétail). Elle est alors mariée au défunt et sa progéniture sera enfant du défunt (qui que soit le père réel). On a alors plus ou moins essayé de compenser une mort par une vie.
Chez les Iroquois, après une mort (même parfois une mort naturelle), on envoyait un groupe de guerriers attaquer un village pour capturer quelqu'un. Cette personne était alors soit mise à mort soit adoptée, pour remplacer le défunt.

### Les dettes de sang (chez les Lele)

Dans les années 50, groupe humain d'environ dix mille personnes. La monnaie sociale sur place était le tissu de raphia, cousu par les locaux (et les barres de cam, essence d'arbre, bien plus rares). Il était principalement utilisé pendant les mariages, les accouchements, pour payer les guérisons etc. Les dons allaient en général à des supérieurs hiérarchiques. Les jeunes qui avaient parfois besoin de tissus pour une occasion particulière pouvait en demander aux plus anciens, ils étaient donc toujours un peu leurs débiteurs.

Contrairement aux Tiv il n'y a pas de moyens "d'acquérir" des femmes. Par contre on considère que toute personne qui meurt est tuée. Il faut donc trouver un coupable, un ennemi, un amant, ou bien quelqu'un qui aura été dénoncé par la magie. Le coupable est redevable d'une dette de sang qui prend la forme d'une femme de sa famille. Il en résulte un système complexe (chacun sait qu'il peut être redevable d'une dette de sang à n'importe quel moment, et va essayer d'obtenir des gages à échanger plutôt que de donner des femmes de sa famille) car il n'y a pas assez de femmes à échanger donc les femmes sont parfois promises avant leur naissance ...

Mais les femmes avaient des moyens d'échapper à ces intruments de contrôle. Si on la forçait, elle pouvait toujours s'enfuir pour un village ennemi ou elle devenait une femme du village (elle était littéralement mariée au village).

Si un village Lele cherchait à éviter les tensions de manière générale, elles existaient toujours et étaient centrées autour les femmes. Cela était aussi du au fait que les anciens prenaient plusieurs femmes, n'en laissant que peu aux jeunes hommes qui essayaient alors de séduire les femmes des ainés.

La règle générale était donc d'une vie pour une vie, sauf dans le cas ou l'on pouvait payer un village pour aller capturer une femme.

### La dette de chair (chez les Tiv)

Les tissus des Lele étaient par ailleurs leur habit, et les barres de cam permettaient de créer une pate rouge qui sert au maquillage. Très souvent, la monnaie sociale est faite de choses qui enbellissent les personnes. Les contre-exemples comme le bétail sont généralement advenus avec les États.

On distingue trois sphères d'échange chez les Tiv. La première est associée aux femmes et aux échanges incessants de dons. La seconde est associée aux hommes, ce sont des échanges effectués avec la monnaie locale (du tissu ou des faisceaux de baguettes de laiton importées). La troisième sphère est celle de l'échange des femmes et de leur contrôle par les hommes.

Les Tiv sont relativement égalitaire, les anciens dominent au sein d'une famille, ont plusieurs femmes mais il n'y a pas de structures politiques.

Les Tiv pensent que le *tsav*, le courage, le charisme, peut être augmenté en mangeant de la chair humaine. En fait, c'est un soupçon, ils n'étaient pas cannibales mais supposaient que peut-être certaines personnes l'étaient, des sortes de sorciers. Les sorciers sont de mauvaises personnes qui essayent, par la ruse, de forcer les gens à manger de la chair humaine, ce qui crée une dette de chair. Si le débiteur n'est pas assez fort en *tsav*, alors la dette continue à jamais et le débiteur est obligé de sacrifier toute sa famille.

### La traite

De manière générale, on retrouve souvent ce genre de récit, qui dans ce cas appelle à se méfier du pouvoir. Mais pourquoi l'exprimer sous forme de dette ?

Les barres de cuivre nous renvoient à la traite des esclaves, car elles venaient d'Angleterre.

A cette époque, les européens avançaient du matériel aux riches commerçants africains qui livraient des esclaves. Pour être sûr d'être remboursés, ils exigeaient souvent des personnes-gages, des membres proches voire de la famille du commerçant qui devaient travailler pour les européens sans être tout à fait esclaves.
La traite au golfe de Biafra a rendu esclave environ un million et demi de personnes. Dans une première étape il semble que c'était simplement le chaos dans la région avec des raids dans tous les sens. Ensuite la confédération Aro proposa de ramener l'ordre mais créa un système juridique si sévère que n'importe quelle infraction pouvait faire de vous un esclave.

Une autre technique a été de créer des société secrètes. Il était très prestigieux, et très cher d'appartenir à celles-ci, ce qui a créé beaucoup d'endettement qui a mené à beaucoup de nouveaux esclaves.

On voit bien le rapprochement avec le mythe Tiv des sorciers.

Pour Graeber, on a affaire à un renversement des économies humaines (du fait du contact avec une économie commerciale). Les systèmes qui visaient à la reproduction humaine sont devenus moyens de les détruire.

### Réflexion sur la violence

Le principe de base est qu'une monnaie sociale ne peut jamais valoir un être humain, et même que deux êtres humains entre eux ne peuvent être comparés. Alors comment en vient-on à échanger des femmes par exemple ? Cela demande de la violence. Il faut d'abord arracher la personne à sa famille, à son contexte, couper les relations humaines qui font d'elles quelqu'un d'unique. Et cela est possible uniquement parce que certains types de violence sont moralement acceptable.

## Chapitre 7 : Honneur et avilissement

### L'honneur est un surplus de dignité

> "L'esclavage est la forme ultime de l'arrachement à son contexte, donc à toutes les relations sociales qui font de quelqu'un un être humain. Il y a une autre façon de le dire : en un sens tout à fait réel, l'esclave est mort."

> "La réponse d'Elwahed est d'une simplicité frappante : on devient esclave dans les situations ou, sans cela, on serait mort."

En effet, on devient généralement esclave après avoir été fait captif soit à la guerre soit lors de raids, ou bien suite à un crime commis qui aurait pu valoir la peine de mort.

> "l'honneur, c'est un surplus de dignité. C'est cette conscience accrue du pouvoir, et de ses dangers, qui vient du fait qu'on a privé d'autres personnes de pouvoir et de dignité; ou, au minimum, que l'on se sait capable de le faire."

Pour Graeber, parmi les monnaies archaiques

> "la valeur de la monnaie était, en définitive, la valeur du pouvoir de transformer d'autres personnes en monnaie"

### Le prix de l'honneur (en Irlande, au début du Moyen-Age)

À partir de 600, on ne pratique plus trop l'esclavage mais on continue d'utiliser les *cumals* comme monnaie, littéralement fille-esclave. L'Irlande dde'époque vit de son bétail, sa monnaie est sociale. Des codes juridiques détaillés existent et précisent notamment le prix de l'honneur des personnes suivant leur rang. L'honneur d'un roi vaut 7 *cumals*, payés avec 21 vaches ou 21 onces d'argent. À cette époque il y avait environ 150 rois en Irlande chacun avec environ 2000 sujets. L'honneur d'un paysan riche valait deux vaches et demie. Ces montants étaient en quelque sorte des amendes en cas d'affront à la dignité d'une personne. Le prix du sang, celui à payer en cas de meurtre, était plus élévé (sauf pour le roi pour qui c'était le même prix), auquel on ajoutait le prix de l'honneur.
De manière général, pour tout vol ou blessure, l'amende à payer était un certain montant auquel on rajoutait le prix de l'honneur de la personne concernée (celle en charge de protéger la victime).

Les juristes avaient également le droit de rétrograder le status d'une personne si celle ne défendait pas son honneur suffisamment.

Le prix de l'honneur d'une femme qui n'a pas ses terres est la moitié de celui de son plus proche parent masculin. Lors du mariage ou lors de l'acquisition de cerfs, on paie le prix de l'honneur et l'on devient tuteur. Son prix de l'honneur augmente comme si l'on avait absorbé l'honneur de l'autre personne, et l'on devient responsable de la dignité de cette personne.

Alors pourquoi utiliser une monnaie qui repose sur des esclaves si leur prix de l'honneur vaut zéro ? Pour Graeber, la valeur de l'esclave est la valeur de l'honneur qu'on lui a pris.

> "si l'honneur de quelqu'un repose en dernière analyse sur sa capacité à extraire l'honneur des autres, c'est parfaitement sensé."

### La Mésopotamie (aux origines du patriarcat)

> "Dans les tout premiers textes sumériens, en particulier ceux qui datent, en gros, de 3000 à 2500 av JC, les femmes sont partout. \[...\] Dans le millénaire qui suit, tout change. La place des femmes dans la vie civique s'érode. Peu à peu, la structure patriarcale qui nous est familière prend forme, avec son insistance sur la chasteté et la virginité avant le mariage, l'affaiblissement et, finalement, la disparition totale du rôle des femmes dans l'État et les professions libérales, et la perte de leur indépendance juridique, qui fait d'elles des pupilles de leur mari. À la fin de l'age de bronze, vers 1200 av JC, nous commençons à voir quantité de femmes séquestrées dans des harems et (dans certains endroits au moins) assujetties au port obligatoire du voile."

Une explication classique jette la faute sur la centralisation des États et l'intensification des guerres. Graeber rappelle que la dette, les marchés, les États et la guerre sont toujours très liés.

Idée classique en anthropologie : si la population est assez peu nombreuse et la terre ne manque pas, on peut observer la *bridewealth*, on paie pour avoir une femme, dans le cas contraire on observe plutôt une dot.

De manière générale, la *bridewealth* ne correspond pas à l'achat d'une femme (notamment parce qu'on ne peut pas la revendre). Chez les sumériens, la dot était pratiquée et est devenue une forme de simple transaction monétaire.

> "Se marier se disait 'prendre possession' d'une femme : c'était le même mot que pour l'appropriation de biens."

Alors pouvait-on vendre sa femme ? En temps normal non, mais en cas d'endettement oui (pareil pour les enfants d'ailleurs). Honneur et crédit sont devenus les deux faces d'une même pièce : être capable d'assurer la protection de sa famille et de sa maisonnée.
En gage d'un prêt il était même possible de louer sa femme ou ses enfants.

La prostitution était courante. Certaines femmes prêtresses effectuaient des rituels érotiques et se livraient aux fidèles. Il n'était pas forcément mal vu de se prostituer même si nombre de prostituées étaient esclaves.

> "Le patriarcat est né, d'abord et avant tout, du rejet des grandes civilisations urbaines"

Autrement dit, ces grandes villes, avec ses dettes, ses impôts, et ses prostituées représentants la marchandisation des corps.

> "l'extraordinaire insistance des rebelles \[ceux qui fuyaient les villes\] sur l'autorité absolue des pères et la protection jalouse de leurs femmes et filles volages était une conséquence de - mais aussi une protestation contre - la marchandisation des personnes dans les cités qu'ils avaient fuies."

Dans l'histoire Babylone est restée l'un des berceaux de la civilisation mais aussi la ville des prostituées. Cette histoire a été reprise par les civilisations grecques, romaines et par les grandes religions.

> "La prostitution commerciale avait une autre source : la paupérisation des paysans et leur recours croissant à l'emprunt pour survivre en période de famine, ce qui conduisait à l'esclavage pour dettes." Gerda Lerner

Un code assyrien entre 1400 et 1100 av JC sépare les femmes respectables (dont le corps ne peut être acheté) des prostituées et esclaves. Ces dernières n'ont pas le droit de porter le voile (les autres le doivent et en ont intérêt).

Le rôle des États dans tout ceci n'est donc pas anodin. En promouvant les marchés et en faisant respecter leurs lois sur la dette, ils contribuaient à créer les conditions propices à la vente des personnes, et donc à la prostitution. En réaction, les conceptions de l'honneur ont changé et le patriarcat s'est développé.

### La Grèce antique (honneur et dette)

Processus habituel : pas de monnaie ou monnaie sociale, développement de la monnaie par l'État pour payer les soldats et recevoir des impôts, développements des marchés (sur l'agora notamment).

Contrairement au Proche-Orient, la solution aux 'crises de la dette' ne furent pas une amnestie régulière mais une abolition du péonage. Les grecs ont préféré une politique d'expansion, créant des colonies de Marseille à la Crimée "qui ont servi de conduit à un commerce d'esclaves très actif".

Les aristocrates (contrairement aux démocrates) méprisaient la monnaie :

> "ils mettaient un monde du don, de la générosité et de l'honneur au-dessus de l'échange commercial sordide."

> "Du temps de Socrate, si l'honneur d'un homme était de plus en plus lié au mépris du commerce et à l'assurance dans la vie publique, l'honneur d'une femme se définissait presque exclusivement en termes sexuels : une question de virginité, de pudeur et de chasteté, puisque les femmes respectables devaient rester cloitrées dans la maison, et que toute femme qui jouait unn rôle dans la vie publique était par la même perçue comme une prostituée ou ne valant pas mieux. L'habitude assyrienne du voile n'a pas fait tache d'huile au Moyen-Orient \[Perse, Syrie, ...\], *mais elle a été adoptée en Grèce*."

La monnaie a donc eu un rôle fondamental dans cette histoire. Si elle mesurait initialement l'honneur en tant que monnaie sociale, elle s'est mise à mesurer tout ce que l'honneur n'était pas. La monnaie efface les différences, car elle peut tout acheter. Elle a aussi démocratisé les désirs car tout le monde en voulait, et toujours davantage.
Enfin, changement majeur, elle n'était pas seulement un désir mais aussi un besoin. Il était auparavant acquis que les besoins vitaux d'une personne était comblés, quitte à devenir servant chez les riches. Désormais, ces besoins se monnayent en argent.

Cela change tout. Si avant une personne pauvre se mettait au service d'une personne riche, les deux parties avaient des obligations l'une envers l'autre. De plus, ces personnes n'étaient pas considérées comme égales. Nous l'avons vu, contracter une dette revient à considéré les parties comme égales. De plus, le créancier n'a plus d'obligations envers le débiteur.

### La Rome antique (propriété et liberté)

Le droit romain a été extrêmement influent, un peu partout dans le monde. Il est notamment basé sur la propriété privée :

> "relation entre une personne et une chose charactérisée par le pouvoir absolu de cette personne sur cette chose."

Cependant cette définition semble bizarre car lorsqu'on parle de propriété, on pense davatange à une relation entre le propriétaire et d'autres personnes. Posséder quelque chose signifie être le seul à pouvoir se servir de cette chose. De plus, on ne peut visiblement pas faire n'importe quoi de ce que l'on possède, il reste des lois (Graeber prend l'exemple d'une tronçonneuse).

Les juristes médiévaux affinent cette définition en introduisant l'*usus*, l'usage, le *fructus*, l'usage des fruits de l'objet et l'*abusus*, le pouvoir de destruction. Mais les romains ne semblaient pas avoir besoin d'aller jusque dans ces précisions.

Il est connu que dans les familles romaines, tout tournait autour de l'homme. Son autorité était absolue et il pouvait faire ce qu'il voulait de sa femme (à ceci près qu'elle appartenait encore en partie à son père), ses enfants, ses domestiques et esclaves. Même les tuer. Il semble possible que les juristes soient donc partis de ce fait pour définir la propriété. Le lien avec les objets étant bien sur les esclaves, considérés comme des *res*, des choses.

Il semble que Rome ait suivi le même chemin que la Grèce. Une crise de la dette qu'il faut résoudre, d'abord. Les lois étaient si sévères que les créanciers pouvaient exécuter les mauvais payeurs. Ensuite, la compréhension qu'on peut interdire l'esclavage pour dette, à condition de mener une politique expansionniste et esclavagiste.

D'ailleurs pour les romains, l'esclavage consistait en une relation de pouvoir. Les esclaves n'étaient pas naturellement inférieurs, ils avaient juste eu la malchance d'être captifs.

Des lois commencent à limiter le pouvoir sur les esclaves avec l'Empire mais sous la République, le droit sur les esclaves est absolu.

De là découle un courant de pensée qui postule que l'on possède nos propres libertés et nos droits. Et donc que l'on peut en faire ce qu'on veut. Y compris les vendre.
Et donc certaines personnes ont commencé à justifier l'esclavage en disant que les esclaves avaient après tout bien le droit de vendre leur liberté s'ils le désiraient.
Thomas Hobbes est aussi parti de ces idées pour déclarer que l'État et ses citoyens passaient un contrat dans lequel les citoyens acceptaient de donner certaines de leur libertés à l'État. Et ainsi de suite jusqu'au travail salarié, ou les gens acceptent de vendre leur force de travail à leurs employeurs.

### Conclusion

page 255, vraiment un bon résumé depuis le début du livre

Graeber rajoute que si les esclaves sont censés n'avoir aucune famille, les rois aussi (ou du moins ils sont censés agir comme s'ils n'en avaient pas). Ce sont en quelque sorte des images en négatif l'une de l'autre, les archétypes des êtres humains isolés. Et c'est à partir du moment ou une personne est isolée, sans relation avec les autres, que l'on peut les vendre etc

Pour justifier une société à la Adam Smith, purement économique, ou chacun est isolé, libre, rationel, il faut en venir au concept de se posséder soi-même, avec ses droits et libertés.

> "L'esclavage officiel a été éliminé, mais la possibilité d'aliéner sa liberté, au moins temporairement demeure (tous ceux qui travaillent huit heures par jour peuvent l'attester). En fait, elle détermine ce que nous avons à faire, pendant l'essentiel de notre vie éveillée, sauf en général le week-end. La violence a été évacuée hors de notre vue. Mais c'est surtout parce que nous ne sommes plus capables d'imaginer à quoi ressemblerait un monde fondé sur des dispositions sociales qui n'exigeraient pas la menace permanente des tasers et des caméras de surveillance."

## Chapitre 8 : Crédit contre lingot, les cycles de l'histoire

L'abolition de l'esclavage est assez remarquable. L'esclavage a plus ou moins cessé d'exister en Europe mais aussi en Chine ou en Inde après la chute de l'Empire romain, vers 600 ap JC.

> "son abolition a eu lieu, semble-t-il, malgré l'opinion des intellectuels et des autorités politiques de l'époque."

Les pièces de monnaie sont apparues à peu près simultanément dans le vallée du Gange (nord-est de l'Inde), dans le grande plaine de Chine du Nord et autout de la mer Égée entre 600 et 50 av JC. Vers 600 ap JC, "il y a eu retour au crédit".

La monnaie semble-t-il s'épanouit dans un cadre de violence généralisée, lors des pillages et des guerres, les métaux précieux triomphent car l'heure n'est plus vraiment à la dette et à la confiance qu'elle demande. Surtout lorsqu'on marchande avec des soldats armés.

### La Mésopotamie (3500-800 av JC)

La monnaie ne servait que pour la comptabilité dans les temples.

Les dettes existaient, sous forme de tablette ou d'espèces d'enveloppe. Des prêts à intérêts étaient donnés aux marchands car on le leur faisait pas confiance. Les dettes (non commerciales) étaient régulièrement effacées (pour éviter les révoltes paysannes qui avaient le même effet).

> "L'usure - au sens de prêt à la consommation portant intérêt - était bien établie aussi"

### L'Égypte (2650-716 av JC)

État beaucoup plus centralisé, notamment de part sa géographie.

Les prêts sont, à l'origine, de l'aide mutuelle entre voisins. Il n'y a pas de prêts à intérêts.
Les mauvais payeurs peuvent être trainés en justice. Ils doivent alors prêter serment de rembourser le double de la somme ou subir cents coups de fouet.

Les crises de la dette et les annulations systémiques de dettes ne se généralisent que sous les Ptolémées (après Alexandre le Grand). D'ailleurs la pierre de Rosette a servi à proclamer une amnestie des débiteurs et des prisonniers.

### La Chine (2200-771 av JC)

Moins bureaucratique que les autres États, d'abord une monnaie sociale puis une monnaie de "coquilllages cauris". Une "large gamme d'instruments de crédit".

## Chapitre 9 : L'Age Axial (800 av JC, 600 ap JC)

Plusieurs évènement similaires en Inde, Chine, Grèce. D'abord un développement de la philosophie avec Confucius, Buddha, Pythagore. En Perse, Zoroastre. On a aussi un développement de la monnaie, qui d'abord frappée par des particuliers revient très vite entre les mains de l'État. À cette époque, on observe également une scission des empires en pleins de petites cités-États, souvent en proie à la guerre.

Les premières pièces viennent de Lydie, Anatolie occidentale (Turquie), vers 600 av JC. Il semble qu'auparavant, les métaux préciaux étaient surtout empilés chez les riches ou dans les temples, et servaient parfois au commerce international. Avec la monnaie, ces métaux ont été 'déthésaurisés', pour arriver dans les mains de tous, en tous petits morceaux.

David Schaps suggère qu'avec la guerre, le pillage a permis aux soldats d'acquérir des métaux précieux qui se sont ensuite répandus grace aux marchés.

Une théorie suppose que la monnaie a été produites massivement pour payer un nouveau type d'armée, des mercenaires, des personnes mobiles que l'on devait payer cher.

Les Phéniciens, de grands marchants plus qu'un empire militaire, sont restés aux lingots et à la dette. Leurs cités tombèrent les unes après les autres : Sidon, Tyr, et Carthage en 146 av JC.

> "il y a eu, disent les textes, des centaines de milliers de personnes violées et massacrées et cinquante mille prisonniers vendus à l'encan, après quoi la ville elle-même a été rasée et son territoire semé de sel."

### La Méditerranée

Pour Graeber, Rome et Athènes ont certains points communs, notamment d'avoir une histoire qui commence par une crise de la dette. Il y a alors deux options possibles : "la victoire des aristocrates", qui donne un État généralement inefficace, ou la victoire "des factions populaires", avec sa redistribution habituelles :

> "elles créaient ainsi les bases d'une classe de paysans libres dont les enfants pourraient sans problème passer une grande partie de leurs temps à s'entrainer pour la guerre."

Il semble que la plupart des États, plutot que d'abolir complètement les prêts prédateurs, préféraient donner de l'argent aux citoyens. Cet argent venait en général de conquêtes (partage du butin, extraction de mines, ...), et les monnaies créées avec étaient distribuées.

> "Mais ce qui rendait tout cela possible, c'était l'esclavage."

Ce sont des esclaves qui travaillaient dans les mines par exemple.

Un exemple, l'armée d'Alexandre le Grand comptait environ 20 000 hommes, qui réclamaient un salaire total d'environ une demi-tonne d'argent par jour. Lorsque l'armée a détruit Tyr, l'argent des temples a été redistribuée sous forme d'une nouvelle monnaie.

> "en quelques mois, Alexandre deversa sur le marché des espèces qui avaient été accumulées durant des siècles."

> "De fait, on pourrait interprêter l'ensemble de l'Empire romain à son apogée comme une immense machine à extraire, des métaux précieux, à les transformer en pièces de monnaie et à les distribuer à l'armée - tout en encourageant les populations conquises, par des politiques fiscales, à utiliser ces pièces dans leurs transactions quotidiennes \[...\] Dans les régions ou il n'y avait ni mines ni opérations militaires, les vieux systèmes de crédit sont probablement restés populaires."

### L'Inde

Différentes organisations, républiques, monarchies, empires, ... L'empire de Magadha finit par l'emporter "parce qu'il controlait la plupart des mines." Ses armées pouvaient être immenses. Tout l'État était au service de cette armée. Les pièces de monnaie étaient la pour payer les soldats. Tout ce qui entourait l'armée, colporteurs, prostituées, passa sous la main-mise de l'État. Par l'exemple, l'État organisa ceux qui commerçaient avec les soldats et monta les prix. Les prostituées furent organisées pour en même temps espionner les troupes. L'État stimulait les marchés, mais était méfiant vis-à-vis des marchands.

> "L'économie de marché, née de la guerre, a donc été prise en main graduellement par l'État."

L'empire controla toute l'Inde et le Pakistan actuel. Toute cette expansion s'est calmée sous le règne de l'empereur Ashoka (vers 250 av JC), qui se convertit au bouddhisme après une conquête particulièrement sanglante ("des centaines de milliers de personnes furent tuées ou réduites en esclavage").

> "Le déclin des grandes armées a fini par conduire à la quasi-disparition des pièces de monnaie, mais aussi à une véritable floraison de formes de crédit de plus en plus raffinées."

### La Chine

Même schéma que précédemment. Les quelques différences étant que la Chine n'a jamais produit de pièces d'or ou d'argent, seulement de la petite monnaie en bronze.
De plus, les mouvements philosophiques venaient plutôt de mouvements sociaux alors que dans les autres régions présentées, elles entouraient plutôt telle ou telle personnalité influente.

### Matérialisme I : la recherche du profit

Cette période de l'Age axial a donc vu un essort de la philosophie et de l'éducation populaire. L'écriture n'était plus seulement réservée à une élite, et le calcul devenait nécessaire avec l'essort des marchés pour ne pas se faire duper.

Pour Graeber, ces marchés impersonnels qui naissent un peu partout changent la donne. On ne traite plus avec des gens, en considérant son rang social par rapport à l'autre, les sentiments, bref tout ce qui est personnel. Dans un marché impersonnel en tant de guerre, tout ce qui compte c'est le calcul rationel. Les marchés permettent parfois aussi de rencontrer des gens très différents et donc potentiellement des opinions très différentes.

> "Dès l'époque de Confucius, les penseurs chinois voyaient dans la recherche du profit la force motrice de la vie humaine. \[...\] Dans ce monde-là les considérations héroiques d'honneur et de gloire, les voeux faits aux dieux ou le désir de vengeance sont, au mieux, des faiblesses à manipuler."

> "Dieux et déesses, magie et oracles, rites sacrificiels, cultes des ancêtres et même systèmes de castes et de statut rituel disparaissent ou sont marginalisés : on ne les voit plus comme des fins en soi, mais comme de simples instruments utilisables dans la quête du gain matériel."

Des théories voient le jour notamment en Chine, pour dire, par exemple qu'il faut arrêter la guerre, car elle n'est pas rentable.

### Matérialisme II : substance

- Les marchés sont apparus en tant qu'effets secondaires des systèmes administratifs des États. Les logiques de marchés se sont liées aux logiques militaires et des mercenaires de guerre. Elles ont fini par devenir la raison d'être des États.
- > "partout ou nous voyons émerger le complexe 'armée-pièces de monnaie-esclavage', nous voyons naitre aussi des philosophies matérialistes. En fait, elles le sont aux deux sens du terme : elles envisagent un monde fait de forces matérielles, et non de puissances divines ; et elles voient dans l'accumulation de richesses matérielles la fin ultime de l'existence humaine, ravalant des idéaux comme la morale et la justice au status d'instruments qui servent à satisfaire les masses."
- des philosophies naissent et se questionnent sur des questions cosmologiques, sur l'ame, sur l'éthique. Ses mouvements font blocs également avec des mouvements sociaux. Ces mouvements étaient principalement pacifistes, récusant la violence et les guerres de l'époque.
- les gouvernements ont d'abord tolérés ces nouvelles religions (confusianismes, chrétientés, ...) puis s'y sont ralliés quand les empires venaient à atteindre leurs limites d'expansion.
- en fin de compte, on voit la séparation de deux sphères, le marché et la religion. Les religions de l'époque mettent l'accent sur la charité et semblent en miroir des logiques aggressives et individualistes du marché.

## Chapitre 10 : Le Moyen-Age (600-1450)

> "Si l'Age axial a vu l'émergence des idéaux complémentaires des marchés des biens et des religions mondiales universelles, le Moyen Age a été la période ou ces deux institutions ont commencé à fusionner."

Les autorités religieuses avaient de plus en plus d'influence sur l'organisation du commerce et commencèrent à interdire le crédit prédateur. Les pièces de monnaie étaient beaucoup moins présentes du fait de l'écroulement des empires.

Pour Graeber, le Moyen Age a vu une nette amélioration du niveau de vie, notamment à la campagne. Les villes se sont d'ailleurs vidées durant cette période.

### L'Inde médiévale (la fuite dans la hiérarchie)

Morcellement en plein de petits villages, réduction des quantités de monnaie disponible (refonte en or), poids plus important de la religion (notamment hindouiste).

Les brahmanes (plus haute caste) refondent la société sur des concepts hiérarchiques de caste. Par exemple la caste la plus basse (*soudra*) a interdiction de s'instruire et est vouée au travail de la terre. Ils reprennent des concepts comme le karma et la résurrection, ils sont censés être complètement non-violents et devenir végétariens.

Les communautés villageoises tendent à être autosuffisantes, avec un grand nombre de métier classés par ordre hiérarchique, le tout sans avoir recours à la monnaie métallique.

Les prêts existent largement et les intérêts sont plus élevés pour les castes les plus pauvres.

### La Chine : le bouddhisme et l'économie de la dette infinie

Si l'on observe que les autorités religieuses sont découplées de l'État dans de le cas de l'Inde, la situation chinoise est complètement opposée. Si l'effondrement constaté ailleurs a bien eu lieu, il semble que l'Empire se soit vite remis sur pieds.

> "une fois que l'on a une bureaucratie vraiment efficace, il est presque impossible de s'en débarasser. Et la bureaucratie chinoise était exceptionnellement efficace."

Les principaux dangers pour cet État étaient les nomades du nord et les révoltes internes qui étaient très nombreuses et souvent victorieuses.

Des mesures ont été prises contre l'usure (notamment avec des prêts de l'État), avec des résultats inégaux.

> "la Chine a été, presque tout au long de son histoire, l'État de marché anticapitaliste par excellence."

Graeber entend par la que si la Chine promouvait activement les marchés (cad échanger des marchandises avec comme intermédiaires l'argent), elle refusait le capitalisme (entendu comme spéculation), cad le moyen à partir d'argent, d'obtenir une marchandise puis de la revendre plus cher.

Si le confucianisme était le système philosophique des Hans, le bouddhiste eut un rôle important en tant que religion.

Le concept de dette karmique eut une grande importance en Chine. On considère que les êtres humains sont en état de dette perpétuelle (ne serait-ce qu'à cause des dettes des vies antérieures). Cette dette est créée tout le temps car tout enrichissement est fait au détriment des autres, ou de la nature. Comment se délivrer de ses dettes ? Faire un don aux trésors inépuisables des monastères. Cette argent était en partie prêté et permettait de financer les temples, de manière très capitalistique et virtuellement infinie. Les autorités chinoises finirent par réagir contre ce phénomène.

> "en 845, 4600 monastères  au total sont rasés avec leurs ateliers et leurs moulins, 260 000 moines et moniales sont défroqués de force et renvoyés dans leurs familles - mais en même temps, selon les rapports de l'État, 150 000 serfs des temples sont affranchis."

> "Les monastères devenaient si importants et si riches, soulignaient les administrateurs que la Chine allait être à court de métal."

La Chine est le seul grand État du Moyen-Age a utiliser le papier-monnaie, qui existait parmi la population avant d'être controler par l'État.

### Le Proche-Orient : l'Islam (le capital comme crédit)

> "Dans une perspective historique mondiale, il est beaucoup plus raisonnable de voir le judaisme, le christianisme et l'islam comme trois manifestations de la même tradition intellectuelle occidentale, qui, pendant la plus grande partie de l'histoire de l'humanité, a eu pour foyer la Mésopotamie et le Levant. \[...\] Économiquement, l'essentiel de l'Europe s'est trouvé, peut-être jusqu'au haut Moyen-Age, exactement dans la même situation que l'essentiel l'Afrique : des territoires rattachés à l'écomomie mondiale - quand ils l'étaient - surtout en tant qu'exportateurs d'esclaves, de matières premières et parfois de produits exotiques \[...\] et importateurs de biens manufacturés."

On peut voir l'attitude islamique comme l'exacte opposée de la philosophie confucéenne : méfiante à l'égard de l'État, promouvant le commerce et aimant le droit.

L'État islamique (enfin le Califat de l'époque on se comprend ...) a généralement choisi la non-intervention en matière religieuse. Il continuait à effectuer des guerres de pillage pour refondre les métaux préciaux. Les soldats étaient très bien payés (quatre fois plus qu'à Rome). Il semble que de nombreux soldats-esclaves aient aussi été embauchés pour la guerre.

L'Islam s'est attaqué aux problèmes de l'Age axial concernant la dette et l'esclavage. L'usure fut interdite et il devint impossible de réduire les musulmans ou les citoyens du Califat (qui pouvaient être chrétiens ou juifs) en esclavage. Par contre la recherche du profit ne semblait pas poser problème, Mahomet lui-même ayant commencé sa vie adulte en tant que marchand.

Les crédits et chèques étaient très utilisés. Étant indépendant de l'État, on ne vit pas l'émergence du papier-monnaie.

En l'absence d'État régissant la sphère économique, les prêts se faisaient en fonction de la réputation des marchands. On pouvait donc littéralement transformer l'honneur en argent. L'archétype du riche marchand est une personne respectée, voire adulée, ayant fait de grands voyages risqués dans sa jeunesse et en étant revenu extrêmement riche. Il finit sa vie dans ses jardins entouré de bonne compagnie, de musique et de danseuses, il fait l'aumône aux pauvres.

> "Les prix dépendent de la volonté de Dieu."

Toute intervention de l'État dans les marchés est donc extrêmement mal vue. Il semble qu'Adam Smith se soit directement inspiré de certaines histoires de cette époque. Une différence fondatementale reste cependant que les musulmans voyaient l'entraide comme la base des marchés, et non la concurrence, ce qui construit nécessairement un système très différent du libre échange concurrentiel actuel.

### L'Extrême-Occident : la chrétienté (commerce, crédit et guerre)

Comme partout, peu d'argent circule dans la vie quotidienne, et l'Église régit la vie économique. Elle interdit l'usure de manière stricte, comme la Bible le demande clairement. La seule exception (l'Exception de Saint Ambroise) est de prêter à des étrangers. On ne peut donc prêter qu'à ceux à qui on est prêt à faire la guerre.
Dans la réalité, les anciens asservis pour dettes devenaient vassaux d'un seigneur qui leur faisait la charité chrétienne.
Contrairement aux musulmans, beaucoup de grandes figures chrétiennes se demandaient s'il était possible d'être marchand et chrétien, et voyaient d'un très mauvais oeil le profit.

> "si la Torah et le Talmud sont tous deux hostiles au prêt à intérêt, on a fait des exceptions dans les transactions avec les Gentils - notamment quand les Juifs d'Europe, aux XIe et XIIe siècel, ont été exclus de la quasi-totalité des autres professions."

En France et en Angleterre, les Juifs étaient régulièrement poussés par les rois à pratiquer l'usure, ces derniers savant pertinemment qu'ils pouvaient retourner leur veste à n'importe quel moment. Si les étrangers de l'Exception d'Ambroise ne devaient être que les Sarrasins à l'époque, le Juifs en payèrent les frais, ils étaient régulièrement arrêtés, torturés, mutilés, exécutés.

Mais il ne faut pas surestimer le rôle des Juifs dans l'usure. La plupart ne prêtaient que peu ou pas du tout. L'usure s'est développée au haut Moyen-Age avec la paysannerie libre.

Il y eut ensuite de grands débats théoriques avec la redécouverte des grecs et des arguments venus de l'Orient. Certains en concluaient qu'il fallait serrer la vis sur l'usure et même aller plus loin en interdisant la propriété privée. D'autres au contraire voulait assouplir le droit d'usure.

De grandes villes marchandes ont fini par émerger, notamment en Italie du Nord, mais souvent sur un fond de guerre, très loin des pratiques musulmanes.

Si l'image du chevalier s'est développé notamment avec les romans de Chrétien de Troyes, il ne correspond pas à la réalité que l'on pouvait observer. À l'origine, les chevaliers sont des personnes de la petite noblesse ne pouvant hériter et qui partaient donc sur les routes pour faire fortune, souvent en rackettant. On a inventé les tournois, les joutes, ou les chevaliers jouaient leur vie dans une énorme foire commerciale. Finalement, l'archétype des chevaliers, partant affronter de périlleux dangers ressemblement plutôt aux marchands de l'époque.

### Donc, qu'est-ce que le Moyen-Age ?

Ce que l'on considère comme le début de la modernité au haut Moyen-Age ressemble en fait au Moyen-Age classique partout ailleurs, l'Europe était 'en retard' à ce niveau.

> "Si l'Age axial a été celui du matérialisme, le Moyen-Age a été surtout celui de la transcendance. L'effondrement des empires antiques n'a pas conduit, en général, à la naissance d'autres empires. Ce sont des mouvements religieux populaires autrefois subversifs qui ont été catapultés en position d'institutions dominantes. L'esclavage a décliné ou disparu, le niveau global de violence a baissé. Avec le développement du commerce, le rythme de l'innovation technologique s'est accéléré ; une monde plus pacifique a offert plus de possibilités à la circulation des soieries et des épices, mais aussi des personnes et des idées."

## Chapitre 11 : L'age des grands empires capitalistes (1450-1971)

Le XVe siècle commence avec beaucoup de catastrophes. La peste bubonique tue un tiers de la population active, l'économie de plusieurs grandes villes s'effondrent, l'Église catholique chancelle, l'Empire ottoman avance. Paradoxalement les gens normaux vivent plutôt bien suite à une augmentation de leur salaire (due à la peste). Les gens s'enrichissent, les gouvernements essayent de légiférer contre ce phénomène mais doivent accepter des compromis suite aux révoltes que cela génère. C'est une période également propice aux fêtes.

Tout ceci va se renverser au siècle suivant avec les réformateurs catholiques et protestants. Le "pouvoir d'achat" a fortement chuté. Une explication classique dit que c'est l'afflux énorme d'or et d'argent venu du Nouveau Monde qui en est à l'origine. Or Graeber affirme que la plupart de l'or et de l'argent ont fini dans des temples indiens ou en Chine.

À l'époque de la domination mongole (1271-1368), la politique chinoise était relativement impopulaire. Le régime était étroitement associé aux marchands et créait un système de caste car personne ne pouvait vraiment changer d'emploi. Ce dernier point fut gardé par la dynastie Ming qui succéda aux mongols. Une économie souterraine s'est développée autour de l'argent grace à des mines illégales (au lieu du papier-monnaie qui régnait). À la suite de révoltes, le régime changea et se basa sur l'argent. Mais les mines chinoises étaient limitées et il fut nécessaire de se tourner vers le Nouveau Monde.

> "Dès l'instant ou Vasco de Gama est entré dans l'océan Indien en 1498, le principe qui faisait des mers une zone de commerce pacifique a pris fin. Les flottilles portugaises ont commencé à bombarder et à saccager toutes les villes portuaires qu'elles rencontraient, puis à prendre le contrôle de points stratégiques et à extorquer aux marchands sans armes de l'Océan Indien le prix de la protection - le droit de poursuivre leurs activités sans être molestés."

> "En 1540, un excédent d'argent provoqua un effondrement des prix dans toute l'Europe. Les mines américaines, à ce stade, auraient simplement cessé de fonctionner et l'ensemble du projet de colonisation de l'Amérique se seraient effondré, s'il n'y avait pas eu la demande de la Chine."

En Europe, la plupart des gens n'avaient pas plus de pièces qu'au Moyen-Age, ce qui a changé c'est que ceux qui avaient un pouvoir sur le métal (État, gros négociants, banquiers) ont commencé à changer les règles du jeu. Ils ont progressivement détruit les sytèmes en oeuvre datant du Moyen-Age pour réclamer d'être payé en argent par exemple. Cela a causé des révoltes, des fuites de paysants accablés, ... Cette fois-ci la réponse des États fut brutale et sans compromis.

### Première partie : cupidité, terrorisme, indignation, dette

Graeber commence par rappeler l'atrocité de la colonisation des Amériques et notamment le travail forcé inhumain dans les mines. Cortez est présenté comme un homme vivant au-dessus de ses moyens, quelqu'un de brillant et d'avide de gloire et d'argent. Il était criblé de dettes.

> "cette relation - entre l'audacieux aventurier, le joueur prêt à prendre toutes sortes de risques, d'un côté, et, de l'autre, le financier prudent dont toutes les opérations s'organisent autour de la production d'une croissance inexorable, mathématique et soutenue de son revenu -, cette relation est au coeur même de ce que nous appelons aujourd'hui le 'capitalisme'."

Pour Graeber, on peut voir que même si Cortez était assis sur de montagnes d'or, il n'était pas le roi, il devait rendre des comptes notamment à ses créanciers. Il en allait de même du roi d'Espagne. Et lorsque les compagnies anonymes continuèrent le travail de Cortez, notamment en Asie, la logique était celle de l'argent et des créances, tout le reste (les atrocités) pouvaient donc être justifié. Ce n'était pas leur faute, ils répondaient à une logique plus générale, qu'ils ne maitrisaient pas.

À l'Age axial, la monnaie était un instrument du gouvernement. Dans le nouvel ordre capitaliste naissant, la monnaie a commencé à avoir son existence propre et indépendante. La monnaie reste dépendante de l'existence des États (puissances politique et militaire) qui se réorganisent autour d'elle.

Nous avons vu que c'est l'interdiction islamique de l'usure qui a rendu possible l'existence de marchés séparés des États. Cette idée a également été proposée par Luther.

Luther a massivement prêché contre l'usure, ceci aboutissant à de nombreuses révoltes populaires (le peuple étant forcément adepte de cette idée) en Allemagne en 1525.

> "les rebelles, la plupart du temps, se présentaient comme de simples chrétiens qui voulaient rétablir le vrai communisme des Évangiles. Plus de cent mille ont été massacrés."

La situation devenant hors de contrôle, Luther a choisi une position plus mesurée. L'usure n'est pas un péché. Calvin en 1650 appuie cette position et des prêts raisonnables (avec des intérêts d'environ 5%) sont jugés acceptables s'ils ne représentent pas un travail à part entière.

### Deuxième partie : monde du crédit et monde de la dette

Dans la vie quotidienne du Moyen-Age, les échanges se basaient surtout sur la confiance. Les seuls qui payaient argent comptant étaient les étrangers et ceux en qui l'ont ne pouvait pas avoir confiance. Des monnaies locales en plomb ou bois pouvaient exister, l'argent et l'or étant utilisés principalement par les grands acteurs, notamment l'État.

La vie quotidienne semble donc (on parle plus précisément de l'Angleterre du haut Moyen-Age) avoir été basée sur le communisme, l'entraide et le partage.

> "Les marchés ne paraissaient pas en contradiction avec cette éthique de l'entraide \[...\] parce qu'ils opéraient entièrement par la confiance et le crédit."

Un livre comme le Léviathan de Hobbes rompt avec ces idées. Il pose que l'homme est en quête d'un intérêt personnel (le mot intérêt étant lié à la dette). Il positionne ainsi l'être humain en calculateur froid.

> "L'histoire des origines du capitalisme n'est donc pas celle de la destruction graduelle des communautés traditionnelles par le pouvoir impersonnel du marché. C'est plutôt l'histoire de la conversion d'une économie du crédit en économie de l'intérêt; de la transformation graduelle des réseaux moraux par l'intrusion du pouvoir impersonnel - et souvent vindicatif -  de l'État."

Les lois anglaises étaient extrêment dures à l'époque, que ce soit pour endettement ou vagabondage (chômage) par exemple, et l'on pouvait rapidement être pendu. Les histoires se réglaient donc normalement hors du cadre judiciaire mais "ceci est devenu de moins en moins vrai au fil du temps".
Avec la légalisation des intérêts au XVIe siècle, les recours judiciaires ont explosé. Une des raisons est que cela permettait de garder une trace juridique des dettes (beaucoup de gens étant illétrés). Et peu de recours allaient jusqu'au jugement, mais la menace a suffit à changer la donne. Alors qu'avant tout le monde était prêteur et emprunteur, il devenait maintenant possible pour un prêteur de causer beaucoup de tort à son emprunteur.

> "La criminalisation de la dette a donc été celle du fondement même de la société humaine."

L'usage de la monnaie est devenu moral en soit, et cela vient des classes aisées.

> "Nous devons tous donner les pièces, dire 's'il vous plait' et 'merci' et quitter le magasin."

C'est ainsi que l'élite cynique voyait un monde idéal, calculateur et froid, bien que ce monde fut à l'époque bien loin de la réalité des gens.

### Troisième partie : monnaie de crédit impersonnelle

On voit l'épanouissement de nouvelles abstractions financières comme le papier-monnaie, les chèques, les obligations, les actions, les rentes. La nouveauté est que l'on a commencé à utiliser des reconnaissances de dette de l'État comme monnaie. Les riches marchands donnaient aux rois de l'or et de l'argent venu d'outre-Atlantique et recevaient en échange une promesse de rente de l'État.

> "Ce n'est qu'avec la création de la banque d'Angleterre en 1694 que l'on peut parler d'un authentique papier-monnaie, puisque ses billets de banque n'étaient absolument pas des obligations. Ils étaient ancrés, comme tous les autres, dans les dettes de guerre du roi. On ne saurait trop le souligner. La monnaie n'était plus une dette du *au* roi, mais une dette due *par* le roi"

Le monnayage britannique subit une crise dans les années 1690. Les prix s'étaient envolés et les pièces valaient moins que leur contenu en argent. La solution mise en place vient de John Locke qui proposait de rappeler les pièces et de les réémettre à leur valeur en métal, car selon lui le rôle du marquage est simplement de garantir la valeur (en métal) de la pièce.

> "les résultats furent désastreux. Dans les années qui suivirent immédiatement l'opération, les pièces cessèrent pratiquement de circuler; les prix et les salaires s'effondrèrent; il y eut des famines et des troubles."

> "se fier à l'or et à l'argent a paru être le seul garde-fou contre les dangers inhérents aux nouvelles formes de monnaie de crédit, qui se multipliaient très vite - notamment lorque les banques ordinaires ont également reçu le droit de créer de la monnaie. On a rapidement constaté que la spéculation financière, affranchie de toute contraite juridique ou communautaire, pouvait produire des résultats qui semblaient friser la démence."

À cette époque matérialiste et rationnelle, la monnaie pose problème car elle semble pouvoir être créée *ex nihilo* :

> "les banquiers créent quelque chose à partir de rien. Ils ne sont pas seulement des escrocs et des magiciens. Ils sont le mal, parce qu'ils veulent jouer le rôle de Dieu."

Ce genre de critiques n'existaient pas à l'époque médiévale qui voyait un problème plus profond au marché : "la cupidité \[...\] le profit illimité considéré comme une fin en soi parfaitement viable", ceci créant des traders, courtiers etc qui n'avaient aucune loyauté envers rien.

### Quatrième partie : donc, qu'est-ce que le capitalisme ?

Pour Graeber, l'ensemble des institutions financières que nous associons au capitalisme étaient déjà là en 1700, bien avant la création d'usines. Les États ont repris à leur compte cette croissance infinie des intérêts, le transformant sur l'indicateur du PIB.

Le lien entre commerce et guerre prend une nouvelle forme extraordinaire avec les compagnies des Indes orientales, entreprises militaro-commerciales très puissantes.

> "Pendant un siècle, une de ces compagnies privée à but lucratif a gouverné l'Inde. Les dettes nationales de l'Angleterre, de la France, et des autres n'étaient pas faites de sommes empruntées pour creuser des canaux et jeter des points, mais afin d'acquérir la poudre à canon nécessaire pour bombarder des villes, ou de construire des camps pour détenir des prisonniers et entrainer des recrues."

Dès le XVIIIe siècle, de nombreuses sociétés à actions sont créées. On crée un projet, on vend les actions, les gens spéculent, et au bout de quelques semaines tout s'écroule. Certains font fortunes, d'autres sont ruinés.

Le capitalisme n'est pas basé sur une main d'oeuvre libre. Le système a toujours fait en sorte que les employés aient besoin d'argent pour manger ou payer des impots et il fallait donc accepter le salariat.

> "les relations maitre-esclave et employeur-employé sont en principe impersonnelles : \[...\] dès l'instant ou l'argent a changé de main, *qui* on est n'a plus d'importance."

> "À Londres au XVIIIe siècle, l'Amirauté royale avait régulièrement plus d'un an de retard dans le paiement des salaires de ceux qui peinaient sur les chantiers navals de Deptford. \[...\] la situation n'a vraiment commencé à se consolider que vers 1800 : L'État a stabilisé ses finances, commencé à payer les salaires en liquide en temps et en heure"

Samuel Bentham qui était chargé de réformer les chantiers navals a du "les transformer en véritable État policier pour pouvoir y instituer un régime de travail purement salarié". Il a eut l'idée d'installer une grande tour centrale, qui donnera l'idée à son frère Jeremy du Panoptique.

### Cinquième partie : Apocalypse

Graeber termine ce chapitre en disant que depuis globalement les Lumières il y a toujours cette idée que le capitalisme ne durera pas éternellement. On l'a vu il y a le risque que des bulles de spéculation explosent, et l'on redoutait le même genre de choses au niveau national.

## Chapitre 12 : Début d'une ère encore indéterminée (1971-?)

> "Le 15 août 1971, le président des États-Unis Richard Nixon fit savoir que les dollars détenus à l'étranger ne seraient plus convertibles en or : il éliminait ainsi le dernier vestige de l'étalon-or international. Cette déclaration mit fin à la politique inaugurée en 1931, puis confirmée par les accords de Bretton Woods à la fin de la Seconde Guerre mondiale : les citoyens des États-Unis avaient alors perdu le droit de convertir leurs dollars en or, mais toutes les devises américaines détenues à l'étranger avaient été déclarées convertibles au taux de 35 dollars l'once. En décidant de supprimer cette convertibilité, Nixon a inauguré le régime des changes flottants qui continue à ce jour."

Les effets furent de faire grimper le prix de l'or (à 600 dollars l'once en 1980), pratique pour les États-Unis qui detenaient la plupart des réserves d'or. Ce fut donc un transfert de richesses des pays pauvres sans réserve en or aux pays riches. Le dollar s'effondra et une inflation persistance débuta aux USA.

Les USA possèdent environ 5000 tonnes d'or soit "entre un cinquième et un quart de tout l'or que l'on a extrait de la terre à toutes les époques."

Les banques centrales, notamment aux USA, ont toujours été l'objet de nombreuses théories du complot.

La Federal Reserve américaine n'est pas publique (et n'est pas non plus complètement privée)

> "La Federal Reserve 'prête' de l'argent au gouvernement des États-Unis en achetant des bons du Trésor, puis elle monétise la dette publique américaine en prêtant à d'autres banques l'argent que lui doit désormais l'État."

On peut dire que si l'or était la référence, le dollar a pris sa place après 1971. Et cela est possible grace à la puissance militaire américaine. Aujourd'hui, en quelques heures, les USA peuvent décider de frapper de manière aérienne n'importe quel point sur la planète. Le dollar, et la dette américaine sont lié à la guerre.

La dette américaine ne peut donc pas et ne sera pas remboursée. Par contre, les États-Unis montrent une politique très dure envers leurs débiteurs.

> "La nouvelle monnaie mondiale est ancrée dans la guerre encore plus solidement que l'ancienne."

Des srtuctures bureaucratiques internationales ont vu le jour, comme le FMI, sous l'égide des États-Unis pour exiger, partout, le remboursement strict des dettes (en instaurant si besoin des mesures de rigueur appauvrissant tout le monde). Mais cette stabilité mondiale se voit compromise. Les États-Unis sont de plus en plus obligé de se tourner vers la Chine pour acheter leurs bons du Trésor, l'Argentine s'est déclarée en défaut de paiement en 2002, et la crise de 2008 a secoué la planète entière.

On peut noter que déjà historiquement, la Chine avait tendance a offrir de nombreux cadeaux aux royaumes qui accepteraient de reconnaitre son autorité. Lors de ses conquêtes, ses bateaux étaient chargés de soldats ... et de cadeaux. On peut voir dans la situation actuelle une continuation de ce phénomène.

Si les années 1945-1975 ont été paisibles d'un point de vue économique (salaire en hausse perpétuelle, protection sociale), la donne a ensuite changé. Les salaires ont arrêté de croitre ("le lien entre la productivité et les salaires a été pulvérisé"), les vieilles théorie de Keynes voulant supprimer les rentiers ont été torpillées. Le capitalisme s'est financiarisé, et s'est concentré plus sur la spéculation que sur la production.

> "C'est là que, pour bien des gens, l'achat d'un 'bout de capitalisme' \[les gens étaient encouragé a emprunté, à jouer en bourse etc\] s'est transformé insensiblement en quelque chose d'indistinguable des fléaux familiers aux travailleurs pauvres : l'usurier et le prêteur sur gage."

Des taux d'intérêts importants (25, 50% ou plus) sont devenus légaux et il est devenu presque obligatoire de s'endetter :

> "On a du s'endetter pour vivre au-delà de la simple survie. \[...\] Au fond c'est la sociabilité elle-même qui est traitée comme abusive, criminelle, démoniaque."

Le développement du micro-crédit et le renflouement des banques au détriments de la population pendant les subprimes a fini d'enfoncer le clou du capitalisme.

Il semble qu'entre cette crise du crédit, et les limites planétaires, le capitalisme n'en n'a plus pour très longtemps. Les États développent leurs arsenals militaire et judiciaire pour prévenir toute révolte, mais la population semble coincée derrière un mur de l'imagination qui ne peut pas penser un autre système.

Graeber nous rappelle que nous sommes ceux qui feront l'histoire et qu'il s'inspiré des luttes passées et se reposer les bonnes questions.

Résumé intéressant marché / État p 470.

Il faut également se souvenir qu'il aura énormément de violence pour reléguer le communisme de la vie ordinaire au second plan.

> "Tout système qui réduit le monde a des chiffres ne peut être maintenu que par les armes \[...\] De plus il ne peut fonctionner qu'en transformant continuellement l'amour en dette. Je sais bien que, l'usage que je fais ici de ce mot, "amour" est encore plus provocateur, à sa façon, que "communisme". Mais il importe de marquer les esprits. De même que les marchés, quand on les laisse dériver en toute liberté loin de leurs origines violentes, se mettent invariablement à se transformer en autre chose - en réseau d'honneur, de confiance et de liens mutuels -, de même que le maintien de systèmes de coercition effectue constamment l'opération inverse : il prend les produits de la coopération, de la créativité, du dévouement, de la confiance et de l'amour humains et en refait des chiffres."

> "Rien ne serait plus bénéfique que d'effacer entièrement l'ardoise pour tout le monde, de rompre avec notre morale coutumière et de prendre un nouveau départ. Qu'est-ce qu'une dette en fin de compte ? Une dette est la perversion d'une promesse. C'est une promesse doublement corrompue par les mathématiques et la violence. Si la liberté (la vraie) est l'aptitude à se faire des amis, elle est aussi, forcément, la capacité de faire de vraies promesses. Quelles sortes de promesses des hommes et des femmes authentiquement libres pourraient-ils se faire entre eux ? Au point ou nous en sommes, nous n'en avons pas la moindre idée. La question est plutôt de trouver comment arriver en un lieu qui nous permettra de le décourvrir. Et le premier pas de ce voyage est d'admettre que, en règle générale, comme nul n'a le droit de nous dire ce que nous valons, nul n'a le droit de nous dire ce que nous devons."
