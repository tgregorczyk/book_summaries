# Sur les rois, D. Graeber & M. Sahlins

## Introduction, D. Graeber & M. Sahlins

### Le régime cosmique

Les sociétés humaines sont prises dans un cosmos hiérarchisé, ou les divinités règnent. Même s'il n'y a pas de chefs à l'horizon "le ciel est peuplé d'êtres royaux" et "l'Etat, ou quelquechose d'approchant, est une condition humaine universelle"
"C'est pourquoi aussi les rois imitent plus les dieux que les dieux n'imitent les rois"
"Le corollaire de cette thèse, c'est qu'il n'existe pas d'autorité séculière: le pouvoir humain est pouvoir spirituel - de quelque façon pratique qu'il s'exerce"
Les rois sont quasi exclusivement des hommes.
Le pouvoir des rois ne s'expriment souvent que en leur présence, d'ou une série de stratégie pour limiter leurs mouvements ou mettre en place des tabous.

### Formations à "roi-étranger"

Un roi étranger héroique prend le controle. Forme la plus abondante d'Etat pré-moderne. Il se peut aussi que les dirigeants prennent l'identité de dirigeants extérieurs. L'important es t que l'aristocratie dirigeante soit supérieure au peuple dominé. Il peut établir sa notoriété par des crimes, des actes transgressifs. Il peut appartenir à un peuple ancien.
A noter qur la création de ce genre de royaume n'est pas forcément sanglant.
Le roi se marie à une femme locale.
Le roi-étranger est adopté par le peuple. Lors de l'intronisation il meurt, renait et est nourri par le peuple. Sa violence innée est sublimée pour servir à tous.
L'avénement d'un roi est aussi vu comme une mission civilisatrice. Cela crée aussi une dimension cosmique, le peuple et le roi se complètent (violence et pacifisme, ciel et terre, homme et femme, ...)
Enfin le peuple ne perd pas toute sa souveraineté. Les anciens rois deviennent prêtres, la culture reste autochtone.

### Politique de la royauté

royauté divine et royauté sacrée. Le divin c'est ce qui permet d'agir hors du champ humain. Le sacré c'est être "enserré par les coutumes et les tabous", "'ne pas toucher la terre, ne pas voir le soleil' selon la célèbre maxime de Frazer."
Il s'ensuit une partie d'échecs entre le peuple qui cherche à sacraliser le roi et le roi qui cherche plus de pouvoir divin.
Au fil du temps, les rois s'éloignent de leur origine divine (par le nombre de générations) et il y aussi de plus en plus de descendants et donc moins de chances pour une personne de famille royale d'accéder au pouvoir. Cela peut créer des conflits voire l'étiolement du pouvoir.
Il arrive aussi que les glorieux ancêtres voient leur pouvoir augmenter avec le temps, ils peuvent ainsi entrer en rivalité avec les monarques vivants. En effet, plus les ancêtres sont différents des mortels et plus ils sont source de puissance (une baleine tueuse ou une larve par exemple) mais si ils leur sont proches, ils deviennent source de rivalité.
Tout ceci est très complexe à gérer. Sacraliser les ancêtres pour les contenir c'est aussi leur donner plus de pouvoir, les détruire c'est oter sa légitimité au roi actuel, il faut donc les éloigner voire les détruire sous couvert de les honorer.
Une option est aussi de fuir et de devenir roi-étranger.

La royauté vit généralement dans un microcosme tellement faste et loin de la réalité de la population que cela crée un fossé infranchissable. Cela ressemble presque à un monde parfait qui pourrait surgir lors de prédications. Le roi incarne ce monde à part. Il n'existe que lui et les autres. Et c'est là le fondement de la justice, tout le monde est égal devant le roi, de manière très similaire à notre conception de tout le monde est égal devant la loi.

On peut aussi parler de la dynamique entre le centre et la périphérie. Tous les royaumes puissants ont un jour été en marge d'une plus grande puissance. On voit tout un jeu de tensions avoir lieu car la périphérie peut essayer de plein de manière de prendre l'avantage sur le centre en s'érigeant comme ayant des ancêtres plus glorieux ou en copiant le centre et en étant meilleurs commerçants ou statèges par exemple. D'un autre côté le centre continue son processus d'acculturation. La forme finale d'une société n'est donc pas décidée par le roi, elle est le résultat d'un jeu à grande échelle très complexe.

### L'économie politique de la royauté traditionnelle

Typiquement, la terre appartient aux ancêtres ou aux esprits. Via les anciens, on autorise les gens à utiliser la terre mais c'est seulement un droit d'usufruit. Par contre, vis-à-vis des dirigeants, la communauté locale est bien propriétaire de la terre. La classe dirigeante dirige de grands domaines et leurs habitants et ont un droit d'impôt sur eux. La population est intéréssée par la production primaire, la royauté veut extraire de la richesse via la récupération de biens finis, qui permetteront de financer le palais.

> "L'économie politique de la royauté est faite d'assujettissement social plutôt que de coercition matérielle. Le pouvoir royal ne repose ps tant, dans son fonctionnement, sur le contrôle privé eds moyens d'existence des sujets que sur les effets bénéfiques ou terrifiques des largesses, du faste et de la prospérité du roi. Le but de cette économie politique est d'accroitre le nombre et la loyauté des sujets - ce qui la distingue de l'entreprise capitaliste qui vise l'accroissement de la richesse sous forme de capital."
> royauté: "le contrôle politique de la population génère une accumulation de richesse qui permet un plus grand contrôle" versus Capitalisme: "le contrôle privé de la richesse productive permet le contrôle de la population en vue d'accroitre la richesse productive."

Il est important de noter que les esprits et aux métapersonnes *sont* des moyens de production. Leur importance fait que le mérite de la production échappe souvent aux producteur.rices et revient aux métapersonnes ou à ceux qui ont avec ces derniers un accès privilégié, ie la sphère politico-religieuse régnante.

### Sur des concepts éculés qui ont perdu toute utilité

Pour Sahlins, il est important de sortir de l'ethnocentrisme culturel classique pour comprendre des peuples différents. Par exemple on ne trouve souvent aucun distinction entre naturel et surnaturel ou entre hommes et esprits. Voir aussi Descola pour la vision de la nature et de la culture.
Certaines sociétés égalitaires ont des cosmologies particulièrement terrifiantes. La ou un roi a besoin d'un appareil de gouvernement et tout le monde est égal devant lui, c'est beaucoup simple devant un dieu soleil effrayant. Les rois du ciel ont beaucoup plus de pouvoir qu'un roi humain. Tout ça pour dire que même des sociétés de chasseurs cueilleurs égalitaires peuvent connaitre les hiérachies d'une point de vue cosmique.

> "plus haut, le ciel est peuplé d'êtres royaux, y compris lorsque sur terre on ne trouve pas trace de chefs."

Cela va contre la sociologie classique qui considère qu'une société vit par elle-même alors qu'un peuple ne vit pa seul, mais avec sa cosmologie et ses voisins.

> "Tle royaume est-il ou non un État? Cette question ne nous renseigne que rarement sur sa politique ou sa constitution. Nous avons certainement appris tout ce qu'il y avait à apprendre des spéculations sans fin sur 'les origines de l'État' et 'le processus de formation de l'État' qui ont dominé les débats théoriques du vingtième siècle. Rétrospectivement. nous pourrions bien aire une découverte: et si l'État qui avait tant absorbé notre attention n'avait jamais existé ou n'avait été, au mieux, que nom d'un econjonction fortuite d'éléments d'origines radicalement hétérogènes (la souverainté, l'administration, un champ politique concurrentiel, etc) qui ont été réunis à certaines époques et en certains lieux, mais qui sont de nouveau, aujourd'hui, en train de se dissocier les uns des autres?"

## Chapitre 1: La société politique originelle, M. Sahlins.

> "les dieux précèdent les rois, lesquels ne sont que la réplique efficace des premiers - ce qui diffère sensiblement de la tradition en sciences sociales qui veut que la cosmologie soit le reflet de la sociologie."

> "pris comme une totalité sociale et comme réalité culturelle, quelque chose comme l'État est la condition générale de l'humanité. On l'appelle habituellement 'religion'."

On a donc cette idée que l'organization politique existe d'abord dans la religion / cosmologie.

### Les exemples Chewong et Inuit

Chewong (Malaisie), société égalitaire en apparence mais régie en pratique par des êtres métahumains. Vaste société animiste dans laquelle on ne voit pas de démarcation significative entre entre le 'naturel' et le 'surnaturel' ou entre 'homme' et 'nature'. Ces catégories, divisions ne font qu'ajouter des biais à l'étude.

Chez les Inuits, on dénombre plusieures métapersonnes à attributs divins, chacunes ayant plus ou moins d'importance en fonction de la région. Elles se précipitent sur terre pour punir le moindre écart aux règles cosmiques.

> "Plusieurs récits qui retracent les origines de Sedna la peignent en orpheline, mutilée sacrificiellement par son père, et/ou responsable de sa mort ; le parcours divin de l'Homme-Lune évoque, lui, un matricide et une relation incestueuse avec sa soeur ; Sila a quitté la terre après que ses parents, des géants, aient été tués par les hommes. On retrouve ici une grande part de ce que Luc de Heusch (1962) a identifié comme 'exploits' au sein des traditions dites de royauté-étrangère : les crimes du fondateur de dynastie perpétrés contre l'ordre et les liens de parenté du peuple, par quoi le nouveau monarque surpasse cet ordre et acquiert la solitude nécessaire qui lui permet de régner sur la société entière libre de toute affiliation partisane spécifique."

> "C'est en transcendant la parenté qu'ils accèdent à une certaine souverainté territoriale."

On recense énormément de 'rèles de vie' chez les Inuits, qu'il faut respecter. Dans le cas contraire, la métapersonne possédant (par exemple tout l'océan ou toute la terre) l'être offensé viendra se venger. Par exemple, les ames des animaux tués peuvent venir se venger, c'est pourquoi il faut les contenter de différentes façons.
Les Inuits avaient souvent une attitude fataliste face à tout cela, ils savaient que la colère de telle ou telle entité pouvait arriver à n'importe quel moment. Dans certaines occasions ils tentaient de se rebeller. Eux aussi sont en effet possédés par un grand esprit, les chamans pouvaient donc l'invoquer pour faire pression sur une autre métapersonne (je ne possède pas les détails techniques).

### Pourquoi les appeler des esprits?

> "l'animisme postule un univers intersubjectif et personnalisé ou la séparation cartésienne entre la personne et la chose se trouve dissoute et rendue spécieuse. Dans le cosmos animiste, les animaux et les plantes, les êtres et les choses sont tout susceptibles d'apparaitre comme des subjets intentionnels et des personnnes capables de volonté, d'intentionnalité, d'agentivité." Kaj Arhem.

Il a été montré que les sociétés amazoniennes voient le "monde" comme quelque chose de multiple. Une société humaine ne sera pas très différente d'une société animale. Ce sont donc aussi des sujets politiques. Et l'environnement n'est donc pas très différents d'une société humaine, c'est une société de sociétés composées d'inidividus qui sont d'autres types d'humains.

> "Il est impossible de distinguer entre humains et ce que nous appelons des esprits." Viveiros de Castro.

*Perspectivisme*, terme créé par l'auteur mentionné just au dessus, "fait état de la coparticipation des humains, des dieux, des fantômes, des animaux-personnes et autres entités à une même société complexe."

> "Il est fréquent de voir humains et non-humains se marier, échanger des biens, quand ils ne se dévorent pas mutuellement."

### Relations sociales entre peuples et altérités-métapersonnelles

Formant un tout, il arrive que les humains se marient avec les esprits d'ancêtres ou fassent toute sorte d'autres pactes avec des entités non-humaines (dans notre conception).

### Les métapersonnes, puissances supérieures

Certains êtres métahumains sont considérés comme supérieur. Dans ce cas, cette notion de supériorité s'exprime commme propriété. De plus le propriétaire "est la forme personnifiée de la classe dont le sujets (personnes inférieures) sont des instances particulières."

### La politique cosmique

On s'intéresse à la politique cosmique des Mins des montagnes de Papouaise Nouvelle-Guinée. Il n'y a pas d'État politique mais une multitude de métapersonnes qui gouvernent. On note surtout le "duo cosmocratique: Afek, mère des humains et du taro, et le petit serpent Magalim, père autochtone des nombreuses créatures sauvages, qui la précède dans le temps". On note aussi que:

> "Leurs règnes respectifs démarrent sous le sign de violentes ruptures des liens de parenté, par quoi ils accèdent à l'indépendance, condition de leur universalité."

On retrouve les idées que les règles à suivre pour contenter les métapersonnes sont très complexes, surtout vu qu'il existe une hiérarchie chez les métapersonnes. Donc une chose peut être possédée par plusieurs entités qui ont chacunes des lois précises qu'elles veulent voir respecter.

### Fondement religieux des déterminations

Dans de nombreuses sociétés, "les esprits possèdent les moyens de production car ils possèdent ou sont les ressources fondamentales (la nature)". De plus ces esprits déterminent aussi souvent le résultat de la production humaine. Un certain nombre de rites sont nécessaires et participent en quelques sortes à la production, on peut citer les sacrifices par exemple. Les rites qui rendent hommage, montrent la soumission des humains etc font partie de l'économie.

> "Les femmes achuar ne 'produisent' pas les plantes qu'elles cultivent: elles ont avec elles un commerce de personne à personne, s'adressant à chacun pour toucher son ame et ainsi se la concilier, favoriser sa croissance et l'aider dans les écueils de la vie, tout comme le fait une mère avec ses enfants." Descola

### Pour conclure

Sahlins insiste: il faut bien comprendre que les "humains dépendent des métapersonnes" et ces premiers n'ont pas d'équivalence avec ces derniers. Mais il faut tout de même noter que certaines personnes d'une société peuvent gagner en prestige en étant plus proches de ces "divinités". Cela peut arriver par quelques exploits (car un exploit n'est pas le fait d'une personne mais d'une bonne connivence avec les métapersonnes), ou un don pour les apaiser ou communiquer avec elles.

> "Notons que ceux qui possèdent ainsi des pouvoirs divins ne sont pas nécessairement placés hors du contrôle de leurs semblables, car des pressions populaires peuvent être exercées sur eux afin qu'ils utilisent ces pouvoirs à des fins bénéfiques. C'est ici que le fameux 'égalitarisme' de ces peuples devient pertinent."

Sahlins s'est inspiré du livre d'Hocart _Rois et Courtisans_, et il conclut:

> "tout forme de gouvernement, en générale, et la royauté, en particulier, se développe, comme organisation du rituel."

### Coda

Pour des arguments similaires, voir Jacobsen, 1946, qui parle d''État cosmique'.
Pour conclure:

> "Si c'est le cas, l'État humain est la réalisation d'un ordre politique déjà préfiguré dans le cosmos : les dieux ne sont pas partis d'ici-bas pour aller au ciel, c'est l'État qui, du ciel, est venu sur terre."

## La royauté divine des Shilluk: Sur la violence, l'utopie et la condition humaine. D. Graeber.

Graeber a écrit que:

> "Les États \[sont\] des formes institutionnalisées de pillage ou d'extorsion et des projets utopiques."

Le royaume Shilluk (Soudan) ne possédait aucune forme d'administration, mais était situé assez proche d'autres empires pour savoir ce qu'était qu;un gouvernement centralisé.
On peut utiliser 'projet cosmologique' comme synonyme d' "utopie":

> "Les palais royaux, les villes royales ou les cours royales deviennent presque invariablement des microcosmes, des images de la totalité. La place centrale est pensée comme un modèle de perfection, mais également comme un modèle de l'univers ; le royaume, idéalement, devait lui-même être une reproduction du même schéma à une échelle plus large."

### Théories de la royauté divine

Les Shilluk furent d'abord connus par la troisième édition du livre de Frazer de 1911, car cette société semble coller parfaitement à sa théorie.
On note que le roi shilluk, *reth*, représentait un dieu et que son état physique était directement lié à l'état du royaume. Quand il faiblissait, il était donc tué.

En 1948, Evans-Pritchard critique complètement ces théories :

> "Evans-Pritchard \[...\] soutenait que les rois divins n'existaient pas, que les rois shilluks n'avaient probablement jamais été exécutés rituellement et que le rituel d'installation ne consistait pas vraiment en un transfert d'ame"

> "C'est la royauté et non le roi qui est d'essence divine". Evans-Pritchard

Les thèses de Frazer virent un regain d'intérêt dans les années 1980.

Un de points d'intérêt d'Evans-Pritchard est le fait que les rois doivent "être dans la société et pourtant s'en tenir à l'extérieur". Comment sortir de la société ? En accomplissant "un geste spectaculaire afin de marquer une rupture fondamentale avec ' l'ordre domestique ' et la morale domestique":

> "De tels "exploits" étaient parfois mimés symboliquement : le roi faisait par exemple semblant de coucher auprès de sa propre soeur ou de marcher sur le corps de son père au moment de monter sur le trône."

Les rois peuvent aller plus loin (Sahlins) en disant qu'ils viennent ou que leur pouvoir vient d'ailleurs. Il faut une énergie créatrice extérieure à la société qui est domestiquée par la mariage avec une fille locale et des rituels par lesquels "le roi \[...\] est conquis par le peuple".

Pour de Heusch, les rois sont comme des "corps fétiches" créés par le peuple. Pour Frazer, le roi "absorbe le péché et la pollution de la nation et doit par conséquent être détruit."

*souverainté* : "la reconnaissance du droit à exercer la violence impunément."

Plein d'autres discussions sur la théorie du bouc-émissaire de Girard, sur le fait que de Heusch est d'accord avec Hocart pour dire que "la royauté était à l'origine une institution rituelle", avant de devenir politique.

"Simon Simonse (2005), par exemple, observe que ce que la plupart des Africains attendent de leurs rois sacrés correspond à ce que la plupart des Européens attendent de leur État-providence: la santé, la prospérité, un certain degré de sécurité et une protection face aux catastrophes naturelles. Il aurait pu ajouter que néanmoins, nombreux sont ceux qui ne trouvent ni nécessaire ni désirable de leur octroyer également des pouvoirs de police afin d'atteindre ces objectifs."

Pour Graeber, si les "exploits" des rois doivent les sortir de la morale c'est justement pour créer la morale. On ne saurait créer un système auquel on appartient. Mais alors n'importe quel bandit peut-il prétendre à un titre royal ? Non, visiblement ce n'est suffisant, mais le potentiel est là.
On peut d'ailleurs voir que de nombreux révolutionnaires dans l'histoire ont eu des pratiques illégales dans leurs systèmes juridiques.

> "En tant que rois divins, les *reths* étaient supposés faire étalage d'une violence absolue et arbitraire, mais les moyens à leur disposition demeuraient extrêmement limités."

On les empêchait surtout de transformer leurs actes de violence ponctuels en quelque chose de systématique.
Par ailleurs les *reths* étaient très impliqués dans la justice même s'ils ne pouvaient pas forcément faire appliquer leurs décisions:

> "Il semble qu'ils aient principalement joué un rôle de médiateurs."

### Les Silluk vus depuis l'Équatoria

Équatoria : sud du Soudan + une partie de l'Ouganda.

Les faiseurs de pluie sont des personnages à part dans cette région. Certains deviennent importants, d'autres restent marginaux. On les choyait pour qu'ils fassent tomber la pluie (avec des offrandes etc), mais si la pluie ne venait pas, la situation devenait plus tendue. Le "peuple" se construit contre ce faiseur (parfois roi) et va parfois jusqu'à l'assassiner.
C'est assez représentatif de la séparation entre le roi et le "peuple". Ils se construisent l'un contre l'autre, et le peuple va finir par vouloir tuer son roi mais ce dernier se défendra du mieux qu'il pourra, en libérant sa violence arbitraire, en acquérant des fusils, en faisant des alliances.

### Trois propositions

> "1. La _royauté divine_, dans la mesure ou l'on accepte la signification de ces termes, ne fait pas référence à l'identification des souverains à des êtres surnaturels (un phénomène étonnamment rare), mais à des rois qui se hissent au rang d'équivalents de dieux - des êtres arbitraires et tout-puissants au-delà de la morale humaine - par l'usage de la violence arbitraire. Les institutions de la royauté sacrée, quelles que soient leurs origines, ont généralement été utilisées pour prévenir ou contrôler le danger que représentent de telles formes de pouvoir, à partir desquelles il est possible de tracer une ligne directe menant aux formes contemporaines de souverainté.

2. La _royauté sacrée_ peut également être conçue comme offrant une forme de résolution (approximative, imparfaite) de la problématique élémentaire de l'existence humaine proposée dans les récits de la création. C'est dans ce sens que Pierre Clastres avait raison d'affirmer que l'autorité de l'État devait sans doute provenir des prophètes plutot que des chefs, du désir de trouver une 'terre sans mal' et de défaire la mort ; c'est aussi en ce sens que l'on peut dire que le Christ (le Rédempteur) était un roi, ou que les rois pouvaient aisément prendre pour modèle le Christ, malgré son manque de qualités martiales. Nous pouvons observer ici, à l'état embryonnaire, ce que j'ai appelé l'élément utopique de l'État.
1. La *violence*, et plus spécifiquement l'antagonisme, joue ici un rôle crucial. C'est une forme particulière de violence qui simplifie les choses, dessinant les lignes claires là ou, en son absence, on ne verrait que des réseaux complexes et imbriqués de rapports humains. C'est la forme particulière de pouvoir souverain qui définit ses sujets comme peuple unique. Cette distinction, dans le cas des royaumes, est antérieure à la distinction ami/ennemi proposée par Carl Schmitt. Ou, pour être plus spécifique, la capacité à se constituer comme peuple unifié dans une potentielle relation de guerre avec d'autres peuples est fondée sur un état de guerre antérieur mais généralement caché _entre le souverain et le peuple_."

### Bref exposé de l'histoire des Shilluk

Les peuples nilotiques en général sont des pasteurs semi-nomades, pratiquement de manière secondaire l'agriculture. "Connus pour leur égalitarisme farouche". Les Shilluk sont différents au sens qu'ils vivent au bord du Nil blanc, favorable à la culture d'une céréale locale. Ils sont plus sédentaires.
Les Shilluk sont connus pour êtres des pilleurs. La première capitale royale permanente fut créée à Fachoda par Tugo vers 1700, un "village presque entièrement composé de femmes." C'est le moment d'un grand changement. Le roi précédent était une reine, Abudok, dernière reine connue. En revanche les épouses royales acquérirent un grand pouvoir par la suite. Il semble que la création de ce système fut fait contre les reines.
Le roi vivait entouré exclusivement de ses femmes (il pouvait y en avoir une centaine), issues du peuple, chacunes logées dans le palais. Ces femmes avaient un rôle politique, de conseillères et de liens entre le pouvoir et les communautés. Elles ordonnaient la mise à mort du roi lorsque ses capacités physiques diminuaient.
Néanmoins la société n'était pas matriarcale, les chefs de village étaient des hommes.
Situation complexe à partir de la deuxième moitié du XIXe siècle, les empires ottomans, les arabes et les britanniques mettant la pression. Plusieurs décennies de guerre civile également puis un contrôle britannique au tournant du XXe siècle qui transforme le *reth* en collecteur d'impôts.

### Mytho-histoire

#### Un mot sur les cosmologies nilotiques

> "Les sociétés nilotiques considèrent généralement Dieu comme une force extrêmement éloignée et à l'écart du monde humain. C'est un culte assez sommaire, quand il existe, qui est rendu à la divinité, et jamais de manière directe. La divinité et habituellement vue comme 'se réfléchissant' à travers le cosmos, présente particulièrement dans les orages, les esprits totémiques, les objets numineux ou les phénomènes inexplicables et extraordinaires."

Dans le mythe originel, les premiers hommes et femmes vivaient en harmonie avec les Dieux, une corde reliait le ciel et la terre. Mais les hommes, par leurs actions allant contre les rèles, brisèrent (involontairement?) ce lien et depuis, doivent trimer pour subvenir à leurs besoins. Similarité avec la naissance.

> "Une fois la séparation introduite dans le monde, le problème réside dans le fait que la conjonction ne peut désormais que signifier la catastrophe."

Et en effet, la Divinité ne se manifeste plus que lors de catastrophe naturelle. Les sacrifices permet de recréer la division originelle:

> "le sacrifice devient un moyen d'expulser l'élément divin de son enchevêtrement désastreux avec les affaires humaines en replaçant chaque chose dans sa propre sphère."

La seule manifestation non violente de la Divinité est la pluie.

#### La légende de Nyikang

Le mythe fondateur des Shilluk tient plus de l'histoire de l'exil de Nyikang, qui est une variante de la version précédente au sens ou Nyikang se voit obliger de partir du paradis originel et va fonder une société. Son fils Dak va à un moment subir une tentative d'assassinat de la foule mais sera substitué par une effigie en bois. Il survit, apparait à son enterrement et ainsi sa "mort" lui confère une aura particulière.

> "\[Cette histoire\] semble refléter un thème récurrent selong lequel la colère et l'hostilité du peuple - aussi paradoxal que cela puisse paraitre - sont la cause immédiate de la transcendance du roi par rapport au status de mortel."

> "Nyikang crée les Shilluk comme un projet 'intellectuel'. Il découvre, transforme, donne des noms, octroie des rôles et des privilèges, établit des frontières, rassemble u groupe varié d'hommes et d'animaux non apparentés les uns aux autres et fait d'eux des parties égales d'un seul et même ordre social. \[...\] La soumission est ce qui fait des hommes des Shilluk (le mot utilisé *Chollo*, signifie simplement sujets du *reth*)"

Il est intéressant de noter que la plupart des prières se faisaient pour Nyikang, mais que si celles-ci n'étaient pas excaussées, ils le maudissaient (et il était toujours maudit lors de la mort de quelqu'un).

Petit cours d'histoire. "Aucun des quatre premiers rois du Shillukland ne mourut comme un être humain normal". Plusieurs d'entre eux disparurent rejetés par leur peuple et furent remplacés par des statuettes.
Quans Duwat devint roi il abaissa le status des fils de ses frères au rang d'*ororos*, "exclus de la succession mais jouant néanmois un rôle centrale dans le rituel royal"

> "Le *mar* était une sorte de talisman ou un élément des insignes royaux qui avait à l'origine appartenu à Nyikang."

Il permettait apparemment de gagner à coup sûr sur le champ de bataille. L'histoire raconte qu'un roi s'en servit à outrance pour conquérir des territoires mais ses soldats se lassèrent et se révoltèrent. Par colère le roi jeta le *mar* dans le Nil. Un roi le récupéra plus tard et tomba dans les même travers et finit par rejeter le *mar* dans le Nil ...

> "L'histoire semble expliquer pourquoi le royaume Shilluk n'est jamais devenu un empire. Il semble que chque fois que les rois outrepassaient la simple défense du territoire national ou conduisaient des expéditions au-delà des frontières, chaque fois qu'ils tentaient de lever des armées et d'échafauder des projets de conquête, la contestation et la résistance passive entravait leurs plans."

### Retour à Fachoda

Pour Lienhardt, les rois sont une continuation de Dieu. Dieu pour les Shilluk n'est ni bon ni mauvais, il est présent dans tout ce qui est extraordinaire.
Les rois sont admirés pour leur ruse et leur intelligence. Les jeux de pouvoir étaient très violents et

> "Les membres de la famille royale massacraient régulièrement leurs frères et leurs cousins à titre préventif."

À noter que cette culture royale n'avait rien à voir et n'avait aucun impact sur la vie quotidienne.

Rien à voir mais les *reth* étaient extrêmement riches en bétails et avaient des serviteurs personnels, le *Bang Reth*, "un groupe d'hommes arrachés à leurs communautés respectives : orohelins, criminels, prisonniers de guerre".

> "chaque fois qu'un village menait un guerre injustifiée contre un autre ou refusait de manière répétée d'obéir à ses ordres, le *reth* procédait dans les villages alentour à une 'levée royale', dont les membres se mettaient alors à chasser le bétail des malfaiteurs et à brûler leurs habitations" Pumphrey 1941

Il n'y avait pas d'autres levée de tribut systématique.

> "ce fut précisément dans les années 1840, lorsque les rois Shilluk, enhardis par leur alliance avec des marchands étrangers, commencèrent à vouloir aller au-delà de ces expéditions et créer un dispositif systématique de collecte du tribut, que de nombreux Shilluk ordinaires commencèrent à mettre en doute la légitimité de l'institution de la royauté en son ensemble, en s'alliant avec un autre groupe de pirates prédateurs. Cette décision se révéla en fin de compte catastrophique. Les marchands d'esclaves arabes avec lesquels ils s'étaient unis se révélèrent bien plus cruels et destructeurs que tout ce qu'ils avaient connu auparavant."

Beaucoup de mystère, danger, doute entoure le village royale et personne ne sait réellement ce qu'il s'y passe. Cependant, Fachoda ressemble à un petit paradis. Il n'y a ni morts ni naissances (les femmes étaient renvoyés dans leur village pour accoucher), et la vie était faite d'abondance et de plaisirs charnels.

> "Fachoda était donc une annulation du dilemme de la condition humaine."

### Les rituels d'intronisation : description

Ces rites impliquaient l'ensemble de la population. Une année de delai était laissée entre la mort d'un *reth* et l'intronisation suivante, c'était une 'année de peur'.
Pour l'intronisation, les statuettes de Nyikang et Dak sont reconstruites et marchent à travers le pays en procession, réclamant régulièrement des offrandes en bétail :

> "La situation est relativement claire. Les effigies, construites à partir d'éléments ramenés de l'extérieur, descendent en pays Shilluk comme une force étrangère et prédatrice."

L'intronisation est complexe, précis et assez long et théatral. Le *reth* élu fuit le cortège de statues et part se réfugier au sud. Avec les chefs locaux, il rassemble une armée et un simulacre de bataille a lieu. Le *reth* est capturé par les effigies et ramené à Fachoda. En suite, l'esprit de Nyikang sortira de la statuette pour pénétrer le *reth*. Elle 'ressort' pour une dernière confrontation en enlevant la "fille de cérémonie" puis le *reth* gagne enfin et aux premières gouttes de pluie qui suivent, les statuettes partent de Fachoda. Il se passent beaucoup de choses, tout au loin de cette intronisation, le roi est régulièrement abaissé, humilié, considéré comme un petit garçon (on lui demande de garder du bétail etc), notamment dans la ville du Sud ou il se réfugie. Le roi rencontre ensuite les grands chefs du royaume et préside des rituels.

### Les rituels d'intronisation : analyze

Symbolique de la lutte entre le nord et le sud.

> "Le Nord est identifié à la 'royauté' éternelle, universelle; le Sud, au roi particulier, mortel."

Ainsi on observe que "la royauté absorbe le roi" (Evans-Pritchard). Le *reth* élu battu comme humain devient Nyikang et devient capable de vaincre l'effigie.
On autre opposition existe entre l'eau et le feu. L'eau est l'éternité (le Nil), "le feu, est comme le sang, la substance de la transformation terrestre."

On observe la hiérarchie suivante : le peuple \< le *reth* \< Nyikang \< Dieu (*Juok*). On remarque souvent des phrases de ce genre:

> "Nyikang est le *reth* mais le *reth* n'est pas Nyikang."

Signifiant par la que le *reth* est un aspect de Nyikang mais Nyikang ne se limite pas à cela. Ces éléments restent différents car ils peuvent être en opposition.

> "Le peuple intercède pour le peuple de demande à Nyikang d'intercéder auprès de Dieu pour amener la pluie \[...\] Le peuple peut hair le *reth* ou vouloir le tuer; il peut maudire Nyikang; le *reth* peut retenir la pluie par ressentiment envers le peuple; le roi et Nyikang peuvent lever des armées et livrer bataille l'un contre l'autre."

Le rôle de Dak et Cal les fils antagoniques de Nyikang n'est pas forcément très clair. Par contre une chose est sure, le rôle de l'interrègne est important. Dans la plupart des sociétés, l'interrègne est associé au chaos et l'on essaye de le rendre aussi bref que possible. Mais pas les Shilluk qui ont conservé la duré d'un an pendant des siècles.
En fait, c'est pendant cette période que le rôle de Dak est important. C'est son tempéramment prédateur et destructeur qui est représenté. Ce sont les pillages perpétrés pour chercher les matériaux des futurs effigies mais aussi la violence indifférenciée, universelle. Et cela correspond à ce que les Shilluk appellent Dieu. Dieu est Dak, mais Dak n'est pas Dieu. Pendant l'interrègne, on joue à Dieu :

> "Pendant l'interrègne, donc, ce n'est pas seulement la politique royale qui se propage à la société tout entière ; c'est la puissance divine elle-même - la puissance divine violente, arbitraire qui est, comme les institutions Shilluk ne cessaient de le rappeler à tout chacun, la réelle essence et l'origine de la royauté."

Cette puissance est aussi créatrice, d'une certaine façon "À travers une combinaison d'appropriation et de création, le peuple de Dak fabrique Nyikang."

> "Les Shilluk deviennent-ils shilluks - les sujets de Nyikang - parce qu'ils construisent collectivement Nyikang (le roi fétiche classique, créé par son peuple) ou parce que celui-ci les conquiert ensuite (le roi divin classique, faisant pleuvoir le désastre ou la menace du désastre de manière égale sur chacun) ?"

Lors de l'élection du nouveau *reth*, on considère que le choix est celui de Dieu, et le rituel est en accord avec cela ("une épreuve du feu"). "Le collège électoral est principalement constitué d'hommes du peuple \[...\] Le peuple et Dieu sont ici interchangeables." Dak disparait à nouveau quand réapparait Nyikang.

On peut noter que la construction de Nyikang se fait avec des matériaux éternels.

> "Il passe ensuite progressivement du générique - et donc de l'intemporel - au particulier, et donc à l'historique."

Le roi, lui, est entouré par le feu et les *ororos* qui lui rappellent sa condition mortelle.

> "Lorsqu'il est vaincu et capturé par Nyikang, il est arraché à sa propre mortalité."

> "La ou le drame du Nord est lié à l'endiguement graduel de la puissance divine, arbitraire, le drame du Sud est lié à la vulnérabilité humaine. Le *reth* à venir est raillé, traité comme un enfant, contraint de monter un boeuf à l'envers. Ses partisans n'exercent jamais de pouvoir arbitraire sur les hommes. Contrairement à Dak et à Nyikang, ils ne pillent ni ne volent ni n'enlèvent de passants en échange de rançon. En revanche, ils offrent constamment des animaux en sacrifice."

Les sacrifices sont importants dans les cultures nilotiques. Ils signifient la restauration des séparations, des frontières. De manière plus large, ils constituent aussi un moment "utopique" ou toute la société se rejoint et partage la viande.

> "D'après Lienhardt, ces moments d'harmonie communautaire sont, pour les Dinka, ce qui se rapproche le plus d'un expérience directe de Dieu - ou, pour être plus exacte, de la Divinité dans sa dimension d'universalité bienveillante"

> "Le but du sacrifice rituel est donc de passer d'une manifestation du divin à une autre : de Dieu comme confusion et désastre à Dieu comme unité et paix."

On peut voir l'intronisation comme commençant par une vision cauchemardesque, sans séparations, la puissance divine imprégnant toute la société. On va assister à "la transformation du roi divin en roi sacré". Nyikang devient effigie (créée par des hommes). On endigue sa puissance destructrice. "Au Sud, nous assistons à la fabrication d'un roi bouc émissaire classsique." A la fin de la cérémonie, l'harmonie est restaurée ainsi que la hiérarchie.

### Quelques mots en guise de conclusion

> "L'utopie est le lieu ou les contraictions sont résolues."

L'hypothèse de Graeber est que chaque société vit avec ses contradictions (cosmiques...) et que "Le sens a horreur du vide", d'ou des mythes divins.

> "L'État ne peut surgir que de prétentions aussi absolutistes, et surtout, d'une rupture explicite avec le monde de la parenté."

Je ne sais pas s'il parle la uniquement des sociétés "farouchement égalitaires".

On peut aussi voir le corps du *reth* comme une utopie. Il est choisi pour être un homme "typique", "générique", parfait.

Pour Graeber, un moyen d'échapper à cette condition mortelle, est de tuer des gens, pour rentrer dans l'histoire et devenir immortel (que ce soit par des guerres, des grands projets de construction, ...)
Comparaison avec le royaume du Bouganda "situé sur les rives du lac Victoria, quelques centaines de kilomètres plus au Sud". Beaucoup de similarités sur la mythologie et l'interrègne. Cependant beaucoup de différences également. Les hommes chez les gandas "formaient une strate essentiellement parasite", les femmes fournissant "l'essentiel du travail de subsistance"."

> "les plus jeunes étaient organisés en bandes militarisées et les plus vieux consitutaient un appareil administratif infiniment élaboré qui semblait surtout servir à maintenir les plus jeunes sous contrôle, ou concentrés sur d'interminables guerres de conquête."

C'était donc un véritable État. Un État très violent ou le roi n'était pas associé à une divinité mais ordonnait le massacre de sa population :

> "Des milliers d'hommes pouvaient être massacrés au cours des funérailles royales, au moment de l'accession au trône, ou lorsque le roi décidait qu'il y qvqit trop d'hommes jeunes sur les routes entourant la capitale et qu'il était temps d'en capturer quelques centaines en vue d'une exécution de masse."

Les rois n'étaient pas tués rituellement. Ils ne pouvaient sortir du palais que portés sur une chaise. Quiconque regardait le roi directement était tué. De nombreuses règles de ce genre existaient et montrent que le roi avait toutes les cartes en main. Contrairement au roi Shilluk qui risquait la mort au moindre fléchissiment physique, et ses femmes toutes-puissantes, le roi ganda tuait ses femmes au moindre écart de comportement (éternuement, toux).

> "Ce sur quoi je tiens néanmoins à attirer l'attentioon ici, en premier lieu, est le rapport intime entre la perfection hors du commun des cours royales et leur degré de violence - et au fait que de telles utopies s'appuient toujours sur ce que nous appelons par euphémisme la 'force'. Le second point es tque la violence est toujours à double tranchant. C'est la vérité qui ressort des histoires shilluks relatant comment l'effigie de Dak - qui représente l'aptitude humaine à s'élever au rang de dieu par la violence - fut créée lorsque le peuple en son entier s'organisa pour tuer Dak, ou comment Nyikang disparute tdevint un dieu lorsque tout le monde se mit à le hair.
> Ce que je voudrais suggérer ici, c'est que tout cela est resté la logique cachée de la souverainté. Ce que nous appelons 'la paix sociale' n'est qu'une simple trêve dans un guerre constitutive entre le pouvoir souverain et 'le peuple' ou 'la nation' - qui accèdent tous deux à l'existence, en tant qu'entités politiques, par leur lutte l'une contre l'autre. Cette guerre fondamentale est en outre antérieure aux guerres *entre* les nations."

Pour Graeber il faut noter qu'une "guerre" que l'on imagine souvent entre États-nations apparait bien plus souvent dans une nation. Pour une "guerre intérieur", on peut noter parfios un attachement plus fort à la notion d'utopie.

> "Leur guerre contre le souverain devient la base de leur être et donc, paradoxalement, la base d'une certaine notion de la perfection - et même de la paix."

Travailler sur la souveraineté devrait donc être d'abord du travail sur cette guerre, qui est d'ailleurs ingagnable pour les souverains. Graeber prend l'example des USA avec leur Guerre contre la Pauvreté puis contre le Crime puis contre la Drogue et le Terrorisme.

> "Ce n'est qu'en ce sens que l'État est, selom la fameuse formule de Thomas Hobbes un 'dieu mortel'."

Mais la souveraineté n'est pas inévitable et il faut la combattre pour "commencer à envisager avec réalisme l'éventail complet des possibilités humaines."

## Chapitre 3: Les dimensions atemporelles de l'histoire, Dans le vieux royaume du Kongo, par exemple, M. Sahlins

### Introduction : histoires paradigmatiques

> "On pourrait considérer notre essai comme un prolongement de l'argument essentiel de Hallowell selon lequel 'ce que nous appelons mythe' ne constitue pas seulement pour les peuples concernés 'le compte rendu véridique d'évènements du passé', mais forme aussi compte un compte rendu actuel 'de la manière dont leur champ phénoménal est culturellement constitué et cognitivement appréhendé'. Des observations similaires ont été enregistrées par l'ethnographie dans le monde entier. En Nouvelle-Guinée, par exemple, ou, dans les Hautes Terres, 'pour les Mbowamb, les mythes sont la vérité - ce sont des faits historiques transmis comme tels. Les forces qu'ils décrivent et représentent ne sont pas du type de celles qui ne se produisent qu'une fois, mais sont continuellement effectives et existent réellement.'"

Sahlins prend l'exemple des Maori qui justifient souvent leurs volonté, décisions politiques par l'histoire, par leurs "mythes", de vieux proverbes ou poèmes. Connaitre ces histoires, controler comment l'histoire s'écrit, choisir les bons "mythes" à raconter, voilà donc des choses très importantes.

> "Culture is what happens when something happened."

On observe souvent que les faits actuels importants (au niveau royal par exemple) ne sont que des métaphores de faits historiques, par inspiration consciente ou non.

### Les rois-étrangers africains

> "Les royaumes-étrangers tels les royaumes lunda, ndembu et nupe en Afrique constituent la forme dominante d'État prémoderne dans le monde, sur tous les continents. Ils se sont développés dans une grande variété d'environnements et ont combiné une gamme considérable de régimes économiques : commerciaux, pirates, agricoles (pluviaux et hydrauliques), esclavagistes, d'élevage ; en outre, le plus souvent ils étaient liés à une forme d'économie mixte. Il s'agit de sociétés typiquement duelles : les privilèges, les pouvoirs et les fonctions sont assez nettement divisées entre des dirigeants d'origine étrangère - étrangers pour toujours, car c'est la condition de possibilité de leur autorité - et les populations autochtones sujettes - qui sont habituellemt 'propriétaires' de la terre et maitresses rituelles de sa fertilité."

On s'intéresse à un royaume particulier, le royaume kongo des XVIe et XVIIe siècles.
À noter que les anthropologues et historiens sont souvent sceptiques de l'histoire et des "mythes". En effet, comment prouver qu'un étranger est réellement devenu roi quand ces récits ne subsistent quand par tradition orale depuis des générations ? Mais pour Sahlins, s'il est intéressant de "comprendre ce qu'il s'est réellement passé", les récits fondateurs doivent être pris en considération car ils sont tenus pour vrai par la société et ont donc une grand influence.

Les royauté-étrangères abondent en Afrique centrale, c'est même la forme normale. Les sociétés sont séparées entre "les conquérants et les habitants originels vaincus."

de Heusch:

> "Tout se passe comme si la structure même de la société lignagère n'était pas susceptible d'engendrer des développements dialectiques tant sur le plan politique que sur le plan économique, sans l'intervention d'une structure symbolique nouvelle. Ce n'est pas par hasard que tant de traditions mythiques, en Afrique occidentale comme en Afrique centrale, présentent le fondateur de la royauté comme un chasseur étranger, détenteur d'une magie plus efficace. Quelque soit l'origine historique de cette institution politico-symbolique, la diachronie mythique fait toujours intervenir des évènements extérieurs, que le bruit des armes les accompagne ou non. La royauté apparait d'abord comme une révolution idéologique, dont la société ancienne n'ignore pas les dangers \[...\] La souveraineté, la source magique du pouvoir, vient toujours d'ailleurs, d'un prétendu lieu originel, extérieur à la société."

Pour autant parler de "conquérants" étrangers ne doit pas faire penser que nous avons la à faire à une simple conquête. En effet, une relation duale s'établit, le peuple autochtone restant propriétaire de la terre et des rites. Deux groupes distincts forment la société qui ne peut exister sans l'un des deux.

### En route vers le royaume

On remarque trois thèmes récurrents. Le "prince turbulent" ou "fils gênant", un fils de roi ambitieux qui est exilé pour une certaine raison ; l'"exploit" de cette personne contre la parenté et la morale commune ; et finalement le fait que ce prince soit un grand chasseur ou guerrier.
L'"exploit" est généralement suffisant pour faire d'un homme un roi. Ce status est donc antérieur à l'arrivée dans une société étrangère.
Ceci est renforcé par le fait que des luttes entre fils de roi pour le trône étaient courantes et il existait donc de nombreux princes non-régnant.

Il y a un parallèle à faire entre le roi exilé qui deviendra roi-étranger et la brousse africaine. On a des éléments extérieurs à une communauté et qui sont très dangereux. Avoir un grand chasseur qui vient de l'extérieur semble être un moyen de se protéger. Les pouvoirs du roi, une capacité à maitriser la vie et la mort (tuer un lion pour protéger des habitants etc) sont des caractéristiques impressionnantes d'un roi.

### Avènement du roi étranger

> "Il semble que le plus souvent, selon les traditions largement admises, la domination étrangère ait été imposée sans violence."

Parfois, le peuple vaincu a lui-même invité voire kidnappé un roi extérieur.

Il est important de comprendre que chaque société se place dans un contexte plus vaste, et possède des voisins plus au moins puissants:

> "Ainsi, le système politique galactique est-il composé d'un certain nombre de royaumes et de chefferies plus ou moins soumises à un État central dominant, dont la portée administrative, tributairee tculturelle diminue généralement en fonction de la distance qui les sépare de la capitale. Les royaumes périphériques continuent souvent à fonctionner sous l'autorité de leurs souverains traditionnels, à condition de respecter leurs obligations tributaires. Southall (1988) a parlé à cet égard d'"État ségmentaire", ajoutant que le status magique exalté du souverain central s'étend généralement plus loin que son autorité réelle. Cela revient à dire que les sociétés marginales, étant ainsi dominée culturellement avant de l'être dans les faits, sont, dans une large mesure, attiréees par le centre."

De part la façon dont sont constitués certaines communautés, "il est certain qu'une partie des autorités autochtones renforcent leur statut et leurs pouvoirs sous l'égide de rois-étrangers." Ainsi il était d'une part bénéfique pour les chefs locaux de faire venir de puissants rois / alliés étrangers pour renforcer leur propre pouvoir et il était intéressant aussi pour la population d'accepter ce genre de roi car ils étaient aussi censés apporter protection voire de la technologie (par exemple les Européens ou certains mythes qui racontent comment le roi à apporter le feu ou le fer).

### Naturaliser le roi éranger

Le processus de naturalisation implique le mariage du roi avec une fille locale. On fait semblant de tuer le chef qui renait dans son nouveau peuple, avec de nouvelles qualités. Il est censé oublier ses vieilles habitudes de chasseur sauvage égoiste. On trouve dans d'autres coutumes une bataille entre le roi et un chef local dans lequel le roi est d'abord vaincu. Si conquête il y a, elle a lieu dans les deux sens. La victoire sur l'étranger est parfois peut-être aussi de l'humiliation.

Dans l'ancien État du kongo, un des grands prêtres *kitomi* permit "l'avènement de Ntinu Wene, fondateur de la dynastie historique." Les rois suivants doivent être adoubés par les successeurs de ce prêtre. Le roi est d'abord vaincu dans un simulacre de bataille, puis le *kitomi* lui tend la main pour traverser une rivière. Le lendemain, le roi et sa femme sont couchés par terre devant la maison du prêtre qui sort avec sa femme, se déshabille et piétine ses habits. On enduit le couple royal d'un mélange de terre et d'eau. Puis, "le *kitomi* et son épouse \[...\] s'offrent sexuellement comme 'épouse' au couple princier."

### Sur la traversée des eaux et le mariage avec la terre

La terre et l'eau dans les rituels sont associés à la fertilité, que les autochtones possèdent. La traversée de la rivière est un thème courant dans les mythologies africaines.
Pour les Bakongo c'est particulièrement important car dans leur cosmologie, les différents royaumes des morts et des vivants (voire des esprits) sont séparés par un fleuve. De plus, les pouvoirs de sorcellerie, ceux qui permettent le succès, de vivre plus longtemps etc viennent de l'autre monde, de l'extérieur de la société. Il est donc primordial que le roi vienne de l'extérieur également.
On peut noter que le roi-étranger possède un condensé de la société, il crée la société, car le mariage originel est une sorte de contrat, de base pour la nouvelle société.

Tout le long de cette partie, Sahlins s'emploie à démonter Thornton qui affirme que le Kongo n'est pas réellement un royaume-étranger en réécrivant apparemment des sources et en partant d'une analyze très marxiste comme quoi c'est l'organisation économique qui a crée deux classes qui sont associés aux étranges et aux autochtones.

Un témoignage du fait qu'il est possible d'avoir un roi-étranger, ce qui ne fait aucun doute pour Sahlins, c'est celui du capucin Montesarchio, qui, tout au long de ses missions, s'est vu offrir un certain nombre de mariages:

> "Ils ont dit que j'étais un 'Blanchita' ce qui revient à dire que j'étais un homme revenu de l'autre monde."

### La société duelle

On rajoute par rapport à avant que les étrangers ne sont pas concernés par le travail (qui est lié à la terre). De même le contrôle des ressources (tribut) est laissé aux prêtres locaux. Le rôle des rois est au niveau de la société, pas au niveau du clan, il doit "assumer le commandement de l'univers au profit de l'ensemble du groupe."
Le roi gouverne le peuple, les clans gouvernent la terre.

> "Dans la mesure ou l'aristocratie dirigeante 'réalise la construction de la société après que le monde ait été transformé par le travail', les indigènes sont en quelque sorte la classe ouvrière dans les royaumes-étrangers \[... mais\] la classe ouvrière assujettie du royaume-étranger détient le monopole des moyens primaires de production. De plus, étant principalement organisé par les relations de parenté, sa production est orientée principalement vers la consommation domestique. Là ou les autochtones sont propriétaires et productifs, l'aristocratie dirigeante dépend du tribut et de l'extraction."

Les dirigeants utilisent les ressources prélevées pour l'accumulation, la redistribution et la consommation ostentatoire. Elle gagne d'autres ressources par la guerre, et de manière générale, toujours par la prédation.

### Royauté-étrangère en série

Il arrive que de nombreuses dynasties de succèdent dans un royaume-étranger.

> "Le pouvoir sur l'ensemble de la société \[...\] pass aux mains du dernier peuple arrivé, le plus grand, tandis que l'autorité rituelle sur la terre est dévolue au plus ancien et au plus petit de ces peuples."

Les anciens souverains deviennent grands prêtres du royaume et occupent des "fonction temporelles". Ils s'occupaient des souveraines morts et vivants, se répartissant rites funéraires, intendance royale, intronisateurs etc etc

### Origines du royaume Kongo

Débat sur les origines de Ntinu Wene, beaucoup de sources, tout est très compliqué et je ne sais pas quoi en tirer. Mais si tu veux 15 pages du récit historique du Kongo avec plein de noms et de descriptions ça se passe page 275.

### Historiographie (fin)

Pour les peuples étudiés, 'l'ensemble de l'ordre culturel \[...\] est fondé sur un récit diachronique', 'les habitants \[...\] se trouvent plongés dans l'histoire'. En effet, le peuple recrée en permanence son histoire et s'y réfère. Certains historiens occidentaux voudraient faire passer ces histoires pour des mythes, dépourvus de réalité historique, mais pour les peuples en question, la question de la véracité en se pose pas. Ces histoires

> "sont vraies parce qu'ils les vivent, et ils les vivent parce qu'elles sont vraies : la société perdure à travers ces tautologies."

Pour éviter le sens péjoratif de "mythe" qui vient notamment de gens comme James Frazer, Bascom propose la typologie suivante :

1. Les mythes sont relatifs au cosmos, aux métapersonnes, aux origines du monde. Ce sont des récits sacrés tenus pour vrais et importants pour les rituels.
1. Les légendes se situent plus proches de nous dans le temps, avec un monde qui ressemble à l'actuel, et ont pour objets les guerres, les chefs, les rois, les actes héroiques. Ils sont également tenus pour véridiques.
1. Le folklore, contes populaires qui ne sont pas tenus pour vrais.

On entend souvent des mythes-légendes en Afrique, qui à la fois *sont* l'histoire mais *font* aussi l'histoire car ils servent de précédents qui permettent de résoudre des problèmes actuels.

Pour Sahlins, certains (surtout les anthropologues) considèrent que la véracité n'est pas importante quand d'autres (surtout des historiens) considèrent que ces histoires sont juste fausses et sont pas l'histoire. Sahlins rejettent ces deux extrêmes, ainsi que la position médiane du "noyau dur" de vérité.

> "Le relativisme culturel est d'abord et avant tout une procédure anthropologique interprétative, cad une procédure méthodologique. Il ne s'agit pas d'un argument moral selon lequel toute culture ou coutume est aussi bonne que n'importe quelle autre, si ce n'est la meilleure. Le relativisme prescrit simplement ceci : pour être intelligibles, les pratiques et les idéaux d'autres personnes doivent être placés dans leur propre contexte historique. On doit les comprendre comme valeurs positionnelles dans le champ de leurs propres relations culturelles et non les apprécier selong des jugements catégoriques et moraux de notre cru. La relativité consiste à suspendre provisoirement nos propres jugements afin de situer les pratiques en question dans l'ordre historique et culturel qui les a rendues possibles. Il ne s'agit en aucun cas d'un plaidoyer."

Pour certains anthropologues donc, le fait qu'un mythe soit vrai ou non n'importe pas car ce qui est important est qu'il soit vu comme était vrai, "ils fonctionnent comme paradigmes historiques, indépendemment de leur statut factuel."

Si l'on pouvait, grace à des moyens archéologiques, déterminer ce qu'il s'est passé, ce serait intéressant car cela montrerait le travail de la culture.

> "la question fondamentale n'est pas de savoir ce qu'il s'est réellement passé, mais de conprendre *ce* qu'il s'est passé."

Un exemple, quand les femmes hawaiennes sont aller manger sur le bateau du capitaine Cook, les marins y ont vu un repas agréable, les femmes ont elles briser un tabou important pour la société, voilà *ce* qu'il s'est passé.

> "Aussi circonstancielle soit-elle, l'histoire est nécessairement atemporelle et culturelle, de part en part. Le contexte culturel transhistorique conditionne la possibilité d'une causalité pertinente, qu'elle soit séquentielle ou analogique, syntagmatique ou paradigmatique - le contenu de *ce* qu'il s'est réellement passé. Sans quoi, abstraction faite de la culture concernée, ce qu'il s'est réellement serait aussi significatif qu'un arbre tombant dans une forêt africaine inhabitée."

## Chapitre 4 : La royauté-étrangère mexicatl, M. Sahlins

> "Les dirigeants d'un nombre remarquable de sociétés dans le monde entier étaient, vis-à-vis de ceux qu'ils gouvernaient, d'origine étrangère"

C'est "la forme dominante de l'État pré-moderne". Le fait de venir de l'étranger est une condition à la souveraineté. Le mariage avec une femme autochtone est le fondement de la nouvelle société. Cela implique que le roi est fils du peuple (les femmes sont autochtones), et l'on s'adresse parfois à lui comme "mon enfant".

> "Le retour du roi originel, souverain du peuple indigène - la figure de Quetzalcoatl dans le texte de Moctezuma - est un autre récit commun des royaumes-étrangers, qui fonde un drame rituel annuel. Le roi, au status divin de mémoire antique, revient réclamer sa souveraineté avant d'être à nouveau déposé par l'usurpateur désormais au pouvoir. Pas avant cependant d'avoir renouvelé la fertilité de la terre pendant son ascension temporaire."

Il est arrivé à un certain nombre d'explorateurs comme le capitaine Cook à Hawaii d'être pris pour un de ces dieux. Mais il faut noter que l'opposition entre roi et dieu peut donner un certains nombres d'issues, parfois peu favorables au Dieu. Cook fut tué.

> "Nous considérerons en effet ici, comme dans le cas des Banyoro, un royaume-étranger, établi par une société périphérique relativement peu développée, sur le territoire légendaire, central, de "haute culture" d'une entité galactique. Bien que de telles prises de contrôle des centres dominants par des peuples de l'arrière-pays ne soient pas si inhabituelles, tout ici semble inverser le cas idéaltypique dans lequel des rois étrangers venus de patries légendaires subjuguent des autochtones incultes."

On prend le cas des Mexica qui se disent fièrement toltèques et chichimèques.

### Rois-étrangers et politique galactique

Le cas typique du roi-étranger est le suivant: un fils de roi puissant qui ne peut être roi dans son propre royaume part pour un voyage périlleux avant d'arriver dans sa futur patrie ou il symbolisera la fertilité et la victoire. Il est souvent connu pour ses actes extraordinaires et immoraux.

> "Quetzalcoatl et Huitzilopochtli sont connus pour avoir trahi ou tué des proches parents \[...\] sur le chemins de leurs royaumes respectifs."

> "Doté d'une puissance cosmique, supérieure à la société, le roi-étranger est en mesure de la réorganiser. L'arrivée du héros étranger est une véritable mission civilisatrice qui extirpe le peuple autochtone de son état originel de sauvagerie nue."

Par exemple les Chichimèques étaient assez "primitifs" avant l'arrivée des Toltèques et notamment de Quetzalcoatl. Ce genre de "conquête" est en général pacifique (le peuple y gagne, et certains peuvent y avoir des intérêts personnels).

Dans la vallée du Mexique, tout semble être inversé. "Les souverains étrangers sont les barbares d'origines chichimèque" et les chefs autochtones "d'ascendance toltèque sacrée".

> "les dirigeants de l'ancien régime deviennent les principaux prêtres d'un pays \[...\] Les pouvoirs cosmiques du roi ne sont effectifs que s'ils sont médiatisés par les offices
> sacrificiels du sacerdoce indigène."

Les autochtones restent propriétaires de la terre.

> "Quoi qu'en dise le matérialisme historique, on observe que dans les États prémodernes décrits ici, la classe subalterne contrôle les moyens de production du secteur primaire."

> "On observe principalement des formes de taxation ou de pillage, plutôt qu'un contrôle du capital et du processus productif en tant que tel."

La forme cosmologique du mandala : schéma de création

> "d'un centre raffiné vers l'extérieur et d'un sommet raffiné vers le bas, chaque réalité ou cercle extérieur formant un représentation progressivement plus faible de la précédente \[..\] plus grossière dans sa constitution, plus captive de ses quêtes sensorielles et de ses désirs." Stanley Tambiah

Le thème vient des études de l'Asie du Sud et du Sud-Est mais peut s'appliquer de manière plus large. Ces zones de pouvoir jouent un rôle important dans "le mouvement des peuples périphériques en direction des 'hautes cultures' des régions centrales."

> "Les systèmes galactiques sont marqués par une politique de 'noblesse ascendante', par laquelle les chefs des régions satellites assument les status politiques, les styles de la cour, les titres et même les généalogies de leurs supérieurs dans la hiérarchie régionale - qui, pour leur part, imitent le souverain galactique, tandis que ce dernier, en opposition aux vassaux ambitieux et aux empereurs rivaux, prétend régner sur le monde."

Il faut imaginer une myriade de peuples, chacun ayant sa culture et son organisation du pouvoir. Cette périphérie possède un centre, comme les toltèques. Les pouvoirs du centre s'exportent en périphérie par l'adoption de coutumes, ce qui change la position du peuple dans la galaxie.
On appelle souvent cela aussi des empires. Les chefs historiques sont laissés en place et les relations sont principalement des prélèvements de tributs.
Les galaxies sont un grand moteur de création de royauté-étrangères.
On peut noter que le centre des galaxies n'a jamais de pouvoir sur tout l'espace qu'elle revendique, son pouvoir et son influence faiblissent avec la distance et la présence d'autres peuples.

### Chichimèques et Toltèques

Pour les Mexica du XIVe siècle : rivalité entre Tenochtitlan et Tlatelolco. Chaque cité fit appel à un roi-étranger pour essayer de dominer l'autre. "Les anciens Tenachca battirent \[leurs suzerains\] Tépanèques ainsi que les Tlatelolco en désignant un souverain au status suprême en Mésoamérique", un descendant de toltèquem ce qui les relia "au grand Tollan, de mémoire antique, et au roi originel Quetzalcoatl."

Il suit beaucoup de descriptions historiques à base de noms compliqués que je ne recopie pas.

On remarque que plusieurs peuples revendiquent des descendants glorieux uniquement pour écraser un peuple concurrent, sans faits réels. Ils prennent alors un certains nombres de leurs valeurs :

> "Les Chichimèques, considérés comme de simples 'barbares' par les Toltèques civilisés, sont de leur propre point de vue de 'robustes et grands guerriers' de l'arrière-pays, opposés aux artistes et artisans toltèques citadins."

> "l'anthropologie se trouve depuis longtemps impliquée dans un scandale théorique majeur, dans la mesure ou elle s'est entêtée futilement à expliquer de diverses manières les cultures de l'intérieur, comme si celles-ci s'autofaçonnaient, quand nous savons pourtant qu'elles se forment, y compris leurs différences, dans leurs relations aux autres - schismogénèse."

## Chapitre 5 : Le peuple comme nourrice du roi, D. Graeber

> "Dans le royaume merina, au nord du plateau central de Madagascar, les rois étaient fréquemment représentés comme des bébés, des enfants ou des adolescents grognons. On les considéraiet à la fois comme têtus, difficiles et totalement dépendants de leurs sujets. Pareille conception des institutions du gouvernement engendrait une alchimie morale d'un genre particulier, en vertu de laquelle l'égoisme, l'autorité et même certains déchainements occasionnels de violence vengeresse pouvaient en réalité être considérés comme atendrissants ou du moins denir renforcer l'idée qu'il était du devoir des gens du peuple de veiller à la satisfaction des besoins du roi."

Cela avait deux implications :

- l'autorité adulte réelle semblait venir des rois morts
- le peuple avait un certain langage pour réprimander un roi qui dépassait les bornes.

### Introduction : Leiloza et le prophète de Valalafotsy

Leiloza, "jeune prince puéril et tyrannique" de l'Imamo, territoire à l'ouest de l'Imerina.

> "Leiloza était si grand et si fort qu'il refusait de marcher sur les sentiers qui longent les collines comme le font les êtres humains ordinaires, mais exigeait qu lieu de cela que les femmes du royaume tissent sans relache de la soie afin de dresser de gigantesque ponts suspendus réversés à son usage personnel entre Ambohitrambo et les autres montagnes alentour. Les souffrances occasionnées furent telles que son père perdit patience et coupa un beau jour les cordes alors même que son fils empruntait un pont, le condamnant à une chute mortelle."

Il est "la véritable incarnation de l'attitude égoiste et puérile des rois." On lui prête parfois de nombreux caprices, comme s'il prenait plaisir à faire souffrir le peuple. On parle encore de lui en le qualifiant de *maditra*, "villain", "têtu".

Un certain nombre de tombes royales parcourent les montagnesm les *doany*, "postes de douane", qui abritent des *andriana*, "rois". La population n'a en général rien de bon à dire de ces rois, ils étaient mauvais et abusaient de leurs pouvoirs. Ils sont cependant appréciés une fois morts. Pour Graeber ce sentiment n'est pas documenté au XIXe siècle et provient de la colonisation. En 1895, les français abolirent du même coup la monarchie et l'esclavage. L'esclavage et par extension toute forme de commandement (même le travail salarié) devint mal vu dans la population ... sauf chez les descendants d'esclaves qui représentaient un tiers de la population. Ils étaient d'ailleurs de loin de les plus susceptibles de devenir *zanadrano*, protecteurs des sépultures royales.
Pour Graeber, on peut cependant voir des remises en cause de l'autorité des rois avant la période coloniale. Les rois firent en effet face à une grande opposition (contrairement aux reines, bien plus nombreuses notamment au XIXe). Il semble qu'il s'agisse d'une clique de roturiers qui detenaient réellement le pouvoir politique qui décidèrent de placer des suzeraines sur le trône.

Questions:

- Pourquoi a-t-on remis en cause la légitimité des rois mais pas des reines ?
- Pourquoi après la colonisation a-t-on commencé à considérer les anciens rois comme mauvais de leur vivant et bons de leur mort ?
- Pourquoi représente-t-on les enfants comme des enfants grognons ?

#### Le vrai Leiloza et la reine brigande

On a un récit du véritable Leiloza. Lors de son règne, un prophète se présenta, ancien esclave ayant acquis la voix d'un ancien roi. Le peuple le crut mais Leiloza sentit son pouvoir en danger et le fit condamner à mort. Par la suite il perdit soutien de son peuple et les envahisseurs prirent le royaume, Leiloza mourut. Finalemenent une prophétesse se fit remarquer par son habilité et prit le pouvoir. Il prétendit être la réincarnation de l'ancien prophéte, ayant les mêmes cicatrices. Elle, et le fils de Leiloza, furent finalement mis à mort par le roi Radama pour de nombreux crimes (vol de bétail royal etc).\\

Notez le statut social originel bas des prétendants "prophètes".

### Le travail emblématique et le roi comme enfant

Résumé :

> "Le Leiloza historique était un tyran si jaloux de l'enfant possédé par l'esprit beinveillant de son grand-père qu'il finit par le tuer. Il perdit ainsi la loyauté de ses sujets et précipita sa déchéance. Le Leiloza légendaire d'aujourd'hui est un enfant tyrannique qui, après sa chute, devient lui-même un esprit bienveillant possédant des médiums."

Une des grandes caractéristiques du royaume merina est le *fanompoana* ou corvée royale, que chaque sujet était censé réaliser. Il semble que certaines des ces taches étaient particulièrement ingrate et constituaient et le principe central d'administration du royaume.

Le défi est de dépasser le *kabary* malgache, la rhétorique, qui déguise beaucoup d'actions derrière des double-sens, des subtilités peut souvent comprises dans les récits ethnographiques. Par exemple :

> "le *hasina* est avant tout une façon de parler du pouvoir que personne ne comprend totalement."

Cela désigne généralement l'autorité royale. On dira que les sujets donnent du *hasina* "au roi, sous forme de dollars d'argent entiers, qui expriment leur désir de créer un royaume unifié", mais les gens étaient sceptiques quant au fait que cela fonctionnne dans ce sens ou bien si c'est le royaume unifié qui crée ce besoin ...

### Sur le travail rituel

Il y a beaucoup de métaphores au sujet des enfants dont nombre de rituels. Les médiums et guérisseurs par exemple ont un nom qui vient de nourrice / soigneur qui sous-entend "l'autorité bienveillante et nourricière et quelqu'un de compétent et d'informé."
Habituellement on considère que tous les membres du peuple sont des enfants et que les seuls adultes sont les ancêtres décédés. Lors des rituels funéraires, cette rhétorique s'inverse, les ancêtres sont choyés comme des enfants, on les déterre, "ils sont dorlotés comme le seraient des enfants."

> "tout le monde peut manipuler les corps, danser avec eux, les rhabiller et les pulvériser partiellement au passage avant de les enfermer à nouveau dans le tombeau ancestral."

Pour Graeber, il est impossible dans ce genre de société de faire la différence entre travail et rituel.

> "ce que nous appelons les 'sociétés' sont toujours de vaste systèmes coordonnés de travail ritualisé"

L'unité fondamentale est le foyer au sein duquel le travail des femmes est dominant. Si les structures plus larges de la société ne ressemblent pas forcément au foyer, **la particularité des monarchies est que le sommet ressemble "trait pour trait" à la base, avec la présence du foyer royal**.
On notera que ce sont généralement des foyers très complexes (avec beaucoup de personnel, des eunuques, ...), et des règles morales perverses (des rois parricides etc) mais ils ont "une unité domestique qui crée, forme et élève des enfants". C'est d'ailleurs sont seul but, engendrer des personnes, et rien de plus comme dans un foyer normal (vêtements, nourriture, outils, ... ). Les foyers royaux sont opposés à la production matérielle, et se concentrent uniquement sur les personnes.

Graeber insiste sur le fait que le travail dans des sociétés aussi différentes des notres que les sociétés merina doit être abordé loin des préconceptions modernes.

> "Si le travail *paradigmatique* est ce que l'on imagine comme le modèle du travail en général, ou de toute une catégorie générale de travail, le travail *emblématique* est un travail considéré comme typique d'un certain groupe de gens, un type de travail qui définit quelle sorte de gens ils sont censés en dernière analyse."

> "Il existait ainsi dans les royaumes des Fidji certains groupes identifiés en tant que pêcheurs - non parce qu'ils passaient l'essentiel de leur temps à pêcher, ni parce qu'ils passaient plus de temps à la pêche que qui que ce soit d'autre (tout le monde pêchait), mais parce qu'il relevait de leur responsabilité de fournir du poisson à la cour et pour les rituels royaux."

On retrouve la même idée chez les merina, au sujet de "la construction des demeures royales et des tombes royales". Le travail paradigmatique correspond alors à ce que les gens pensent en premier lorsqu'on parle de travail ie déplacer, tirer, transporter des choses.

### Parler, porter et faire

Une image paradigmatique du travail pourrait être :

> "une femme jeune, un nouveau-né dans le dos, travaillant dans une rizière ou transportant de l'eau ou des denrées alimentaires sur sa tête."

Ce qui est très différent de l'image que l'on peu avoir aujourd'hui d'un homme allant à l'usine. À Madagascar, le travail est l'apanage des femmes et les hommes sont souvent traités de paresseux.

> "La forme paradigmatique de travail était donc \[...\] l'action consistant à soulever des choses et à les déplacer dans l'espace."

Et culturellement, *qui* porte *quoi* et *comment* sont des questions très réglementées. Si l'on prend un homme et une femme du même age, alors la femme doit porter le fardeau. Cela est particulièrement valable pendant les rituels.
Le principal critère cependant est l'age, on considère que la réciprocité entre frères (ou soeurs) va ainsi: l'ainé peut exiger du cadet qu'il porte ses affaires et à l'inverse le·a plus jeune peut demander à ce que le·a plus vieux·ielle parle pour elle·lui lors des tribunaux ou assemblées.

> "le mot malgache pour "oppression" est précisément *tsindriana* : être pressé vers le bas. Et il est intuitivement logique qu'il en soit ainsi : on peut aisément se figurer que le premier profond sentiment d'injustice éprouvé par un enfant se situera précisément à ce moment ou il devra soudainement passer de l'abscence de responsabilités à la responsabilité la plus lourde de toutes."

L'élément important est l'opposition entre *parler* et *porter*. Parler était lié à la fabrication, à la 'production', quand porter était associé à un travail non créatif, et donc au fait de nourrir, entretenir, ou maintenir des choses.

Les royaumes des Hautes Terres sont séparées entre les *andriana*, mot qui désigne aussi bien le souverain que le tiers de la population libre qui se revendique de descendance royale ou a été anoblie, et les *hova*, "roturiers".
Les *andriana* doivent effectuer des corvées royales, les *hova* sont au service du roi et les esclaves se distinguent en ça qu'ils servent leur maitre.
Ces deux groupes de personnes libres passaient la majorité de leur temps à la culture du riz l'été et à l'artisanat l'hiver qui leur apportait de quoi vivre, mais les statuts sociaux étaient réellement déterminer par quelle tache une personne exerçait pour le roi.

> "Les *andriana* jouissaient du monopole de la puissance de créativité \[...\] et était considéré comme les maitres du discours oratoire et poétique."

> "Les *andriana* fabriquaient \[...\] de beaux objets. Les *hova* transportaient des choses d'un endroit à un autre."

Pour donner des exemples, les *andriana* pouvaient être (en fonction de leur lignée) charpentiers ou tailleurs de pierre pour s'occuper du tombeau du roi, forgerons qui fabriquaient "l'imposant cercueil d'argent dans lequel les rois étaient enterrés"; les *hova* pouvaient, eux, acheminer les matériaux ou s'occuper des déchets.

### La corvée royale comme principe de gouvernement

Si les corvées royales sont questions de status pour les *andriana*, elles sont principe de souveraineté pour le roi: il est celui qui peut faire faire ce qu'il souhaite à quiconque. Au cours de l'histoire, ces *fanompoana* furent invoquées comme principe de gouvernement,

> "le roi Andrianampoinimerina invoqua le principe de la *fanompoana* afin de rassembler la main d'oeuvre nécessaire à la bonification de milliers d'hectares de marécages ; au XIXe siècle, son fils Radama fit de même pour obliger les enfants à fréquenter les écoles missionnaires et les adolescents à servir dans l'armée régulière nouvellement créée."

Graeber note que, si les impôts étaient surtout symboliques, ils étaient consignés au centime près dans les registres, alors que la *fanompoana*, qui semblait être la base de tout, n'y était jamais détaillée. On ne sait donc pas ce qui consistuait réellement la *fanompoana*, comme si l'on n'avait pas souhaité la faire entrer dans le domaine bureaucratique. On sait cependant ce que certains groupes privilégiés étaient exemptés de faire ce qui peut donner une idée:

- *Manao Hazolava*, "trainer des arbres",
- *Mihady Tany*, "creuser la terre",
- *Manao Ari-Mainty*, "fabriquer du charbon de bois",
- *Mitondra Entan'Andriana*, "porter les bagages royaux"

Ces catégories étant à prendre à un sens plutôt large.

Tout cela est aussi lié au contexte géographique de l'Imerina: pas de bête de somme ni d'engin roulant ou de route carrossable implique que tout devait êre déplacé par des humains. Il n'y avait pas de forêts non plus.

Les porteurs étaient souvent raflés. Lors de voyages du roi ou de la reine, on déplaçait de grandes quantités de mobilier, et on forçait les habitants des villages à la porter, pendant que la cour royale consommait toutes les provisions de la région traversée, le tout causant des centaines voire des milliers de morts.

### Renversements: le roi comme enfant

> "Porter est donc à la fois la forme paradigmatique du travail et la forme emblématique de la subordination ; la production, la création est, au contraire, considérée comme bien plus proche de l'activité de parole, capable de créer des réalités sociales *ex nihilo*"

On parle souvent de la soumission à l'autorité des plus anciens en terme de réciprocité, c'est "la réciproque pour avoir été porté sur le dos". L'élément pivot est donc ici la femme, puisque c'est elle qui élève l'enfant, le porte. Graeber fait un parallèle avec certains rites royaux. Pendant les récoltes, le premier échantillon revient au roi, propriétaire naturel de tout ce qui existe, qui en l'acceptant, renonce à ses droits sur le reste des récoltes.

### Réflexions sur le roi comme nouveau-né

On sait qu'il y a un lien rhétorique entre la façon de parler des rois et des bébés. Par exemple, le personnel du palais, vassaux, conseillers étaient appelés "nourrice du roi". Ces nourrices parlaient aux rois en les tutoyant (ou du moins l'équivalent en langue locale).

Graeber fait remarquer que le royaume merina n'est pas l'archétype du royaume-étranger, tout le peuple est présenté comme des étrangers envahisseurs qui ont chassé la population autochtone. Cependant le roi comme enfant comporte les deux personnages typiques du "roi-comme-étranger-extramoral et le roi-apprivoisé-par-le-roi".

> "L'attention considérable portée à l'éducation des enfants royaux avait toutefois pour conséquence paradoxale l'inachèvement constant de cette tache. Les rois et les reines ne grandissaient jamais vraiment et ne devenaient jamais vraiment autonomes. Ils restaient, en un sens, des nouveau-nés permanents. Cela représentait à la fois la clé de leur légitimité - ils étaient chéris - et leur santé et leur bien-être étaient l'objectif commun des Ambaniandro, les membres du peuple ou les 'Sous le ciel' - mais également une limite évident à l'exercice de leur pouvoir, dans la mesure ou, bien que certaines démonstrations puériles d'irascibilité fussent certes attendues, comme nous le verrons, la conception du roi comme enfant permettait à tout le monde, en principe, d'intervenir et d'imposer une discipline maternelle certes douce mais ferme chaque foi que le monarque était jugé avoir dépassé les bornes de l'acceptable.
> Je voudrais suggérer qu'il s'agit précisément de la relation entre les royautés divine et sacrée qui s'exprime ici."

### Le système rituel depuis la perspective de l'enfant-roi

L'arbitraire du·de la souverain·e lea confine au statut d'enfant, hors de l'ordre moral des adultes. Les souverain·es peuvent être 'vilain·e' mais ne peuvent pas réellement avoir tort.

### Contestation populaire, rébellions des femmes et retour des anêtres morts

Questions: comment se fait-il que les rois soient passés d'un statut d'enfant attendrissant à une quelque chose de contestable ? Pourquoi avoir privilégier les souveraines par la suite ?

Graeber prend l'exemple du roi Tsimarofy, qui avait une tendance prononcée pour l'alcoolisme et l'opium. Il livrait une bataille pour Antananarivo contre un roi du nord ayant un tempéramment complètement opposé au sien.
Les sujets de Tsimarofy s'organisèrent un jour et décidèrent en assemblée que le roi devait céder sa place à son fils s'il n'arrêtait pas la boisson.
Tsimarofy finit par perdre la guerre au profit du roi du nord puis de son fils, Radama 1er. Ce dernier "se prenait pour un nouveau Napoléon", "un despote éclairé déterminé à employer ses pouvoirs illimités à remodeler la société selon des critères modernes et progressistes"

> "Il institua un système scolaire et une fonction publique. Il promut des projets industriels et des campagnes de modernisation des techniques de construction, des styles vestimentaires et des normes d'hygiène publique. Il divisa toute la population male d'Imerina en deux grandes catégories, les militaires (*miaramila*) et les civils (*borizano*), et invoqua le principe de la *fanompoana* pour appeler les premiers sous les drapeaux tandis que les seconds furent répartis en colonne de travaux affectés à des corvées royales toujours plus pénibles"

Radama était également en rupture avec les vieilles coutumes et superstitions.

Il faut noter que la coiffure était très importante. Coiffer ses proches était une des formes paradigmatiques du travail féminin. Radama "coupa ses cheveux et adopta une coiffure en brosse de style militaire à la mode européenne". Il exigea la même chose de tous les *miaramila*.

> "Cet ordre fut l'apogée d'une série de réformes qui eurent pour effet cumulatif de transformer la *fanompoana*, jusque-là système de travail rituel axé sur la famille royale, en principe d'organisation d'un État moderne."

Contre ces ruptures rituelles une assemblée de plusieurs milliers de femmes se mobilisa. Elles reprochèrent au roi sa collusion avec l'étranger et d'oublier les rituels. Elles se positionnèrent comme 'nourrices du roi'. Elles furent sévèrement réprimées.

Ranavalona succèda à Radama. Elle était sa femme la plus agée et règna malgré tout plus de trente ans. Sans toutefois remettre en cause les changements de Radama au niveau de l'école ou de l'armée, elle rompit complèment avec la politique de son ancien époux. Elle expulsa les étrangers, remis en place les coutumes et eu un long, calme et prospère règne.
Ce changement peut aussi se voir comme le passage d'un pouvoir modéré mais constant (création de l'état) à un pouvoir plus brutal mais moins intrusif (comme chez les Shilluks par exemple).
On retrouve chez cette reine des descriptions de petite fille capricieuse, et d'un système global nourricier à son service.

Traditionaliste, elle règna de plus en plus avec brutalité et arbitraire. Son fils RakotondRadama, né après la mort de Radama, était des progressistes. Il était ami avec les rares étrangers tolérés dans la ville, ami des chrétiens du royaume. Il allait dans les prisons et libéraient ceux qu'il jugeait enfermés à tort.
Arrivé au pouvoir en 1861, Radama II abrogea rapidement toutes les décisions politiques de sa mère, la conscription, la peine de mort, l'épreuve du poison, il ouvrit le pays et réclama la liberté religieuse.

Fin 1863, une étrange épidémie commença. On l'attribua à des esprits. La maladie faisait plonger les infectés dans une sorte de transe, caractérisée par le besoin de danser. Rapidement des groupes de danseurs, accompagnés de musiciens (la musique a des vertues curatives) se réunirent aux quatre coins du pays. Et cela ressembla de plus en plus à une révolte, une révolte théatrale. Composés principalement de femmes, ces groupes avaient-ils en tête le sort de la dernière révolution des femmes ?
Les personnes malades agissait comme possédés par Ranavalona, comme si elle était revenue punir son fils. Les transes faisaient ressortir des vieilles traditions et les comportements refutaient les actions du roi. La situation dégénéra rapidement, le roi fut tué et sa femme accéda au pouvoir.

### Conclusions

On a donc des souverain·es considéré·es comme des enfants, ce qui fait que le peuple pardonne les comportements extrêmes mais lui donne aussi un moyen de contrôle.
Au XIXe siècle, les rois sont des figures napoléoniennes réformatrices qui font entrés des forces extérieures dans le royaume et qui sont combattus par les femmes.

Pour Graeber, la particularité de Madagascar est qu'il n'existe pas vraiment de temps mythologiques qui inspireraient les souverain·es actuels. Pour lui, Madagascar vit encore dans les temps mythologiques.

La particularité du roi comme enfant est le potentiel qu'il dégage. On ne sait pas ce qu'il deviendra, et c'est donc une sorte d'encouragement à l'innovation et à la créativité. D'ailleurs si les ancêtres sont aussi honorés c'est aussi un moyen des les apaiser pour pouvoir créer quelque chose de nouveau.
Mais pour Graeber, les derniers rois ont eu trop souvent tendance à voir dans l'ouverture aux techniques occidentales, des possibilités d'innovations irrésistibles. Les reines au contraire comprenait que l'innovation repose avant tout sur "un système complexe de travail de soin ritualisé".

Avoir une reine et des hommes qui effectuent des taches pénibles pour elle, c'est un renversement du foyer ordinaire. Et après des décennies de colonisation, il est resté compliqué pour beaucoup d'accepter que travailler pour un salaire était quelque chose de respectable.

Théories féministes du travail du soin: Nancy Folbre, Silvia Federici, Evelyn Glenn Nakano.
Classiquement, les hommes sont associés au travail "productif" et les femmes au travail du "soin", "reproductif", non reconnu.
Dans le royaume merina, le travail, caractérisé par le port de charge lourde, est féminin. La ritualisation du travail, ce qui le rendait créatif, était le domaine des hommes.

> "il est indéniable que la monarchie représente, dans le cadre de l'histoire mondiale, une forme extrêmement courante de gouvernement, et qu'elle se révèle souvent terriblement efficace ; il y a apparemment *quelque chose* dans le fait de se focaliser sur un seul et unique foyer, avec tous ses inévitables drames communs à tous les foyers, et de lie placer au sommet d'un système politique capable de saisir l'imagination et de susciter l'affection des populations assujetties. Ceci est du en partie, évidemment, au fait que les monarques engendrent des bébés ; dans presque toutes les autres formes de gouvernement, les enfants et les bébés n'apparaissent pas sur la scène politique. Il n'en faut pas beaucoup plus pour suggérer que les monarques *sont* à bien des égards des bébés. L'infantilisme - la mauvaise humeur infantile, l'égoisme infantile, la mégalomanie infantile - caractérise partout la vie de cour."

Pour Graeber, la grande différence entre un système dédié à un foyer royal et le fait d'élever des enfants est que l'on élève des enfants pour les voir grandir et non pour les enfermer dans un système traditionnel.
Les enfants sont forcément autoritaires à certains moments et il est naturel alors de les amener à prendre conscience du monde extérieur, de leur expliquer qu'ils ne sont pas seuls au monde.

> "Celq signifie peut-être que nous partageons tous une expérience originaire de (notre propre) autocratie, mais que nous faisons aussi l'expérience d'une forme d'amour destinée à nous permettre de la transcender, et à nous ouvrir à la capacité de faire autre chose."

## Chapitre 6: La politique culturelle des relations centre-périphérie, M. Sahlins

L'idée de base est qu'une société, pour être comprise, doit être étudiée avec son environnement, sa périphérie. Cela inclut à la fois les relations culturelles, économiques, martiales avec d'autres peuples mais aussi les relations avec les métapersonnes avec lesquelles les gens vivent ("esprit", nature, Dieux, ...).

> "l'autorité culturelle et politique des sociétés dominantes dans de nombreuses configurations classiques axées autour d'un noyau et d'une périphérie, notamment lorsque cette autorité s'étend et fonctionne comme *soft power* à l'égard des régions situées hors de portée du pouvoir coercitif central, repose sur une anthropologie indigène qui privilégie les sources métahumaines du bien-être humain."

Exemple classique ou le roi est considéré comme ayant accès à des ressources divines, par exemple la pluie en Afrique. En réalité un peuple choisit souvent un roi pour cela, ce n'est pas le roi qui assujettit forcément le peuple. La caste dominante est extractrice dans le sens ou les moyens de production reste à la population (ou à des esprits). Les rois ne cherchent pas à augmenter la production (contrairement aux capitalistes) mais à augmenter leur nombre de sujets.
Leur richesse est par la même occasion pour le peuple une preuve de leurs liens avec le divin.

### L'anthropologie des relations centre-périphérie

Sahlins nous liste différentes théories anthropologues du centre et de la périphérie. Il s'attarde sur la théorie de la *politique galactique* de Tambiah et Helms, qui s'appuie principalement sur l'étude de l'Asie du Sud-Est:

> "Tambiah observe que le système politique galactique était 'centré' plutôt aue 'centralisé', dans la mesure ou l'autorité du souverain, bien que s'étendant en principe indéfiniment à travers le monde dans toutes les directions, se limitait en pratique à la gouvernance de la capitale et des provinces environnantes, au-delà desquelles de trouvaient des principautés autonomes qui payaient un tribut ; et plus loin encore, une zone sauvage reliée au centre au mieux par des raids et des échanges commerciaux."

### Les dynamiques culturelles des régimes galactiques

Il faut noter qu'un certain nombre de ces centres galactiques avait une prétention hégémonique. Cela implique une forme de compétition culturelle avec d'autres centres. Il était alors parfois utile de récupérer des attributs de cultures transcendantes comme les cultures hindous, bouddhistes ou chinoises de l'époque. Ainsi des centres régionaux galactiques ont pu se transformer. On parle d'"auto-hindouisation" des khmères du Cambodge par exemple.
Cela invalide la fameuse "détermination par la base économique", car il y a donc des cas d'assimilation (de subordination?) culturelle sans invasion ni contrainte ni menaces, mais par calcul politique.
On peut noter qu'il y a là une certaine volonté d'invoquer un élément puissant extérieur à la société. Ainsi certains souverains (asiatiques) se sont revendiqués de la version coranique d'Alexandre le Grand, certains des colons européens (le sultan Hairun de Ternate s'habillait et parlait couramment portugais).

### Mimesis galactique: développement inégal des systèmes noyau-périphérie

> "Les chefs 'barbares' d'ascendance chinoise ; les villageois de Nouvelle-Guinée qui adoptent les clans, totems, ancêtres et rituels de leurs voisins dominants ; les royaumes cambodgiens ou javanais gouvernés en termes sanskrits par des rois hindouisés ; les souverains indonésiens devenus sultans islamiques qui adoptent un style de vie portugais"

> "Les souverains bouddhistes d'Asie du Sud-Est invoquent Ashoka. Les souverains islamiques d'Afrique de l'Ouest et leurs homologues paiens voisins font remonter leurs dynasties à la Mecque : les permiers généralement aux descendants du Prophète, les seconds à ses ennemis. Les rois gaulois et spartiates disent descendre d'Héraclès, bien qu'après les conquêtes romaines, certains chefs gaulois aient revendiqué des ancêtres juliens ou augustiniens."

> "Reste à ajouter que les ambitions des souverains galactiques, qui pousent ainsi à une ascension cosmique, sont complétées par une politique d'expansion extérieure à base de guerre, de commerce et de diplomatie, sans oublier les présents matériels stratégiques visant à englober les redoutables puissances animistes de la barbarie sauvage."

> "Les forces indomptées de la périphérie sont tranformées en puissance et en prospérité du centre. Comme dit Helms, le commerce et les tributs qui pèsent sur l'arrière-pays entrainent avec eux des animaux rares, étranges étrangers, pierres et minéraux précieux, bois rares, épices et drogues, cornes, queues, fourrures et plumes de bêtes et d'oiseaux exotiques."

Deux processus:

- l'acculturation antagoniste: un peuple 'inférieur' imite un peuple 'supérieur' avec lequel il est en conflit / opposition,
- la schismogenèse symétrique: une compétition dans entre groupes de mêmes status fait que certains groupes vont aller chercher des techniques à l'extérieur du système pour prendre l'avantage.

> "Poussées par la compétition interne, ces aspirations à acquérir les pouvoirs merveilleux des supérieurs galactiques ont été des moyens efficaces de mimesis culturelle dans les relations centre-périphérie."

Autre point important contre les théories qui cherchent à expliquer un système politique par des réalités sociales: l'influence venue de civilisations "plus avancées" peut créer chez certaines communautés des systèmes politiques qui ne sont pas en adéquation avec leurs réalités sociales.

Dernier point: les systèmes galactiques sont généralement peut stable. On retrouve un mouvement centrifuge, le centre cherche à s'étendre toujours plus et un mouvement centripète, car des groupes périphériques cherchent à absorber les cultures du centre et à s'en rapprocher.

## Notes sur la politique de la royauté divine ou Éléments pour une archéologie de la souveraineté, D. Graeber.

On commence par définir la souveraineté dans un sens large comme la possibilité de faire la loi, et donc la possibilité de d'extraire de l'ordre juridique, comme le veut l'origine du terme de souveraineté que l'on peut lier aux rois.

Dans le livre, on accepte quelques unes des affirmations de Hocart: le gouvernement provient du rituel, il faut donc redéfinir le terme classique de gouvernement, et un point d'entrée idéal pour cela est le concept de royauté-étrangère.

Classiquement dans l'anthropologie on fait une distinction nette entre rituel et politique. Graeber suggère que dans le cadre de la souveraineté, il faut justement pouvoir briser ce cadre.

### La souveraineté contenue dans le temps et l'espace

On trouve des traces d'inégalités il y a près de trente mille ans avec des personnes de morphologies atypiques (très grands, atteints de nanisme, squelettes déformés) enterrés dans de grandes tombes. Graeber suppose que ces personnes atypiques ne possédaient probablement pas de pouvoir de manière permanente, ils ne formaient pas une caste supérieure. Il se peut qu'ils n'aient eu de rôle que lors de certains rituels.
L'idée est que le gouvernement humain n'est pas né directement du gouvernement métahumain ou rituel et que la souveraineté (entendue comme capacité d'émettre et de faire respecter des commandements) n'a pu exister que dans des contextes rituels, avec des personnages représentants des métahumains.

> "L'idée que je défends ici est que, même si l'émergence de pouvoirs souverains a vraisemblablement suivi un chemin menant des divinités royales ou rois divins, ce chemin ne repreésente absolument pas une voie directe. Il est au contraire passé par une galerie de curiosités digne d'un cirque."

Chez les peuples indigènes de Californie, les chefs ne disposaient pas de pouvoir de commandement ou de punition.
Pendant le rituel d'incarnation des dieux, *Kuksu*, les anciens se déguisaient en esprit qui terrorisaient les gens. Mais les seuls qui avaient réellement le pouvoir d'adresser des ordres (sous peine d'amendes) étaient les clowns.

> "Ils se comportaient comme des goinfres, des satyres et des bouffons \[...\] se moquant des officiants, des musiciens et même des dieux."

A noter que pendant ces rituels, il était strictement interdit de rire aux farces des clowns.
Ils faisaient respecter les coutumes mais pouvaient aussi inventer des règles de manière totalement arbitraire. Ils étaient également les seuls à pouvoir enfreindre les règles de la cérémonie. Les clowns étaient généralement issus des catégories les moins favorisées, des mendiants et vagabonds.

Chez les Indiens pueblos, les clowns faisaient surtout peur aux enfants, et pouvaient les punir, agissant en croquemitaine.

Chez les Kwakiutl, en hiver,

> "les gens changeaient de noms, et la société était organisée autour des droits d'incarner certains rôles par la danse au cours de grands drames rituels: danseurs cannibales, oiseaux-tempêtes, ours grizzlis, baleines tueuses, phoques, fantômes et, ce qui est essentiel ici, bouffons."

> "Les bouffons menacent ou attaquent quiconque trébuche en dansant, rit ou tousse pendant une représentation, comment l'erreur d'appeler quelqu'un par son nom d'été, ou même prend son temps pour manger"

> "Leur méthode d'attaque consiste à lancer des pierres sur les gens, à les frapper avec des batons ou, dans les cas les plus sérieux, à les transpercer et les tuer avec des lances et des haches de guerre"

Ils étaient généralement des guerriers expérimentés. Ils portent des haillons, ont en permanence le nez qui coule et se projette de la morve les uns sur les autres. Ils jouent à l'homme fou. Ils détestent ce qui est propre, n'hésitent pas à souiller les gens, leur maison, détruire des choses.

> "Nous pouvons donc observer trois phases de progression logique :

1. Californie : les clowns comme incarnations du pouvoir divin, avec une logique morale extérieure et contraire à la société, exerçant des pouvoirs arbitraires de commandement et de punition, mais seulement au cours des rituels.
1. Côte nord-ouest : les bouffons comme délégués du pouvoir divin, avec une logique morale extérieure et contraire à la société, exerçant des pouvoirs arbitraires de police pendant les cérémonies qui s'étendent tout au long de la saison rituelle.
1. Plaines : les sociétés de police temporaire, non plus divines ni extérieures, mais exerçant des pouvoirs arbitraires de coercition qui leur sont délégués le temps de la saison rituelle, mais ne sont plus limités aux rituels eux-mêmes."

Lowie 1948: les indigènes américains ont créé des arrangements institutionnels afin que les États n'émergent pas. Clastres 1974 va dans le même sens quand il décrit les chefferies des sociétés sud-américaines. Les deux concluent sur le fait que si un État a pu émerger c'est forcément depuis la sphère religieuse. On trouve cependant très peu d'exemples d'État fondé par des prophètes.

### La royauté divine des Natchez

Seul véritable exemple de royauté divine au nord du Rio Grande.

Il s'agit d'un véritable régime despotique. Le roi est le frère du Soleil qu'il salue tous les matins. Il peut demander n'importe quoi, et la mort de n'import qui, sauf de sa famille noble. Étant donné que la femme / le mari et les serviteurs des nobles sont tués lors de leurs obsèques, les nobles ne pouvaient se marier qu'avec des roturiers.

Comme dans le cas du *reth* Shilluk, le pouvoir divin et le pouvoir politique du roi étaient bien différents. En conseil de guerre, l'avis du roi était souvent ignorer, et son pouvoir absolutiste n'était guère valable qu'en sa présence, c'est-à-dire au village royale. Il est donc logique que ce village n'abritait pas grand monde car les gens le fuyait.

On peut conclure que les clowns et les bouffons sont une manière de contenir la souveraineté dans le temps (saison des rituels) alors que les natchez contiennent la souveraineté dans l'espace.

### Sur la guerre constitutive entre le roi et le peuple

Comme décrit précédemment dans le livre, Graeber nous dit qu'il y a une guerre entre le gouvernement et le peuple. Dans le sens ou chacun essaye de conquérir l'autre.

Distinction entre royauté divine, le roi *agit* comme un dieu et royauté sacrée, le roi *est* dieu. Le *kabaka* ougandais par exemple ne faisait l'objet d'aucun culte et n'était appelé "dieu" que lorsqu'il commettait quelque chose de scandaleux.
Les rois essayent d'augmenter leur divinité, le peuple de les sacraliser, c'est la l'enjeu de la guerre.

#### Quand les rois perdent: la tyrannie de l'abstraction

Quelques restrictions dans les royautés sacrées: la santé du roi représente celle du royaume. Il est donc surprotégé, on lui interdit de traverser des étendues d'eau voire de toucher l'eau. Il est donc parfois complètement isolé. Il ne peut pas non plus mourir de mort naturelle.

> "Chez les Rukuba du Nigeria, la sainteté du roi est préservée lorsque, tous les quatorze ans, l'entièreté des impuretés qui l'affectent sont transmises à un vieil homme qui est expulsé de la communauté et survit pendnt un mendiant pendant sept ans, avant de mourir."

Description des rois jukuns. Ils sont entourés d'énormément de taboust, isolé, ne peuvent pas toucher le sol, mangent en privé. Ils ne peuvent pas être malade (sinon ils sont "discrèment étranglés"). Ils peuvent être tués pour plein de raisons différentes (mauvaises récoltes, transgression des tabous qui mettraient en péril la société, etc).

> "Chaque nouveau roi acquiert son pouvoir en consommant le coeur de son prédécesseur réduit en poudre, mélangé à de la bière, au moment de son couronnement ; la main droite de l'ancien roi est également conservée comme une amulette."

> "le roi \[...\] est lui-même les cultures : lors de ses apparitions publiques, il est régulièrement salué comme 'notre mais, nos haricots, nos arachides'."

Graeber oppose deux concepts : les rapports d'évitement et les rapports de plaisanterie. D'un côté on a la déférance auprès des supérieurs, mais aussi l'évitement en public d'un certain nombre de concepts liés à la honte (en gros pipi caca). de l'autre côté on a les rapports de familiarité joyeuse, les blagues, qui bien souvent sont un négatif de concepts évités. L'évitement renvoie au sacré (ce qui est séparé), aux tabous. Les blagues rigolent de ces concepts.

On peut placer les clowns et les rois dans ces différents registres.

Il apparait qu'historiquement, le côté sacré de la royauté ne venait qu'au fil du temps, pour limiter le pouvoir du roi. Dans certains cas, être roi n'était absolument pas enviable et revenait à être une amulette pour son propre peuple :

> "Il n'est donc pas étonnant que certains chefs bakongos fortunés de cette époque ne sortissent jamais de chez eux désarmés, de peur d'être kidnappés et forcés de devenir rois.

Il apparait qu'historiquement, le côté sacré de la royauté ne venait qu'au fil du temps, pour limiter le pouvoir du roi. Dans certains cas, être roi n'était absolument pas enviable et revenait à être une amulette pour son propre peuple :

> "Il n'est donc pas étonnant que certains chefs bakongos fortunés de cette époque ne sortissent jamais de chez eux désarmés, de peur d'être kidnappés et forcés de devenir rois.

Il apparait qu'historiquement, le côté sacré de la royauté ne venait qu'au fil du temps, pour limiter le pouvoir du roi. Dans certains cas, être roi n'était absolument pas enviable et revenait à être une amulette pour son propre peuple :

> "Il n'est donc pas étonnant que certains chefs bakongos fortunés de cette époque ne sortissent jamais de chez eux désarmés, de peur d'être kidnappés et forcés de devenir rois.

Il apparait qu'historiquement, le côté sacré de la royauté ne venait qu'au fil du temps, pour limiter le pouvoir du roi. Dans certains cas, être roi n'était absolument pas enviable et revenait à être une amulette pour son propre peuple :

> "Il n'est donc pas étonnant que certains chefs bakongos fortunés de cette époque ne sortissent jamais de chez eux désarmés, de peur d'être kidnappés et forcés de devenir rois."

Un certain nombre de théorie sont en construction encore aujourd'hui concernant la sacralité des rois. Pour certains, le royauté devient plus sacrée lorsque le royaume s'écroule sous la pression d'étrangers, de commerçants, ... Pour d'autres c'est l'inverse, la sacralité n'arrive qu'avec l'explosion du pouvoir royale.
Je ne suis pas sur qu'une règle générale puisse être trouvée, cela ressemble à une grande partie d'échecs dans laquelle chaque partie en présence essaye de jouer ses meilleurs coups.

#### Quand les rois gagnent : la guerre contre les morts

Même si les rois gagnent et maitrisent l'espace, reste à maitrise le temps.

> "Tous cherchaient l'immortalité (1) en transformant le paysage par des ouvrages d'architecture ou d'ingénierie monumentale, (2) en faisant en sorte que leurs exploits et leurs réalisations soient préservées dans des légendes et des romans et (3) en essayant de fonder une dynastie durable et florissante"

Le problème c'est que le poids des ancêtres est lourd à porter et que les deux premiers points semblent entrer en contradiction avec le dernier.

Le cas des Incas est assez extrême. Le roi mort était finalement toujours considéré comme vivant et disposait toujours de leur cours et de leurs privilèges. En réalité la cour de l'empereur défunt continuait d'entretenir ses propriétés et d'agir comme s'il était en vie. Le nouveau roi devait donc partir faire des conquêtes pour gagner sa croute.

Cela signifie que les enfants cadets et autres personnes n'ayant par régné se retrouvent en charge de leur défunt parent et occupent finalement une place plus élevée dans la hiérarchie que l'empereur règnant. Cela est très différent de pas mal de sociétés dans lesquelles les lignées non règnantes perdent progressivement leur pouvoir.

> "J'ai suggéré que lorsque les rois gagnent définitivement ce que j'ai appelé la guerre consitutive entre le souverain et le peuple, en parvenant à étendre leur pouvoir souverain sur le rpyaume dans son entier, ils ont tendance à utiliser l'idée de leur status divin (précisément l'arme utilisée contre eux lorsque ce sont les forces populaires qui l'emportent) comme inspiration dans leur tentative de transcender effectivement la mortalité. Ils se considèrent comme des légendes, transforment le paysage, créent des dynasties. Néanmoins, s'ils y arrivent, tout cela causera nécessairement des ennuis à leurs successeurs, particulièrement si ceux-ci désirent les imiter."

Si les ancêtres sont fondamentalement différent des vivants, alors ils peuvent être considérés comme sources de pouvoir (région pratiquement généralement le totémisme et connaissant rarement les royaumes). Sinon, ce sont des rivaux (régions contenant généralement des royaumes).

Alors comment lutter contre ses ancêtres ? Il peut y avoir plusieurs moyens.

Le plus simple est l'amnésie générale. Chez les Nuer on oubliait on oubliait les ancêtres après cinq générations. On peut aussi s'en prendre directement aux reliques pour essayer de les détruire. Ou bien essayer d'exiler ou de marginaliser les ancêtres.

Une autre voie consiste à s'identifier aux morts. C'est-à-dire, au sens littéral ou figuré, prétendre être (ou être l'incarnation) d'un ancêtre célèbre (comme Nyikang chez les shilluks). On peut aussi penser à la ligue iroquoise possédant un stock fixe de noms réutilisés sans cesse.

Une façon peut aisée de rivaliser avec les morts est de les surpasser. Qui se souvient des ancêtres d'Alexandre le Grand.

Une autre façon encore est de changer le sens de l'histoire. C'est la fameuse question de savoir si *l'état de nature* de l'humanité était un havre de paix ou une scène de violence. Si l'évolution de l'humanité est présentée comme un progrès avec des inventions et une amélioration de la vie, les nouveaux rois peuvent plus facilement se placer au-dessus des anciens (rois créatifs, réformateurs). Dans le cas inverse ou le monde tombe en déchéance depuis les temps de l'Éden, c'est plus difficile.

#### Conclusions

Graeber commence par dire ACAB.

Ensuite, la souveraineté n'est pas une évidence dans l'histoire. Si les chasseurs-cueilleurs ont des rois, ils se gardent bien de laisser le pouvoir divin tomber entre les mains de mortels. De plus, là ou l'on peut effectivement observer la souveraineté, des actes de commandement arbitraire, c'est souvent lors de périodes bien précises (rituelles) pendant lesquelles on précise bien que l'on se situe or de la morale. Et si jamais la souveraineté n'est plus contenue dans le temps, elle l'est dans l'espace :

> "Quel que soit le contexte l'ayant précédé, le cas des Natchez illustre des caractéristiques structurelles que l'on peut retrouver, à mon sens, dans toute royauté divine. Il pourrait être utile de les énumérer:
>
> - les origines du souverain sont extérieures à la société (roi-étranger) ;
> - le souverain reste extérieur à la société en un sens fondamental, dans la mesure ou il n'est soums ni à la morale ni à la foi ordinaire ;
> - tous mettent l'accent sur le pouvoir absolu du souverain sur la vie, la mort et la propriété de ses sujets lorsqu'il est physiquement présent ;
> - l'aire de présence physique du souverain est soigneusement limitée ;
> - le souverain et le peuple évoluent dans un antogonisme constitutif fondamental (une guerre), qui est paradoxalement considéré comme la clé de l'immortalité du souverain, et comme une caractéristique transcendante, métahumaine. D'ou il suit que :
>   a. dans la vie, cela se manifeste par une sacralisation négative : l'imposition et l'élaboration de tabous qui nient les caractéristiques mortelles du roi, le coupent de tout contact régulier avec ses sujets et, souvent, l'emprisonnent dans un espace physique scrupuleusement délimité (village, enceinte, palais) ;
>   b. ces actes de sacralisation négative font également du village, de l'enceinte ou du palais une sorte de paradis miniature ou les dilemmes basiques de l'existence mortelle peuvent être considérés comme au moins temporairement ou provisoirement résolus ;
>   c. le régicide rituel (attesté principalement en Afrique) n'est que la forme la plus extrême de ce type de sacralisation négative."

Graeber dit Berlusconi et Trump c'est des bouffons.

> "La seule façon de s'assurer que ces bouffons n'obtiennent jamais un pouvoir coercitif systématique sur leurs semblables est de se débarasser de l'appareil coercitif lui-même."
