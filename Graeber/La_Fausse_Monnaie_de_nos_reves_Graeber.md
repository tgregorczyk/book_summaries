# La fausse monnaie de nos rêves, vers une théorie anthropologique de la valeur, D. Graeber

## Chapitre 1 : Les trois acceptations du term "valeur"

1. les "valeurs" au sens sociologique : les conceptions du bien, du juste et du souhaitable pour les êtres humains
2. la "valeur" au sens économique : la mesure de la désirabilité des objets, notamment calculée en fonction de ce à quoi autrui est prêt à renoncer pour les obtenir
3. la "valeur" au sens linguistique qui remonte à la linguistique structurale de Ferdinand de Saussure (1966) et que l'on pourrait qualifier plus simplement de "différence significative

### Le projet sur la valeur de Clyde Kluckhohn

Le but est de redéfinir l'anthropologie comme étude comparative des valeurs.
Hypothèse générale : les valeurs sont des "conceptions du souhaitable". Ce n'est pas juste ce que les personnes veulent mais devraient vouloir, ce que l'on est en droit d'attendre du monde. En gros les sociétés se distingueraient par ce que l'on est en droit d'exiger du monde, à juste titre. Malgré le projet de comparer certaines sociétés américaines (navajo, texane, mormone, ...) le projet finit dans une impasse.

### L'individu maximisateur

Théorie classique de l'économie comme quoi un individu est rationel et sait ce qu'il veut obtenir et va chercher à l'obtenir en produisant le moins d'efforts possibles. C'est l'approche min/max. Un exemple qui montre bien à quel point c'est absure : les Trobriandais dépensaient énormément d'énergie dans leur jardin, de manière assez inutile. La plupart de la récolte allait à la belle-famille mais les jardins étaient généralement si fournis que la moitié des fruits pourrissaient.
Graeber détruit une fois de plus les économistes en les traitant de cynique, pour notre plus grand plaisir. Ils considèrent en effet que les hommes font tout par égoisme d'une certaine manière. Cela implique aussi de réifier un certain nombres de concept, comme le prestige ou l'honneur. Plus généralement les économistes cherchent à raisonner sans notion du culture ou de société, mais seulement avec des individus rationels et des choses. On ne va évidemment pas très loin comme ça.

Karl Polanyi, *La Grande Transformation* : dans ce livre l'auteur hongrois discrédite l'idée reçue d'alors que les marchés sont un phénomème naturel et qui ont donc pu éclore après la chute du féodalisme (les marchés ne peuvent se construire que dans un système qui promeut la liberté et non les contraites puisque c'est un phénomène naturel). Polanyi nous raconte qu'en réalité le marché "doit sa création à l'État et à ses pouvoirs coercitifs."
Polanyi décrit également deux types de théories économiques. Les "formalistes" essaient de comprendre les lois naturelles du marché, ce sont Smith, Malthus et Ricardo. Les "substantivistes", eux, dont un théoricien éminent est Dalton, élève de Polanyi, veulent regarder comment les biens sont distribuées dans une société pour en déduire des règles. Cela apporta beaucoup d'analyses importantes mais on ne peut pas tout expliquer seulement à partir de la répartition de biens ...

Alors que les économistes considèrent que ce sont les actions individuelles qui forment la société, les "fonctionnalistes" (de l'école de Durkheim) "envisageaient la société comme une force active à part entière - voire comme un agent conscient et intentionnel dont l'unique objectif semblait plutôt tenir de l'instinct de conservation animal."

> "Un durkheimien considère volontiers les institutions économiques comme un mécanisme d'intégration sociale - l'un des moyens par lesquels la société crée un réseau de liens moraux au sein de ce qui autrement ne serait qu'une masse chaotique d'individus - ou, à défaut, comme un mécanisme d'allocation des ressources de la 'société'."

### Le structuralisme et la valeur linguistique

Ferdinand de Saussure, fondateur de la linguistique moderne, structurale.
La valeur d'un mot est essentiellement négative, par exemple le mot "rouge" aura une signification différente s'il existe aussi un mot pour "orange" ou si le mot désignant la couleur la plus "proche" est le jaune auquel cas le orange sera peut-être compris dans le rouge.

> "la signification d'un terme renvoie à sa place dans un système total."

Cela donna naissance au structuralisme, l'idée est qu'on ne peut pas étudier un élément isolé. Quelque chose fait toujours partie d'une totalité plus grande qui sous-tend le tout. On va donc chercher les sens cachés, le système symbolique global que n'est pas forcément apparemment de prime abord. Cette méthode eut de grands succès avec notamment Lévi-Strauss, mais possède un problème principal, celui de l'évaluation. Il parait compliquer de donner la valeur d'un objet ou même sa valeur relative.
Pour Dumont, cette approche structuraliste classique pose un problème, elle considère des idées et non des valeurs. Par exemple on va s'intéressé aux couples masculins / féminins, droite / gauche, cru / cuit, pur / impur etc mais la réalité est que dans chacun de ces couples, un des termes est supérieur à l'autre (c'est "l'englobement du contraire").
Un soucis de l'approche dumontienne est la séparation entre les sociétés "modernes", qui placent une grande importance sur l'individu, et qui semble résister à l'analyze et les sociétés "holistiques", "hiérarchisées par principe, formant une série de domaines toujours plus englobants". Et ce sont ces dernières que Dumont et ses fidèles étudient et montrent comment les idées, les totalités sont hiérarchisées et imbriquées les unes dans les autres. On arrive alors à dégager les valeurs suprêmes d'une société. Exemple de *Des relations et des morts* 1984.
Une autre faiblesse de cette approche est que les sociétés sont vues comme des entités isolées.

## Chapitre 2 : Orientations actuelles de la théorie de l'échange

#### Le moment marxiste et ses lendemains

Années 1960 : débat entre formalistes et substantivistes ; Années 1970 : débat entre structuralistes et marxistes.

L'anthropologie marxiste s'intéresse avant tout aux modes de production et aux activités créatrices dans une société. Mais cette théorie cible principalement les sociétés avec État, là ou l'on peut trouver des classes sociales, dont une qui extrait la plus-value du travail de l'autre, du marxisme quoi. 

La champ académique se gauchise jusqu'à la fin des années 1970 avec le marxisme et le féminisme. On peut noter que ces mouvements ont une base assez européocentrée. Alors que jusque là on se contentait d'essayer de comprendre les sociétés sans émettre de jugement sur celles-ci, le marxisme et le féminisme ont étudient de manière bien particulière des sociétés *en ayant à l'esprit* des oppressions occidentales. 

> "Lorsqu'un·e marxiste critiquait l'ordre social de sociétés non occidentales, ce n'était guère parce que celui-ci était différent de l'organisation chez lui ou chez elle, mais bien plutôt parce qu'il lui était *semblable*."

### Le retour de l'*homo oeconomicus*

> "il est assez difficile de dégager un thème unique autour duquel s'articulerait les travaux des différents auteurs [poststructuralistes] habituellement mentionnés sou scette rubrique (Foucault, Derrida, Bourdieu, Deleuze et Guattari, Lyotard, etc.). Pourtant s'il fallait en trouver un, ce serait à coup sûr l'exhortation à briser les totalités quelles qu'elles soient, qu'il s'agisse de la 'société', de l''ordre symbolique', du 'langage'"

Graeber prend l'exemple de Bourdieu et de sa vision de l'économie de dons. Pour Mauss, les dons permettent de créer des relations sociales, hypothèses très fonctionnaliste. Lévi-Strauss aura le même genre d'arguments à propos du mariage qu'il voit comme un moyen d'échanger des femmes dans une société et de créer ainsi des alliances.
Pour Bourdieu, le don n'est qu'une version du troc étalée dans le temps. Pour lui, ce genre de sociétés cache sa réalité économique de pleins de façons comme celle-ci. Comme si l'économie telle que nous la connaissons, avait toujours lieu. Bourdieu exprime donc ici une vision assez formaliste.

> "Les pratiques ne cessent pas d'obéir au calcul économique lors même qu'elles donnent toutes les apparences du désintéressement parce qu'elles échappent à la logique du calcul intéressé (au sens restreint) et qu'elles s'orientent vers des enjeux non matériels et difficilement quantifiables." Bourdieu

Par exemple si donner est une coutume, alors donner est une bonne stratégie pour accroitre son capital symbolique. On retombe dans une certaine vision économique.

Pour Graeber, le problème du poststructuralisme est qu'il cherche à tout voir sous le prisme des relations de pouvoir et de domination. Et forcément, quand on cherche, on trouve. Il en résulte une théorie qui semble oublier les "bons côtés", la bienveillance, la générosité.

#### Les "politiques de la valeur" d'Appadurai

*Les marchandises et les politiques de la valeur*, 2009.

Appadurai propose de laisser de côté la vision marxiste de la valeur (la valeur provient du travail humain) et se tourne vers Simmel. Pour ce dernier, la valeur "est créée par l'échange ; elle est donc simplement l'effet du désir individuel" pour acquérir quelque chose. Cette vision est plus facilement transposable aux sociétés non capitalistes.
Si la valeur dépend de l'échange, Appadurai propose de considérer la trajectoire de vie d'un objet pour comprendre sa valeur.
Mais Appadurai finit par aller encore plus loin que Bourdieu :

> "alors que l'échange de marchandises vise à établir des équivalences entre la valeur des objets, les 'dons' concernent principalement les relations entre personnes [...] Appadurai [...] finit par considérer dans son article que tous les échanges ne concernent que des choses et qu'ils ne jouent aucun rôle dans la création, le maintien ou la rupture de relations sociales."

#### Parenthèse : Annette Weiner sur les biens inaliénables

L'idée est ici que l'histoire d'un objet est très importante pour sa valeur et cela finit en reflet de la notion de Simmel. Plutôt que valeur comme désir d'aquérir un objet, Weiner parle de peur de perdre un objet et donc de sa substitution au marché.

### L'approche néo-maussienne de Strathern

Mauss : pourquoi rend-on un don ? Réponse : parce qu'il contient quelque chose du·de la donateur·rice.
Gregory 1982 : "il s'agit d'une propension générale. Les économies de don tendent à personnifier les objets. Les économies marchandes comme la notre, ont au contraire tendance à chosifier les humains, ou du moins certaines de leurs caractéristiques."

Strathern, comparaison des sociétés de Papouasie-Nouvelle-Guinée et les sociétés occidentales.

#### Critique marxienne, réplique maussienne

Josephides 1985 sur les Melpa. Au cours des fêtes un moment important est l'échange de porcs. L'échange a lieu entre hommes et laisse dans l'ombre les femmes qui pourtant élèvent et nourrissent les porcs. Pour Josephides, cela est censé montrer que les femmes Melpa sont opprimées. 
Pour Strathern il en va différemment. La vision marxienne de Josephides considérent que le travailleur a un droit sur sa production, qu'il en détermine la valeur. Pour Strathern :

> "La 'valeur' est donc la signification our l'importance que la société attribue à un objet."

Les Melpa "ne considèrent pas les objets comme produits par des individus mais comme résultat de leurs relations."

Si la version occidentale considère que chaque personne a une personnalité, un "noyau", les Mélanésiens voient la chose différemment. Pour eux, nous sommes d'abord ce qu'autrui voit de nous. Une personne a donc un certain nombre de potentialités cachées (ou de personnalités) qui ne se révèlent que lorsqu'une personne tierce les met en lumière. Et ces identités sont fonctions de relations sociales. On peut être le mari de, le propriétaire de ... 
Il en a de même pour les propriétés. Leurs valeurs se construisent dans le regard d'autrui :

> "en contrepartie, cet autre définit l'objet comme le produit de relations sociales passées, mais aussi comme quelaue chose qui peut 'être détaché'."

On retrouve l'idée que la chose possède quelque chose du propriétaire comme le pensait Mauss mais pas seulement. Dans le cas des porcs Melpa, les porcs ont une valeur qui vient de leur "origine", "les relations sociales qui l'ont fait naitre". 
Le fait de "détacher" signifie que l'objet, l'animal "incarnera désormais une nouvelle relation sociale", entre le nouveau propriétaire et moi.

> "Lorsque je parviens à convaincre le propriétaire de me donner son porc, je déplace la valeur d'une relation à l'autre. Et l'objet en vient à incarner ma capacité à opérer ce déplacement, mon pouvoir de créer de nouvelles relations."

### Vers une synthèse ?

Chez les Mae-Enga, il existe six classes d'objets. La plus élevée comprend les porcs et les casoars vivants. Cela signifie que ces animaux ne peuvent être échangés qu'entre eux. La classe suivante comprend les pendentifs en nacre, les coiffes de plumes et les haches en pierre. Et ainsi de suite jusqu'à la classe la plus basse des denrées alimentaires de base.
Il est donc difficile de parler de valeurs car de nombreux objets ne peuvent tout simplement pas être comparés.

### Munn: la valeur des actions

Nancy Munn a étudié principalement l'ile de Gawa, au sud-est de la Nouvelle-Guinée, qui fait partie du cercle *kula*:

> "Ce circuit se définit par l'échange de brassards et de colliers de coquillage de très grande valeur."

Les brassards ne peuvent s'échanger que contre des colliers et vice versa. Les brassards voyagent ainsi dans le sens des aiguilles d'une montre, les colliers dans l'autre sens.

Il semble que dans cette ile la valeur dépende de la capacité d'un objet à avoir une histoire, à être unique. Pour Munn, posséder un tel objet permet d'étendre son "domaine spatio-temporel intersubjectif". Par exemple, donner à manger permet de créer des alliances, attacher son nom à un coquillage *kula* très prisé permet de pouvoir participer aux échanges les plus importants. Ce qui semble importer, ce sont ces actions humaines qui ont un impact. Quand Strathern parle de relations sociales, Munn part de l'activité:

> "La valeur émerge dans l'action; elle est le processus par lequel la 'puissance' invisible d'une personne - sa capacité d'agir - est transformée en formes concrètes et perceptibles."

Ainsi lorsqu'on donne à manger, ce n'est pas la nourriture qui compte mais l'acte de donner à manger. Les formes hautes de valeur se concrétise dans le prestige, des personnes éloignées dans le temps et l'espace connaissent le nom d'une personne.

Ainsi on peut voir que le processus de création d'un objet n'est pas si différent du processus de création de relations sociales. Graeber suggère que cela laisse penser à une théorie marxienne de la valeur si l'on étend la notion de travail.

### Conclusions (pourquoi si peu d'actions ?)

Pour Graeber, depuis les années 1960, la plupart des théories sont une variante d'économisme ou de structuralisme saussurien. Les théories économiques ont tendance à réifier les relations sociales (l'honneur devient un objet que les acteurs cherchent à acquérir). Le structuralisme issu de la linguistique cherche à étudier une langue ou les codes d'une société de manière atemporelle.
Ces deux approches sont donc très différentes de l'approche de Munn car elles sont **statiques**.

> "Prendre appui au contraire sur des puissances d'agir cachées et génératives renverse la problématique. La valeur devient, comme je l'ai dit, la manière dont les gens se représentent la portée de leurs actions - généralement sous une forme sociale ou une autre clairement identifiée. Mais les formes en elles-mêmes ne sont pas les sources de la valeur."

L'analyse de Strathern parle de rendre visible la valeur d'un objet dans les yeux d'une personne, valeur qui existe donc toujours :

> "Ainsi, là ou Strathern met l'accent sur la visibilité, le vocabulaire de Munn parle de 'puissances', de 'potentialités transformatrices', de capacités humaines finalement génériques et invisibles."


## Chapitre 3 : la valeur comme portée des actions

L'analyse précédente est à rapprocher du débat entre Héraclite et Parménide. Pour le premier, les objets sont en réalité des processus, ils évoluent en permanence. Pour Parménide, il faut considérer les objets comme hors du temps pour bien les comprendre. Cela a donné la géométrie grecque et a fortement influencé la pensée et la science occidentale de ces derniers millénaires.
Certains comme Roy Bashkar essayent de promouvoir aujourd'hui la perspective héraclitéenne. C'est assez complexe voir p93-94 mais pour Graeber il s'agit de la seule option possible pour poser une ontologie solide en base de la science. C'est une expérience d'humilité aussi qui nous suggère d'abandonner l'idée de tout connaitre parfaitement.

> "La réalité est ce que l'on ne peut jamais connaitre parfaitement."

À propos du matérialisme, Graeber pose qu'il faut aller plus loin que la séparation classique entre une réalité matérielle (production de biens) et des sphères plus abstraites telles que la littérature. Pour Graeber, la littérature est également matérielle (papier, crayon, et autres contraintes) et fait référence à des actions bien concrètes, écrire, penser, imaginer.

### La théorie de la valeur de Marx

Pour Ricardo, la valeur s'exprime en "heures-hommes", c'est assez simple. Marx, lui, préfère penser à la valeur relative d'un objet dans un société. Il se fiche du prix, qui peut fluctuer, sa définition de la valeur est donc *la proportion d'énergie créatrice dépensée pour produire et entretenir un objet*,

> "si les Américains dépensent 7% de leurs énergies créatrices au cours d'une année donnée pour fabriquer des automobiles, voilà qui indique vraiment l'intérêt des voitures à leurs yeux."

La largesse de la définition d'énergie créatrice permet de tenir compte de sujets aussi divers que la politique ou les valeurs familiales. Mais cette définition de la valeur est-elle applicable à la majorité des sociétés humaines, souvent non-capitalistes ?

### L'approche "praxéologique"

L'approche d'Hegel est de retracer l'histoire de manière dynamique pour comprendre comment l'humanité s'est developpée en interagissant avec son environnement. Le but ultime étant "l'humanité devenue pleinement consciente d'elle-même à travers ses propres actions; c'est la véritable conscience de soi [...] ouvrant la voie à la possibilité de la liberté humaine."
Mais Marx trouve les théories hégéliennes trop abstraites, fondées sur "l'Esprit", "la Nation", etc et envisage une approche plus matérialiste. Il ajoute un ingrédient supplémentaire, l'humain n'est pas passif dans cette histoire, 

> "les "humains" font leur propre histoire, mais pas dans des conditions choisies par eux"

Pour Marx, ce qui différencie les humains des animaux est qu'ils produisent leurs moyens de subsistance, en en étant conscients.

> "Une araignée fait des opérations qui ressemblent à celles du tisserand, et l'abeille confond par la structure de ses cellules de cire l'habileté de plus d'un architecte. Mais ce qui distingue dès l'abord le plus mauvais architecte de l'abeille la plus experte, c'est qu'il a construit la cellule dans sa tête avant de la construire dans la ruche." Marx, *Le Capital* I

> "L'intelligence humaine est donc intrinsèquement critique [...] il signifie que la révolution est possible."

Ainsi va alors le processus de production:

1. L'être humain produit pour subvenir à ses besoins, et
2. étant sociaux, les humains s'organisent et créent ainsi des relations sociales,
3. ce faisant, on produit le producteur en tant qu'individu.
4. À la suite de quoi de nouveaux besoins sont créés et le cycle recommence.

Notons que si les humains sont conscients de la première étape, ils ne le sont pas (du moins en totalité) des étapes suivantes.

### Structures dynamiques

Structuralisme héraclitéen de Jean Piaget : c'est une théorie axé sur le développement des enfants. Il considère que les enfants se développent par l'action. On comprend le concept de nombre en en faisant l'expérience, en comptant. La structure ne précède pas l'action. 
Pour Piaget, le théroème de Godel qui montre qu'un ensemble logique ne peut pas démontrer sa propre cohérence interne est important. Pour comprendre le monde on construit ainsi des théories qui s'englobent de manière de plus en plus générale / abstraite : c'est ça le développement d'un individu.

### Égocentrisme et conscience partielle

> "L'égocentrisme, selon Piaget, consiste à supposer que sa propre conception subjective du monde est identique à la nature du monde."

La maturité revient donc à la capacité de pouvoir se décentrer, adopter un autre point de vue.

Prenons le système marchand actuel, il implique une fétichisation des objets. Les foyers sont tellement loin des usines qu'on ne voit en un objet que ses "qualités propres" qui n'existent pas en réalité. L'objet a été créé pour répondre à un besoin, c'est une intention humaine qui est derrière, mais que l'on oublie pour l'attribuer directement à l'objet. Cela ressemble fort à de l'égocentrisme.

### *Das Kapital* comme analyse symbolique

L'argent permet bien sur de faciliter les échanges sur un marché, c'est un instrument qui permet de comparer des prix. Marx fait remarquer que dans un système de marché capitaliste, l'argent est bien plus, il est la finalité du travail salarié. Ainsi pour les travailleurs, la valeur de leur puissance créatrice, qu'ils vendent, voit sa valeur se refléter dans le salaire.

> "La monnaie est un signe de valeur concret. La valeur est la manière dont les acions d'un acteur individuel prennent un sens, pour l'acteur lui-même, en étant incorporés dans un ensemble sociable plus large."

### Sociétés sans marché

Si l'on considère des sociétés sans État, on se rend compte que les individus ne passent absolument pas la majeur partie de leur temps à effectuer des activités "économiques", de producion des moyens de subsistance, mais participent à des processus de socialisation. C'est pourquoi Eric Wolf, anthropologue marxiste parla en 1983 de "mode de production de la parenté".

> "De quelles manières les activités consistant à modeler les personnes s'incarnent-elles dans des formes-valeurs, c'est-à-dire des formes qui reflètent tangiblement le sens qu'ont pour moi mes actions sous la forme d'un objet que je convoite ou d'une action à laquelle j'aspire ? Dans quelle mesure ce processus favorise-t-il le fétichisme - qui désigne le fait que les personnes ne se rendent même plus compte de la valeur qu'elles produisent en partie - et l'exploitation - un moyen pour certains de s'approprier la plus-value produite par d'autres ?"

### Les Baining : prooduction et réalisation

Baining : Papouasie-Nouvelle-Guinée. C'est une société très égalitaire sans structure formelle quelle qu'elle soit. Le travail maraicher est important (transformation de la nature en culture par le travail humain) mais l'acte le plus prestigieux chez les Baining est "de donner de la nourriture". Cela est lié à l'éducation des enfants et donc à la reproduction de la société.

> "la production de nourriture grace au travail de maraichage est A l'origine de toute valeur, mais cette valeur n'est 'réalisée' que lorsqu'on offre une part de cette nourriture à quelu'un d'autre. Dès lors, l'acte le plus prestigieux est de bien nourrir les enfants et, de cette façon, de les transformer en êtres sociaux ; pour cela, il faut que la société existe. Au fond, sans la société, la socialisaton des enfants ne serait pas source de prestige ; de la même façon, sans a socialisation continue des enfants en qualité de nouveaux producteurs, la société elle-même ne se perpétuerait pas."

### Les Kayapo : le cycle domestique et la structure du village

Brésil. Vie sociale très complexe composée notamment de deux phratries. Chaque enfant a besoin d'une famille d'adoption dans la phratrie opposée qui met en place l'initiation. Dans les familles, deux types de relation sont importantes. D'abord la domination des parents sur les enfants, surtout des beaux-parents sur les gendres. Ensuite, les alliances entre grands-parents et petits enfants. 
Les hommes s'élèvent progressivement dans les instances du villages en fonction de leur niveau dans le cycle domestique (avoir un ou plusieurs enfants etc).
Les deux axes de la famille sont reflétés au niveau de la communauté par deux instances qui recrutent dans chacun de ces deux axes. De plus, on y associe deux valeurs cardinales de la société : la beauté ("perfection, complétude, finesse") et la domination. Ces valeurs se voient unifiées dans une série de chants de trois types différents dont le plus important est une scansion oratoire réservée aux chefs.
On observe de façon similaire deux processus de production au niveau domestique, d'abord celle qui va transformer les enfants en adultes bons à marier. Ensuite, celles qui transforment le couple subordonné en chefs dominants. Cette dernière transformation est réalisée non par le couple lui-même mais par la famille.

### Signes de valeur

Il est nature pour les Kayapo de chercher l'ascension sociale ie l'accès à des formes de plus en plus élevées de valeurs. Ces activités, ces actions porteuses de valeur ne peuvent se réaliser que grace à un public. Autrement dit, les chants sont les actions dotées de valeurs mais elles ne valent pas grand chose sans un public. Ce public, c'est la société, c'est "le processus global par lequel toutes ces activités sont coordonnées, et la valeur est, à son tour, la manière dont les acteurs considèrent leur propre activité comme significative dans le cadre de ce processus." 
Et contrairement à ce que pense Strathern, ce n'est pas le regard du public qui donne la valeur à l'action, la valeur est déjà là, elle est juste mise en lumière.

### Valeur et valeurs, fétichisme

L'idée est de maintenant faire un lien entre la valeur vue comme chose économique et les valeurs vues commes choses morales. Il semble déjà que les valeurs sont plus présentes dans les situations loin du marché (société sans marché, mais aussi domaine domestique, religieux, ...). Pour Turner ces deux catégories sont les deux côtés d'une même pièce.
On peut noter qu'avec la monnaie, on peut stocker l'argent facilement. Ainsi la sphère de circulation et de réalisation (acquérir, profiter d'un objet) peuvent être distinct ce qui n'est pas le cas pour les Kayapo par exemple, car un chant ne se stocke pas. Ces deux sphères coincident donc.

Graeber étend l'analyse de Marx (sphères de production, circulation, réalisation) à une société moderne. Il y voit deux "ensembles d'unités minimales", l'usine et la famille. La première produit des biens, la seconde, des êres humains. Il décrit un double processus de fétichisation qui découle de l'invisibilisation d'une unité pour l'autre. Les biens sont coupés de leur production et acquiert des qualités subjectives ; et dans l'industrie, les humains sont réifiés et vus comme des ressources.

Pour Jane Fajans, il faut séparer l'échange de la circulation. L'échange se réfère ici aux biens physiques et la circulation aux valeurs. 

### Note 1 : la valeur négative

Dans pas mal d'endroits, au lieu de louer une valeur positive de la société, on va faire la chasse à sa version opposée. Par exemple, on peut faire la chasse à la sorcellerie vue comme essayant de subvertir l'égalitarisme d'une société.

### Note 2 : appropriation directe versus indirecte

L'approche marxiste "modes de production" catégorise les sociétés suivant le mode d'extraction de la plus-value, qui tient en dernier ressort sur l'usage de la force. Il s'agit donc d'une théorie de l'État. Pour les sociétés sans États, Graeber propose de parler d'extraction d'une plus-value de manière indirecte, sous forme de valeur (plutôt qu'une plus-value matérielle). Ce processus d'extraction est en un sens bien plus efficace car il nécessite pas la force pour tenir bon. Pour Graeber cela vient du fait qu'il se base sur des inégalités plus fondamentales, celles au sein d'un foyer, des inégalités de genre.

> "dans la mesure ou les strctures étatiques réussissent à légitimer leur assise, elles le font quasi systématiquement en faisant appel avec succès aux valeurs de la sphère domestique, lesqueles s'enracinent évidemment dans des formes d'inégalité beaucoup plus fondamentales et dans des formes de distorsion idéologiques bien plus efficaces - de toute évidence, celles de genre."

### Conclusions : un millier de totalités

 On remarque que si les valeurs se réalisent face à un public, une *totalité*, les totalités sont parfois très différentes dans la pratique et dans la tête des protagonistes. Par exemple les Kayapo continuent de se définir comme deux phratries alors que cette organisation est révolue depuis 1936.

 Pour Turner, la politique consiste à définir les valeurs, ou en tout cas, ce qui a le plus de valeur dans uns société. Et cela passe donc aussi par la définition des totalités correspondantes. Et la réalité actuelle est celle de la logique du marché :

 > "Toute notion de liberté, qu'il s'agisse de sa version la plus individualiste, qui la réduit à une forme de consommation créative, ou de celles que j'ai tenté d'évoquer dans ces pages - une sorte de faculté culturelle à la créativité et au décentrement (Turner 1996) -, requiert *à la fois* de résister à l'imposition de conceptions totalisantes de ce que doit être la société ou la valeur et d'accepter que des mécanismes de régulation *d'un genre ou d'un autre* soient pourtant nécessaires. En conséquence, elle nous invite à mener à bien une réflexion sérieuse sur ces mécanismes qui garantiront au mieux que toutes les personnes soient libres de concevoir la valeur sous la forme qu'elles souhaitent. Sans quoi, nous finirons par reproduire la logique du marché sans le savoir - du moins pour l'instant. Et, dans nos tentatives pour réfléchir sérieusement à des alternatives à la version de la "liberté" qui nous est actuellement proposée - une version dans laquelle les États-nations servent principalement à protéger la propriété privée des entreprises, ou des institutions internationales non élues régulent un "marché libre" sinon débridé dans le but essentiel de préserver les intérêts des opérateurs financiers, et ou la liberté personnelle se limite aux choix de consommation individuels -, nous ferions mieux de nous oter de l'idée que ces question se vont se régler d'elles-mêmes et de nous demander à quoi pourraient ressembler des mécanismes de régulation plus viables et, espérons-le, moins coercitifs."

## Chapitre 4 : Action ou réflexion, ou notes en vue d'une théorie de la richesse et du pouvoir

Dans beaucoup de régions du monde, les perles étaient facilement acceptées comme monnaie d'échange, comme nombre d'autres objets ornementaux.

### Les signes ostentatoires de richesse

Les signes de richesse sont souvent consistitués d'objets ornementaux importants comme des joyaux patrimoniaux. Cependant, ces objets, qui ont pourtant beaucoup de valeur, ne sont pas utilisé pour représenter la valeur ie comme monnaie. En effet, ils sont uniques, avec une histoire et participent généralement à l'identité d'une personne, un roi n'est plus roi sans sa couronne. À l'opposé, la monnaie est quelque chose de générique, sans histoire.

### Action et réflexion

Foucault : à l'époque féodale, on montre le pouvoir, il est visible et correspond à des personnes bien connues. Ensuite se développent des "systèmes disciplinaires", gérés par des bureaucraties invisibles.

Équivalent dans les relations de genre, 

> "une femme doit se surveiller sans cesse. L'image qu'elle d'elle-même l'accompagne pour toujours. Lorsqu'elle traverse une piêce ou pleure la mort de son père, elle ne peut pas ne pas se voir marcher ou pleurer." Berger

Passage intéressant sur l'évolution de la mode. Les hommes après la renaissance choisissent un style vestimentaire plus sportif, chasseur, délaissant les anciennes parures flamboyantes, es laissant aux femmes. Leur style assez générique et le but de ces habits représentent en quelque sorte leur capacité d'action.

On va voir que les distinctions entre capacité d'action et réflexion se retrouvent souvent.

Tylor a passé en revue plein de langues et leur vocabulaire pour dire "ame". Il a trouvé deux conceptions. La première, "l'ame vitale", liée au coeur ou au souffle, représente une "force cachée qui anime le corps", la capacité d'action. La deuxième, "l'ame image" est liée à l'ombre ou au reflet, "le terme évoque l'apparence physique d'une personne, détachée de son être physique".

On voit donc que la capacité d'action est lié à quelque chose d'invisible, de potentiel, 

> "Ce qui est entièrement inconnu peut être n'importe quoi - et, donc, peut aussi *faire* n'importe quoi."

> "Au contraire, être visible, c'est être concret et spécifique [...]. C'est aussi être l'objet d'une action plutôt que la personne qui agit sur les autres"

### Argent versus numéraire

> "Jusqu'à présent, j'ai fait valoir que les dons de Mauss sont enchassés dans l'identité sociale spécifique de leurs donataires et récipiendaires (leur "image" extérieur pourrait-on dire"), tandis que l'argent est identifié à la force intérieure, générique et invisible d'une personne."

Graeber compare cela avec les notions de contenu abstrait et forme concrète de Marx. Pour ce dernier, l'objet qu'on souhaite acquérir est concret, et l'on échange pour l'obtenir quelque chose de valeur identique mais qui ne nous intéresse pas, un contenu abstrait.

Graeber retrace l'apparition de la monnaie en Grèce,

> "Dans l'antiquité, et aujourd'hui encore, on attribue À Gygès, roi de Lydie au VIe siècle [BCE], d'avoir été le premier souverain à frapper monnaie."

Il raconte que cela n'a pas vraiment permis d'améliorer les échanges marchands, et suppose que cela a surtout permis de rendre la monnaie visible, et donc moins dangereuse pour l'État.

### Diverses formes de fétichisme

> "j'ai établi une distinction entre deux types de pouvoir social : le pouvoir d'agir directement sur autrui et celui de se définir soi-même de manière à convaincre autrui qu'il doit agir envers vous d'une certaine façon."

On peut dire que les biens uniques ont une puissance et volonté propre comme si elles avaient intériorisé leur histoire et les personnes qui y ont contribué. La différence avec les biens de l'économie marchande courante serait que l'histoire de ces biens génériques (qui proviennent du travail humain) a été oublié.

Pour Graeber, il y a transformation continuelle entre les deux formes visibles et cachées et les perles ont été prisées car elles se transforment de façon très simple. Elles peuvent être séparées en composantes uniques ou bien assemblées en bel ornement.

### Madagascar et la traite des esclaves

Le corail et les perles de verre rouge étaient utilisés comme monnaie.
L'Imerina est la région de Madagascar dont l'histoire est la mieux connue. Elle se situe "dans la zone centrale des hauts plateaux de l'ile". Premier compte-rendu européen de cette société en 1777.

> "En guise de monnaie métallique, les dollars d'argent étaient fractionnés en menue monnaie - la plus petite dénomination était de 1/720e de dollar - pesée lors de chaque transaction."

L'argent et les perles étaient les ornements personnels les plus importants. On transformait l'argent en chaine, contenant parfois jusqu'à 400 dollars d'argent.
Après l'époque de Radama, cela tomba en désuétude et l'argent ne fut plus utilisé que pour la fabrication d'amulettes, les *ody*.

### *Ody* et *sampy*

Les *ody* sont des amulettes possédées par des individus particuliers et qui ont un pouvoir très spécifiques comme aider à la réussit d'un voyage ou prévenir des attaques de crocodiles. Les *sampy* sont des talismans à l'effet plus global et protégeant plus de personnes. 
Ces objets étaient généralement cachés, et étaient consistués de bois, feuilles, écorces, racines d'arbres d'essences rares.

> "L'efficacité d'une amulette était attribué à son *hasina*. Dans l'Imerina du XIXe siècle, presque toutes les actions rituelles avaient pour objet la création ou la manipulation de *hasina*, un terme qu'Alain Delivré définit comme la capacité d'affecter le monde par des moyens imperceptibles. Le plus souvent ajoute-t-il, le *hasina* repose sur la relation qu'entretiennent un esprit invisible et un objet matériel grace auquel cet esprit peut entrer en contact avec les êtres humains."

*masina* signifie détenteur de *hasina*

Les *ody* sont considérés comme ayant une conscience, pour l'utiliser on les réveille et on leur explique ce que l'on attend d'eux. Ils sont associés aux esprits Ranakandriana. Ceux-ci sont considérés comme difficile à voir et assez générique :

> "Le plus souvent, les forces spirituelles du cosmos malgache sont des êtres génériques."

Ces esprits n'acquiert un "sens" spécifique que via l'*ody*, ou plutôt ses ingrédients qui eux sont spécifiques.

Une dernière particularité des *ody* est que leur action s'applique vers l'extérieur : un *ody* protégeant des balles ne protègent pas réellement directement mais dévie les balles ; un filtre d'amour ne rend pas plus beau mais convoque le désir d'autrui.

### Sacrifice et création d'amulettes

> "Les *sorona* étaient des symboles matériels propitiatoires. Ils représentaient les désirs ou intetions de deux qui en faisaient l'offrande, l'action qu'ils souhaitaient voir accomplie par les puissancs informelles et insivisbles dont ils sollicitaient l'intervention. Ils constituaient presque des hiéroglyphes physiques, reproduisant sous une forme visible les mots de leur invocation. Une fois ces prières exaucées, leur statut changeait. Ils étaient alors perçus commes des incarnations ou des canaux de ces mêmes puissances invisibles, des objets au travers desquels les êtres humains pouvaient entrer en relation avec ces forces. Alors, ils n'étaient plus exposés, mais mis au secret en tant qu'éléments *ody* - placés dans des cornes, des boites ou des sachets, enveloppés dans de la soie rouge ou remisés à l'abri des regards."

### Perspectives et conclusions

> "L'argent est volontiers représenté telle une puissance invisible en raison de sa grande capacité à se convertir en tout autre chose ; il est la potentialité d'une spécificité à venir, même si la réalisation de ce potentiel ne peut se faire que lors d'un acte d'échange futur. En cela, il s'oppose aux choses dont la valeur est enracinée dans des actions passées (quelles qu'elles soient). Celles-ci ne sont pas uniquement des objets de parade à part entière : elles possèdent également le pouvoir d'influencer les actions d'autrui, et ce pouvoir, clairement, partage des caractéristiques communes avec ceux de l'ostentation artistocraitique ou du faste royal. Mais si, dans sa forme la plus simple, l'ostentation artistocraitique commande au spectateur d'abonder en richesses ou de rendre hommage à celui qui s'expose en alléguant que d'autres avant lui l'ont fait, la forme la plus élémentaire de la valeur d'échange est son exact opposé : elle pousse à tacher d'obtenir un objet au motif que d'autres ont tenté de faire de même par le passé."

> "comprendre la valeur attribuée à un objet particulier signifie saisir le sens des différentes procédures de création, de consécraton, d'utilisation, d'appropriation, etc., qui composent son histoire. Il faut se demander : lesquels de ces actes déterminent tel aspect de sa valeur ? Parmi ces actions, quelles sont celles qui attestent sa valeur et sont appelés à se répéter ?"

> "Fétichiser un objet revient alors à confondre la puissance d'une histoire intériorisée au coeur de son propre désir avec la puissance intrinsèque de cet objet. Les fétiches deviennent un miroir des intentions manipulées de celui ou celle qui les regarde."


## Chapitre 5 : Le *wampum* et la créativité sociale chez les Iroquois·es

### Les Iroquois

<!-- NOTE:  --> je lis "peuple Iroquoien", "théorie iroquoise", ...

*wampum* : perles violettes ou blanches très prisées par les peuples Iroquois.

Ces peuples sont connus pour être très belliqueux. Ils pratiquent des "guerre de deuils" cad quand quelqu'un est assassiné, on va se venger. Les raids de capture d'ennemis finissent généralement de deux façons possibles, soit par une exécution violente et douloureuse du prisonnier (qui est parfois mangé ensuite) soit par l'adoption du prisonnier en remplacement de la personne tuée.
Peu de morts à la guerre car le but est la capture de prisonniers. À partir du 17e la guerre contre les autres nations de la région se fait plus violente.

Les *wampum* étaient utilisés avec les Européens comme monnaie d'échange, principalement contre des fourrures de castors.
Les Iroquois·es battent durablement les Hurons-Wendat lors de la guerre des castors de 1641 à 1649.

Comme beaucoup de peuples de la région, les Iroquois·es pratiquent la "résurrection des noms", c'est-à-dire que le nom d'une personne décédée va être transmis à quelqu'un d'autre. C'est encore plus valable pour les postes à responsabilité comme celui de chef, dans ce cas le nom fait presque office de titre. Transférer un nom se fait avec un collier de *wampum*, on "suspend le nom autour du coup".

Plusieurs niveaux d'organisation: la maison dirigée par les femmes, les conseils de village mixte et le niveau de la Ligue (principalement masculine).

### Les *wampum* dans la société iroquoise

La Ligue a pour but de préserver la paix et cela passe beaucoup par les *wampum*. Ceux-ci accompagnent beaucoup d'occasions et sont toujours présents lors des discours.

> "aucun argument notable n'était pris au sérieux sans [*wampum*]"

On peut presque dire que le discours s'incarne dans les colliers de *wampum*, on "parle" dans les colliers de *wampum*.
Un exemple, pour éviter une représaille en cas de meurtre on peut offrir des brasses de *wampum* à la famille endeuillée, ce qui montre bien l'action pacifiante du *wampum*.

> "On considérait que le *wampum* possédait la capacité intrinsèque de dissiper le chagrin"

Mythe fondateur: création de la Ligue via le don de noms + vainqueur du mal par le don de branches de *wampum* et de paroles appaisantes. Ces mêmes actes répétés plus tard permettent en quelques sortes de re-créer en permanence la société.

Le fait que le *wampum* vienne d'ailleurs (il vient de coquillage donc plus sur la côte rappelle le problème gödelien, comment créer quelque chose auquel on appartient).
Le *wampum* n'est pas une monnaie d'échange au sein des nations Iroquoises.
Outre les échanges avec les Européens, le *wampum* pouvait servir de tribut auprès de peuples vaincus.

Chez les Iroquois·es, les colliers sont généralement gardés cachés.

> "Le *wampum* caché représentait donc un potentiel d'action politique: pour faire la paix, mais aussi pour faire la guerre."
> Deux visions du *wampum*, les dons de *wampum* générique qui accompagne un discours "quelconque" (pour lequel le plus important est de parler, lien social), et le *wampum* des grands évènements qui prend la valeur de l'évènement lui-même.

### La divination des rêves

Les rêves seraient l'expression de désirs cachés et qu'il faudrait réaliser sous peine de tomber malade. Généralement l'explication des rêves et leur réalisation ont un caractère public. S'il est impossible de réaliser un rêve, on essaye de le réaliser au moins de manière symbolique. Cependant si réaliser un rêve était possible, il fallait le faire:

> "Ce serait une cruauté et une espèce de meurtre, note Dablon, de ne pas donner à un homme ce qu'il a songé: car ce refus serait capable de le faire mourir"

Dans le cadre des festivals de divination, il était courant de faire deviner des rêves, par exemple sous forme de charade (souvent à la phratrie opposée). Cela se produisait aussi dans le cas d'une personne malade. Il arrivait que, pour la soulager, les gens foutent le bordel dans le village, le chaos étant censé soulager la personne.

Graeber met en exergue les deux conceptions de la personne. La première est associée au noms, elle est visible et liée au *wampum*. La deuxième est liée aux rêves et aux désirs et est profondément sociale.

> "le caché ne peut devenir visible (ou le spécifique devenir générique) que si l'individu devient social (ou le spécifique, générique)."

### Plusieurs notes

- Structuration de la société en phratries opposées, ce qui permet certaines mises en scène lors des grands évènements. Cela permet de créer des totalités imaginaires qui représentent la société dans son ensemble.
  Par exemple lors de la fête du solstice d'hiver, la phratrie endeuillée est consolée par l'autre.

> "les membres d'une phratrie pourvoient aux soins nourrissiers de l'autre"

L'idée est que les désirs refoulés et l'affliction du deuil peuvent mener à la folie donc on essaye d'aider ces personnes qui en ont besoin. À côté de ça, certaines phases de violence (destruction de matériel etc) peuvent s'exprimer et semblent exprimer toute la violence que l'on tend à soigner par ailleurs.

Que ce soit lors de la divination des rêves ou lors des deuils, des objets sont échangés (objets particuliers dans un cas, générique dans l'autre, les *wampum*).
On remarque cela aussi chez les Onondaga et les devinettes de rêves finissent en des dons spécifiques et particuliers d'un côté (tasliman, ...) et génériques de l'autre (l'équivalent du premier présent en sac de mais par exemple).

- Les Iroquois ne brident pas du tout leurs enfants mais les contraintes arrivent progressivement car on attend beaucoup des adultes (idéaux de générosité, de résistances aux difficultés etc)
- Individus autonomes:

  > "comme on est en droit de s'y attendre dans une société qui accordait une pareille importance à l'égalitarisme et à l'individualisme"

- mythe des phratries exogame, incitation aux jeunes hommes de se répartir, de se marier avec des femmes d'autres clans.

- Les femmes possédent la plupart des biens et outils dans la société (individuellement ou à plusieurs) mais pas les *wampum* qui existent et s'échangent quasi exclusivement entre hommes. C'est comme si les femmes s'échangeaient des hommes (pour le mariage) et les hommes des colliers aux attributs féminins (la vie et la lumière).

- La valeur cardinale pour les Iroquois·es est la paix, ce qui implique aussi l'existence de son contraire.

- Chez les hurons, description d'une grande pratique du don et de la générosité. Pas de marché dans la société, tout circule librement (voir p223 extrait de Delage).

- Les rêves et leur implication de changement social permettait de sans cesse évoluer, de créer de nouveaux rituels etc

- On observe souvent une explosion culturelle de peuples qui viennent d'être en contact avec le monde extérieur, l'économie globale de marché etc, du moins quand ces peuples peuvent rester assez autonomes et qu'ils sont en situation favorable par rapport au nouveau marché. Cela ne dure généralement que quelques décennies avant qu'il ne se passe d'une manière ou d'une autre un retour de baton.

### Conclusion

Les Iroquois·es devinrent de plus en plus dépendants des Européens pour différentes denrées comme les habits, objets en laiton, acier ou verre. En échange, ils ne vendaient que de la fourrure. Cette dépendance s'exprime aussi par le fait que la monnaie d'échange (le *wampum*) devint à cette période un élément culturel important pour les Iroquois·es.
Graeber fait le lien:

> "en convertissant l'argent du commerce, cad ce dont la violence même était faite, en un potentiel de création de la paix, les chefs de la Ligue suivaient en fait une logique rituelle ancienne. Et les perles, dont la forme oscillait entre potentiel abstrait et artéfacts concrets, servirent ainsi de pont entre un système marchand voué à l'accumulation matérielle et un système social dont le grand impératif consistait à assembler toujours plus de personnes : le plus souvent, en déposant un collier de *wampum* autour des épaules d'un captif et en lui donnant de ce fait un nom."

## Chapitre 6 : Marcel Mauss revisité

### Contexte

> "En réalité, je suis convaincu que la portée du corpus théorique de Mauss est la plus considérable de toute l'histoire de l'anthropologie."

À l'époque de Mauss, une grand question centrale à plusieurs disciplines était : quelle est l'origine du contrat, cad du lien social entre les individus. Pour Hobbes il est nécessaire de se lier par l'État pour éviter une situation de guerre permanente.
Le contexte social du 19e siècle est bien sûr la montée du capitalisme et de l'individualisme.
Spencer suggère que le rôle de l'État va s'affaiblir avec la montée du marché concurrentiel. Durkheim au contraire (oncle de Mauss) pense que l'augmentation des contrats privés augmentera l'action de l'État.

Dans ce contexte, Mauss avance dans son _essai sur le don_ que les premiers contrats concernaient des communautés (et non des individus comme le prétendaient certains éconimistes) et qu'ils étaient totaux ("*prestation totale*") cad qu'ils n'étaient pas spécifiques mais concernaient beaucoup de pans de la société en même temps. La question que se pose Mauss est:

> "Quelle est la règle de droit et d'intérêt qui, dans les sociétés de type arriéré ou archaique, fait que le présent reçu est obligatoirement rendu? Quelle force y a-t-il dans la chose qu'on donne qui fait que le donataire la rend?"

Mauss pose ainsi que l'origine du contrat est bien antérieur à l'État et il démonte les théories courantes de l'économie primitive et du troc.
La réponse de Mauss à sa question est : "les choses participent de la personnalité du·de la donateur·rice". Les choses veulent rentrer à la maison.
Mauss s'intéresse aux relations entre hommes et choses. Et si ces deux catégories sont considérées comme séparées aujourd'hui, c'est parce que c'est le prix à payer pour parler d'intérêt personnel. Mauss critique cette notion d'"intérêt personnel" et en même temps les postulats de la théorie économique.

### Mauss socialiste

Mauss fut un fervent socialiste toute sa vie, engagé à la SFIO et proche de Jaurès, mais aussi engagé dans le monde des coopératives.
Après la révolution russe, la SFIO se scinde en partis communistes et socialistes et Mauss, bien que soutenant la révolution voit d'un mauvais oeil les bolcheviks et privilégie un socialisme ascendant.

Concernant le marché, avec la NEP de Lénine annoncée en 1921, il était clair qu'on ne pouvait pas interdire le commerce par décret. Même en Russie dans le pays le moins monétarisé d'Europe. Mauss cherche donc à comprendre quelle place donner au marché dans une visée socialiste:

> "La visée de Mauss était donc d'aller au coeur de ce qui, dans la logique du marché, malmenait tant le sens de la justice et l'humanité des gens ordinaires. Il lui fallait comprendre la popularité des partis socialistes et des programmes d'aide sociale, et imaginer, par l'examen des archives ethnographiques, ce à quoi pourrait ressembler une société en accord avec ces normes populaires de justice: une société dans laquelle le marché serait relégué à sa fonction propre, celle d'une technique de prise de décision décentralisée, d'un dispositif populaire de sondage quant à l'attractivité des différentes sortes de biens de consommation", ...

L'oeuvre de Marx était peu connu en France à cette époque mais Mauss avait une compréhension fine du *Capital*.

Pour Mauss, on peut ne pas opposer communisme et individualisme. Communisme dans le sens ou l'on peut prendre ce qu'on veut sans paiement direct ni reciprocité mais sans pour autant avoir une propriété purement collective. On peut imaginer une base individuelle, les individus étant liés par des obligations à durée indéterminée, qu'elles soient unilatérale ou non.

Il reste cette question, comment est-on arrivé à la prévalence de l'intérêt personnel, de l'accumulation pour l'accumulation. Exemples chez Mauss de société dans lesquelles il était possible d'accumuler mais le peuple attendait une redistribution. 

Idée intéressante que le don purement désintéressé n'est qu'une image en négatif du marché, et que n'attendre strictement rien en retour c'est aussi se couper du lien social créé par un don. Cette idée pure ne peut exister tout comme l'intérêt personnel pur de la théorie économique capitaliste. Et pour Mauss le capitalisme a échoué en voulant imposer ses idées moralement dégoutantes pour beaucoup.
Pour Mauss, le travail salarié en tant que contrat est donc assez misérable au sens ou le travailleur donne tout mais ne reçoit pas assez pour vivre.

### L'exemple océanien: les iles Trobriand

On parle du nord de la Papouasie-Nouvelle-Guinée ou est pratiqué le cerle d'échange *kula* (iles Trobriand).
Une chose intéressante est que "la filiation était strictement matrilinéaire", l'enfant est considéré comme issu du corps de la mère (et pas seulement de la mère, du clan maternel) **sauf** pour le visage et donc l'apparence physique.
Il en va de même pour l'éducation, la mère et son frère apportent soins et nourriture et le père s'occupe de la parure et l'initie à ce qui lui permettra de participer au *kula* . 
Il faut noter que les hommes de l'ile de Gawa notamment cherchent la renommée et plus précisément l'extension de leur personne dans l'espace-temps. Et c'est ce à quoi les hommes préparent leur fils.

> "La renommée est en soi un type d'ornement personnel"

Mais cela n'est possible que grace à une base solide, féminine.

Les hommes envoient des dons de nourriture à leur soeurs mariées, les maris de celles-ci répondent par des dons plus occasionnels et plus substantiels.

On retrouve la binarité entre ce qui est caché, solide, qui représente le potentiel d'action (qui vient de la mère) et ce qui est apparent (qui vient du père).

On observe le cas inverse sur l'ile Sabarl. Lors des cérémonies funéraires, la lignée paternelle offre des haches en pierre verte (forme supreme de richesse locale) au clan matrilinéaire qui va ensuite construire une effigie avec ces haches dans un lieu secret et l'invoque comme un ancêtre. La forme masculine visible se transforme en forme féminine secrète. Pour Graeber, c'est le cas parce qu'on ne parle pas ici de la création d'un individu mais de sa dissolution.

**On voit donc que la circulation des choses précieuses dans les "économies de don" est liée à la création et la dissolution de personnes humaines**

### Maori 

On parle des échanges chez les Maori vers 1750 (et du *hau*, l'esprit du don), peuple renommé comme philosophe et intellectuel et des potlach chez les Kwakiutl (vers 1895) connu comme un peuple plus théatral qu'intellectuel.

Maori : cosmos homomgène, tout le monde descend des dieux. Les seigneurs hawaiens cherchaient à mettre la main sur des trésors uniques.
Kwakiutl : cosmos hétérogène, plein de groupes d'origines mythologiques différentes. Les chefs amassaient des milliers et milliers de couverture à points européennes.

> "Les récits Maori parlent de réciprocité, très peu de richesses. Les récits Kwakiutl son peu nombreux, mais très détaillés à propos de toutes sortes de richesses; sur la réciprocité, ils ne disent presque rien. Et ainsi de suite."

Mauss était fasciné par les Maori et enseignait leur langue.

Pour les Maori la généalogie est très importante, chacun connait sa place sur l'arbre et peut s'en servir pour se trouver des ancêtres célèbres etc.

En Polynésie en général, le *mana* est la puissance créatrice qui vient des dieux. Chez les Maori il signifie plutot autorité, prestige, influence ... et doit être constamment entretenu. Les hommes étaient très susceptibles et ne laissaient pas passer les affronts.
Le *tapu* (tabou) est l'autre revert de la pièce, il se réfère à l'état d'interdit qui entoure des personnes ou des choses et qui les rendaient sacrés. Elles ne devaient pas être en contact avec des choses profanes comme l'alimentation (particulièrement le aliments cuits), les excrétions, la sexualité...
Les hommes à fort *tapu* ne participaient donc jamais a la cuisine ou au ramassage du bois de chauffe. Ces taches étaient assurées par les esclaves et les femmes.
Les fruits et légumes aussi descendent des dieux et ont un *tapu* qu'il faut garder pour leur croissance puis lever pour pouvoir les récolter "et faire effectivement la guerre aux cultures pour être libre de les tuer et les manger". La mort humaine correspond à la même chose, les individus sont mangés par les dieux.

On distingue les *ariki*, chef·fe·s sacré·e·s, incarnation des ancêtres et donc des dieux , des *hapu*, personnages qui conquiert leur place par leurs efforts voire leurs transgressions. Les premiers sont souvent des premières, responsables de la fertilité, tellement entouré de *tapu* qu'elles ne pouvaient pas faire grand chose à part exister. Les deuxièmes, souvent hommes, reposent leur pouvoir sur le *mana* et sont associés à la destruction.

Néanmonins *mana* et *tapu* restent les facettes d'une même pièce. Par exemple, le *tapu* s'étend aux possessions d'une personne. Quelqu'un de reconnu peut s'approprier un objet en le faisant rentrer dans son cercle de *tapu* en disant par exemple "ce truc est mon crane" ou "ce machin est ma colonne vertébrale", cette appropriation est le *tapatapa*. Il fait donc parler son *mana*, sa capacité d'action et quiconque s'y oppose, lance un terrible défi.
A l'inverse "les malédictions les plus puissantes consistaient à identifier sa victime avec de la nourriture cuite"

Entre hommes libres, ces appropriations faisaient office de dons. On pouvait simplement admirer ou faire l'éloge de quelque chose pour ce le voir offrir. Et le·a donateur·rice pouvait exiger quelque chose de valeur similaire en retour, un peu plus tard.

Autre forme d'appropriation, le *muru*. En cas d'infraction, de conduite inappropriée, de blessure, une personne pouvait subir un raid chez elle dont le but était de la dépouiller de ses biens. 

Les *mauri* et *hau* sont aussi très liés. Pour un endroit ils peuvent symboliser la fertilité, pour un humain, plutot son essence vitale. Les *mauri* sont la version cachée. On peut y enfouir la fertilité d'un lieu et l'enfouir dans son champ de patates.
Le *hau* est connu par Mauss qui le désigne comme l'esprit de quelqu'un qui habite un objet donné et oblige le récipiendaire à le rendre. 
Pour Graeber le *hau* a des sens plus large "d'idée de mouvement intentionnel".
Un sens assez concret du *hau* est aussi dans les offrandes aux dieux, cela permet permet par exemple de lever un *tapu* et de récolter son champ de patates. Pour cela on peut par exemple offrir de la nourriture cuite car cela détruit le tapu (*pure*).
On peut voir le contre-don de la même façon. Le *hau* de l'objet rendu lève le *tapu* du don initial (le don reste dans le *tapu* du·de la donateur·rice jusqu'à être levé par le *hau* du·de la receveur·reuse). Cela est d'ailleurs assez proche du *pure*, les dieux (leur *mauri*) étant responsables de tout, tout est sous leur tapu. L'offrande *hau* peut se voir comme le contre-don qui lève le *tapu*.

Les biens patrimoniaux (*taonga*) sont particuliers. Ils appartiennent véritablement à l'histoire d'un clan et sont considérés comme des ancêtres. Leur valeur vient en partie de leur histoire (armes, ...). Les guerres qui ont parfois lieu pour les obtenir renforcent leurs histoires et peuvent les lier à différents clans. Ils sont souvent cachés et participent au *mana* du groupe.

### Kwakiutl

Nature très riches mais les Kwakiutl travaillent dur à produire toujours plus, autant en nourriture qu'en produits non alimentaires. Peuple assez inhabituel car assez stratifié pour des non-agriculteurs et très intéressé par l'accumulation de richesses.
Importance particulière de la faim. 

L'identité d'une personne se confond avec ses propriétés qui donnent certains droits (un plat de fête sculpté donne le droit de distribuer certaines nourritures dedans et d'avoir accès aux endroits ou l'on peut trouver cette nourriture). Il en découle que seuls les nobles étaient réellement des personnes.

Notes:
- Niveau cosmologique, les mythes fondateurs font état de différentes créatures (animales) fondatrices qui deviennent l'emblème d'une lignée. Des masques de ces animaux sont parfois portés. Il semble que les nobles se voient comme des descendants de ces créatures mais que leurs actions dans le présent ne marquent que temporairement le monde, on les oublie ensuite, seuls les créatures mythiques fondatrices restent connues. 

- Géographiquement vers Vancouver
- récits ethnographiques pendant des périodes troublées.
- Société très théatrale, on aime impressionner par des mises en scène. D'ailleurs les différentes personnalités peuvent être vues comme des masques.

<!-- TODO:  --> correct
Le *numaym* contient un certain nombre de statut avec leurs noms, qui donne des places au potlach. Tout est hiérarchisé, aussi bien dans le *numaym* qu'entre ceux-ci. Le titre principal d'une personne est rattaché au *numaym* (avoir ce nom implique appartenir aux *numaym*), les titres secondaires peuvent s'échanger entre les *numaym*. Les premiers étaient généralement transmis héréditairement mais pouvaient aussi être donné ou pris par le meurtre. Les deuxièmes étaient souvent l'objet de l'accumulation, ce qui "augmentait" la personnalité d'un individu et le rendait plus "lourd". On parle là de noms de fêtes, de cérémonies d'hiver ... On dit souvent que les femmes sont "lourdes" et que les hommes consitituent leur richesse en vue de les attirer.

L'enjeu du mariage tenait au transfert de la propriété ancestrale. Les nobles cherchaient à conquérir des femmes pour gagner des titres. 

Le potlach est une cérémonie de validation des titres, et les titres ouvrent des privilèges et des sièges aux assemblées qui permettent de distribuer certains objets. Donner des choses lors d'un potlach apporte de la grandeur. En fait il faut amasser des richesses pour acquérir du poids puis donner ces richesses pour valider son statut. 

> "cette image parfaite de la réussite: l'invitant y est comparé à une montagne géante, infiniment lourde; les couvertures et autres richesses roulent sur ses pentes en avalanche de biens, enrichissant et mettant en péril simultanément le monde alentour. Cette image résume admirablement la combinaison singulière d'agressivité et de générosité des Kwakiutl."

Celui qui donne ses richesses se place à coté d'un mat-totem symbolisant le lien entre le ciel et la terre. Mais comme chacun possède des origines cosmiques différentes, "tout le monde prétend représenter le centre de l'univers". D'ou "la grandiosité et l'hyperbole qui firent en partie la notoriété des chefs Kwakiutl". 
Ces différences impliquent la nécéssité d'un moyen de comparaison, d'ou les couvertures.

La richesse principale était les cuivres. Pour acheter un cuivre on devait le payer plus cher que le dernier acquéreur. Il s'agit de richesses, au même titre que les couvertures mais à un niveau bien plus élevé, d'une valeur semblable à celle de la vie d'un esclave. Les cuivres n'ont donc pas le même niveau que les trésors qui participent aux personnes. Les cuivres provenait de métal européen. 
Les cuivres étaient tous uniques et très lourd. Il fallait amasser une grande pile de couvertures pour pouvoir les attirer jusqu'à soi. De part la comparaison avec des esclaves, les cuivres étaient aussi vivants et pouvaient donc être tués.

Un point également important est la confrontation entre chefs ou *numaym*, que ce soit pour clarifier une position hiérarchique relative ou laver un affront. Les opposants détruisaient le plus de choses possibles (par exemple des cuivres mais aussi des canots, ...) pour gagner. On organisait des potlach grandioses.

> "Récapitulons: les titres et les biens constitutifs y afférents étaient transférés, que ce soit d'un parent à un enfant ou du père de la femme à son gendre, sans aucune "obligation de rendre". Ces titres étaient ensuite validés lors de distributions collectives ou le principe de réciprocité ne s'appliquait pas non plus. Il n'y a en réalité que peu de circonstances dans lesquelles on peut parler de réciprocité:
1. Dans le monde de la "finance" [... remboursement des prêts de couvertures]
2. Dans le cadre du mariage: alors que les relations entre les deux parties sont globalement très déséquilibrées et non réciproques, le père de la femme peut 'racheter' sa fille. Non seulement cela ne crée pas de liens permanents, mais qui plus est, cela annule les liens existants.
3. Dans les dons de rivalité: les défis directs entre chefs conduisent à une tentative pour faire des dons que l'autre ne peut pas égaler."

### Conclusion

> "la valeur des choses les plus précieuses dans les économies du don vient principalement de ce qu'elles incarnent une qualité humaine, qu'il s'agisse de la puissance créatrice de l'agir humain, de la fertilité ou d'histoires et d'identités particulières déjà réalisées dans le passé"

Il faudra peut-être relire les pages 324-330

Tableau récapitulatif:
Maori:
- Système de filiation cognatique, unique et global 
- Philosophie complexe des puissance intérieures, invisibles, génériques, les *taonga* (richesses supremes) faisant figures d'exceptions spécifiques 
- La réciprocité comme thème récurrent dans les récits
- Le thème de la propriété n'a que peu d'importance dans les histoires
- La réalisation de soi passe avant tout par l'appropriation
- Les dons préservent l'identité du·de la donateur·rice

Kwakiutl:
- Groupe aux origines totalement hétérogènes
- Théorie de l'ame à peu près inexistante, prééminence des apparences, les Cuivres (richesses supremes) faisant office d'exceptions génériques
- La réciprocité c'est pas un thème en soim sauf dans les relations antagonistes
- Les histoires ont toutes pour objet des transferts de propriété
- La réalisation de soi passe par le don
- Les dons constituent l'identité du·de la récipiendaire.

Chez les Maori, de part la cosmologie les gens sont identiques, la question est donc de savoir comment se différencier (créer une individualité), et cela se passe par "l'appropriation, et tout particulièrement l'incorporation par la violence (explicite ou implicite) de terres, de biens ou de personnes"
Chez les Kwakiutl, les individualités ne manquent pas et la question est plutôt de créer une société. Le potlach permet de recréer la société en permanence, ce qui signifie dans ce cas là, rassembler un public et avoir un effet spectaculaire sur lui.

En conclusion, les dons ne sont pas forcément rendus.

> "Contrairement aux échanges de dons de rivalité, les prestations totales crézient entre individus et groupes des relations permanentes qui se perpétuaient précisément parce qu'aucune contre-prestation ne permettait de les annuler"

Pour Graeber, les relations de réciprocité dans une économie de dons sont de nature assez faibles. On prétend que l'on ne doit rien en retour, on se prétend donc beaucoup plus libre qu'on ne l'est vraiment. C'est très loin d'une relation entre amis, pour laquelle on part du principe qu'elle durera pour toujours et pour laquelle on ne tient aucun compte. Chez les Polynésiens, on fait un contre-don pour se libérer du premiers dons, on n'a donc pas affaire a un lien symétrique de réciprocité (le premier don met la pression sur le receveur).
On devrait donc plutôt parler de réciprocité plus ou moins ouverte. Ouverte, pour parler du communisme (toujours selon la formulation de L. Blanc), fermée lorsqu'une "mise a l'équilibre des comptes referme la relation".
Pour Graeber, les economies de dons sont donc fermées et assez proche d'un système marchand car elles sont compétitives et individualistes. De plus elles "peuvent facilement déraper vers des relations de hiérarchie, de népotisme et d'exploitation".
Il semble que l'organisation de la famille soit un des points cardinaux en ce sens.
"J'ai la forte intuition que plus les relations au sein du foyer sont hiérarchisées, plus les relations entre chefs de famille (mâles) sont médiatisées par des formes d'échange de dons équilibrées et potentiellement compétitives"

La vision de Graeber concernant la question de savoir quand le don doit être rendu est la suivante:

> "l'obligation de rendre fonctionne lorsque les relations 'communistes' sont tellement identifiées à des positions inégalitaires que le fait de ne pas rendre le don placerait le·la récipiendaire dans une position d'infériorité. Ces formes d'échange consistent donc à établir une sorte d'égalité fragile et compétitive entre des acteurs qui, presque toujours, sont eux-même hiérarchiquement supérieurs à quelqu'un d'autre."

De ce que je comprends, ne pas rendre le don ferait basculer une personne dans un régime communiste (ou l'on ne compte pas qui donne et qui reçoit ie pas d'obligation de rendre) qui est jugé de classe inférieur dans la société.

> "J'essaye d'avancer l'idée que, pour comprendre le fonctionnement de tout système de valeurs, il est nécessaire d'analyser à la fois ce qui ne doit pas et ce qui ne peut pas être mesuré en son sein"

Les relations communistes relève de la première idée, on refuse de tenir les comptes. L'accent mis sur les choses précieuses uniques relève de la deuxième.
Ainsi pour Graeber, on retrouve un schéma dans la plupart des sociétés sans marché. Une sphère de la vie quotidienne ou l'hospitalité est de mise et l'on ne compte pas vraiment; et "une sphère de prestige caractérisée par une multitude de comptabilité minutieuse". Dans la première sphère, on refuse de compter, l'action est sur l'humain. Dans la deuxième, on ne peut pas compter car la valeur des objets est incommensurable.
Dernière remarque, Graeber pense que Mauss a largement exagéré son propos en disant que les objets avaient une ame. Cette mystification "se produit à un degré étonnament faible".

Définition du don, par le groupe M.A.U.S.S :

> "Donner, c'est opérer un transfert sans retour immédiat ni garantie qu'il y aura un jour contrepartie."

Fin de chapitre intéressante mais complexe à résumer.

## Chapitre 7 : La Fausse monnaie de nos rêves, ou le problème du fétiche

> "J'ai proposé [...] que nous appréhendions les systèmes sociaux comme des structures d'action créatrice, et la valeur comme la façon dont les personnes mesurent la portée de leur propre actions au sein de ces structures." 

### Le roi et la pièce de monnaie

En Imerina, Madagascar, le *hasina* est une vertu intrinsèque qui ne pouvait pas être créée par les humains. Ce mot pouvait désigner également des pièces qui étaient régulièrement données au roi. On peut faire un parallèle avec le mot honorer. On peut posséder de l'honneur ou bien honorer quelqu'un cad lui reconnaitre cette qualité. (tout ceci est rapporté dans un article de Bloch). On peut voir une espèce d'échange car le roi bénit en retour et assure fertilité.
Pour Graeber (qui a été sur le terrain), le *hasina* n'est guère utilisé en terme de supériorité hiérarchique intrinsèque mais plus comme le "pouvoir d'agir grace à des moyens invisibles ou imperceptibles". Et le *hasina* est créé par l'action humaine et doit être entretenu.
Lors de la cérémonie du Bain Royal, on présente des *hasina* au roi qui ensuite se cache, se baigne puis asperge la foule de l'eau du bain. On faisait la même chose avec les *sampy*, des *ody* partciculièrement puissants qui possédaient leur propre personnalité. On peut ainsi dire en quelques sortes que le roi devenait une amulette au cours de cette cérémonie.
Ces cérémonies permettaient également de recréer la société. Le roi est roi parce qu'on lui fait don de pièces (*hasina*) et le peuple est peuple de la même manière. Il y a donc un espèce de consensus, le peuple semble accepter cette situation. On peut faire un parallèle avec certains *ody* importants auxquels on prêtait serment, faute de quoi l'*ody* ne valait rien.
On crée ainsi une relation d'engagements mutuels, ce qui implique une tierce puissance (violente) capable de faire respecter ces engagements. Cela est vérifiable au niveau du roi qui avait une capacité de punir très violente. 

Il semble que les cérémonies se soient inspirés de beaucoup d'éléments différents et que ce que raconte Bloch n'est pas totalement à coté de la plaque. En réalité, Bloch s'est plus intéressé aux rituels liés à la parenté et à la filiation alors que Graeber s'intéressait plus aux pratiques magiques et à la création de *hasina*. Ce serait cette différence d'intérêts qui auraient créé des compte-rendus si différents. 
Pour Graeber, si Bloch s'est peu intéressé à la magie c'est de part son bagage marxiste.

### Magie et marxisme

critique de la religion par Marx : les humains sont des créatures créatrices, pourtant elles ne se considèrent pas comme telles. Elles préférent projeter leurs élans créateurs sur des personnages imaginaires et se prosterner pour invoquer leurs faveurs. La magie est cependant différente car elle implique pour une personne de changer directement le monde, non en demandant une faveur (sans projection fétichisée) mais en la réalisant soi-même. La critique marxiste ne s'applique donc pas.

### Magie et anthropologie

Depuis Malinowski et Evans-Pritchard, pas beaucoup de textes sur la magie. Concept difficile à appréhender et aujourd'hui on a créé un certain nombres de sous divisions (chamanisme, médecine, sorcellerie, ...). 
La magie pose problème pour les anthropologues relativistes car les actions magiques sont vérifiables et sont généralement fausses. Non cette personne ne sait pas maitriser la foudre contrairement à ce qu'elle dit, elle ne sait pas non plus créer du vent ou des gros fruits. Alors s'il est évident que c'est faux, pourquoi la magie ? (pour poser une question assez directe). Il semble qu'il puisse s'agir souvent de performances ayant pour but de marquer l'auditoire. Il semble qu'il existe partout un grand scepticisme concernant les pratiques magiques mais qu'il importe peu car certaines pratiques magiques sont jugées comme véritables malgré tous les charlatans.
Et peu importe si un charlatan sait qu'il ne fait que mettre en scène des illusions si dans le même temps il guérit des gens.
Il semble de notoriété publique que la magie ne fonctionne que si l'on y croit, elle a un pouvoir de suggestion.

De manière générale, tout ceci semble toujours être contradictoire. On sait que la magie est placebo et pourtant on y croit.
Graeber fait la comparaison avec un pouvoir (par exemple Etatique), le pouvoir est d'abord cette "capacité de convaincre les autres que vous en êtes détenteur·rices"

On pourrait résumer la magie suivant ces deux concepts, une abscence de fétichisation et un fort scepticisme.
