# [Queering Anarchism](https://theanarchistlibrary.org/library/c-b-daring-j-rogue-deric-shannon-and-abbey-volcano-queering-anarchism)

## Queer Meet Anarchism, Anarchism Meet Queer

Queer theory was highly influenced by Foucault who explained that

> "'the homosexual', as an identity, could be traced to the rise of sexual science in the mid 1800s."

While same-sex pratices are happening before, it stopped being a pratice to become an identity. This also means that we created certain categories, hetero, homo, bi and we classify people (or ourselves) in these boxes. This is in fact a clear limitations int he realm of possibilities when we talk about identities, genders, pratices ... Not all people are 100% hetero, or gay, or bi.

> "Queer served as a space for critiquing identity and playing with theory, bodies, power, and desire that didn't need to be reducible to easy definitions."

Queer is defined by what is at odds with normality. And normality isn't just heterosexuality, but heteronormativity, a monoganous happily-married couple ... So queer can include BDSM, sex work, nonmonogamy etc

> "This antigonistic relationship with the normal has also led to an anti-assimilationist ethic that often sets queer politics apart from mainstream 'G(lbt)' politics."

The State and the institutions being promoters and enforcers of heteropatriachy with its boxes and categories, queer theory is defiant towards institutions.

## Gay Marriage and Queer Love (Ryan Conrad)

Marriage is an institution of the state which had been historically used to 'own' and control women. When fighting for the recognition of same-sex marriage, what is at stake is not love but "benefits and privileges", "money and property".
Marriage is also a defender of classical nuclear family which has been known for years to be the "primary site of sexual violence against children" and women.

So it seems that with same-sex marriage, queer (gay and lesbian) imagination has been tamed. The contract is to accept the old violent nuclear family construct in order of privileges (like healthcare, ...)

> "\[Equality rhetoric\] demands that we put out time and energy into the desperate fight to be equal participants in oppressive and archaic institutions instead of attempting to actualize our dreams of queer utopia."

## De-essentializing Anarchist Feminism: Lessons from the Transfeminist Movement (J. Rogue)

First Wave Feminism: pre-WWII, for voting rights.
Second Wave Feminism: late 60s, early 80s. This movement can be divided into different competing theories.

Liberal feminists are concerned with the glass ceiling, with putting women in positions of power and have no problems with capitalism.
Radical feminists are coming from Civil Rights and anti-war movements where they experienced sexism. Their main focus is then on patriarchy.
Marxist feminists think that the main focus should be on capitalism because equality cannot be achieved in this system. We need a new economic structure, more egalitarian.
Social feminists, adept of the 'dual systems theory' wanted to put together the last two tendencies, and recognize that patriarchy and capitalism are both problematic. This approach however left out many other forms of oppressions (race, ability, age, ...)

More than this, the movement created a "stereotypical female", subject of oppression, meaning that all women should fit this mold presented in the feminist discourse ("universal female experience"). This vision has been challenged by transfeminism, completing the work of Black feminists.

For the author, transfeminism must escape the realm of academia where it lies nowadays and take a place in current struggles. Unfortunately, gay and lesbian movements have often compomised with the state and thrown trans people under the bus.

> "Anarchists need to be developping working class theory that includes an awareness of the diversity of the working class."

## Police at the Borders (Abbey Volcano)

The author claims that a major problem of heteronormativity is the creation of boxed of identities, which then favors some over others, creating at the same time hierarchies. We then shouldn't create new boxes (such as queer) and try to reverse the hierarchy, favoring this time queerness over cis-hetero-monogamous... relationships.

There is also a risk to create identities (boxes) more oppressed than others who then become fetishized.

> "Often this tactic of agreeing with 'the most marginalized in the room' will be used as a substitue for developping critical analyses around race, gender, sexuality, etc. This tactic is intellectually lazy, lacks political depth, and leads toward tokenization."

It is of course beneficial to hear people being particularly oppressed because they only will live this kind of oppression and express it. But the point of anarchism is to destroy hierarchies and not to create new ones at the margins.

New freer forms of sexuality are great, but we shouldn't feel oppressed to test them. The author argues that in 'queer circles', queer is often defined by the sexual pratices (what we do becoming what we are) and it is "policing the borders of identity and practice."

## Gender Sabotage (Stacy aka Sallydarity)

> "To achieve liberation, we must reject the binary gender system, which divides us into two mutually exclusive categories."

> "Clearly those who do not fit into these gender boxes are seem as a threat and are disciplined through threats or acts of discrimination, verbal abuse, harassment, and/or violence."

As the previous author, "I argue not that gender transgression or deviance is in itself revolutionary".
Then what is really revolutionary is to to "destroy the gender-based power relations".

Historically, women have been oppressed especially after the witch hunt which forcefully tamed (because they were taking part in revolts). This story is based on Federici's (controversial) *Caliban and the Witch*.

> "Early on, European women were defined as unruly, mentally waek, and in need of being controlled. The witch hunts served to reinforce this, but at the same time to discipline women into a new 'nature' - that of the docile, moral, and motherly (yet in need of being controlled)."

There is also this idea that patriarchy was instituted because it was necessary. For instance, it was a tool massively used during the colonization of the Americas.

> "The sex/gender hierarchy is inseparable from race, colonization, and capitalism."

This shows how hierarchies are been created:

> "Those in power divide the people on the basis of a physical difference (ignoring exceptions and gray area) and amplify the significance of those differences through criminalization and limitations of legal and economic freedoms, as well as through violence (justified by the alleged transgressions), while affording the favored group (men/whites) freedom from most repression."

Then men feel privileged (and they are) so they reproduce the system of oppression. bell hooks then argues that we must understand that the system harms also men (and if not patriarchy then the state). So feminism must also address the men question.

So do not forget that dominations are manufactured, they are not organic.

## Anarchy without Opposition (Jamie Heckert)

The problem with boxes / categories is that they create identities that wouldn't exist otherwise. For example, nobody is gay if this category doesn't exist. Furthermore, once a hierarchy arises, we are incentized to conform to the system. So defining a movement in opposition to such a category in dangerous as it legitimates it.

The author advocates for a diversity of point of views, to create a world based of the realities of everyone, to be able to listen to everyone.

> "These hierarchies aren't just 'out there'. They are also in here: in the way we hold our bodies, in our thoughts, in our emotional reactions, in the ways we learn to see the world and to imagine what is real and what is possible. These hierarchies arise in the ways we relate to ourselves, to other humans, and to the rest of the natural world. And that's okay."

That's okay in the sense that we must accept the world as it is, yes hierarchies exist and no anarchist is perfect. We need to practice and experience.

> "Here's a queer proposal: the State is always a state of mind. It's putting life in boxes and then judging it in terms of those boxes, those borders, as if they were what really mattered."

## Lessons from Queertopia (Farhang Rouhani)

Interesting case study of the Richmond Queer Space Project.

## Tyranny of the State and Trans Liberation (Jerimarie Liesegang)

Same basis as other texts: the LGBT movement has become reformist and is working with the State.

Historically, the struggle was centered on homosexuality in Europe. In the US, "it arose from the anarchist movement". Emma Goldman especially lectured on homosexual liberation which was a first in the US.
The crackdown was brutal (the Sedition Act against the anarchist after WWI and Nazi Germany attacking Hirschfeld's Institute).

From 1930 to 1969, the left was dominated by the Communist Party:

> "In theory the CP enacted the first 'Don't Ask Don't Tell policy' against homosexuals, even though many prominent sex radicals and homosexuals of the left were members of CP"

Note: The Don't Ask Don't Tell policy was enforced between 1994 and 2011 in the US army. It made possible for non-heterosexual person to serve but only if their sexual orientation was hidden.

This second wave of activism is still pretty influencial, with homosexual and lesbian publications, but was way less critical about the role of the state. It wanted to "\[improve\] homosexual's standing within a capitalistic and hierarchical State".

Major event: Stonewall riots of 1969.

> "a police raid on a Greenwich Village bar called the Stonewall Inn provoked a series of riots that mobilized drag queens, street hustlers, lesbians, and gay men, many of whom had been politicized by the ongoing police brutalization of queer street youth as well as the civil rights and anti-war movements."

And so militantism took similar shape as for other struggles with organizations like the Black Panther, the Young Lords etc

Two main groups formed after Stonewall: the Gay Liberation Front (GLF) and the Stonewall Rebellion and the Street Transvestites Action Revolutionaries (STAR).

In the next decades, we saw a distinction between LGBT movements (seen as assimilationist) and queer groups.

For the author, trans people are inherently revolutionaries as the defy the norm, the binary system. And one can hope that they choose to follow the 'unsafe' revolutionary route, rather than the assimilationist way some LGBT groups have chosen.

## Harm Reduction as Pleasure Activism (Benjamin Shepard)

One particularity of the anarchist and queer movements is the emphasize on the right to take pleasure. This happens with sex, with drug use ... And this has to be promoted alongside harm reduction.

Ideologies against pleasure have deep roots. In the XIIIth century, St Thomas Asquinas was already classifying masturbation as a crime against nature.
In the US, the Puritans are also very influencial.

> "When founder of US psychiatry Benjamin Rush was not busy experimenting with torture devies designed to treat mental illness, he railed against bodily pleasures as 'a disease of the body and the mind'."

Prohibition from 1919 to 1933 with its negative consequences. It didn't decrease the consumption of alcohol.
When the AIDS epidemic started, it was clear that abstinence was not an answer. So harm reduction meant informing people about low and high risk activities etc

> "We were able to invent safe sex because we have always known that sex is not, in an epidemic or not, limited to penetrative sex. Our promiscuity taught us many things, not only about the pleasures of sex, but about the great multiplicity of those pleasures." ACT UP veteran Douglas Crimp

The basis of harm reduction is to meet where they are. To understand what they are doing, what they are comsuming, how they're having sex, and not what you think they should be doing.

In the end, pleasure must have a central place in our struggles, as a fuel, a motivation, but also as political end, something desired.

## Radical Queers and Class Struggle: A Match To Be Made (Gayge Operaista)

This is *clearly* a marxist essay.

> "The task is for radical queers to become class struggle militants."

For the author, le struggle of the working class against the capitalist class should still be a major focus. By working less, self-organizing ... Capitalism is not just another oppression. Thinking like that could cut off queer people from the proletariat.

For the author:

> "race, gender, and sexuality were built into the working class as a means of control and hyperexploitation and as the midwife to capitalism's birth."

We should be careful about 'anti-assimilationism'. The ultimate goal is to destroy the (capitalist) system, so if we engage in anti-assimilationism, it must be for this reason, not to "keep ourselves separate from 'the straights'".
On this basis, we have nothing to do with bourgeois queer because we do not share their class interests.

At the same time, working-class should self-organize in order for the meaning of queer no to be lost or diffused out, and not to be lead by other organizations.

Except from these organizations organized by queers:

> "The task of queer communists in relation to queer movements is to place themselves into mass organizations, arguing for working class queer issues into mass organizations, arguing for working class queer issues in straight-dominated organizations, and arguing for true anti-capitalist class analysis, direct action, and unmediated struggle in queer organizations."

## Queering the Economy (Stephanie Grohmann)

Queer theory notes that until a few centuries ago, there was no fundamental differences between men and women. Yes women were oppressed but they were not part of a different category of human beings.
In the same vein, economy was not a special category that was thought of, it was part of the daily life.

the distinctions arose with capitalism:

> "Ina massive wave of expropriation, millions of peasants were forced off their land and made to work in factories that were located somewhere other than where they lived, therevy creating the now common distinction between the workplace and the home. The traditional social structures thus destroyed the new system created a new form of gender relations - while it was the men who went to earn a wage, women were supposed to stay in the home, look after the kids and provide the necessary regeneration for the working make so he could turn up for work the next day."

The world of work was on of competition was developped aggresive, rational, calculating men. the world of home was one of care etc

The author then talks a bit of marxist theory, surplus value, fetishism, ...

Everything that is not present on the market (basically not created within the capitalist society) has no value, it is devalued (e.g. the domestic sphere).

So there are two options: include women into this capitalist economy or "feminize" the economy, to opt out of this masculine capitalist economy and create something else (that may be based on care for example).

> "Both traditions \[...\] do not \[...\] challenge the very distinction between 'male' commodity exchange and 'female' economies of giving and the persistent hierarchy between the two."

An example alternative would be Free Shops, in which people give stuff they don't need anymore or take anything.

## queering heterosexuality (sandra jeppesen)

for the author, what is interesting in the queer scene is to be able to reinvent from scratch all relationships. for example, polyamory is quite popular in these radical circles. and this is quite a new field, so people have to experience new strategies, listen to each other. this is very radical because as far as heteronormativity is concerned, we just follow established conceptions of what a couple should be. the idea with polyamory is to just listen to the needs and desires of everyone and create a relationship accordingly.

another important concept is the one of community, which can replace the nuclear family. this implies friendships, love, responsability, nurturance in many new ways.

> "i believe and hope that we can queer our practices, without claiming queer as our own, or appropriating it. in other words, the idea is to support queer struggles, to integrate queer ideas into our practices, to be as queer as possible, in order to work as allies to end queer oppression. the idea certainly is not - and this is another risk - to perform queer identities when it is convenient and then return to our heterosexual privilege unchanged or unchallenged by the experience."

> "as jamie heckert argues, breaking down micro-fascisms at the level of identities and intimate relationships is at the root of resistance to macro-fascisms at the level of institutions and structures of power."

(very cool zine)

## Polyamory and Queer Anarchism: Infinite Possibilities for Resistance (Susan Song)

T. Laqueur in "Discovery of the Sexes": in the XVIIIth, we started to carefully examine women's anatomy. Before, we had a "one-sex model" where there was one name for both ovaries and testicles for example. New names were given at this time, and lots of research were made to understand the difference of women's sex in comparison with the "reference", the male body.

> "Where classical anarchism is mostly focused on analyzing power relations between people, the economy, and the state, queer theory understands people in relation to the normal and the deviant, creating infinite possibilities for resistance. Queer theory seeks to disrupt the 'normal' with the same impulse that anarchists do with relations of hierarchy, exploitation, and oppression."

So one idea will to be destroy the norm by thinking and practicing many different forms of relations. Again practicing BDSM does not make you a revolutionary but it does challenge the norm. The goal is not to make everyone engage in queer sex but at least to broaden the imagination and let us have the power to create the relationships and the sexuality we want.

> "We want more than class liberation alone. We want to be liberated from the bourgeois expectations that we should be married, that there is only a binary of men and women in rigit normative roles who can date monogamously and express their gender in normative, restrictive ways."

## *Sex and the City*: Beyond Liberal Politics and towards Holistic Revolutionary Practice (Diana C.S. Becerra)

It is important to study the impact of pop culture such as the serie *Sex in the City*, which is basically liberal feminism.

> "Gramsci developed the concept of 'hegemony' to understand the process by which the working class is socialized to think and act within the dominant framework of class elites."

SITC's liberal feminism is about choice, the choice for women to live their lives the way the want. But when choice is mentioned we have to understand what are the opportunities. Because some people are freer to choose than others.

> "Individual choice as the sole means of resistance will always be limited by hierarchical institutions that deny us or other meaningful choice."

In the show, wealthy white women are represented with almost exclusively heterosexual practices. They are of course less oppressed than others.

> "If we fail to recognize this point, we perpetuate the exploitation of marginalized people."

Capitalism, hereto-patriarchy and white supremacy feed each other, that's why we cannot prioritize "one form of oppression over another".

## Queering Our Analysis of Sex Work: Laying Capitalism Bare (C. B. Daring)

We could say that sex work nowadays is a product of capitalism (you pay for everything, why not for sex), but it is interesting that sex is always a relation between people, and power relations are never far.

> "Sex is not necessarily 'freely' had when money is absent."

Sex work is disturbing for lots of people because "the sex act" is considered sacred, and to alienate it from someone seems utterly violent.
Creating hierarchies of alienation is problematic because it can be used to justify that some capitalist work is less exploitative than others.
The idea that sex is sacred is basically used to shame people of the way they're having sex. This is a tool of control, who impose a vision of a chaste and heterosexual woman as the ideal.

Basically, sex work could be seen as another capitalist work, done by women but not only / exclusively (also trans, queer, men, ...). What is special is the relation to alienatioon which is more intimate.

> "I no more chose sex work than I chose retail, tourism or food service. We all have a spectrum of limited options to choose from \[...\] It is never an option to choose not to be exploited."

Sex work is everywhere regulated by the state. For the author, these regulations must come from the workers, and not be imposed as a form of control and criminalization.

> "Sex work fundamentally upsets this balance by requiring money be exchanged for a finite amount of time, as opposed to a housewife (or unwaged home worker) who exchanges unlimited labor for an infinite amount of time without wage."

And the state relies on the unwaged housework to reproduce society.

> "Ultimately it is the stigma that sex workers face that is unique - not the nature of their work. "

The stigma being the violence of the state and of the clients.

## Tearing Down the Walls: Queerness, Anarchism and the Prison Industrial Complex (JAson Lydon)

The prison industrial complex is compared to an abusive relationship, because it controls all the behaviors of the prisoner while limiting access "to loved ones an support structures".

As abolitionists, we must oppose (and propose alternatives to) mainstream / assimilationist gay and lesbian organizations which want to ensure their safety through more surveillance, police control, laws and prison.

> "When a gay group protests lack of police protction, by making an alliance with police to form a gay task force, they ain't making a stand against the system they are joining it." Kuwasi Balagoon, 1983

The author advocates for the study of the past (Pink Panthers, Berkman, ...), for short and long-term strategies, for relations between different movements, support people in prison, ...

Abolition is a process which can take the following steps, decarceration (putting people out of prison) and stop the construction of prison, decriminalization, excarceration (not putting people in prison) and using transformative justice

## Queer-Cripping Anarchism: Intersections and Reflections on Anarchism, Queer-ness, and Dis-Ability (Liat Ben-Moshe, Anthony J. Nocella, II, and AJ Withers)

There is a link between disability and queer activism. Homosexuality has been presented as as a disease \[a disability\] (and there was the "threat of eugenic extermination") and both movement advocate for the creation of safe / accessible spaces.

> "Ableism and hetronormativity \[note: this term seems to be chosen by the author instead of heteronormativity\] are both oppressive ideologies and cultural constructs that hinder the full potential or realizing the scope of human sexuality and modes of being in the world."

Ableism sees disability not as a difference but as inferiority.

Homosexuality is not a psychiatric illness since 1973.

> "Many LGBTQ folks today may want to distance themselves from any community that has been medicalized in similar ways, in order to cut off the historical ties to notions of pathology and abnormality."

This could explain why marginalized groups such as the disabled have been sometimes rejected by LGBTQ movements. Furthermore, the disabled people can be seen as inferior because they are seen as sexually inferior.

> "the 4 'D's of Disability Criminology - demonizing, delinquency, deviance, and dissent, - similarly critique how these labels stigmatize marginalized groups, including queers and those with disabilities."

The term 'crip' (noun and verb) had been coined to "\[resist\] the negative connotations of disability that have been bestowed upon it by an abelist culture."

Of course, hetronormativity and ableism are interconnected. For example, heterosexuality presumes able bodies, and disabled bodies are viewed as a-sexual.

It is important to note that there is no duality between being able or being disabled. Disability is a continuum and it depends on time and on the context. We are disabled if we need to read but don't have our glasses, we will be disabled one day if we live long enough, some people have learning disablities which are only visible in a scholar context.

> "Normalcy is a relatively new concept, which arose as part of th emodernity project in 1800-1850 in Western Europe and its North American colonized spaces."

Before that, especially in the Roman-Greek culture, there was this idea of ideal, which is an unreachable perfection. Through this lens we all are imperfect to various degrees. Normalcy is related to the average:

> "Normalcy began with the creation of measurements and statistics. Qualities are represented on a bell curve, and the extremes of the cruve are abnormal."

This was developped by the State (state-istics), to make individuals and groups governable.

> "all the early statisticians (Galton, Pearson, and others) were also known eugenicists."

Normalcy is the ideology behind normality. For Davis, it is not an unusual practice for the modernist project ("the creation of modern nation states, democracy, measurement and science, capitalism"). Normalcy helps justify this project. If wealth is looked at on a bell curve, then it is ok that lots of people are in the midlle but some are also very poor and some are very rich. Very poor people are then just a normal part of the system.

> "There is a need for people at the margin, but they are punished for being placed there."

Disability is also strongly linked to capitalism because the disabled can't work as the others, they are then stigmatized as useless / unproductive.

Some kinds of "services" and "healthcare" are devoted to disabled persons but this is often capitalistic work, as with psychiatry. There is a market worth a lot of money but the people are more in prison than being taken care of.

> "Much of the anarchist tradition rejects the ideology of individuality and focuses on mutual aid, or in queercrip language, interdependence."

A queercrip lens rejects some forms of (green-)anarchism focused on DIY and individuality. It views independence as a capitalist value (we are independent and relations are only economic). The focus should instead be on DIT, do it together, because we are interdependent.

Radical circles should also be careful with inclusivity. It is not possible to have conferences whose access if after a flight of stairs, and which don't provide "interpreters, note-takers, and accessible bathrooms and entranceways". March are also exclusive.

## Straightness Must Be Destroyed (Saffo Papantonopoulou)

When we think about it, it's crazy that just after birth (or sometimes before), we assign a gender to a baby.

> "Our heteronormativity society considers it urgent that every*body* be labeled 'boy' or 'girl' - to the point that it will not shy away from using violence against intersex bodies in order to apply these labels."

> "This label dertermines everything from what sort of toys we are supposed to play with, to what sorts of expectations are to be placed upon us in social situations, to what kind of sexual desires we are supposed to have. We cannot escape these assumptions; they are everywhere. Daring to transgress these norms can result in violent punishment. These norms, and the threats that back them up, are all a part of straightness."

The destruction of straightness would mean the destruction of the ideal man and woman which are the directions in which people are raised. This would not mean the end of heterosexuality.

Straightness seems here to be taken as an equivalent for heteronormativity, the opposite being queerness.

The follows a list of interesting examples which show that internalized straightness can be a liability or even a weakness or a threat in many cases.

> "There is an old Situationist slogan that calls to 'Kill the Cop Inside Your Head'."

## Anarchy, BDSM and Consent Based Culture (Hexe)

BDSM:

> "It is derived from the terms bondage and discipline (B&D or B/D), dominance and submission (D&S or D/s), and sadism and masochism (S&M or S/M)."

Consent is essential.

> "Emma Goldman was a really huge badass."
