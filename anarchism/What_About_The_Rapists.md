TW: rape

# what about the rapists ? anarchist approaches to crime & justice

## On Crime, By (A)legal

"Crime is the term used by the state to designate behaviour it will not tolerate". Solving the actual problems of capitalism and states could efface crimes related to private proverty, fraud, migration, etc...

> "it is delusional to think that this implies a world without transgressions or oppressive behaviour"

And solving these questions is important although too often neglected by anarchist theories. Fighting capitalism is a necessity, but so is fighting capitalist values. This will be a gradual change and "we need to build anarchist cultures here and now, fighting acts of domination and abuse every day".

We need to fight the judiciary system which is especially hard on dominated people (black, poor, ...). But fighting prisons should also be done while seeking "to address every woman's fear of gendered and sexual violence".

"A transgression is the violation of a social norm". Transgressions of social norms are not per se a problem and can be valuable.
Domination is defined by Foucault as relations that are "fixed in such a awy that they are perpetually asymmetrical and allow an extremely limited margin of freedom".

Rather than fighting "crime", we could thrive to fight domination (which is structural) and abuses of power ("exploitation of superior power in a given moment").

## Part One: Transformative Justice (TJ)

### Beautiful, Difficult, Powerful: ending sexual assault through transformative justice (The Chrysalis Collective)

TJ is about healing a survivor, and a community, confronting the perpetrator, and transforming him.

The first step is to gather a survivor support team (SST) and define what are the needs of the survivor and what will be the goals of TJ.
Then a team is also being created around the perpetrator (called Tom here), an accountability team (AT).
The goal of these teams is to create safe spaces.

The SST is focused on the survivor's needs and in contact with the AT. The AT is also "commited to a survivor-centred praxis at all times". It works with Tom "to achieve accountability and transformation" with compassion.

The two groups form a continuum allowing indirect communication between Tom and the survivor.

The AT plan includes: ideas to approach Tom and prevent backlashes, guidelines to work with Tom, how to build trust, make him overcome denial and understand that his position has structural power over the survivor's, ...

This work is really important before approaching Tom. The idea is to have trusted comrades of Tom in the AT in order to be sure he wants to take part in the process. Havin him participate is crucial.

Regular meetings between the AT and Tom (and AT and SST) then need to focus first on the safety of the survivor and then on the transformation of Tom. The idea is to challenge rape culture (because we are talking about this) step by step on focus on the point of view of the survivor. The perpetrator must be accompanied at his own pace, which is no easy task.

AT & SST want to transform Tom, to make him recognize his acts and at the end to commit to acts of restitution to Diane and the community. The final is obviously the healing of the survivor and the community.

### Accounting for Ourselves: Breaking the Impasse Around Assault and Abuse in Anarchist Scenes (CrimethInc.)

> "Sexual assault is political; it is a function of patriarchy, not just an individual harm done by individual people (usually men) to others (most often women)."

accountability process:

> "collective efforts to address harm - in this case, sexual assault and abuse - that focus not on punishment or legal 'justice' but on keeping people safe and challenging the underlying social patterns and power structures that support abusive behavior."

These processes can take different forms (such as the one described above) and should adapt depending on the situation and the need of the victim.

Sexualt assaults have always been a problem, in anarchist circles as everywhere else. Historically womyn had to deal themselves with it. Collectives started organizing in the 2000s and "Assault, accountability, and consent became topics at nearly all anarchist conferences and gatherings."

CrimethInc lists some points to show how hard in pratice it is to make TJ real:

- "There is no clear sense of when it's over, or what constitutes success or failure", "if we're going to invest so much time and energy in these processes, we need a way to assess if it's worthwhile, and when to admit failure."
- "Standards for success are unrealistic", we are talking about lifelong processes.
- We lack the collective ability to realize many demands. (how to provide a safe space?)
- We lack skills in counseling, mediation, and conflict resolution.
- This stuff depresses people and burns them out.
- Accountability processes such up disproportionate time and energy.
- Subcultural bonds are weak enough that people just drop out.
- Collective norms encourage and excuse unaccountable behavior, eg anarchists organizing big parties with alcohol and other drugs create "highly sexualized spaces with strong pressure to get intoxicated", quite a good space for sexual assaults... So we need to rethink our cultural norms
- The residue of the adversarial justice system taints our application of community accountability models.
- Sexual assault accountability language and methods are used in situations for which they were not inteded (like conflict resolution)

What can be some solutions then?

One is physical confrontation (vigilantism). It is a quite way to achieve a goal: claim out loud than sexual assaults are unacceptable.
Some problems can arise though. First, state's justice can be severe against women (more than for an alledged sexual assault). Second, will violence make the survivor felle better ? Will it prevent future assaults or make men less violent ?

> "Outside of anarchist circles, prevention work around gender violence usually centers on education: for women, around self-defense and harm reduction; for men, around combating rape myths and taking responsability for ending male violence; and for all, healthy communication and relationship skills."

Prevention can of course be gender-based, because "distinc patterns of oppressive behavior and power still fall pretty predictably along gender lines."

An important question is of course how to define a community because:

> "You can't have community accountability without community."

Because unacceptable behaviours are harming a community and TJ aims at healing both the victim and the community. How does this work if the perpetrator is a total stranger?

We should think of relations with others as "concentric circles of affinity" and adapt TJ (or other reponses) to the circle in question.

## Part two: Retribution

### "Anarchist" Rapist Gets the Bat: We'll Show You Crazy Bitches Part II

> "jacob onto is a piece of shit rapist. we are tired of accountability processes that force the survivor to relive, over and over, the trauma of assault"

> "we will respond to sexual violence with violence"

> "we rolled in with a baseball bat. we pulled his books off his shelves: he admitted it, not a single one mentioned consent. we made him say it: 'i am a rapist'. we left him crying in the dark on his bed: he will never feel safe there again."

> "this is the beginning of a new kind of accountability process, one that leaves the perpetrator in pain"

### i. Communiqué

The writer denounces the sexual assaults in the anarchist milieu, despite men having a fminist façade.
Violence is a valid tactic:

> "When he refused to shut up, we shut him up. The intent was to inflict pain"

## Beyond Revenge & Reconciliation: demolishing the straw men, (a)legal

Retributive justice: respond to a transgression with some kind of harm (prisons, community service as well as vigilante campaigns and survivor-led action).
Violent action (physical, public shaming, ...) can be interesting by "its cathartic and liberatory aspects". But "Do we want to have a culture in which we're disciplined by fear of shame and rejection?"

> "Despite the potential dangers of retributive action, if used in appropriate situations ans with a critical analysis of power, it may be key to establishing safety, empowering survivors, warning potential abusers, and realising the collective strength of oppressed groups."

TJ can also be a valid reponse, and has its own advantages and limitations. Basically, the question always boils down to on which person are we willing to take time to create a TJ process, and who is not worth it.

The zine gives some guidelines in order to choose between Reconciliation (the accused remains in the group) and Expulsion. They are based on the gravity of the harm caused, the relations in the group etc (see p73/74).

> "We should always be starting from a position of believing the survivor and tking steps to immediately establish their safety. Yet that does not mean that allegations can never be called into question."

At the same time, it is always a good idea in communities to have spaces for healing, to learn self-defence and unlearn patriarchal behaviors.

> "No process is going to be free of pain and distress, but if we are to have some degree of satisfaction with their outcomes whilst minimising their impact on our collectives, we need to forgo dogmatism, question our assumptions &  objectives, and critically experiment with a range of tools."
