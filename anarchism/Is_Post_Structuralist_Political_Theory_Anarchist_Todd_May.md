# [Is Post-Structuralist Political Theory Anarchist ?](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) (Todd May)

Post-structuralists (in particular Foucault, Deleuze and Lyotard in this article) have given tools and analysis about different oppressions but it is difficult to view their general political philosophy.
For May, you got to have a set of values first, then it is possible to construct a political philosophy upon it. This is not done by post-structuralists that basically critize liberalism and marxism and try to provide a better understanding of relations of oppressions.

> "That they have rejected Marxism as an adequate account of our social and political situation is clear."

The idea of May is that post-structuralist thought fits well in the framework of anarchist political philosophy.

## Anarchism

> "Anarchism can be defined as the struggle against representation in public life. Representation, as a political concept, is the handing over of power by a group of people to another person or group of people ostensibly in order to have the interests of the former realized."

Marxism was first thought of as a critic of capitalism, this oppression being the father of all other oppressions and the locus of struggle. Anarchism can be better defined as the fight against all oppressions, which were identified by the first classical anarchists (such as Bakunin) as being mainly the State, capitalism, and the Church. But anarchism has always had the potential to include many other struggles because of its fundamental refusal of all oppressions and representation.

> "thus, in all areas of an individual's social life, anarchism prommotes direct consensual decision-making rather than a delegation of authority."

Anarchism is then bottom-up and decentralized while marxism's aim is to conquer the state in a centralized manner. Moreover,

> "As anarchists have pointed out [...] means are not to be divorced from their ends."

For anarchists, the individual must be free and cannot be oppressed by a greater cause :

> "What anarchism resists are the many ways in which the individual becomes subordinated to something outside him or herself. [...] Whether it be 'the good', 'the march of history' or 'the needs of society', anarchism is suspicious of ideals that function to coerce individuals into subordinating themselves to a larger cause."

There are then two cardinal values, freedom and equality, because freedom without equality means that some have more freedom than others (let's say the poor is less free than the rich), and equality without freedom "means that we are all slaves together".

> "Here lies the *a priori* of traditional anarchism: trust in the individual. From its inception, anarchism has founded itself on a faith in the individual to realise his or her decision-making power morally and effectually. [...] In this sens, the distinctive feature shared by all institutions that oppress - political, economic, religious, patriarchal, or other - is the repression of individual potential."

## Post-Structuralism

> "Decentralization, local action, discovering power in its various networks rather than in the state alone, are hallmark traits of post-structuralist analyses."

However, one major point of post-structuralist theory is to criticize the notion of individual / subject. This notion is viewed as a construction, there are no individuals that waits to fulfill their potential in the nature, individuals are created by power relations. This seems to be in contradiction with anarchism which focuses on individual autonomy.

For Foucault, networks of knowledge / power are constituting subjects. Power does not repress but creates.

> "what has been read as a journey of scientific discovery can as easily be read as an increasingly subtle display of disciplinary technique."

> "In the nexus of science and discipline, the subject as such is being constituted."

So when anarchism focuses on a subject to free, post-structuralism tells us that the subject actually doesn't exist but is being constituted by the power relation. The subject is actually a norm, and focusing on a subject is reproducing the norm. For instance, we could say that proletarians do not exist outside capitalism, and women don't exist outside patriarchy (or heteronormativity).

Post-structuralism focuses on micropolitics and the analysis of every type of power relation on a small scale.

> "For traditional anarchism, abnormality is to be cured rather than expressed; and though far more tolerant fo deviance from the norm in matters of sexuality and other behaviors, there remains in such an anarchism the concept of the norm as the prototype of the properly human. This prototype, the post-structuralists have argued, does not constitute the source of resistance against oppression in the contemporary age; rather, through its unity and its concrete operation it is one form of such oppression."

> "Post-structuralism leaves the decision of how the oppressed are to determine themselves to the oppressed; it merely provides them with intellectual tools that they may find helpful along the way."

In conclusion,

> "post-structuralist theory is indeed anarchist. It is in fact more consistently anarchist than traditional anarchist theory has proven to be."
