# Ownership and Nurture: Studies in Native Amazonian Property Relations (Marc Brightman, Carlos Fausto, Vanessa Grotti)

## Introduction: Altering Ownership in Amazonia (Marc Brightman, Carlos Fausto, Vanessa Grotti)

Ownership is a concept that first seemed absent in Amazonia, mostly because of the period of the studies (mid 1900s) where colonization was weakening the communities and because of projections of western concepts on these peoples.

### Ownership in Perspective

Occupancy: Roman concept which designs the fact that someone appropriates for himself something that belongs to no-one. One important assumption here is that ownership is concerning individuals. Historically this idea has been more and more predominant after the rise of capitalism.

The concept of ownership has been worked upon a lot after the discovery of America and its native inhabitants. How to justify taking their lands? Well, ccertain were conceived as natural-born slaves, so without any right to ownership. For the others, Locke found an argument: god gave humans lands for them to be cultivated. Humans possess lands only if they mix their labor with it.

Western conceptions of ownership have been shiped everywhere around the world. The problem is that when you try to force indigenous concepts into western laws it cannot fit. Let's say that, as Strathern thought, property should be framed in terms of relations between people, or that

> "property relations are less relations between people with regard to things than they are rights over relationships"

Then this conception of property is no longer a static concept and cannot enter Brazilian's law.

### Property in Amazonia

Lowie is one of the first who wrote about the incorporeal properties in Amazonia. Several theories started to challenge common ideas on ownership. Some started to challenge absolute ownership, others view ownership as a process with a special relation with time.

> "In Amazonia, the transformation of space to make gardens, for example, involves cutting a network of living beings and extending networks of domesticated plants; thus, the ownership of nonhuman persons is part of the process of place-making. Creativity begets ownership, and the making of artefacts may create new persons who may be in turn become owners themselves. Such processes of creative appropriation are, as we shall see, very close to familiarizing processes of nurture."

The role of perspectivism (humanhood is not reserved to humans basically) is also important, because objects are not only objects:

> "To give one example, among the Mamaindê, a carrying basket abandoned in the forest can 'become a jaguar and return to attack its owner'. Such objects 'must be constantly "domnesticated" lest they turn into animals'."

### Nurture and Relation of Dependence

Studies on nurture in Amazonia in pretty important, 'alimentary relations' being one major constituent of social life.

> "The phrase 'alimentary relations' must here be understood broadly, for it includes not only feeding or sharing *with* someone, but also eating or being eaten *by* someone."

This happens in a variety of ways: feeding a child, taming a wild animal, sharing food, feeding a captive, ...
These relations are in their essence asymmetrical, they produce a certain degree of difference and diversity in the community. This diversity is actually something wanted and is seen as sane (in opposition to the uniformity associated to incest). And note that asymmetry doesn't have to be expressed as inequality.

### Ownership, Authorship and the Self

To highlight how difficult the problem of ownership can be:

> "Songs are given by enemies in dreams to men and are generically called 'jaguar'. A dreamer is said to be a 'master oa jaguar' (*jawajara*), meaning that he is the owner of both songs and the dreamt enemy, conceived as his pet. Owners never kill their pets, so the ritual act of dancing and singing is considered killing: the dreamer cannot perform his own song in the ritual but must give it to a third person who will be the jaguar-song executioner. Who, in this case, would then be the author of the song?"

So instead of ownership, alterity is a good concept. One does not create a song, s·he alters relations and the result is a song, which has not one author but which is linked to different people through different relations.

## Chapter 1. Masters, Slaves and Real People: Native Understandings of Ownership and Humanness in Tropical American Capturing Societies (Fernando Santos-Granero)

Property has long be seen as the root to inequalities (Rousseau, Proudhon). Pierre Clastres had much of the same argument: if Amazonian societies are egalitarian, it is because they refuse the concept of property.

But property was known. In the roman sense, it is based on three characteristics:

- *usus*, the right to use or possess the property
- *fructus*, the right to enjoy the fruits of property
- *absus*, the right to abuse or alienate the property by transforming it, transferring it, or even destroying it.

The point of the author is that "Amerindian masters had total power over their war captives" under the three forms described above. War captives can be seen as property because:

- Enemy peoples were seen as inferior
- Captives are forever in debt to the captor who spared their lives
- The captor acquired captives by his effort, as he would acquire a house or clothes.

But there are also major differences with the Western concept of property:

> "In native tropical America, property relations are grounded as much in capture and predation as in nurturing as familiarization."

### Taxonomies of Humanness and Alterity

These societies are slave-capturing ones. The slaves are coming from other peoples (*exo-servitude*).
A large number of beings are assumed *persons* (including some animals and objects) but some people are more human than others, a people usually refers to itself as the 'true' or 'real' people. The lesser humans are then "prone to be enslaved". They are characterized by cultural difference (they live a wild life, do not speak the 'true' language, do not pratice certain rituals such as excision or head elongation, ...).

### Captivity as a Gift for Life

During raids, old men and women were usually killed. Men, warriors, were either killed to get "bodily trophies" or eaten and women and children would be enslaved.

> "By sparing their lives, captors forces their captives into a debt relation that ended only with death."

### Productive Agency and Ensoulment

Property usually stems from human creation in a broad sense or productive agency. It corresponds to the making of material objects as well as songs, spirits, children, prey or fishing expeditions. But

> "one cannot own the land, the rivers, the forest or the wild animals - none of which are of human creation."

During the creation process, the creator gives a bit of his soul to the "object", "artefacts are often described as the 'children' of their makers". The result is in part an extension of the maker's body.

So slaves can be viewed as the children of the captors who risked their life to capture them, and will put effort into raising them.

> "As quasi-animals they had to be tamed; as quasi-humans, they had to be civilized. \[...\] it could be said that indigenous slavery was as much about turning people into pets as about turning pets into people."

### Mastery and Absolute Property Rights

> "indigenous owners do not always have the right to *abusus* (alienation) over everything they own"

But often, masters had every right on their captives: make them do the heavy work, punish them, rape them, barter them, kill them.

### Consubstantiality and the Unmaking of Slave Property

> "Slaves but more often their descendants in the first or second generations, were eventually incorporated into their captor's society through marriage or adoption."

It seems that by sharing food and eating it from one plate, captives become part of the captor's kin. Of course, eating food alone couldn't make a captive a 'true human'. Learning the new language and etiquette was not especially hard but some cultural criteria were sometimes impossible to meet (like head elongation).

### Final Remarks

> "for native tropical American socities, the slave condition was less an opposition between freedom and lack of freedom than one between humanity and non-humanity, sociality and non-sociality."

Slavery didn't exist for economic purposes, its goal was to get as much life force as possible from the Others: scalps for fecundity, people for demographic purposes.

> "the ultimate aim was the production of kin \[...\] making Others into kin."

## Chapter 2. First Contacts, Slavery and Kinship in North-Eastern Amazonia (Vanessa Grotti and Marc Brightman)
