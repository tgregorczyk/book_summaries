# La pensée straight (Monique Wittig)

## La catégorie de sexe

> "comme il n'existe pas d'esclaves sans maitres, il n'existe pas de femmes sans hommes."

Toute domination est encodée par les dominants dans la politique et l'idéologie et résulte en des différences matérielles et économiques. Ces différences sont justifiées par des arguments biologiques mais les dominé·es qui se révoltent comprennent bien la nature sociale des dominations. Wittig fait le parallèle entre lutte des classes (marxiste) et la lutte des classes qu'elles souhaitent pour les femmes (contre les hommes). La dialectique matérialiste c'est rendre visible l'opposition entre classes et l'abolir.
Le constat est simple : les femmes sont dominées. Elles fournissent la majeure partie du travail et sont brutalisées par les hommes. Elles ne savent pas qu'elles sont soumises aux hommes, ont du mal à prendre conscience de ce fait.

> "De leur côté, les hommes savent parfaitement qu'ils dominent les femmes, \[...\] et ils sont formés pour le faire."

La pensée dominante, la pensée de la domination justifie l'état de fait par une distinction naturelle.

> "La catégorie de sexe est une catégorie politique qui fonde la société en tant qu'hétérosexuelle."

La société hétérosexuelle produit la catégorie de sexe car elle a besoin de femmes pour reproduire la société économiquement. Tout comme le capitalisme crée des prolétaires.

> "la fabrication des femmes est semblable à la fabrication des eunuques, à l'élevage des esclaves et des animaux"

Wittig compare le contrat de mariage à un contrat salarié. La femme appartient à son mari. Dans l'espace public, les femmes sont rendues extrêmement visibles sexuellement mais sont invisibilisées socialement, "elles doivent se faire aussi petites que possible et toujours s'en excuser".

Les femmes sont réduites à leur sexe tout comme les esclaves étaient réduit·es à leur couleur de peau. Aujourd'hui, on ne demande plus à ce qu'on déclare sa couleur à l'administration, on voit cela comme une discrimination. Wittig voudrait la même chose pour le sexe, l'abolir.

## On ne nait pas femme

Il faut vraiment se défaire des justifications naturalisantes. Tout comme le concept de race dans son acceptation actuelle nous vient de l'esclavage (cf Colette Guillaumin), le concept de sexe n'est qu'une marque de la domination : arrêtons de le voir comme une donnée de fait.
On peut bien associer la femme à sa capacité à enfanter, mais imaginons une seconde une société lesbienne et cela s'écroule. Être lesbienne, c'est ne pas appartenir à cette catégorie de femmes que la société a construite. Il faut donc refuser d'être femme contrairement à ce que disent certains courants féministes qui célèbrent la femme (donc qui célèbrent) les bons côtés qui ont été attribués à la catégorie femme. Pour Wittig, être féministe c'est lutter pour la disparition de la catégorie de femme, et non pour son maintien et l'augmentation de ses droits. Il faut distinguer la femme en tant que classe de la femme en tant que mythe.

> "Notre combat vise à supprimer les hommes en tant que classe, au cours d'une lutte de classe politique - non un génocide."

Le problème du marxisme vient du fait qu'il refuse l'individu, un individu sujet de sa lutte. Il n'y a que la masse, et les personnes luttent pour le parti / la révolution etc
Il faut au contraire que les problèmes subjectifs et individuels soient vus comme des problèmes sociaux.

Il faut détruire cette catégorie de sexe. Le lesbianisme est intéressant car il constitue un cadre dans lequel les femmes sont libres. Car les lesbiennes ne sont pas des femmes, les femmes se définissent par leur relation (de domination) aux hommes.

> "Nous sommes transfuges à notre classe de la même façon que les esclaves 'marrons' américains l'étaient en échappant à l'esclavage et en devenant des hommes et des femmes libres, c'est-à-dire que c'est pour nous une nécessité absolue, et comme pour eux et pour elles, notre survie exige de contribuer de toutes nos forces à la destruction de la classe - les femmes - dans laquelle les hommes s'approprient les femmes et cela ne peut s'accomplir que par la destruction de l'hétérosexualité comme système social basé sur l'oppression et l'appropriation des femmes par les hommes et qui produit le corps de doctrines sur la différence entre les sexes pour justifier cette oppression."

## La pensée straight

Le monde des sciences sociales s'est largement construit sur l'étude des discours. Un de ses champions est la psychanalyse. Il écoute le discours et, en expert, interprète, délivre les sens cachés. Mais ce champ est dominé par le discours omniprésent de l'hétérosexualité et nient les réalités des lesbiennes et des hommes homosexuels. Ces spécialistes et autres experts en sémiologie semblent vivre dans un monde des idées qui nient les réalités matérielles d'oppressions. Car pour les dominé·es, le discours est oppression.
Ces sciences sont dominées par un cadre théorique qui a continué de survivre malgré le fait que l'on ait déclaré que tout est culture et non nature. Ce cadre, *la pensée straight* (en hommage à *la pensée sauvage* de Lévi-Strauss) fixe la catégorie, hommes, femmes, et de leurs relations qui sont les bases de nombre de théorie.

> "Ayant posé comme un principe évident, comme une donnée antérieure à toute science, l'inéluctabilité de cette relation, la pensée straight se livre à une interprétation totalisante à la fois de l'histoire, de la réalité sociale, de la culture et des sociétés, du langage et de tous les phénomènes subjectifs."

Les dominants créent des catégories et maintiennent leur pouvoir en les contrôlant. Les catégories d'hommes et de femmes sont politiques et il faut les abolir.
Wittig critique les structuralistes (en particulier Lévi-Strauss et Lacan) qui résument des sociétés avec de grands concepts (structures, échange, discours). On en vient à dire que si des femmes sont échangées c'est le fait de l'inconscient collectif, structuré par la culture. Les individus échappent à la responsabilité de leur conscience et produisent les schémas nécessaires à la reproduction de la société. La domination est ainsi niée. Or domination il y a, et tous ces concepts restent imprégnés de la pensée straight.

Les lesbiennes sortent de cette relation homme-femme, relation qui définit la domination des femmes, relation qui crée la catégorie de femme.

> "Les lesbiennes ne sont pas des femmes"

## À propos du contrat social

Wittig compare la situation des femmes à celle des serfs, enchainé·es à une terre / maison et obligé·es de servir. À ces deux catégories peut s'appliquer un contrat social, un contrat individuel que l'on accepte ou que l'on fuit. Il faut donc regarder ce contrat, prendre conscience de ce qu'il signifie dans la société dans laquelle on vit. Ce contrat, c'est un contrat hétérosexuel qui rythme nos vies, impose le couple hétérosexuel, le mariage, les enfants ...

> "Et vivre en société c'est vivre en hétérosexualité."

Wittig finit par donner un nouveau coup dans la théorie de l'échange des femmes de Lévi-Strauss. Elle considère que cela représente bien le contrat exclusivement masculin qui asservit les femmes qui en sont exclues.

## Homo sum

L'intérêt de la théorie de Marx et d'Engels est un dépassement de la dialectique hégélienne. Les oppositions deviennent un conflit, une lutte qui doit mener à leur résolution. Mais la théorie marxiste est réductionniste en ne considérant que la lutte des classes, les autres dominations devant attendre l'après résolution.
Un problème ancien (depuis la Grèce antique) est présent dans notre conception de la dialectique. Si certaines oppositions ne relèvent que de l'observation (droit / courbé), d'autres relèves de l'interprétation, de la métaphysique. Ainsi, déjà Aristote mettait dans son tableau "male" et "bon" ensemble en opposition à "femmelle" et "mauvais".

Dans les révolutions qu'a connues l'histoire, il s'est généralement produit des substitutions. Les dominants ("L'Un") et les dominés ("L'Autre") se sont intervertis. Pour le moment pas résolution et d'abolition des catégories. Et les femmes restent des Autres.

## Paradigmes

> "Si le désir pouvait se libérer, il n'aurait rien à voir avec le marquage préliminaire par les sexes."

> "toute différence fondamentale (y compris la différence sexuelle) entre des catégories d'individus, toute différence qui se constitue en concepts d'opposition est une différence d'ordre politique, économique et idéologique \[...\] L'hétérosexualité est une construction culturelle qui justifie le système entier de la domination sociale fondé sur la fonction de la reproduction obligatoire pour les femmes et sur l'appropriation de cette reproduction."

> "Femme, féminin sont des termes qui indiquent sémantiquement que la moitié de la population a été exclue de l'humanité."

> "On doit bien reconnaitre que le lesbianisme a du représenter une menace très grave pour justifier d'une telle persécution, de fait, une oblitération totale, la plus spectaculaire quiait été entreprise dans l'histoire avec celle des Amazones."

## Le point de vue, universel ou particulier

L'écriture féminine ça n'existe pas, c'est simplement reprendre les stéréotypes qu'on a créés pour la catégorie femme.

Il n'existe qu'un seul genre, le féminin, le masculin n'étant pas un genre mais le général.

Wittig parle de la littérature (lesbienne) de Djuna Barnes et de l'impression de décentrage que sa lecture produit. Elle invite à ne pas attirer ces textes et les garder dans l'espace lesbien mais plutôt de les laisser dans la littérature. Tout comme *À la recherche du temps perdu* de Proust (dans le sujet iest l'homosexualité), les textes de Barnes sont ultra-minoritaires de part leur sujet (le lesbianisme), ils détonnent dans la littérature.

> "Qu'y a-t-il en littérature entre Sappho et *Ladies Almanack* de Barnes ? Rien."

______________________________________________________________________

Les essais suivants sont tout aussi intéressants mais plus flous pour moi, et je ne vais pas essayer de les résumer. Ils parlent de littérature, de language, de la façon dont la langue, matériau de l'écrivain·e, est marqué par l'hétéronormativité / la pensée straight. Ce matériau n'est donc pas brut.
Et donc le language est un champ de bataille, et chaque oeuvre qui défit les normes, un cheval de Troie.
La façon de genrer la langue est également une question importante. Wittig semble pencher pour essayer d'universaliser la langue, abattre les genres, en utilisant notamment le pronom "on", même si cela ne constitue qu'une étape provisoire. Dans la langue française, le masculin est l'universel, le général, le féminin, le particulier. La tendance actuelle qui vise à remettre le féminin au même plan que le masculin (donc en le sortant de son universalité pour le réserver aux hommes) accentuent pour Wittig l'opposition à abolir et ne va donc pas dans le bon sens.
