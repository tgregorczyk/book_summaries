# Deleuze - Guattari : une philosophie des devenirs révolutionnaires (Igor Krtolica)

## Chapitre 1 : Désir, nature, politique

Pour Spinoza, il est extrêmement étonnant que les personnes dominées n'aient pas plus la volonté d'être libre. Il s'inscrit contre le mouvement contractualiste (Hobbes qui prétend que la puissance naturelle des individus se transfert au roi via les institutions) et dans le mouvement antijuridique / naturaliste. Pour D/G, la question est donc de laisse libre cours au désir, le plus possible, et de créer des organisations sociales qui vont dans ce sens.

Le problème de la servitude volontaire revient régulièrement chez les philosophes. Chez Nietzche, Reich (qui voit l'adhésion à la montée du fascisme), et D/G qui observent une contre-révolution capitaliste après Mai 68.
La situation des années 20/30 et de la montée du fascisme est particulièrement intéressante pour D/G car on voit les masses désirer ce mouvement activement alors que les tendances socialo-révolutionnaires semblaient avoir le vent en poupe. Reich étudie ce phénomène en inventant le "freudo-marxisme". Georg Lukacs se place dans le champ du marxisme occidental et montre que les conditions matérielles (l'infrastructure) ne suffisent pas à développer une conscience de classe, il faut également s'intéresser à l'idéologie et à la répression bourgeoise, la superstructure idéologique.
Reich essaie de faire dialoguer la psychanalyse, en l'extrayant de la seule étude des névroses, et le marxisme, sans s'intéresser uniquement aux conditions matérielles. Les névroses sont donc dues à un refoulement sexuel dès l'enfance, refoulement qui est opéré par la famille bourgeoise. Et c'est ainsi que l'on peut expliquer comment l'idéologie et la répression bourgeoise et capitaliste sont intégrées par les prolétaires. Reich argue donc de la nécessité d'une révolution sexuelle en même temps qu'une révolution marxiste. Là ou le fascisme a réussi, c'est dans "l'adhésion érotique et mystique au chef." Cela va contre la conclusion de Freud qui jugeait impossible de réaliser une société non-oppressive.

La théorie fondamentale de D/G est qu'il faut dépasser le dualisme du freudo-marxisme qui considère le désir comme quelque chose de naturel qui est réprimé par l'idéologie. D/G montrent comment le désir est également un produit social.

L'expérience de la schizophrénie est fondamentale pour D/G. On peut définir deux phases à l'expérience désirante schizophrènique : la percée et l'effondrement. La percée c'est une perte de lien avec soi-même, c'est une expérience ou l'on est inspiré par tout ce qui nous entoure, par tous les êtres, par toute l'histoire. C'est une expérience désirante pure, qui n'est pas vue sous le prisme de la maladie. L'effondrement, c'est la répression de cette folie, de ce désir, c'est la percée qui ne peut pas se faire et lea schizophrène qui se prend un mur.

> "l'interruption du processus schizophrènique se trouve elle-même favorisée par la tendance qu'à la société à assigner les individus à un moi déterminé, et qu'à la psychiatrie à enfermer les fous pour les exclure du champ social."

Le problème est donc de "réussir sa folie". Il faut partir de l'expérience schizophrènique, "expérience pure du réel", pour interroger toute formation sociale et les possibles détournements de la production naturelle-désirante.

## Chapitre 2 : La répression sociale et le capitalisme

Si le désir est social, il faut arriver à comprendre comment le désir peut se réprimer lui-même. Cela semble étrange, mais pour D/G cela peut s'expliquer du fait qu'il existe deux méchanismes de production différents, la production sociale et la production désirante.
Lorsque des éléments ou des flux se rencontrent, ils forment une instance d'anti-production, une organisation provisoire des flux. Cette organisation oscille entre deux pôles : le pôle schizophrénique-nomadique, dans lequel l'anti-production se retrouve réinjectée dans la production sans acquérir d'autonomie (on parle de déterritorialisation), contrairement au cas du pôle paranoiaque-despotique, ou l'anti-production acquiert une autonomie (on parle de reterritorialisation). C'est dans ce dernier cas qu'il y a production sociale, l'instance d'anti-production prend le nom de socius. On a donc deux niveaux, la production désirante schizophrène et immanente et le socius, transcendant, qui vient contrôler ce désir libre, ce corps sans organe. Il convient donc d'étudier différents socius (primitif, étatique, capitaliste, ...) et leur rapport au processus de production désirant.

> "quelle place une société donnée fait-elle au processus de la production désirante ?"

Le processus de production impliquant l'anti-production, il y a toujours répression sociale. Il faut renoncer au désir naturel, et à la fin de l'histoire. Le socius se distingue de la production désirante dans sa capacité à coder les flux. Dans les sociétés primitives, les flux sont codés, on crée ainsi la valeur des choses et leur agencement dans la société, les sphères d'échange par exemple. Pour D/G, l'État n'est pas vu comme une évolution, mais comme une possibilité immanente à toutes les sociétés, d'ou l'explication de Clastres montrant que certaines sociétés sans État sont en réalité créées contre l'État. L'État est vu comme l' "accomplissement de la tendance à la reterritorialisation".

> "Le capitalisme présente deux différences décisives avec l'État : il ne surgit pas d'un coup, mais se constitue lentement ; il n'incarne pas le maximum de transcendance de l'antiproduction sur la production sociale, mais le moment ou l'antiproduction tend à se confondre avec le champ social."

En effet, le capitalisme se targue de libérer les flux, les frontières, de lever les contraintes. On peut dire que le capitalisme *décode* les flux pour obtenir un flux de travail, "pur et abstrait" et un flux de capital "pure puissance d'appropriation". La capitalisme se réapproprie donc des flux décodés pour qu'ils servent sa logique de profit. D/G parlent d'axiomatique, de "mesures économiques, juridiques et politiques garantissant la propriété privée", ainsi que tout un cadre financier et législatif qui permet au capitalisme de se reproduire.
Le capitalisme va donc décoder des flux (par exemple en changeant le modèle agraire de l'ancien régime, en rendant possible l'appropriation des moyens de production ...) pour créer les flux libres dont il a besoin (travail / capital). Pour éviter que ces flux partent dans tous les sens, il créer des axiomes, des contraintes qui vont permettre au capitalisme de se reproduire. On peut dire que le capitalisme déterritorialise les flux pour les reterritorialiser dans sa logique de profit.
L'État est mis au service de ce système, que ce soit dans sa forme despotique ou dans la social-démocratie. Tout fonctionne comme si la production sociale et la production désirante ne faisait plus qu'un, que tous les flux étaient dirigés pour le profit. Comme si les seuls désirs subsistants étaient des désirs rentables. Comme si toutes les contraintes présentes dans toutes les autres sociétés, contraintes qui permettent de faire la société et de la reproduire, avaient sauté. Faire sauter ces verrous permet de changer l'objectif de la société pour n'avoir qu'un but : l'accroissement du capital.

> "Désormais, l'antiproduction s'est insinuée dans tous les rouages de la machine. Le capitalisme n'a que des esclaves à son service, bourgeois et prolétaires étant unis dans un même désir servile. \[...\] L'effusion de l'antiproduction dans l'ensemble du champ de la production sociale fait qu'il devient impossible de se représenter adéquetement le lieu de la répression."

Le fascisme est défini par D/G comme un retournement du désir contre lui-même, une auto-répression, un désir de répression. Dans leur système de pensée, D/G analyse que le fascisme d'avant-guerre a pu se développer car il fut bénéfique au capitalisme mais que ses tendances guerrières l'ont rendu dangereux pour le capital lui-même. Se crée alors après-guerre la possibilité d'un nouveau type de fascisme:

> "Il s'agit d'un fascisme moléculaire, caractérisé par une déconcentration et une miniaturisation des méchanismes de répression, sans terreur ni propagande."

Un exemple est la popularité de la psychanalyse, qui semble prendre le pas sur la psychiatrie. On passe de fols enfermés à des gens qui viennent d'eux-mêmes se faire expliquer leur complexe d'Oedipe.

> "à ce stade, le désir servile ne se distingue plus de l'idée d'autonomie, le sujet se soumettant d'autant plus à la réalité sociale dominante qu'il se présente lui-même comme libre et conscient, alors qu'il ne fait que tourner en rond dans une série de positions subjectives socialement aménagées qu'il revendique comme siennes."

L'avantage du micro-fascisme est de proposer beaucoup de narratifs différents dans lesquels il est possible de se reconnaitre, "sorte d'immense supermarché de subjectivités précuites", et non plus un narratif global, englobant, homogène. On peut donc crier "No pasaran", mais le fascisme passe toujours par les mailles les plus fines et prend pour carburant les désirs en chacun de nous. Entre l'industrie des loisirs, les média, le language, les signes, le monde capitaliste n'a pas de mal à nous faire désirer des comportements répressifs.

## Chapitre 3 : Minorités, nomades et machine de guerre

La question qui vient est donc celle de créer une production du social qui soit au service de la production désirante au lieu d'être une répression de celle-ci.
Pour D/G, le capitalisme crée de manière inhérente des lignes de fuite. Cela signifie qu'en décodant il ouvre le champ des possibles mais qu'il n'arrive pas à tout contrôler via l'axiomatique, il y a toujours des fuites qui ne sont pas colmatées, des lignes de fuite.
La division fondamentale n'est plus pensée en terme de prolétariat et de bourgeoisie car ce sont deux pans indispensables au système capitaliste. Ces deux catégories participent au capitalisme. Suivant le mouvement opéraiste,

> "D/G soutiennent que l' 'opposition théorique n'est pas entre deux classes', mais plutôt 'entre les servants de la machine et ceux qui la font sauter ou font sauter les rouages', donc 'entre la classe et les hors-classe', ou entre la classe et les masses qui lui échappent."

Une distinction donc entre le prolétariat territorialisé sur l'usine et les hors-classe, déterritorialisé et nomade.
Ces lignes de fuite prennent aujourd'hui pour nom les minorités, pour ces personnes qui se retrouvent dans le creux des États-Nation (Kurdes, Basques) et qui développent des mouvements nationalistes, mais aussi toutes les personnes qui se retrouvent laissées pour compte par le réagencement du prolétariat qui n'est plus monolithique mais un archipel de minorités.

> "Voilà le problème de la révolution : comment une machine de guerre pourrait tenir compte de toutes les lignes de fuites qui se font sans reproduire un appareil d'État ?" Deleuze

> "Le problème de la machine de guerre a donc d'abord un aspect négatif : comment faire en sorte que la relation entre les différentes minorités qui constituent les lignes de fuite du capitalisme actuel ne donne lieu ni à la reproduction d'un appareil d'État centralisé et hiérarchisé, ni à l'isolement de communautés autonomes et dispersées ? Autrement dit, comment conjurer à la fois le léninisme et l'anarchisme, le centralisme et le spontanéisme ?"

Si le rejet de l'étatisme parait évident au vu de la philosophie de D/G, ces derniers critiquent aussi l'anarchisme bien qu'ils s'inscrivent dans un ensemble d'études anarchiste (anthropologie anarchiste par exemple). Ils critiquent notamment le spontanéisme (car les désirs peuvent être complice d'un fascisme moléculaire) ainsi que la tendance à l'autarcie. Il faut en effet s'interroger sur l'intérêt de communautés (minorités) isolées, coupées les unes et des autres et toujours enserrées dans le capitalisme.

> "Deleuze et Guattari manifestent \[...\] leur intérêt pour un type de société qui a échappé en partie à l'Histoire et qui n'a cessé d'affronter l'État : les sociétés nomades."

Les sociétés nomades habitent un "espace lisse" et se caractérisent plus par leur distribution, leur façon d'habiter l'espace (distribution nomade) que par leur non-sédentarité. Cela s'oppose à l'"espace strié" de l'État et des cadastres (distribution sédentaire). La machine de guerre est créée par les nomades pour défendre leur prétention à la Terre et combattre l'État. Le but n'est pas la guerre en soi mais la production d'un espace lisse.
Il arrive que l'État reprenne cette machine de guerre, cela intervient notamment chez l'État fasciste qui a la particularité d'être suicidaire. Il ne cherche pas à colmater ses fuites mais se construit au contraire sur une ligne de suite intense qui tend à son autodestruction, la machine de guerre n'a plus alors que pour but la guerre elle-même.
Au niveau de la machine de guerre capitaliste mondial, on assiste à des guerres permanents d'un point de vue étatique (concurrence pour la sécurisation et le contrôle du marché mondial) et d'un point de vue interne (répression des dissidences internes). La question est donc de récupérer la machine de guerre dans une perspective révolutionnaire pour créer des lignes de fuite.

## Chapitre 4 : Micropolitique minoritaire et créations de la pensée

Nous avons donc vu que D/G réfutent la stratégie majoritaire consistance pour les minorités à devenir majoritaire. Ils lui opposent le devenir-minoritaire. Les minorités ont un rapport particulier à l'État qui peut se présenter suivant deux pôles :

> "l'un anarcho-capitaliste, ou la soustraction d'axiomes laisse les minorités dans un état d'abandon aux marges de la production et les livre à la puissance répressive de l'État ; et l'autre, social-démocrate, ou l'ajout d'axiomes implique l'intégration différentielle des minorités dans le cadre juridico-politique de l'État-mation et dans le procès économique de production capitaliste."

Il ne faut donc pas oublier la macropolitique et D/G soutiennent ce qu'on pourrait appeler des luttes progressistes (droit de vote des femmes, luttes de libération nationale). L'idée n'est donc pas pour les minorités de devenir majoritaire mais de se soustraire à la norme majeure en même temps à l'état de minorité. Cela dans le but de renverser la norme tout en l'empêchant d'avoir prise sur les minorités. Ce renversement a forcément des impacts sur les majoritaires en même temps, qui peuvent devenir des alliés. Pour D/G, personne ne correspondant totalement à une norme, tout le monde est d'une certaine façon minoritaire. Ainsi il faut refuser les normes, se soustraire de l'objet de ces normes pour les rendre caduque

> "Le concept de bloc d'alliance (ou bloc de devenir) désigne ce mode de connexion micropolitique ou majorité et minorité entrent dans un devenir-minoritaire commun qui conditionne la consitution d'une machine révolutionnaire, opposant à l'universalité du marché capitaliste l'universalité d'une conscience minoritaire, faisant appel à une nouvelle terre, essentiellement déterritorialisée, et à un nouveau peuple, peuple qui manque essentiellement à sa place, peuple errant, éternellement minoritaire."

D/G apporte beaucoup d'importance à l'art et à la production créatrice. La création minoritaire, la ligne de fuite créatrice, est un fait impérativement politique qui cherche à jouer avec et contre les normes établies. D'ou l'ouvrage commun de D/G sur Kafka, issu de la minorité juive tchèque de l'empire habsbourgeois.
